#include<stdio.h>
class Data
{
	char a;
	int b;
public:
	void set_data(char _a, int _b) { a = _a; b = _b; }
	void put_data() { printf("%c %d\n", a, b); }
};
void main()
{
	//객체 복사
	Data d1, d2;
	d2.set_data('G', 100); //d2의 데이터를 채운다.
	d2.put_data(); //d2의 데이터를 확인한다.
	d1 = d2; //d2의 데이터를 d1으로 옮겨준다.(객체복사)
	d1.put_data(); //d1으로 데이터가 잘 옮겨졌는지 확인한다.
	//-----------------------------------------------------//
	//Data타입의 포인터 2개를 만들고, 그곳에 각각 Data타입의 객체들을 생성해서 연결한다.
	Data *d_p1, *d_p2;
	d_p1 = new Data(); d_p2 = new Data();
	//포인터를 이용해서 어떤 한쪽의 객체에 데이터를 채워준다.
	d_p1->set_data('H', 900);
	//또 다른 포인터에 데이터가 채워진 객체의 주소를 연결한다.
	d_p2 = d_p1;
	//2개의 포인터는 같은 객체의 주소를 가리키고 있다는 것이 중요하다.
	d_p1->put_data(); d_p2->put_data();
	//2개의 포인터를 사용해서 데이터를 출력해본다.
}