#include<stdio.h>
class Block
{
private:
	int data; //데이터
public: //일단 직접접근 가능하게 함
	Block *front;//이 클래스로 만든 객체를 연결할 수 있는 리모콘
	Block *back; //이 클래스로 만든 객체를 연결할 수 있는 리모콘
public:
	void set_n(int n) { data = n; }
	void put_n() { printf("%d\n", data); }
	int get_n() { return data; }
};
void main()
{
	/*Block a, b, c; //2개의 객체를 쇠사슬처럼 연결해주기
	//a ---> b
	a.set_n(0); a.back = &b;
	//b ---> c
	b.set_n(1);
	b.back = &c; //b객체의 리모콘에 c객체를 연결해주기
	//b.back->set_n(2); //b객체의 back포인터 -> c객체의 set_n함수 -> 2채움
	//b.back->put_n(); //b객체의 back포인터 -> c객체의 put_n함수 -> 2출력
	//a --> b --> c 이런식으로 접근해서 c의 데이터를 채우고, 출력하기
	//a객체의 포인터 -> b객체의 포인터 -> c객체의 함수
	a.back->back->set_n(-1);
	a.back->back->put_n();*/
	//---------------------------------------------------------------------//
	/*Block a, b; //2개의 객체를 양쪽으로 연결해주기
	//a <---> b
	a.set_n(100); //a객체의 데이터를 채워준다.
	a.front = NULL; //a객체의 front포인터에 NULL을 채워준다.(연결될것이 없다는 의미)
	a.back = &b; //a객체의 back포인터에 b객체를 연결한다.

	b.set_n(200); //b객체의 데이터를 채워준다.
	b.front = &a; //b객체의 front포인터에 a을 연결한다.
	b.back = NULL; //b객체의 back포인터에 NULL을 채워준다.(연결될것이 없다는 의미)

	a.back->put_n(); //a를 이용해서 b의 데이터를 출력해보자.
	b.front->put_n();//b를 이용해서 a의 데이터를 출력해보자.*/
	//---------------------------------------------------------------------//
	/*Block a, b, c, d, e; //5개의 객체를 양쪽으로 연결해주기
	Block *search = &a; //검색자에게 시작점을 연결
	search->set_n(100); search->put_n(); //검색자를 이용해서 연결된 객체의 데이터 출력
	//a <-> b <-> c <-> d <-> e (더블링크드리스트)
	//a데이터 채움 ---> a앞쪽포인터 NULL ---> a뒤쪽포인터에 b연결
	//b데이터 채움 ---> b앞쪽포인터에 a연결 ---> b뒤쪽포인터에 c연결
	//c데이터 채움 ---> c앞쪽포인터 b연결 ---> c뒤쪽포인터에 d연결
	//d데이터 채움 ---> d앞쪽포인터 c연결 ---> d뒤쪽포인터에 e연결
	//e데이터 채움 ---> e앞쪽포인터 d연결 ---> e뒤쪽포인터에 NULL
	a.set_n(1); a.front = NULL; a.back = &b;
	b.set_n(2); b.front = &a; b.back = &c;
	c.set_n(3); c.front = &b; c.back = &d;
	d.set_n(4); d.front = &c; d.back = &e;
	e.set_n(5); e.front = &d; e.back = NULL;
	a.put_n(); //a ---> e의 데이터출력
	e.put_n(); //e ---> a의 데이터출력
	//반복문을 이용해서 처음부터 끝까지 모든 객체의 데이터를 출력
	//검색자를 이용해야 함
	//사용자로부터 찾고 싶은 데이터를 입력받는다(scanf)
	int data, counter = 1;
	scanf("%d", &data);
	do {
		if (data == search->get_n()) //찾을 데이터와 현재 객체의 데이터가 일치하는가?
		{
			//그렇다면 "찾았다"라고 출력해줘라.//몇번째에서 찾았는지 출력
			printf("%d번째에서 찾았다.\n", counter);
			break;//찾았으니까 반복을 중단하고, 탈출한다!(break)
		}
		//search->put_n(); //출력
		counter++;
		search = search->back; //검색자가 다음 객체를 가리키도록 연결해준다.
	} while (search != NULL); //마지막 객체가 아니면 반복*/
	//---------------------------------------------------------------------//
	Block a, b, c, d, e; //5개의 객체를 양쪽으로 연결해주기
	Block *search = &a; //검색자에게 시작점을 연결
	//a <-> b <-> c <-> d <-> e (더블링크드리스트)
	a.set_n(1); a.front = NULL; a.back = &b;
	b.set_n(2); b.front = &a; b.back = &c;
	c.set_n(3); c.front = &b; c.back = &d;
	d.set_n(4); d.front = &c; d.back = &e;
	e.set_n(5); e.front = &d; e.back = NULL;
	//b객체와 c객체 사이에 새로운 객체를 끼워넣기(추가)	
	Block *new_block = new Block(); //추가할 객체
	new_block->set_n(100);
	b.back = new_block; //b객체의 back포인터가 new_block을 가리키면 된다.
	new_block->front = &b; //new_block의 front포인터가 b객체를 가리키면 된다.
	c.front = new_block; //c객체의 front포인터가 new_block을 가리키면 된다.
	new_block->back = &c; //new_block의 back포인터가 c객체를 가리키면 된다.
	//search를 이용해서 6개의 객체의 데이터를 모두 출력하라.
	//반복문 사용
	while (search != NULL)
	{
		printf("[%d] ", search->get_n());
		search = search->back;
	}
	//새로 추가한 [100]을 가진 객체를 다시 삭제하기
	//삭제후에는 반복문을 사용해서 잘 지워졌는지 확인
	//대신, 삭제할 객체는 delete로 메모리에서 소멸되어야 한다.
	b.back = &c; c.front = &b;
	delete new_block;
	search = &a; //다시 가장 앞으로 포인터를 돌려놔야한다.
	while (search != NULL)
	{
		printf("[%d] ", search->get_n());
		search = search->back;
	}
}