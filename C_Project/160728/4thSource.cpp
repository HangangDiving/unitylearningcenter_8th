#include<stdio.h>
#include<string.h>
class Monster
{
private:
	char m_name[20]; int m_hp;
public:
	Monster() {}
	Monster(char name[20], int hp) { strcpy_s(m_name, name); m_hp = hp;	}
	void set_info(char name[20], int hp) { strcpy_s(m_name, name); m_hp = hp; }
	void put_info() { printf("[%s] [%d]\n", m_name, m_hp); }
	virtual void attack() {};
};
class Orc : public Monster
{
public:
	Orc(char name[20], int hp) : Monster(name, hp) {}
	void attack() override { printf("오크가 공격한다.\n"); }
};
class Troll : public Monster
{
public:
	Troll(char name[20], int hp) : Monster(name, hp) {}
	void attack() override { printf("트롤이 공격한다.\n"); }
};
void main()
{
	Monster *monster[5]; //Monster타입의 포인터배열을 만들어준다.(5개)
	//Monster타입으로 된 여러개의 포인터가 붙어있는것뿐이다.
	//Orc, Troll 타입의 객체(동적생성)들을 만들어준다. (오크2개, 트롤3개)
	//포인터배열의 각각의 칸에 오크와 트롤들을 저장해준다.(연결해준다는 개념)
	//포인터배열에 5마리가 잘 연결되었다면, 포인터배열로 그들에게 접근할 수 있는가?
	Orc *o1 = new Orc("boss_orc", 1000); 
	Orc *o2 = new Orc("orc1", 100);	Troll *t3 = new Troll("troll2", 200);
	Troll *t4 = new Troll("troll3", 300); Troll *t5 = new Troll("troll4", 400);
	//monster타입의 포인터로 Orc타입/Troll타입의 객체를 포인팅한다.
	monster[0] = o1; monster[1] = o2; monster[2] = t3; monster[3] = t4; monster[4] = t5;
	//monster타입의 포인터로 5마리의 attack을 실행시켜보세요
	//반복문으로 처리하세요. 에러가 난다면 그 원인을 찾아서 해결하세요.
	int counter = 0;
	while (counter < 5)
	{
		monster[counter++]->attack();
	}
}