#include<stdio.h>
class Data
{
private:
	int number;
public:
	void set_n(int n) { number = n; }
	void put_n() { printf("%d\n", number); }
	int get_n() { return number; }
	//소멸자 : 객체가 죽을때(delete) 자동으로 작동됨.
	~Data() { printf("나 소멸된다"); }
};
int main()
{
	Data d;
	d.set_n(1); d.put_n(); printf("%d\n", d.get_n());

	//Data타입의 포인터를 만들어서 Data타입의 객체를 연결해준다.
	//포인터를 이용해서 위와 같은 3가지 기능을 테스트한다.
	Data *d_pointer = new Data();
	d_pointer->set_n(2); d_pointer->put_n();
	printf("가져온 데이터는 %d\n", d_pointer->get_n());

	delete d_pointer; //소멸자가 작동하는 타이밍
	d_pointer = NULL; //현재 연결된 객체가 없습니다.
	return 1;
}