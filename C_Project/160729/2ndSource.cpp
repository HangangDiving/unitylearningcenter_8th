#include<stdio.h>
int global_number = 100; //전역변수(프로그램 종료시까지 유지됨)
void sub_func(int n)
{
	global_number++;
	printf("global_number:%d\n", global_number);
	static int static_number = 123; //정적변수(함수안에서만 접근 가능. 유지)
	static_number += n;
	printf("static:%d\n", static_number);
}
void main()
{
	int local_number = 1; //로컬변수(이 함수가 끝나면 없어짐)
	printf("local_number:%d\n", local_number);
	global_number++;
	printf("global_number:%d\n", global_number);
	sub_func(1); sub_func(2);
}