#include<stdio.h>
class Zergling
{
	int hp;
	friend class Marine; //마린은 내 친구다.
public:
	Zergling() {}
	Zergling(int hp) { this->hp = hp; }
	void put_info(Marine &m);
};
class Marine
{
	int m_hp;
	friend class Zergling; //저글링은 내 친구다.
public:
	Marine() {}
	Marine(int hp) { this->m_hp = hp; }
	void put_info(Zergling &z) {
		printf("내 친구 저글링의 체력은 [%d]이다\n", z.hp);
		printf("hp[%d]\n", this->m_hp);
	}
};
void Zergling::put_info(Marine &m) {
	printf("내 친구 마린의 체력은 [%d]이다\n", m.m_hp);
	printf("hp[%d]\n", this->hp);
}
void main()
{
	Zergling z(1000); Marine m(500);
	m.put_info(z);
	z.put_info(m);
}