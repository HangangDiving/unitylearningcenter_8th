#include<stdio.h>
#include<string.h>
class Orc
{
	char o_name[20]; int o_hp;
public:
	void check_this(int o_hp) //this포인터 때문에 만든 함수
	{
		//이 함수는 파라미터로 o_hp라는 변수를 받는다.
		//그런데, 이 클래스에는 이미 같은 이름이 있다.
		//때문에 문제가 생긴다. 이걸 해결하려면 this라는 것을 사용하면 된다.
		//this->o_hp라고 하면 이건 이 클래스 안에 있는 변수를 의미하는 것이 된다.
		this->o_hp = o_hp;
	}

	//생성자(이름, 체력)
	Orc() {}
	Orc(int hp) { this->o_hp = hp; }
	Orc(char name[20], int hp) { strcpy_s(this->o_name, name); this->o_hp = hp; }
	void set_info(char name[20], int hp); //set_info(이름, 체력)
	void put_info();//put_info() - 이름, 체력 출력
	~Orc() {}//소멸자()
	Orc operator+(Orc &ref)
	{
		//strcat_s(o_name, ref.o_name);
		return Orc(this->o_hp + ref.o_hp);
	}
};
//함수의 본체는 여기에다 만드세요
void Orc::set_info(char name[20], int hp) {
		strcpy_s(this->o_name, name); o_hp = hp; 
	}
void Orc::put_info() { printf("%s %d\n", this->o_name, o_hp); }
//------------------------------------------------------------------//
void main()
{
	Orc o1("orc1", 100), o2("orc2", 120), oboss("boss", 1000);
	o1.put_info(); o2.put_info(); oboss.put_info();
	//오크1의 체력과 오크2의 체력을 더해서 보스에게 준다.
	//실제로 처리될 때는 o1.operator+(o2) 처럼 처리된다.
	oboss = o1 + o2;
	oboss.put_info();
}