#include<stdio.h>
class Data
{
	int num;
public:
	static int static_num;
public:
	Data(int a, int b) { num = a; static_num = b; }
	void put_info() {
		printf("num = %d static_num = %d\n", num, static_num);
	}
};
int Data::static_num;
void main()
{
	Data d(1, 2); d.put_info();
	Data::static_num = -100;
	printf("%d\n", Data::static_num);
	try
	{
		if (Data::static_num < 0)
			throw 9876;
		Data::static_num += 1000;
		printf("%d\n", Data::static_num);
	}
	catch (int error_code)
	{
		printf("�����ڵ�:%d\n", error_code);
	}

	d.put_info();
}