#include<stdio.h>
#include<string.h>
class Zerg
{
protected:
	int z_hp;
public:
	Zerg() {}
	Zerg(int z_h) { z_hp = z_h; }
	void Move();
};
void Zerg::Move() { printf("moving...\n"); }
//*******************************************************************
class Zergling : public Zerg
{
private:
public:
	Zergling(){}
	Zergling(int z_h) { z_hp = z_h;	}
	void Move();
	void Set_Hp(int h); //셋터
	void Put_Hp(); //겟터
};
void Zergling::Move() { printf("저글링이달린다\n"); }
void Zergling::Set_Hp(int h) { z_hp = h; }
void Zergling::Put_Hp() { printf("체력:%d\n", z_hp); }
//*******************************************************************
void main(void)
{
	Zergling zg[5];//5마리 생성(5마리가 연속적으로 붙어 있다는 것이 중요)
	//[0][1][2][3][4]
	Zergling *z;//포인터(저글링들을 1마리씩 연결해줄 역할)
	z = &(zg[0]);//가장 처음의 저글링을 연결해둔다.

	//while 이나 for를 사용해서 zg의 5마리를 초기화시켜주기
	int x = 1;
	while (x <= 5)
	{
		z->Set_Hp(x * 10);//포인터를 이용해서 5마리의 저글링을 개별적으로 초기화시켜준다.
		z->Put_Hp();
		z += 1;
		x++;
	}
	z = &(zg[0]);
	int y = 0;
	while (y < 5)
	{
		z->Put_Hp();
		z->Move();//z포인터를 이용해서 5마리의 move를 모두 작동시킨다.		
		z += 1;//z포인터로 다음 저글링을 연결해준다.
		y++;
	}
}