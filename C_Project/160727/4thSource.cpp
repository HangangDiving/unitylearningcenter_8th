#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<time.h>
class Zerg
{
protected:
	int z_hp;
public:
	Zerg() {}
	Zerg(int z_h) { z_hp = z_h; }
	void Move();
};
void Zerg::Move() { printf("moving...\n"); }
//*******************************************************************
class Zergling : public Zerg
{
private:
public:
	Zergling() {}
	Zergling(int z_h) { z_hp = z_h; }
	void Move();
	void Set_Hp(int h); //셋터
	int Put_Hp(); //겟터
};
void Zergling::Move() { printf("저글링이달린다\n"); }
void Zergling::Set_Hp(int h) { z_hp = h; }
int Zergling::Put_Hp() { return z_hp; }
//*******************************************************************
void main(void)
{
	//검색패턴을 구현해봅시다!!
	Zergling zg[10];
	//저글링 10마리의 초기화를 시켜준다.(체력치 범위 : 100 ~ 110)
	//랜덤기능을 사용해서 초기화시켜주세요.
	srand((unsigned int)time(NULL));
	int hp = 0;
	for (int i = 0; i < 10; i++)
	{
		hp = (rand() % 100) + 1;
		printf("[%d] ", hp);
		zg[i].Set_Hp(hp);
	}
	//특정한 조건에 일치하는 저글링을 찾아서 어떤 처리를 하는 코드.
	int loop = 0, counter = 0;
	while (loop < 10)
	{
		//현재 체력이 50이하인 저글링을 찾으면, "Die"라는 메시지를 출력해준다.
		if (zg[loop].Put_Hp() <= 50) {
			counter++;//죽을때마다 어떤 변수를 1씩 증가시켜준다.
			printf("Die\n");
		}
		else
		{ }
		loop++;
	}
	printf("죽은 저글링은 %d 마리입니다!\n", counter);
}