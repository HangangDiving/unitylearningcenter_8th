#include<stdio.h>
#include<string.h>
class Zerg
{
protected:
	char z_name[10]; int z_hp;
public:
	Zerg() {}
	Zerg(char z_n[10], int z_h) { strcpy(z_name, z_n); z_hp = z_h; }
	//조상클래스 소속 함수중에 후손들에게 물려주기만 하고, 구현은 하지 않는 함수
	virtual void Move() = 0;
	virtual void Attack() = 0;
};
//*******************************************************************
class Zergling : public Zerg
{
private:
public:
	Zergling() { printf("저글링의 생성자1\n"); }
	Zergling(char z_n[10], int z_h = 0)
	{
		strcpy(z_name, z_n);//조상이물려준 이름
		z_hp = z_h;//체력
		printf("%s %d\n", z_name, z_hp);//확인
	}
	virtual void Move();//override
	virtual void Attack();//override
};
void Zergling::Move() { printf("저글링이움직한다\n"); }//동적바인딩
void Zergling::Attack() { printf("저글링이공격한다\n"); }//동적바인딩
//*******************************************************************
class Hydralisk : public Zerg
{
private:
public:
	Hydralisk() { printf("히드라의 생성자1\n"); }
	Hydralisk(char z_n[10], int z_h = 0)
	{
		strcpy(z_name, z_n);
		z_hp = z_h;
		printf("%s %d\n", z_name, z_hp);
	}
	virtual void Move();//override
	virtual void Attack();//override
};
void Hydralisk::Move() { printf("히드라가움직한다\n"); }//동적바인딩
void Hydralisk::Attack() { printf("히드라가공격한다\n"); }//동적바인딩
//***********************
void main(void)
{
	Zerg *z_p;//Zerg타입의 포인터를 만든다.
	//Zerg z;//순수가상함수를 사용하면 그 클래스는 그 순간부터 객체를 만들 수 없다.
	Zergling zg; Hydralisk h;//Zerg, Zergling, Hydralisk 3개의 객체를 만든다.
	//z_p = &z;//Zerg타입의 포인터에 Zerg연결, Move함수 실행
	//z_p->Move(); z_p->Attack();
	z_p = &zg;//Zerg타입의 포인터에 Zergling연결, Move함수 실행
	z_p->Move(); z_p->Attack();
	z_p = &h;//Zerg타입의 포인터에 Hydralisk연결, Move함수 실행
	z_p->Move(); z_p->Attack();
}