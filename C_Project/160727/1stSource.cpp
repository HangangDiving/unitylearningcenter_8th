#include<stdio.h>
#include<string.h>
class A //클래스A (조상클래스)
{
private:
	int a = 0;//데이터(숫자변수 1개-int)
public:
	//함수(변수를 설정하는 생성자. 출력하는 함수)
	A() {}
	A(int a) { this->a = a; }
	virtual void Put_Message() {
		printf("%d\n", a);
	}
};
class B : public A //클래스B (후손클래스)
{
public:
	virtual void Put_Message() {
		printf("B class\n");//함수(B클래스라고 출력해주는 함수)
	}
};
class C : public A //클래스C (후손클래스)
{
public:
	virtual void Put_Message() {
		printf("C class\n");//함수(C클래스라고 출력해주는 함수)
	}
};
void main() //메인함수
{
	A *p_a; //포인터(누군가를 여기에 연결할것임-object)
	B b; C c;
	//객체를 포인터에 연결했음.
	p_a = &b; p_a = &c; //정적바인딩(포인터변수의 타입이 무엇인가에 따라 작동함)

	A a_object(100); B b_object; C c_object;
	A *a_pointer; B *b_pointer; C *c_pointer;
	a_pointer = &a_object;
	//포인터타입이 기준이냐(정적바인딩) 객체기준이냐(동적바인딩)
	a_pointer->Put_Message();
	a_pointer = &b_object;
	a_pointer->Put_Message();
	a_pointer = &c_object;
	a_pointer->Put_Message();
}