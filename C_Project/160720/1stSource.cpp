#include<stdio.h>
#include<string.h>
//-----------------------------------------------------------------------//
int Func(int a);
void Func1(int a, int b, char c);
int Work(int a, int b); //2개 숫자를 받아서 더해서 결과를 돌려주는 함수
float Work(float a, float b);
void String_Save(char data[5]); //배열을 전달받아서 처리할 수 있는 함수
//-----------------------------------------------------------------------//
void main(void)
{	
	//새로 만든 코드조각(function)을 실행시켜보자!!
	int result = Func(100); //function call
	printf("돌려받은 데이터는 %d\n", result);
	Func1(100, 200, 'K');
	printf("return 성공\n");

	//2개 숫자를 Work함수에게 전달하고, 그 결과를 받아서 출력해본다!!
	/*printf("더하려는 두 개의 정수를 입력해주세요.\n");
	int _a = 0, _b = 0;
	scanf("%d", &_a);
	scanf("%d", &_b);*/
	printf("더한 정수값은 %d입니다.\n", Work(100, 200));
	printf("더한 실수값은 %f입니다.\n", Work(100.1f, 200.2f));

	//배열의 이름은 그 배열의 첫번째 시작 위치를 가리킨다.
	char Ori_Data[5] = {"ABCD"};
	String_Save(Ori_Data);
	//printf("[%c]\n", Ori_Data[3]);
	//배열에 채워진 5개의 문자를 각각 출력해보세요. (for / while을 이용해보세요)
	for (int i = 0; i < sizeof(Ori_Data); i++)
		printf("[%c]\n", Ori_Data[i]);
}

int Func(int a)
{
	printf("call 성공\n");
	printf("전달받은 데이터는 %d\n", a);
	a -= 1;
	printf("전달받은 데이터를 조작한 후 %d\n", a);
	return a; //돌려준다.
}

void Func1(int a, int b, char c)
{
	printf("call 성공\n");
	printf("전달받은 데이터는 %d %d %c\n", a, b, c);

}

int Work(int a, int b)
{
	return a + b;
}

float Work(float a, float b)
{
	return a + b;
}

void String_Save(char data[5])
{
	//printf("[%c]\n", data[3]);
	//data[3] = 'X';
	//배열에 전혀 다른 문자 5개를 채워보세요.
	data[0] = 'U', data[1] = 'N', data[2] = 'I', data[3] = 'T', data[4] = 'Y';
}