#include<stdio.h>
#include<string.h>
//포인터 변수에 대한 내용
struct sData
{
	char name[10]; //이것을 포인터로 어떻게 접근할것인지를 찾아볼것!
	char a;
	int b;
	float c;
};
void main(void)
{
	//char data = 'x';
	//char *p_data = 0; //pointer
	//p_data = &data; //메모리주소를 저장가능함.(포인팅 이라고도 한다.)
	//printf("저장하고있는 메모리주소 %d \n", p_data);
	//printf("data의 메모리위치 %d \n", &data); //address
	//printf("data에 저장된 값 %d \n", data); //data
	//----------------------------------------------------------------------------//
	//char data[5] = { 'a', 'b', 'c', 'd', 'e' };
	//char *p_data = 0; //pointer
	//p_data = &(data[0]); //배열의 시작위치를 저장해둔다. ()로 우선순위 꼭 해준다.
	//printf("%c\n", *p_data); //해당메모리주소에 접근한 후, 데이터에 접근
	//printf("%c\n", *(p_data + 1));
	//printf("%c\n", *(p_data + 2));

	//p_data = &(data[4]);
	//printf("%c\n", *p_data);
	//printf("%c\n", *(p_data - 1));
	//printf("%c\n", *(p_data - 2));

	//int data2[5] = {1, 2, 3, 4, 5};
	//int *p_data2 = &(data2[0]);
	//printf("%d\n", *p_data2);
	//printf("%d\n", *(p_data2 + 1));
	//printf("%d\n", *(p_data2 + 2));
	//----------------------------------------------------------------------------//
	sData d;
	sData *p_d;
	d.a = 'x';
	d.b = 123;
	d.c = 3.141592f;
	p_d = &d; //구조체의 메모리상의 시작위치를 저장해둔다.
	printf("%c\n", p_d->a); //포인터로 접근한 첫번째 데이터
	printf("%d\n", p_d->b); //포인터로 접근한 두번째 데이터
	printf("%f\n", p_d->c); //포인터로 접근한 세번째 데이터
	
	//구조체 멤버변수 배열에 접근하기
	strcpy_s(d.name, "Overwatch");
	//한번에 출력
	printf("%s\n", p_d->name); //p_d->name == p_d->(&name[0])
	//하나하나 출력
	for (int i = 0; i < sizeof(d.name); i++)
	{
		printf("%c\n", p_d->name[i]);
	}
}