#include<stdio.h>
#include<string.h>
//----------------------------------------------------------------//
//struct
//주인공캐릭터의 정보를 저장할 구조체설계도
//1.이름(배열) - 길이는 임의로 정함
//2.체력(정수)
//3.스테미너(정수)
//4.무기종류(정수) - 갯수와 종류는 임의로 정함
//5.무기내구도(정수)
struct sCharacter
{
	char name[20];
	int health;
	int stamina;
	int weapon;
	int weaponDurability;
};
//----------------------------------------------------------------//
//function 선언
//걷기(Walk) - 이 함수를 실행시키면 일정량의 스테미너를 감소시킨다.
//달리기(Run) - 걷기보다 더 많은 스테미너를 감소시킨다.
//공격하기(Attack) - 무기의 내구도를 감소시킨다.
//공격당함(Damaged) - 체력을 감소시킨다.
void Initial(sCharacter *p_character);
void Walk(int *p_stamina);
void Run(int *p_stamina);
void Attack(int *p_weaponDurability);
void Damaged(int *p_health);
void Weapon(int weapon);
//----------------------------------------------------------------//
void main(void)
{
	//캐릭터를 만든후에 데이터를 채워준다(초기값 - 임의로 정함)
	//걷는다. ---> 현재 스테미너를 확인한다.
	//뛴다. ---> 현재 스테미너를 확인한다.
	//공격한다. ---> 현재 무기내구도를 확인한다.
	//공격당한다. ---> 현재 체력을 확인한다.
	sCharacter character;
	Initial(&character);

	while (true)
	{
		int keyboard = 0;
		printf("하고 싶은 행동을 선택해주세요. -> ");
		scanf("%d", &keyboard);
		switch (keyboard)
		{
			case 1:
				Walk(&(character.stamina));
				break;
			case 2:
				Run(&(character.stamina));
				break;
			case 3:
				Attack(&(character.weaponDurability));
				break;
			case 4:
				Damaged(&(character.health));
				break;
		default:
			printf("없는 행동이므로 종료.\n");
			return;
		}
	}
}
//----------------------------------------------------------------//
//function 정의
void Initial(sCharacter *p_character)
{
	//strcpy_s(p_character->name, "Overwatch");
	p_character->health = 500;
	p_character->stamina = 1000;
	//p_character->weapon = 1;
	p_character->weaponDurability = 100;
	
	printf("이름을 입력해주십시요.(20자이내) -> ");
	scanf("%s", p_character->name);

	printf("무기를 선택해주십시요. -> ");
	scanf("%d", &(p_character->weapon));

	printf("생성한 캐릭터입니다.\n캐릭터명:%s\n체력:%d\n스테미너:%d\n무기내구도:%d\n",
		p_character->name, p_character->health, p_character->stamina, p_character->weaponDurability);
	printf("무기명:");
	Weapon(p_character->weapon);
}
void Walk(int *p_stamina)
{
	int prev = *p_stamina;
	*p_stamina -= 10;
	printf("이전스테미너:[%d]  현재스테미너:[%d]\n", prev, *p_stamina);
}
void Run(int *p_stamina)
{
	int prev = *p_stamina;
	*p_stamina -= 20;
	printf("이전스테미너:[%d]  현재스테미너:[%d]\n", prev, *p_stamina);
}
void Attack(int *p_weaponDurability)
{
	int prev = *p_weaponDurability;
	*p_weaponDurability -= 5;
	printf("이전무기내구도:[%d]  현재무기내구도:[%d]\n", prev, *p_weaponDurability);
}
void Damaged(int *p_health)
{
	int prev = *p_health;
	*p_health -= 10;
	printf("이전체력:[%d]  현재체력:[%d]\n", prev, *p_health);
}
void Weapon(int weapon)
{
	switch (weapon)
	{
	case 1:
		printf("Sword\n");
		break;
	case 2:
		printf("Gun\n");
		break;
	case 3:
		printf("Laser\n");
		break;
	case 4:
		printf("LocketLauncher\n");
		break;
	case 5:
		printf("Plasma\n");
		break;
	default:
		printf("무기가 없습니다.\n");
		break;
	}
}
