#include<stdio.h>
#include<string.h>
struct sData
{
	char name[10]; //이름
	int hp; //체력
	int st; //스테미너
};

void Init_Character(sData *p_d); //main이 넘겨준 캐릭터의 정보를 여기서 채워준다!

void main(void)
{
	sData d; //캐릭터 생성
	Init_Character(&d); //캐릭터 데이터 초기화
	//캐릭터의 정보를 출력해준다!
	printf("캐릭터명:[%s] 현재체력:[%d] 현재스테미너:[%d]\n",
		d.name, d.hp, d.st);
}

void Init_Character(sData *p_d)
{	
	strcpy_s(p_d->name, "Overwatch");
	p_d->hp = 100;
	p_d->st = 50;
}