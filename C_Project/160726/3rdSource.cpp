#include<stdio.h>
class cData
{
private:
	char a;
	int b;
public:
	void Set_Data(char i, int j) { a = i; b = j; }
	void Put_Data() { printf("%c %d\n", a, b); }
};
void main()
{
	class cData d[2];//[0], [1]
	//for 또는 while 을 사용하여 2개의 객체를 셋팅하고 출력하기
	for (int i = 0; i < 2; i++)
	{
		//문자 1개와 숫자 1개를 입력받아서 객체에 저장해줌.(Set_Data)
		char key1; int key2;
		scanf("%c%d", &key1, &key2);
		d[i].Set_Data(key1, key2);
		//저장된 것을 출력함(Put_Data)
		d[i].Put_Data();
	}
}