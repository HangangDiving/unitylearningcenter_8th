#include<stdio.h>
#include<string.h>
class Human //스타크래프트(인간종족: 공통점을 모아놓은곳)
{
protected://이름(char[]),체력(float),무기(int)
	char h_name[20];
	float h_hp;
	int h_wp;
public://이동,죽음,공격.(메시지출력), 생성자(Human-이름,체력,무기)
	Human() {}
	Human(char h_n[20], float h_h, int h_w)
	{
		strcpy(h_name, h_n); h_hp = h_h; h_wp = h_w;
	}
	void Human::move();
	void Human::dead();
	void Human::attack();
};//*******************************************************************
void Human::move() { printf("moving..."); };
void Human::dead() { printf("you die..."); };
void Human::attack() { printf("attacking..."); };
//*******************************************************************
class Marine : public Human//보병
{
private:
	int m_speed;//속력
	float m_fire_rate;//연사속도
public://스팀팩사용(use_steam_pack) 생성자(Marine-이름,체력,무기)
	Marine() {}
	//이름,체력,무기,속력,연사속도 초기화...
	Marine(char m_n[20], float m_h, int m_w, int m_s, float m_f_r)
	{
		strcpy(h_name, m_n); h_hp = m_h; h_wp = m_w;
		m_speed = m_s; m_fire_rate = m_f_r;
	}
	void Marine::use_steam_pack(void);
	void Marine::attack(void);
};//*******************************************************************
void Marine::use_steam_pack(void)
{
	h_hp -= 0.5f;// 스팀팩을 사용하면 캐릭터의 체력(float)을 일정치(-0.5) 감소시켜야한다.
	m_speed += 2;// 캐릭터의 속력(int) 이 일정치(+2) 증가해야한다.
	m_fire_rate += 0.5f;// 무기의 연사속도가(float) 일정치(+0.5) 증가해야한다.
						// 화면에 현재 체력,속력,연사속도를 보여준다.
	printf("h_hp:%f m_speed:%d m_fire_rate:%f\n",
		h_hp, m_speed, m_fire_rate);
}
void Marine::attack(void)
{
	printf("마린공격!!!");
}
//*******************************************************************
class Firebat : public Human//화염방사기+보병
{
private://기름(fuel-float)
	float fuel;
public://스팀팩사용(use_steam_pack),화염방사기쏘기(shoot_fire)
	   //생성자(Firebat-이름,체력,무기)
	Firebat() {}
	Firebat(char f_n[20], float f_h, int f_w, float f_fuel)
	{
		strcpy(h_name, f_n); h_hp = f_h; h_wp = f_w;
		fuel = f_fuel;
	}
	void Firebat::use_steam_pack(void);
	void Firebat::shoot_fire(void);
	void Firebat::attack(void);
};//*******************************************************************
void Firebat::use_steam_pack(void)
{
	printf("using steam-pack...");
}
void Firebat::attack(void)
{
	printf("화염방사기발사~");
}
void Firebat::shoot_fire(void)
{
	printf("shooting fire...");
	fuel -= 2;//화염방사기를 발사하면 기름이 일정치(-2) 줄어든다.
	printf("fuel:%f\n", fuel);//남은 기름을 화면에 보여준다.
}
//*******************************************************************
void main(void)
{
	Marine m("레이너", 1000, 1, 20, 1.0f);//Marine 과 Firebat 을 생성한다.
	Firebat f("제임스", 700, 2, 100);
	//Marine 이 움직임-->공격-->스팀팩사용-->죽음
	m.use_steam_pack();
	//m.move(); m.attack(); m.use_steam_pack(); m.dead();
	//Firebat 이 움직임-->공격-->스팀팩사용-->화염방사기쏘기-->죽음
	//f.move(); f.attack(); f.use_steam_pack(); f.shoot_fire(); f.dead();
}