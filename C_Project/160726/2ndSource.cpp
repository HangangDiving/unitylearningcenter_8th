#include<windows.h>
#include<stdio.h>
#define MAX_COOL_TIME 2000 //최대 쿨타임 시간(2초)
void main()
{
	//-------------------------------------------------------------//
	//1.무한루프를 만든다(while)
	//2.키보드입력을 받는다.
	//3.입력받은 키가 'S'이면 sw변수를 true로 만들어준다.(무기를 발사했다는 의미)
	//4.그리고 "발사했음" 이라고 출력
	//5.일정시간동안 지연시킴.
	//6.지연시간이 지난후에는 sw변수를 다시 false로 만들어준다.
	//7.현재 sw변수가 false라면 "발사가능"이라고 출력.
	//-------------------------------------------------------------//
	bool sw = false;//스위치를 껏다는 의미. (상태변수)
	char key;//키보드 입력용
	while (true)
	{
		scanf("%c", &key);
		if (key == 'S') sw = true;
		else sw = false;

		if (sw == true)
			printf("발사했음\n");
		else
			continue;

		Sleep(MAX_COOL_TIME); //2초 있다가 자동으로 sw가 false로 바뀜
		printf("발사가능\n");
	}
}