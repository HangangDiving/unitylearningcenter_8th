#include<stdio.h>
#include<time.h>
#include<stdlib.h>
class cShip
{
private:
	//비행기의 체력(int), 폭탄갯수(int), 속도(float)
	int hp, count; float speed;
public:
	//생성자(체력/폭탄갯수/속도)
	//이동(Move), 미사일발사(Shoot), 폭탄발사(Bomb), 죽음(Dead)
	cShip() {}
	cShip(int hp, int count, float speed) {
		this->hp = hp; this->count = count; this->speed = speed;
	}
	void Move() { printf("Plyaer Move\n"); }
	void Shoot() { printf("Plyaer Shoot\n"); }
	void Bomb() { printf("Plyaer Bomb\n"); }
	void Dead() { printf("Plyaer Dead\n"); }
};
class cEnemy_Ship
{
private:
	int hp; float speed;
public:
	cEnemy_Ship() {}
	cEnemy_Ship(int hp, float speed) {
		this->hp = hp; this->speed = speed;
	}
	void Move();
	void Shoot();
	void Dead();
	void Display_Info();
};
void cEnemy_Ship::Move() { printf("Enemy Move\n"); }
void cEnemy_Ship::Shoot() { printf("Enemy Shoot\n"); }
void cEnemy_Ship::Dead() { printf("Enemy Dead\n"); }
void cEnemy_Ship::Display_Info() { printf("hp:%d speed:%f\n", hp, speed); }

void main()
{
	srand((unsigned int)time(NULL));

	//적비행기를 배열로 5개 만들어낸다.
	//5개의 적비행기는 각각 다른 능력치를 가져야 한다.(랜덤기능을 사용한다)
	cEnemy_Ship *enemy[5];
	//for (int i = 0; i < 5; i++)
	//{
	//	cEnemy_Ship temp((rand() % 5) * 100, (float)(rand() * 5));
	//	enemy[i] = &temp;
	//	enemy[i]->Show();
	//}
	int x = 0;
	while (x < 5)
	{
		enemy[x++] =
			new cEnemy_Ship((rand() % 5) * 100, ((rand() * 5.0f) * 10.0f));
	}
	//5대의 능력치가 다른 것을 화면에 출력해서 보여줘야 합니다.
	x = 0;
	while (x < 5)
	{
		enemy[x++]->Display_Info();
	}

	//주인공 비행기를 만들어낸다.
	cShip player(500, 50, 10); //객체의 이름은 player이다.
	cShip *p_player = &player; //객체포인터 생성. p_player라는 포인터는 player라는 객체를 가리키고 있다.(간접접근)
}