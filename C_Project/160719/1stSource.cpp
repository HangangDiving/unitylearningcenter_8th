//헤더파일영역
//특정명령어(함수)를 사용하고 싶을 시 이것들을 로딩해야 한다.
#include<stdio.h> //strcmp
#include<string.h> //printf, scanf
void main(void)
{
	//여러개의 변수를 묶어놓은 것 (배열)
	//들어가는 데이터의 종류는 다양함. 대신 같은 종류로만 가능함.
	//크기는 자유이지만, 메모리가 어느정도 허락하는 선을 지키는것이 좋음.
	//int Data[3] = { 'A', 'B', 'C'};
	//int Data[3] = { 100, 200, 300 };
	float Data[3] = { 0.1f, 0.2f, 0.3f };

	//배열에는 각각의 위치번호가 붙는데 이것을 이용하면 아래와 같이 데이터를 지정할 수 있다.
	//printf("%c %c %c \n", Data[0], Data[1], Data[2]);
	//printf("%d %d %d \n", Data[0], Data[1], Data[2]);
	//printf("%f %f %f \n", Data[0], Data[1], Data[2]);

	for (int i = 1; i <= 3; i++)
	{
		//배열의 방번호는 고정적인것이 아니라, 변수를 사용하는것이 가능하다.
		//변수를 사용하는것뿐만 아니라, 수식을 사용하는것도 가능하다.
		printf("%f \n", Data[i - 1]);
	}
	//----------------------------------------------------------------------//
	int loop = 1;
	while (loop <= 3)
	{
		printf("%f \n", Data[loop++ - 1]);
		//loop++;
	}

	//S - > Sword , G -> Gun , R -> Rocket Launcher , L -> Laser Gun , P -> Plasma Rifle
	char Inven[5] = { 'S', 'G', 'R', 'L', 'P' };
	char search_word = 'G';

	//검색패턴
	for (loop = 1; loop <= 5; loop++)
	{
		if (Inven[loop - 1] == search_word)
		{
			printf("%c \n", Inven[loop - 1]);
		}
	}
	//----------------------------------------------------------------------//
	//게임에서 키보드를 누르면 특정무기로 바뀌는 것
	char keyboard;
	printf("G, L, R, B, P 중 하나를 입력해주세요.\n");
	scanf("%c", &keyboard);
	switch (keyboard)
	{
	case 'G':
		printf("Gun\n");
		break;
	case 'L':
		printf("Laser Gun\n");
		break;
	case 'R':
		printf("Rocket Launcher\n");
		break;
	case 'B':
		printf("BFG-9000\n");
		break;
	case 'P':
		printf("Plasma Rifle\n");
		break;
	default:
		printf("잘못된 입력입니다.\n");
	}
	//if ~ else if 로 키보드 무기 바꾸기
	if (keyboard == 'G')
		printf("Gun\n");
	else if (keyboard == 'L')
		printf("Laser Gun\n");
	else if (keyboard == 'R')
		printf("Rocket Launcher\n");
	else if (keyboard == 'B')
		printf("BFG-9000\n");
	else if (keyboard == 'P')
		printf("Plasma Rifle\n");
	else
		printf("잘못된 입력입니다.\n");
	//----------------------------------------------------------------------//
	//인벤토리를 문자열스타일로 새로만들기
	//Inventory2[0] 에는 Plasma-Lifle 의 첫글자(P)의 메모리위치가 들어있다.
	//Inventory2[1] 에는 Rocket Launcher 의 첫글자(R)의 메모리위치가 들어있다.
	//Inventory2[2] 에는 Laser Gun 의 첫글자(L)의 메모리위치가 들어있다.
	char *Inventory2[3] = { "Plasma-Lifle", "Rocket Launcher", "Laser Gun" };
	//특정한 문자열의 첫글자의 위치를 지정하면 "%s"가 알아서 마지막글자까지 출력함
	//사실 문자열의 마지막에는 자동으로 "0"이 들어감.
	printf("%s\n", Inventory2[2]);
	//----------------------------------------------------------------------//
	//여러개의 배열이 묶여있는 형태
	char Belt[2][3] =
	{
		{'a', 'b', 'c'}, //첫번때 벨트에 있는 물건들 [0]----->[0][1][2]
		{'d', 'e', 'f'} //두번째 벨트에 있는 물건들  [1]----->[0][1][2]
	};
	//첫번째 벨트의 두번째 물건은?
	printf("%c\n", Belt[0][1]);
	//두번째 벨트의 세번째 물건은?
	printf("%c\n", Belt[1][2]);
	//----------------------------------------------------------------------//
	//문자열을 비교해주는 함수(function)
	//같을 경우(0), 틀릴 경우(1, -1) - 리턴값(어떤 명령어의 처리결과를 통지받는것)
	int result = strcmp("abc", "def");
	printf("%d\n", result);

	//무기의 이름이 들어있는 인벤토리를 만들고, 그 인벤토리에서 내가 원하는 무기를
	//지정하면, 해당무기의 이름이 출력되는 것.
	char Weapon[18]; //최대 10글자까지 저장가능(10글자 + 0)
	printf("Gun, LaserGun, RocketLauncher, BFG-9000, PlasmaRifle, Sword 중에 원하는 무기를 입력하시오\n");
	scanf("%s", Weapon);
	printf("%s 을 입력했습니다.\n", Weapon);
	char WeaponInventory[6][18] =
	{
		{"Gun"}, {"LaserGun"}, { "RocketLauncher" }, { "BFG-9000" }, { "PlasmaRifle" }, { "Sword" }
	};

	bool is = false;
	for (int i = 0; i < 6; i++)
	{
		if (strcmp(Weapon, WeaponInventory[i]) == 0)
		{
			printf("선택한 무기는 %s 입니다.\n", WeaponInventory[i]);
			is = true;
			break;
		}
	}
	if(!is)
		printf("입력한 무기는 없습니다.\n");
	//----------------------------------------------------------------------//
	//break, continue 라는 특수한 명령어 (반복문에서 사용하는 전용명령어)
	loop = 0;
	while (loop < 10)
	{
		loop++;
		//if (++loop == 5)
		if (loop == 5)
			//break; //while문을 중단하고 빠져나감
			continue;
		printf("%d\n", loop);
	}
	//탈출위치
}