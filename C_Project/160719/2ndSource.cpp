#include<stdio.h>
#include<string.h>
//-----------------------------------------------------------------------//
//종류가 여러가지인 데이터들을 묶어서 저장할 수 있는 기술(구조체)
//문자, 숫자를 동시에 저장해보자
//이러한 저장소를 앞으로 사용할 것이다(설계도)
struct sData
{
	char a; //문자
	int b; //숫자
	float c; //실수

	sData() {}
	sData(char _a, int _b, float _c)
	{
		a = _a;
		b = _b;
		c = _c;
	}
};
//던전에 생성되는 괴물의 속성들을 묶어놓은것
struct sMonster
{
	char name[10]; //이름
	//int health; //체력(int의 한계치 : -2,147,483,648 ~ 2,147,483,648)
	unsigned int health; //체력(int의 한계치 : 0 ~ 4,294,967,295)
	//char weapon; //무기(메모리절약-1바이트만 사용하면 가능) <- 제일 작은 메모리임
	int weapon;
	int pos[2]; //생성위치
};
//-----------------------------------------------------------------------//
void main(void)
{
	//내가 직접디자인한 저장소를 사용해서 데이터를 저장해보자
	/*sData data;
	data.a = 'A';
	data.b = 100;
	data.c = 3.141592f;
	printf("%c %d %f\n", data.a, data.b, data.c);

	sData d1('X', 500, 1.23f), d2('Y', 900, 4.56f);
	printf("%c %d %f\n", d1.a, d1.b, d1.c);
	printf("%c %d %f\n", d2.a, d2.b, d2.c);*/
	//-----------------------------------------------------------------------//
	//sMonster m1, m2;
	////strcpy를 사용하면 실제이름을 배열에 넣어줄수있음
	//strcpy_s(m1.name, "Orc");
	//m1.health = 200;
	////m1.weapon = 'A';
	//m1.weapon = 2;
	//m1.pos[0] = 10;
	//m1.pos[1] = 20;

	//strcpy_s(m2.name, "Troll");
	//m2.health = 1000;
	////m2.weapon = 'B';
	//m2.weapon = 1;
	//m2.pos[0] = -10;
	//m2.pos[1] = -20;

	//printf("이름 %s   %s\n", m1.name, m2.name);
	//printf("체력 %d   %d\n", m1.health, m2.health);
	//printf("무기 %c     %c\n", m1.weapon, m2.weapon);
	//printf("좌표 %d,%d %d,%d\n", m1.pos[0], m1.pos[1], m2.pos[0], m2.pos[1]);
	//-----------------------------------------------------------------------//
	/*
	//현재 괴물이 가지고 있는 무기의 종류를 화면에 출력해주는 코드를 추가하기.
	//switch나 if ~ else if를 사용할 것
	//무기의 종류는 아래와 같이 정한다.
	//1.도끼 2.칼 3.총 4.로켓포
	//괴물의 정보를 출력할 때 이름, 무기 2가지만 출력해줌
	sMonster m1, m2;
	//strcpy를 사용하면 실제이름을 배열에 넣어줄수있음
	strcpy_s(m1.name, "Orc");
	m1.health = 200;
	m1.pos[0] = 10;
	m1.pos[1] = 20;
	strcpy_s(m2.name, "Troll");
	m2.health = 1000;
	m2.pos[0] = -10;
	m2.pos[1] = -20;

	int keyboard = 0;
	printf("무기종류입니다. 1.도끼 2.칼 3.총 4.로켓포\n");
	printf("Orc의 무기를 선택해주세요. - ");
	scanf("%d", &keyboard);
	m1.weapon = keyboard;
	printf("Troll의 무기를 선택해주세요. - ");
	scanf("%d", &keyboard);
	m2.weapon = keyboard;
	switch (m1.weapon)
	{
	case 1:
		printf("%s의 무기는 도끼입니다.\n", m1.name);
		break;
	case 2:
		printf("%s의 무기는 칼입니다.\n", m1.name);
		break;
	case 3:
		printf("%s의 무기는 총입니다.\n", m1.name);
		break;
	case 4:
		printf("%s의 무기는 로켓포입니다.\n", m1.name);
		break;
	default:
		printf("%s의 무기는 없습니다.\n", m1.name);
		break;
	}

	if(m2.weapon == 1)
		printf("%s의 무기는 도끼입니다.\n", m2.name);
	else if (m2.weapon == 2)
		printf("%s의 무기는 칼입니다.\n", m2.name);
	else if (m2.weapon == 3)
		printf("%s의 무기는 총입니다.\n", m2.name);
	else if (m2.weapon == 4)
		printf("%s의 무기는 로켓포입니다.\n", m2.name);
	else
		printf("%s의 무기는 없습니다.\n", m2.name);*/
}
