#include<stdio.h>
void main(void)
{
	//while이나 for등의 반복문을 중첩시킨 코드패턴
	int a;
	int b;
	for (a = 1; a <= 2; a++) //a loop
	{
		printf("a=%d ", a); //1~2

		for (b = 100; b <= 105; b++) //b loop
		{
			printf("b=%d ", b); //100~105
		}
		printf("\n");//b루프를 다돌고나면 한줄내림
		printf("------------------------------\n");
	}
	printf("\n");//b루프를 다돌고나면 한줄내림
	printf("------------------------------\n");
	//while로 바꿔줄것 !!!
	a = 1;
	while (a <= 2) //a loop
	{
		printf("a=%d ", a);//1~2
		b = 100;
		while (b <= 105)//b loop
		{
			printf("b=%d ", b);//100~105
			b++;
		}
		printf("\n");//b루프를 다돌고나면 한줄내림
		printf("------------------------------\n");
		a++;
	}
}