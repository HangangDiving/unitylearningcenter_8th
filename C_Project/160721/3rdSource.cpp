#include<stdio.h>
//#include<stdlib.h>
#include<windows.h> //윈도우전용(비표준 헤더 파일 <- 다른 OS에서는 작동안함)
#include<conio.h> //윈도우전용(비표준 헤더 파일 <- 다른 OS에서는 작동안함)

void OptionMenu();

void main(void)
{
	int key; //키입력을 받은것을 저장할 곳
	while (true)
	{
		printf("원하는 작업을 입력하세요\n1.게임시작 2.옵션 3.나가기 : ");
		//사용자가 선택할 수 있는 메뉴번호를 만들어줄것.
		//1.게임시작
		//2.옵션
		//3.나가기
		scanf("%d", &key); //원하는 작업번호 입력
		//-----------------------------------------------------------------//
		//입력한 작업번호에 따른 처리.(switch / if~else if)
		switch (key)
		{
		case 1:
			printf("Game Start !!\n");
			break;
		case 2:
			//printf("Options\n");
			//system("cls"); //화면지우기
			//printf("1.sound 2.graphic 3.controll 4.prevMenu\n"); //서브메뉴 출력
			//getch();
			OptionMenu(); //서브메뉴 호출
			continue;
			break;
		case 3:
			printf("Bye...\n");
			//return; //편법이므로 다르게 사용해봅시다.
			break;
		default:
			printf("잘못선택했습니다.\n");
			break;
		}
		//-----------------------------------------------------------------//
		//1번 선택한 경우는 "Game Start !!" 라는 메시지를 출력
		//2번 선택한 경우는 "Options" 라는 메시지를 출력
		//3번 선택한 경우는 "Bye..." 라는 메시지를 출력 후 코드가 종료되어야 함
		//-----------------------------------------------------------------//
		if (key == 3)
			return;
		getch(); //키보드 아무거나 한개 입력하면 바로 반응
		system("cls");
	}
}

//서브메뉴 모드
void OptionMenu()
{
	int key;
	while (true)
	{
		system("cls");
		printf("원하는 작업을 입력하세요.\n1.사운드 2.그래픽 3.컨트롤 4.이전메뉴로 복귀 : ");
		scanf("%d", &key);
		switch (key)
		{
		case 1:
			printf("사운드 설정\n");
			break;
		case 2:
			printf("그래픽 설정\n");
			break;
		case 3:
			printf("컨트롤 설정\n");
			break;
		case 4:
			system("cls");
			break;
		default:
			printf("잘못선택했습니다.\n");
			break;
		}
		if (key == 4)
			return;
		getch(); //키보드 아무거나 한개 입력하면 바로 반응
	}
}