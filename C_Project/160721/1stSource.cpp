#include<stdio.h>
#include<string.h>
//적 캐릭터의 정보를 저장할 구조체(이름, 체력)
struct sEnemy
{
	char name[20];
	int health;
};
//공격당함(Damaged) - 일정치만큼 체력을 깎음. 0이하가 되면 죽는다. 이때 Dead를 호출한다.
//죽음(Dead) - Damaged에서 체력이 0이하가 되면 이곳으로 온 후 죽었다는 메시지를 출력한다.
//정보채움(Init_Info) - 초기치를 채워준다.
//정보확인(Display_Info) - 현재정보를 보여준다.
void Damaged(sEnemy *enemy, int damage);
bool Dead(sEnemy *enemy);
int Init_Info(sEnemy *enemy);
void Display_Info(sEnemy *enemy);
void main(void)
{
	//적 캐릭터 1개 생성
	//정보채움
	//공격당함
	//정보확인
	sEnemy enemy0;
	int damage = Init_Info(&enemy0);
	while (true)
	{
		Damaged(&enemy0, damage);
		if (Dead(&enemy0))
			break;
	}
}

void Damaged(sEnemy * enemy, int damage)
{
	//int prev = enemy->health;
	enemy->health -= damage;
	//if (enemy->health <= 0)
		//return Dead();
	
	//printf("이전 체력:%d , 현재 체력:%d\n", prev, enemy->health);	
	Display_Info(enemy);
}

bool Dead(sEnemy *enemy)
{
	if (enemy->health <= 0)
	{
		printf("!!~~죽었다~~!!\n");
		return true;
	}

	return false;
}

int Init_Info(sEnemy * enemy)
{
	printf("이름을 입력해주세요.(20자 이내) : ");
	scanf("%s", enemy->name);
	
	printf("체력을 입력해주세요. : ");
	scanf("%d", &(enemy->health));
	if (enemy->health < 0)
		enemy->health *= -1;
	else if(enemy->health == 0)
		enemy->health = 50;

	int damage;
	printf("데미지를 입력해주세요. : ");
	scanf("%d", &damage);
	if (damage < 0)
		damage *= -1;
	else if (damage == 0)
		enemy->health = 10;
	return damage;
}

void Display_Info(sEnemy * enemy)
{
	printf("현재 이름:%s , 현재 체력:%d\n", enemy->name, enemy->health);
}
