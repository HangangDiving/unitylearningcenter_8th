#include<stdio.h>
#include<windows.h>
//캐릭터의 정보를 저장.(struct)
//이름,체력
struct sCharacter
{
	char name[20];
	int health;
};
//캐릭터의 정보를 셋팅(초기화)-Init_Player
void Init_Player(sCharacter *pCharacter);
//캐릭터의 정보를 보여줌-Show_Player
void Show_Player(sCharacter *pCharacter);
//캐릭터에게 데미지를 줌-Damaged_Player
void Damaged_Player(sCharacter *pCharacter, int damage, int damageCount);
void main()
{
	//캐릭터 생성, 셋팅
	sCharacter character;
	Init_Player(&character);
	//캐릭터 정보 확인
	Show_Player(&character);
	while (true)
	{
		int damage;
		int damageCnt;
		printf("데미지를 입력하세요 : ");
		scanf("%d", &damage);
		printf("데미지횟수를 입력하세요 : ");
		scanf("%d", &damageCnt);
		//데미지를 줌(임의의 횟수)
		Damaged_Player(&character, damage, damageCnt);
		
		Show_Player(&character);
		//캐릭터의 현재 체력을 확인해서..
		//0 이하이면 죽었다고 출력.
		if (character.health <= 0)
		{
			printf("죽었다\n");
			break;
		}
		//1~40 이면 체력회복이 필요하다고 출력.
		else if (character.health > 0 && character.health <= 40)
		{
			printf("체력회복 필요\n");
		}
		//41~100 이면 정상이라고 출력.
		else if (character.health > 40 && character.health <= 100)
		{
			printf("정상\n");
		}
	}
}
void Init_Player(sCharacter *pCharacter)
{
	printf("캐릭터 이름을 입력하세요 : ");
	scanf("%s", pCharacter->name);
	//printf("캐릭터 체력을 입력하세요 : ");
	//scanf("%d", pCharacter->health);
	pCharacter->health = 100;
}
void Show_Player(sCharacter *pCharacter)
{
	system("cls");
	printf("캐릭터 이름 : %s / 캐릭터 체력 %d : \n", pCharacter->name, pCharacter->health);
}
void Damaged_Player(sCharacter *pCharacter, int damage, int damageCount)
{
	for (int i = 0; i < damageCount; i++)
	{
		pCharacter->health -= damage;
	}
}