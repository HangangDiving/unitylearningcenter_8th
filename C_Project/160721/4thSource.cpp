#include<stdio.h>
#include<string.h>
struct sMonster
{
	char name[10];
	int hp;
};

void main(void)
{
	//2마리 생성(일괄적인 관리가 가능 - 번호에 따른)
	sMonster monster[2];
	//2마리의 데이터 초기화시킴.확인
	strcpy_s(monster[0].name, "Orc");
	monster[0].hp = 100;
	strcpy_s(monster[1].name, "Troll");
	monster[1].hp = 200;
	printf("0번 괴물 이름 : %s , 0번 괴물 체력 : %d\n", monster[0].name, monster[0].hp);
	printf("1번 괴물 이름 : %s , 1번 괴물 체력 : %d\n", monster[1].name, monster[1].hp);
	//2마리의 데이터변경.확인
	monster[0].hp = 50;
	monster[1].hp = 100;
	printf("0번 괴물 체력 : %d , 1번 괴물 체력 : %d\n", monster[0].hp, monster[1].hp);
}