#include<stdio.h>
void main(void)
{
	int num = 0;
	//num = 1 (1부터 시작함)
	//num <= 3 (3까지는 반복함)
	//num++ (1씩 증가할것임)
	for (num = 1; num <= 10; num++)
	{ //반복구간 시작
		printf("%d\n", num); //반복할 것들
	} //반복구간 끝
	//-----------------------------------//
	printf("\n");

	//while의 용도 : 딱 정해진 수만큼 반복시킬 용도가 아닌 몇번 반복할지 알 수 없을 때 사용
	num = 1;
	while (num <= 10)
	{
		printf("%d\n", num++);
	}
}