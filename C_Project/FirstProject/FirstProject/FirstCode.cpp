#include<stdio.h> //라이브러리파일을 가져옴. (미리 만들어 놓은 코드)
//stdio.h 파일 안에는 printf 라는 명령어가 있음. (화면출력담당)
void main(void)
{
	/*
	int keyboard = 0; //저장소(변수)-숫자
	scanf("%d", &keyboard); //키보드로 입력받아서 저장하는 명령어 //%d : 10진수 정수 //& : 포인터와 관련..래퍼런스?
	//컴파일 시 나오는 scanf 관련 워닝은 보안문제이므로 무시해도 된다.(해당 함수는 보안 관련 취약한 부분이 있다)

	//변수에 저장된 숫자를 출력해줍니다.
	printf("입력한 숫자는 %d 입니다.\n", keyboard);
	*/

	//더한 결과를 저장할 곳
	//int result;
	float result;
	//숫자2개를 저장할 곳
	//int num1, num2;
	float num1, num2;
	/*
	//숫자2개를 저장한다.
	//num1 = 123, num2 = 456;
	num1 = 123.45f, num2 = 567.78f; //f를 안 붙이면 double이다. 타입이 다르므로 경고 발생
	//저장된 숫자를 더한다.
	result = num1 + num2;
	//출력
	//printf("더한것은 %d 입니다\n", result);
	printf("더한것은 %f 입니다\n", result);
	*/
	num1 = 7.0f, num2 = 2.0f;
	result = num1 / num2; //나눗셈(소수점이 잘리는 문제가 발생할 수 있음)
	printf("나눈결과는 %.2f 입니다.\n", result); //%.2f : 자릿수 2자리로. %.0f : 자릿수 아예 없음
}