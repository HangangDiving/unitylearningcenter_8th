#include<stdio.h>
void main(void)
{
	//변수 준비
	int startNum = 0, endNum = 1;
	//시작숫자 받기
	printf("시작 숫자를 입력하시오\n");
	scanf("%d", &startNum);
	//종료숫자 받기
	printf("종료 숫자를 입력하시오\n");
	scanf("%d", &endNum);
	
	while (startNum == endNum) //시작점수와 종료점수가 같지 않을때까지..
	{
		//종료숫자 입력을 다시 요청한다.
		printf("종료 숫자가 시작 숫자와 같습니다.\n종료 숫자를 다시 입력하시오\n");
		scanf("%d", &endNum);
	}

	printf("\n"); //가독성 위해 빈줄 넣기

	if (startNum < endNum) //시작점수가 종료점수보다 낮다면..
		for (int i = startNum; i <= endNum; i++) //증가시켜나간다.
			printf("%d\n", i);
	else if (startNum > endNum) //시작점수가 종료점수보다 높다면..
		for (int i = startNum; i >= endNum; i--) //감소시켜나간다.
			printf("%d\n", i);
}