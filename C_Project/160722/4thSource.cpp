#include<stdio.h>
#include<string.h>
class Player
{
private:
	//데이터:이름(char),체력(int),무기종류(int),무기내구도(int)
	char name[20]; int hp; int wp; int du;
public:
	//생성자는 객체가 만들어지는 순간에 자동으로 실행되는 함수이다.
	//반드시 이름을 클래스와 같은 이름으로 만들어줘야 한다.
	//반환형이 없다. 함수이름왼쪽에 void 나 int / float 같은 데이터형이 올 수 없다.
	Player(char *n, int h, int w, int d)
	{
		printf("내가 생성자다!!\n");
		//strcpy는 양쪽다 메모리주소를 필요로한다.(복사할곳,원본이있는곳)
		strcpy(name, n);
		hp = h;
		wp = w;
		du = d;
	}
	//플레이어셋팅(Init_Player)--->이름,체력,무기종류,무기내구도셋팅 
	void Init_Player(char *n, int h, int w, int d)
	{
		//strcpy는 양쪽다 메모리주소를 필요로한다.(복사할곳,원본이있는곳)
		strcpy(name, n);
		hp = h;
		wp = w;
		du = d;
	}
	void Attack(int d)//공격(Attack)---> 무기내구도감소
	{
		du = du - d;//전달해준만큼의 내구도를 깎는다.(밸런스조절이 편리함)
	}
	void Damaged(int d)//데미지(Damaged)----> 체력감소 
	{
		hp = hp - d;//전달해준만큼의 데미지를 깎는다.(밸런스조절이 편리함)
	}
	void Show_Player()//플레이어정보확인(Show_Player)---> 이름,체력,무기종류,무기내구도출력
	{
		printf("\n-----------플레이어 현재정보-------------\n");
		printf("1.이름[ %s ]\n", name);
		printf("2.체력[ %d ]\n", hp);
		printf("3.무기[ %d ]\n", wp);
		printf("4.내구도[ %d ]\n", du);
		printf("\n--------------------------------------\n");
	}
};//------------------
void main()
{
	class Player p("sniper", 100, 5, 20);//플레이어생성,셋팅
	//p.Init_Player("sniper", 100, 5, 20);
	//플레이어공격 ---> 데미지 ---> 플레이어정보확인 
	p.Attack(3);//무기내구도를 3감소
	p.Damaged(2);//체력 2감소
	p.Show_Player();
}