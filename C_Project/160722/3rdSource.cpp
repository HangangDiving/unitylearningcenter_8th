#include<iostream>

using namespace std;

class cPlayer
{
private:
	//데이터 + 함수
	//데이터 : 이름(char), 체력(int), 무기종류(int), 무기내구도(int)
	char name[20]; int health; int weapon; int dural;
public:
	//함수
	//플레이어셋팅(Init_Player) -> 이름, 체력, 무기종류, 무기내구도 셋팅
	void Init_Player(char *_name, int _health, int _weapon, int _dural)
	{
		strcpy_s(name, _name);
		health = _health;
		weapon = _weapon;
		dural = _dural;
	}
	//공격(Attack) -> 무기내구도 감소
	void Attack(int num)
	{
		dural -= num;
		if (dural < 0)
		{
			dural = 0;
			cout << "내구도가 없다." << endl;
		}
	}
	//데미지(Damaged) -> 체력 감소
	void Damaged(int damage)
	{
		health -= damage;
		if (health < 0)
		{
			health = 0;
			cout << "체력이 없다." << endl;
		}
	}
	//플레이어정보확인(Show_Player) -> 이름, 체력, 무기종류, 무기내구도 출력
	void Show_Player()
	{
		cout << "이름 : " << name << " / 체력 : " << health <<
			" / 무기 : " << weapon << " / 내구도 : " << dural << endl;
	}
};

int main()
{
	//플레이어 생성, 셋팅
	cPlayer player;

	char name[20];
	int health, weapon, dural, damage, minus;
	cout << "이름 입력(20자 내로 문자) : ";
	cin >> name;
	cout << "체력 입력(숫자) : ";
	cin >> health;
	cout << "무기 입력(숫자) : ";
	cin >> weapon;
	cout << "내구도 입력(숫자) : ";
	cin >> dural;
	cout << "데미지 입력(숫자) : ";
	cin >> damage;
	cout << "무기감소도 입력(숫자) : ";
	cin >> minus;

	player.Init_Player(name, health, weapon, dural);

	//플레이어 공격 -> 데미지 > 플레이어 정보 확인
	int key;
	bool loop = true;
	while (loop)
	{
		player.Show_Player();
		cout << "작업을 선택(1.공격 2.맞음 3.종료) : ";
		cin >> key;
		system("cls");
		switch (key)
		{
		case 1:
			player.Attack(damage);
			break;
		case 2:
			player.Damaged(minus);
			break;
		case 3:
			cout << "종료" << endl;
			loop = false;
			break;
		default:
			cout << "잘못 입력." << endl;
			break;
		}			
	}
	return 0;
}
