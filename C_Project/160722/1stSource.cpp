#include<stdio.h>
#include<stdlib.h> //rand 사용을 위함
#include<time.h> //time 사용을 위함
#include<windows.h> //sleep 사용을 위함

#define MIN 100
#define MAX 900

//game에서 많이 쓰이는 기능인 Random 과 Delay 를 사용해보자 !
//운영체제나 틀마다 명령어가 다르고 사용법이 많이 다름.
void main(void)
{
	//printf("만들어낼수있는 무작위숫자의 최대치는 %d\n", RAND_MAX);

	//1.랜덤한 숫자를 저장할 변수를 만듬.
	//int rand_number;

	//2.랜덤명령어로 무작위숫자를 만들어내서 저장함.
	//srand((unsigned int)time(NULL)); //현재 시간을 기준점으로 무작위숫자를 생성할 것이다.
	//rand_number = rand(); //무작위 숫자 발생

	////3.저장된숫자를 출력함.
	//printf("만들어진 숫자는 %d 입니다.\n", rand_number);
	//
	//rand_number = rand() % 10; //0~9 사이의 난수가 나옵니다.
	//printf("0~9 사이의 숫자는 %d 입니다.\n", rand_number);
	//for (int i = 0; i < 5; i++)
	//{
	//	//범위지정 1~100 이 가능한가?
	//	rand_number = rand() % 100; //0~99 사이의 난수가 나옵니다.
	//	printf("0~99 사이의 숫자는 %d 입니다.\n", rand_number);
	//}
	//무작위숫자를 만들어낼때 1,2,3 중 한가지만 나오도록 할 수 있는가?
	//범위지정 1~3 이 가능한가?
	//rand_number = (rand() % 3) + 1; //0~2사이의 난수에 +1
	//printf("1,2,3 중 나온 숫자는 %d 입니다.\n", rand_number);
	//for (int i = 0; i < 10; i++)
	//{
	//	rand_number = (rand() % 3) + 1;
	//	//이곳에서 무작위숫자를 이용한 캐릭터의 동작을 구현한다.
	//	//switch 를 사용함
	//	//1일때는 공격, 2일때는 회피, 3일때는 방어라고 출력
	//	//그 이외의 숫자는 에러라고 출력.
	//	switch (rand_number)
	//	{
	//	case 1:
	//		printf("공격\n");
	//		break;
	//	case 2:
	//		printf("회피\n");
	//		break;
	//	case 3:
	//		printf("방어\n");
	//		break;
	//	default:
	//		printf("Error\n");
	//		break;
	//	}
	//}

	int y = 1, action;
	srand((unsigned int)time(NULL));
	while (y <= 5)
	{
		action = (rand() % 3) + 1;
		//이곳에서 무작위숫자를 이용한 캐릭터의 동작을 구현한다.
		//switch 를 사용함
		//1일때는 공격, 2일때는 회피, 3일때는 방어라고 출력
		//그 이외의 숫자는 에러라고 출력.
		switch (action)
		{
		case 1: printf("공격\n"); break;
		case 2: printf("회피\n"); break;
		case 3:	printf("방어\n"); break;
		default: printf("Error\n"); break;
		}
		//일정시간(1초)을 여기서 지연시켜준다.
		//즉, 이 코드를 실행하면 1초마다 "공격" 이라는 메시지가 나온다.
		//Sleep(1000);
		int delay_sec;
		delay_sec = (rand() % 3) + 1;
		printf("현재 지연 시간 : %dsec\n", delay_sec);
		Sleep(delay_sec * 1000);
		//printf("공격!!\n");
		y++;
	}
}