//#include<stdio.h>
#include<iostream> //입출력관련함수 (cin, cout)
using namespace std;
//-----------------------------------------------------------------------------//
//기본적으로 클래스의 데이터는 전부 lock이 걸림(private이라는 기능이 적용됨)
//클래스는 기본적으로 데이터를 보호하게끔 되어있음.(직접접근불가능)
//간접접근을 해야함.(함수를 통해서)
class cData
{
	//멤버변수(프로퍼티)
private: //lock
	char a;
	int b;
	float c;

	//멤버함수(메소드)
public: //unlock
	//셋터(setter)
	void Input_Data(char _a, int _b, float _c) //전달받은 데이터를 같은 멤버변수에게 저장해준다.
	{
		//멤버변수에게 전달받은 데이터를 저장한다.
		a = _a, b = _b, c = _c;
	}
	//겟터(getter)
	void Output_Data()
	{
		cout << a << endl << b << endl << c << endl;
	}
};
//-----------------------------------------------------------------------------//
int main()
{
	//int number1, number2;
	////std::cin (키보드를 의미함. 기본입력장치) //입력받은 것 ---> 변수
	//cin >> number1 >> number2; //== scanf
	////std::cout (모니터를 의미함. 기본출력장치) //모니터 <--- 변수 <--- 한줄내림
	//cout << number1 << endl << number2 << endl; //== printf  //endl == \n

	cData d; //객체
	//d.a = 'X';
	//d.b = 100;
	//d.c = 3.141592f;
	//cout << d.a << endl << d.b << endl << d.c << endl;
	d.Input_Data('X', 100, 3.141592f);
	d.Output_Data();

	return 0;
}