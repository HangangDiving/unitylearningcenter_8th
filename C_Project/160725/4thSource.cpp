#include<stdio.h>
#include<string.h>
class Human //스타크래프트(인간종족 : 공통점을 모아놓은 곳)
{
protected: //이름(char[]), 체력(float), 무기(int)
	char name[10]; float hp; int weapon;
public: //이동, 죽음, 공격. (메시지 출력), 생성자(Human-이름,체력,무기)
	Human() {};
	Human(char name[10], float hp, int weapon) {
		strcpy_s(this->name, name); this->hp = hp; this->weapon = weapon;
	};
	void Move();
	void Dead();
	void Attack();
};
void Human::Move() { printf("%s(이)가 이동\n", this->name); }
void Human::Dead() { printf("%s(이)가 죽음\n", this->name); }
void Human::Attack() { printf("%s(이)가 공격\n", this->name); }

class Marine : public Human //보병
{
private: //스팀팩(float)
	float steamPack;
public: //스팀팩 사용(use_steam_pack), , 생성자(Marine-이름,체력,무기)
	Marine() {};
	Marine(char name[10], float hp = 100.0f, int weapon = 1, float steamPack = 100.0f)
		: Human(name, hp, weapon) {
		this->steamPack = steamPack;
	};
	void UseSteamPack();
	void Attack();
};
void Marine::UseSteamPack() { printf("마린 스팀팩 사용\n"); }
void Marine::Attack() { printf("마린공격!!"); } //오버라이드 된 함수

class Firebat : public Human //화염방사기+보병
{
private: //스팀팩(float), 기름(fuel-float)
	float steamPack, fuel;
public: //스팀팩 사용(use_steam_pack), 화염방사기쏘기(shoot_fire),
		//생성자(Firebat-이름,체력,무기)
	Firebat(char name[10], float hp = 200.0f, int weapon = 2, float steamPack = 100.0f, float fuel = 50.0f)
		: Human(name, hp, weapon) {
		this->steamPack = steamPack;
		this->fuel = fuel;
	};
		void UseSteamPack();
		//void ShootFire();
		void Attack();
};
void Firebat::UseSteamPack() { printf("파이어뱃 스팀팩 사용\n"); }
//void Firebat::ShootFire() { printf("화염방사기 쏘기\n"); }
void Firebat::Attack() { printf("화염방사기 발사~!!"); }

void main()
{
	//Marine 과 Firebat 을 생성한다.
	Marine m1("마린1", 100.0f, 1, 100.0f); Firebat f1("파이어뱃1", 200.0f, 2, 100.0f, 50.0f);
	//Marine 이 움직임 ---> 공격 ---> 스팀팩 사용 ---> 죽음
	m1.Move(); m1.Attack(); m1.UseSteamPack(); m1.Dead();
	//Firebat 이 움직임 ---> 공격 ---> 스팀팩 사용 ---> 화염방사기 쏘기 ---> 죽음
	f1.Move(); f1.Attack(); f1.UseSteamPack(); /*f1.ShootFire();*/ f1.Dead();
}