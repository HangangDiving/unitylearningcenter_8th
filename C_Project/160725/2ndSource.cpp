#include<stdio.h>
#include<string.h>
class cOrc
{
private:
	char o_name[10]; int o_hp;//이름, 체력
	
public:
	//함수(Init_Info) -> 이 부분을 생성자로 바꿔줘야 함
	//(메인함수쪽에서 이름,체력을 전달해주는 방식으로 변경되어야 함)
	//객체가 생성되면서 자동으로 채워진다는 것이 포인트 !!
	//void Init_Info() { printf("오크보스\n"); }
	
	//기본 생성자라고 부르는 것(전달 받는 데이터가 없는 생성자. 이것은 무조건 클래스에 있어야 함)
	cOrc() {}
	//실제로 사용할 생성자(전달 받은 데이터로 내부변수들을 채워줌)
	cOrc(char name[10], int hp)
	{ 
		strcpy_s(o_name, name);
		o_hp = hp;
	}
	//함수(Dead, Damaged, Display)
	void Dead();
	void Damaged();
	void Display_Info(); //이 함수는 우리 소속이다.
};
void cOrc::Dead() { printf("죽었다.\n"); }
void cOrc::Damaged() { printf("데미지 입었음\n"); }
void cOrc::Display_Info()
{
	printf("이름:%s 체력:%d\n", o_name, o_hp);
}
void main()
{
	//객체생성(오크를 만듬)
	//정보 채움(Init_Info)
	//공격당함(Damaged)
	//정보확인(Display_Info)
	//죽었음(Dead)

	//구조체(구조체변수) ---> 클래스(오브젝트)
	//일반함수 ---> 소속이 있는 함수(멤버함수)	
	cOrc o("boss1", 10000);//보스 캐릭터 1개생성
	cOrc o1("monster1", 100);
	cOrc o2("monster2", 200);
	//o.Init_Info();//정보채움
	o.Damaged();//공격당함
	o.Display_Info();//정보확인
	o.Dead();//죽었음

	o1.Damaged();//공격당함
	o1.Display_Info();//정보확인
	o1.Dead();//죽었음

	o2.Damaged();//공격당함
	o2.Display_Info();//정보확인
	o2.Dead();//죽었음
}