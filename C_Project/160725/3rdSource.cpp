#include<stdio.h>
#include<string.h>

class Zerg
{
protected: //상속 받는 클래스측에서 이 데이터들을 사용할 수 있게 해준다.
	char m_name[10]; int m_hp;
public:
	Zerg() { printf("저그의 생성자1\n"); }
	Zerg(char name[10], int hp)	{
		strcpy_s(m_name, name);
		m_hp = hp;
		printf("저그의 생성자2\n");
	}
	void Move();
};
void Zerg::Move() { printf("%s(이)가 움직인다.\n", m_name); }

class Zergling : public Zerg
{
private:
public:
	//객체가 태어날 때 생성자가 실행되면서, 만약 데이터를 넘겨주지 않으면
	//기본적으로 셋팅되는 데이터를 줄 수 있다. Default Parameter
	//이것을 아래의 생성자에 적용시켜보세요 !!
	Zergling() { printf("저글링의 생성자1\n"); }
	Zergling(char name[10], int hp = 0) {
		strcpy_s(m_name, name); //조상이 물려준 이름
		m_hp = hp; //체력
		printf("%s, %d\n", m_name, m_hp); //확인
		printf("저글링의 생성자2\n");
	}
	void Attack();
	void Jump(); //그냥 뛴다.
	void Jump(int height); //이만큼 뛴다.
	void Jump(int height, int direction); //이쪽으로 이만큼 뛴다.
	void Jump(int height, char direction); //direction쪽으로 이만큼 뛴다.
};
void Zergling::Attack() { printf("저글링이 공격한다.\n"); }
void Zergling::Jump() { printf("저글링이 뛰었다.\n"); }
void Zergling::Jump(int height) { printf("저글링이 이만큼 뛰었다.\n"); }
void Zergling::Jump(int height, int direction) { printf("저글링이 이쪽으로 이만큼 뛰었다.\n"); }
void Zergling::Jump(int height, char direction) { printf("저글링이 %c쪽으로 이만큼 뛰었다.\n", direction); }

class Hydralisk : public Zerg
{
private:
public:
	Hydralisk() { printf("히드라의 생성자1\n"); }
	Hydralisk(char name[10], int hp = 0) {
		strcpy_s(m_name, name);
		m_hp = hp;
		printf("%s, %d\n", m_name, m_hp);
		printf("히드라의 생성자2\n");
	}
	void Attack();
};
void Hydralisk::Attack() { printf("히드라가 공격한다.\n"); }

//다음과 같은 다중 상속은 반드시 피해야 한다!!
//class Hybrid_Zergling : public Zergling, Hydralisk
//{};

void main()
{
	//히드라와 저글링을 만듬.
	Zergling z1("저글링", 1000); Hydralisk h1("히드라", 5000);

	z1.Move(); h1.Move();

	z1.Attack(); //저글링으로 공격한다.
	h1.Attack(); //히드라로 공격한다.

	//자동으로 상황에 따라서 선택되는 함수들(Overloading)
	z1.Jump(100, 1);
	z1.Jump(100, 'B');
	z1.Jump(100);
	z1.Jump();
}