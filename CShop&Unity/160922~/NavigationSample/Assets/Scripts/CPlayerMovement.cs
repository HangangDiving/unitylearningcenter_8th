﻿using UnityEngine;
using System.Collections;

public class CPlayerMovement : MonoBehaviour {

    NavMeshAgent _navMeshAgent; //네브메쉬에이전트

    public Transform _target; //이동 대상

    public bool _isMoving = false; //이동 여부

    public bool _showDebugLine = false;
    public bool _showDebugSphere = false;
    private NavMeshPath _path;
    private Vector3 _debugSpherePos;

    void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();

        _path = new NavMeshPath();
    }

    void Start()
    {
        //추적 대상을 설정
        //_navMeshAgent.SetDestination(_target.position);
    }

    // Update is called once per frame
    void Update () {

        if (!_isMoving)
        {
            //추적을 중지 함
            _navMeshAgent.Stop();
        }
        else
        {
            //목표 지점을 다시 찾아감
            _navMeshAgent.SetDestination(_target.position);
            _navMeshAgent.Resume();
        }
        
        if (Input.anyKeyDown)
        {
            //경로 성공여부 확인
            //if(_navMeshAgent.hasPath)
            //{
            //    print("hasPath");

            //    print(_navMeshAgent.pathStatus);
            //}

            //경로 계산
            //_navMeshAgent.CalculatePath(_target.position, _path);
            //NavMesh.CalculatePath(transform.position, _target.position, NavMesh.AllAreas, _path);

            //가까운 네비메쉬 찾기
            NavMeshHit hit;
            //_navMeshAgent.FindClosestEdge(out hit);
            //if (hit.hit)
            //{
            //    _debugSpherePos = hit.position;
            //}

            if (_navMeshAgent.Raycast(_target.position, out hit))
                _debugSpherePos = hit.position;
        }
	}

    void OnDrawGizmos()
    {
        if (_showDebugLine)
        {
            if (_path == null) return;

            Gizmos.color = Color.red;

            for (int i = 0; i < _path.corners.Length - 1; i++)
                Gizmos.DrawLine(_path.corners[i], _path.corners[i + 1]);
        }
        if (_showDebugSphere)
        {
            if (_debugSpherePos == Vector3.zero) return;

            Gizmos.color = Color.red;

            Gizmos.DrawSphere(_debugSpherePos, 0.5f);
        }
    }
}
