﻿using UnityEngine;
using System.Collections;

public class CKnightAnim : MonoBehaviour {

    Animator _animator;

    CKnightStat _stat;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _stat = GetComponent<CKnightStat>();
    }

    public void PlayAnim(CKnightStat.STATE state)
    {
        _stat._state = state;

        switch (_stat._state)
        {
            case CKnightStat.STATE.IDLE:
                _animator.SetBool("Walk", false);
                break;
            case CKnightStat.STATE.WALK:
                _animator.SetBool("Walk", true);
                break;
            case CKnightStat.STATE.ATTACK:
                _animator.SetTrigger("Attack");
                break;
        }
    }

    public bool IsAttackAnim()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            return true;

        return false;
    }
}
