﻿using UnityEngine;
using System.Collections;

public class CKnightMovement : MonoBehaviour {

    public float _speed;

    public float _gravity;

    Vector3 _moveDirection;

    CharacterController _CC;

    CKnightAnim _anim;

    void Awake()
    {
        _CC = GetComponent<CharacterController>();
        _anim = GetComponent<CKnightAnim>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    void Move()
    {
        if (_anim.IsAttackAnim()) return;

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        _moveDirection = new Vector3(h, 0f, v);

        if (_moveDirection == Vector3.zero)
        {
            //정지 애니메이션 실행
            _anim.PlayAnim(CKnightStat.STATE.IDLE);
        }
        else
        {
            //걷기 애니메이션 실행
            _anim.PlayAnim(CKnightStat.STATE.WALK);

            transform.forward = _moveDirection.normalized;

            float speed = _speed;

            if (h != 0 && v != 0)
            {
                float degree = Mathf.Cos(45f * Mathf.Deg2Rad);
                speed *= degree;
            }

            _moveDirection *= speed;

            _moveDirection.y -= _gravity;

            _CC.Move(_moveDirection * Time.deltaTime);
        }
    }
}
