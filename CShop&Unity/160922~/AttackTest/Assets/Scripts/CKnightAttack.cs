﻿using UnityEngine;
using System.Collections;

public class CKnightAttack : MonoBehaviour {

    CKnightAnim _anim;

    public Transform _attackPoint;

    public float _attackRange;

    void Awake()
    {
        _anim = GetComponent<CKnightAnim>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Attack();

	}

    void Attack()
    {
        if (_anim.IsAttackAnim()) return;

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            _anim.PlayAnim(CKnightStat.STATE.ATTACK);
        }
    }

    public void AttackAnimEvent()
    {
        Collider[] hitColliders = Physics.OverlapSphere(
            _attackPoint.position, _attackRange, 1 << LayerMask.NameToLayer("Monster"));

        if (hitColliders.Length > 0)
        {
            Debug.Log("Hit!!!");
        }
    }
}
