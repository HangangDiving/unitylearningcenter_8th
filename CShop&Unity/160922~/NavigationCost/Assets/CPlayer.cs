﻿using UnityEngine;
using System.Collections;

public class CPlayer : MonoBehaviour {

    public Transform _endPoint;

    public NavMeshAgent _navMeshAgent;

    public GameObject _areaObject;
    public string _areaName;

	// Use this for initialization
	void Start () {

        _navMeshAgent.SetDestination(_endPoint.position);

	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Return))
        {
            _navMeshAgent.Stop();

            //UnityEditor.GameObjectUtility.SetStaticEditorFlags(_areaObject, UnityEditor.StaticEditorFlags.NavigationStatic);

            //int layer = NavMesh.GetAreaFromName(_areaName);
            int layer = UnityEditor.GameObjectUtility.GetNavMeshAreaFromName(_areaName);
            UnityEditor.GameObjectUtility.SetNavMeshArea(_areaObject, layer);

            //NavMesh.SetAreaCost(NavMesh.GetAreaFromName("Walk1"), 1f);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            _navMeshAgent.ResetPath();

            _navMeshAgent.SetDestination(_endPoint.position);
        }

        print("pathStatus : " + _navMeshAgent.pathStatus
            + " / isPathStale : " + _navMeshAgent.isPathStale
            + " / remainingDistance : " + _navMeshAgent.remainingDistance);
    }
}
