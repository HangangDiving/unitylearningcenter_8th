﻿using UnityEngine;
using System.Collections;

//플레이어쉽 이동 조작
public class CPlayerShipMovement : MonoBehaviour {

    public float _speed; //이동 속도

	//[유니티 이벤트 메소드 : 유니티가 특정 이벤트 상황에서 호출해 줌]
    //개요 : 현재 오브젝트가 랜더링 되기 바로 전에 1회 호출됨
    //목적 : 오브젝트의 초기화
	void Start () {
	
	}

    //[유니티 이벤트 메소드]
    //개요 : 현재 오브젝트가 랜더링 될때마다 1회 호출됨 (24 FPS면 24회)
    //목적 : 랜더링 시 해야할 작업을 수행함(이동, 발포 등)
    void Update () {

        //키보드 방향 입력
        //Input.GetAxis("방향") => Horizontal, Vertical

        //수평키 입력
        //리턴값 : -1(왼쪽) ~ 0(정지) ~ 1(오른쪽)
        float h = Input.GetAxis("Horizontal");

        //수직키 입력
        //리턴값 : -1(아래쪽) ~ 0(정지) ~ 1(위쪽)
        float v = Input.GetAxis("Vertical");

        //Transform 컴포넌트의 이동 메소드
        //Time.deltaTime : 프레임의 호출횟수가 다른 디바이스끼리의 보정 수치
        //transform.Translate(방향 * 속도 * Time.deltaTime);
        //Vector2 dir = new Vector2(-1, 0); //왼쪽 방향 벡터
        //transform.Translate(dir * _speed * Time.deltaTime);

        //transform에게 이동을 요청함
        transform.Translate( (Vector2.right * h) * _speed * Time.deltaTime);

        //수직이동벡트를 구함
        Vector2 vValue = (Vector2.up * v) * _speed * Time.deltaTime;
        //이동을 수행함
        transform.Translate(vValue);

        //transform.position.x : 현재 오브젝트 x값 접근
        //transform.position.x = 10f => 사용할 수 없음
        //transform.position = new Vector2(10f, 0f); => 사용할 수 있음

        //화면 제한 영역
        //x : -6 ~ 6
        //y : -4.5 ~ 4.5

        //[문제]
        //현재 플레이어쉽이 화면을 벗어 나지 않게 하시오.

        //좌우 이동 제한
        Vector2 newPos = this.transform.position;
        if (transform.position.x < -6)
            newPos.x = -6f;
        else if(transform.position.x > 6)
            newPos.x = 6f;
        //상하 이동 제한
        if (transform.position.y < -4.5)
            newPos.y = -4.5f;
        else if (transform.position.y > 4.5)
            newPos.y = 4.5f;

        this.transform.position = newPos;
    }
}
