﻿using UnityEngine;
using System.Collections;

public class CShield : MonoBehaviour {

    public float _lifeTime = 3f;

    //오브젝트 비활성화 상태에서 활성 상태로 변경될 때 호출
    void OnEnable()
    {
        Invoke("DisableShield", this._lifeTime);
    }

    //쉴드와 다른 오브젝트가 충돌함
    void OnTriggerEnter2D(Collider2D col)
    {
        //운석과 충돌함
        if (col.tag.Equals("Meteor"))
        {
            Destroy(col.gameObject);
        }
    }

    private void DisableShield()
    {
        this.gameObject.SetActive(false);
    }
}