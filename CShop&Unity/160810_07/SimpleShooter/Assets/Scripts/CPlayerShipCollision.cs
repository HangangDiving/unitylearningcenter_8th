﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CPlayerShipCollision : MonoBehaviour {

    //방어막 오브젝트 참조
    public GameObject _shield;

    public CGameManager _gameManager;

    //[유니티 이벤트 메소드]
    //Trigger 옵션이 설정된 오브젝트와 충돌이 됨

    //충돌이 시작됨
    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Enter - " + col.name + "과 부딪힘");

        //if(col.tag == "Meteor")
        if (col.tag.Equals("Meteor"))
        {
            Destroy(col.gameObject);
            //Debug.Log("Meteor Trigger Enter");

            //게임 종료 씬으로 이동
            //SceneManager.LoadScene("이동하려는 씬 이름");
            //SceneManager.LoadScene("End");
        }
        else if (col.tag.Equals("DestroyAllItem"))
        {
            Destroy(col.gameObject);

            //지정한 태그를 가진 오브젝트를 찾아줌
            //GameObject obj = GameObject.FindGameObjectWithTag("태그명");

            //지정한 태그를 가진 오브젝트 목록을 찾아줌
            //GameObject[] objs = GameObject.FindGameObjectsWithTag("태그명");
            GameObject[] meteors = GameObject.FindGameObjectsWithTag("Meteor");

            //C# 배열 갯수 : 배열명.Length
            for (int i = 0; i < meteors.Length; i++)
            {
                Destroy(meteors[i]);
            }
        }
        else if (col.tag.Equals("ShieldItem"))
        {
            Destroy(col.gameObject);

            //오브젝트.activeSelf : 오브젝트 활성 여부(true/false)
            if (this._shield.activeSelf)
                return;

            //오브젝트를 활성/비활성
            //오브젝트.SetActive(true/false);
            this._shield.SetActive(true);
        }
        else if (col.tag.Equals("SpeedDownItem"))
        {
            Destroy(col.gameObject);
            this._gameManager.MeteorSpeedDown();
        }
    }

    //충돌이 진행중
    void OnTriggerStay2D(Collider2D col)
    {
        //Debug.Log("Stay - " + col.name + "과 부딪힘");
    }

    //충돌이 끝남
    void OnTriggerExit2D(Collider2D col)
    {
        //Debug.Log("Exit - " + col.name + "과 부딪힘");
    }
}
