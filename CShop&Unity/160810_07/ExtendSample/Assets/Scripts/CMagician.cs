﻿using UnityEngine;
using System.Collections;

public class CMagician : CHuman {

    public int _int; //지능

    //CHuman의 Eat() 메소드를 재정의함
    protected override void Eat()
    {
        //CHuman의 Eat() 메소드를 호출함
        base.Eat();

        this._int += 2;
        Debug.Log(this._name + "(이)가 음식을 먹고 2만큼 지능(을)를 보충했습니다.[현재 지능 : "
            + this._int + "]");
    }

    public override void Attack(string monsterName)
    {
        Debug.Log(this._name + "캐릭터(이)가 " + monsterName + " 몬스터를 " + this._int
            + " 지능으로 공격합니다.");
    }
}
