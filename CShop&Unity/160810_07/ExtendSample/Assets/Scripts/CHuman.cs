﻿using UnityEngine;
using System.Collections;

public class CHuman : MonoBehaviour {

    public string _name; //이름
    public int _hp; //체력
    public float _speed; //속도
//-----------------------------------------------------------------------------//
    void Start () {
        this.Move();
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Food"))
        {
            Destroy(col.gameObject);
            this.Eat();
        }
    }
//-----------------------------------------------------------------------------//
    //이동한다
    private void Move()
    {
        Debug.Log(this._name + " 캐릭터가 " + this._speed + " 로 이동했습니다.");
        this.transform.Translate(Vector2.up * this._speed);
    }

    //오버라이드 될 메소드 정의
    //[부모]
    //protected virtual 오버라이드 될 메소드명() { ... }

    //먹다
    protected virtual void Eat()
    {
        this._hp += 20;
        Debug.Log(this._name + "(이)가 음식을 먹고 20만큼 체력(을)를 보충했습니다.[" + this._hp + "]");
    }

    public virtual void Attack(string monsterName) { }
}
