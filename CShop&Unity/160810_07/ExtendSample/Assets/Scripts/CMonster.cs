﻿using UnityEngine;
using System.Collections;

public class CMonster : MonoBehaviour {

    public string _monsterName;

    void OnTriggerEnter2D(Collider2D col)
    {
        this.Stop();

        /*if (col.name.Equals("Fighter"))
        {
            //전사의 공격 메소드 호출
            CFighter ch = col.GetComponent<CFighter>();
            ch.Attack(this._monsterName);
        }
        else if (col.name.Equals("Magician"))
        {
            //마법사의 공격 메소드 호출
            CMagician ch = col.GetComponent<CMagician>();
            ch.Attack(this._monsterName);
        }
        else if (col.name.Equals("Knight"))
        {
            //기사의 공격 메소드 호출
            CKnight ch = col.GetComponent<CKnight>();
            ch.Attack(this._monsterName);
        }*/

        //업캐스팅을 통한 다형성
        CHuman ch = col.GetComponent<CHuman>();

        //휴먼 명세를 통해 Attack 메소드를 호출하면
        //오버라이드 된 실 자식객체의 Attack 메소드를 호출할 수 있음
        ch.Attack(this._monsterName);

        //if (ch.name.Equals("Knight"))
        if(ch is CKnight)
        {
            //다운캐스팅을 통해 자식 객체 타입의 요소를 접근함
            //CKnight knight = ch.GetComponent<CKnight>();
            CKnight knight = ch as CKnight;
            Debug.Log("기사의 성령 수치는 " + knight._holy + "입니다.");
        }
    }

    private void Stop()
    {
        Rigidbody2D rigidbody2d = this.GetComponent<Rigidbody2D>();
        rigidbody2d.velocity = Vector2.zero; //속도 0으로
        rigidbody2d.gravityScale = 0f; //중력 없음
    }
}
