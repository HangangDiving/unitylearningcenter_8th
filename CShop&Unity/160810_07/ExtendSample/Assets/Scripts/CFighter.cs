﻿using UnityEngine;
using System.Collections;

//전사 클래스

//class 자식클래스 : 부모클래스 {
public class CFighter : CHuman {

    public int _str; //힘
//-----------------------------------------------------------------------------//   

    //오버라이드 할 메소드 정의
    //[자식]
    //protected override 오버라이드 할 메소드명() { ... }

    protected override void Eat()
    {
        //부모의 메소드를 호출
        //base.부모메소드();
        
        //CHuman의 Eat() 메소드를 호출
        base.Eat();

        this._str += 2;
        Debug.Log(this._name + "(이)가 음식을 먹고 2만큼 힘(을)를 보충했습니다.[현재 힘 : "
            + this._str + "]");
    }

    public override void Attack(string monsterName)
    {
        Debug.Log(this._name + "캐릭터(이)가 " + monsterName + " 몬스터를 " + this._str
            + " 힘으로 공격합니다.");
    }
}
