﻿using UnityEngine;
using System.Collections;

public class CKnight : CMagician {

    public int _holy; //성령

    protected override void Eat()
    {
        base.Eat(); //CHuman.Eat(), CMagician.Eat()

        this._holy += 5;
        Debug.Log(this._name + "(이)가 음식을 먹고 5만큼 성령(을)를 보충했습니다.[현재 성령 : "
            + this._holy + "]");
    }

    public override void Attack(string monsterName)
    {
        //CMagician의 Attack() 메소드 호출
        base.Attack(monsterName);

        Debug.Log(this._name + "캐릭터(이)가 " + monsterName + " 몬스터를 " + this._holy
            + " 성령으로 공격합니다.");
    }
}
