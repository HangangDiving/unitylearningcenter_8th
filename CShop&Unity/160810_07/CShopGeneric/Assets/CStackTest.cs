﻿using UnityEngine;
using System.Collections;

public class Monster
{
    string name = "몬스터";
    int level = 1;
    int hp = 100;

    public void PrintStat()
    {
        Debug.Log("name : " + name + ", level : " + level.ToString() + ", hp : " + hp.ToString());
    }
}

//제네릭 스택 클래스
//public class 클래스명<제네릭타입>
public class CStack<T>
{
    int emptyStack; //빈스텍 인덱스
    T[] items; //int 아이템 배열
    int top; //현재 스택 커서(번호)
    int size; //스택 크기(아이템 저장 갯수)

    public CStack(int size)
    {
        this.size = size;
        items = new T[size];
        emptyStack = -1;
        top = emptyStack;
    }

    public void Push(T item)
    {
        items[++top] = item;
    }

    public T Pop()
    {
        return items[top--];
    }

    public bool IsFull()
    {
        return (top + 1) == size;
    }

    public bool IsEmpty()
    {
        return top == emptyStack;
    }
}

public class CStackTest : MonoBehaviour {

	void Start () {

        //제네릭 사용하지 않았을 때 스택을 사용하는 코드

        int size = 5;

        //정수
        CStack<int> istack = new CStack<int>(size);
        for (int i = 0; i < 7; i++)
        {
            if (!istack.IsFull())
                istack.Push(i);
            else
                Debug.Log("스택이 꽉 찼습니다. [저장 실패 : " + i + "]");
        }
        while (!istack.IsEmpty())
        {
            Debug.Log(istack.Pop().ToString());
        }
        
        //실수
        CStack<float> fstack = new CStack<float>(size);
        for (int i = 0; i < 7; i++)
        {
            if (!fstack.IsFull())
                fstack.Push(i * 1.2f);
            else
                Debug.Log("스택이 꽉 찼습니다. [저장 실패 : " + i * 1.2f + "]");
        }
        while (!fstack.IsEmpty())
        {
            Debug.Log(fstack.Pop().ToString());
        }

        //문자열
        CStack<string> sstack = new CStack<string>(size);
        for (int i = 0; i < 7; i++)
        {
            if (!sstack.IsFull())
                sstack.Push(i + "번째 데이터");
            else
                Debug.Log("스택이 꽉 찼습니다. [저장 실패 : " + i + "번째 데이터" + "]");
        }
        while (!sstack.IsEmpty())
        {
            Debug.Log(sstack.Pop().ToString());
        }

        //몬스터 클래스
        CStack<Monster> mstack = new CStack<Monster>(size);
        for (int i = 0; i < 7; i++)
        {
            if (!mstack.IsFull())
            {
                mstack.Push(new Monster());
            }
            else
                Debug.Log("스택이 꽉 찼습니다. [저장 실패 : " + i + "]");
        }
        while (!mstack.IsEmpty())
        {
            mstack.Pop().PrintStat();
        }
    }
}
