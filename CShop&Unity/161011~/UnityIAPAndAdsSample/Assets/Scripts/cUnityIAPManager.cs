﻿using UnityEngine;
using System.Collections;

using UnityEngine.Purchasing; //서비스탭의 인앱에서 임포트해야 사용 가능
using System;
using UnityEngine.UI;

public class cUnityIAPManager : MonoBehaviour, IStoreListener {

    public Text _messageText;

    //유니티 구매 시스템 참조
    private static IStoreController _storeController;
    //매장별 구매 서브 시스템
    private static IExtensionProvider _storeExtensionProvider;

    //인앱 상품 코드 등록
    public static string _item01ProductId = "com.devlth.iap.item01";
    public static string _item02ProductId = "com.devlth.iap.item02";
    public static string _item03ProductId = "com.devlth.iap.item03";

    //인앱 관련 컨트롤러와 프로바이더가 참조 되었는지를 체크함
    bool IsInitialized()
    {
        return _storeController != null && _storeExtensionProvider != null;
    }

    //인앱 시스템(컨트롤러, 프로바이더) 초기화
    public void InitializePurcharsing()
    {
        if (IsInitialized())
        {
            _messageText.text = "인앱 시스템이 이미 초기화 됨";
            return;
        }

        //표준 인앱 시스템 빌더를 생성함
        ConfigurationBuilder builder = 
            ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        //구매 상품 목록을 빌더에 등록함
        builder.AddProduct(_item01ProductId, ProductType.Consumable);
        builder.AddProduct(_item02ProductId, ProductType.Consumable);
        builder.AddProduct(_item03ProductId, ProductType.Consumable);

        //등록한 상품 정보를 가진 빌더를 이용해 인앱 시스템을 초기화함
        UnityPurchasing.Initialize(this, builder);
    }

    //인앱 시스템 초기화 성공 이벤트 메소드
    void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        _messageText.text = "인앱 시스템 초기화 성공";

        //인앱 시스템(컨트롤러, 프로바이더) 관련 객체를 참조함
        _storeController = controller;
        _storeExtensionProvider = extensions;
    }

    //인앱 시스템 초기화 실패 메소드
    void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
    {
        _messageText.text = "인앱 시스템 초기화 실패 코드 : " + error.ToString();
    }

    //구매 실행
    public void PurchaseProduct(string productId)
    {
        if (!IsInitialized())
        {
            //만약 초기화 되지 않으면 골치 아픔
            //등록된 앱을 삭제 후 재등록 하는등 처음부터 다시 해야 되는 경우 발생
            _messageText.text = "아직 인앱 시스템이 초기화 되지 않음";
            return;
        }

        //인앱 시스템에 등록된 상품 정보를 불러옴
        Product product = _storeController.products.WithID(productId);

        //상품이 존재하고 구매가 가능하다면
        if (product != null && product.availableToPurchase)
        {
            _messageText.text = product.definition.id + " 상품의 구매를 시도함";

            //상품을 구매함(구매 신청)
            _storeController.InitiatePurchase(product);
        }
        else
        {
            _messageText.text = product.definition.id + " 상품의 구매를 요청할 수 없음";
        }
    }
    
    //인앱 상품 구매 실패 처리
    void IStoreListener.OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        _messageText.text = "상품 구매 완료 : " + i.definition.id + " (이유 : " + p.ToString() + ")";
    }

    //인앱 상품 구매 완료 처리
    PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e)
    {
        _messageText.text = "상품 구매 완료 : " + e.purchasedProduct.definition.id;

        //구매가 완료된 상품에 따른 후속 처리(어떤 인앱 상품 코드인지 확인)
        if (string.Equals(e.purchasedProduct.definition.id, _item01ProductId, StringComparison.Ordinal))
        {
            //후속 처리...
        }
        else if (string.Equals(e.purchasedProduct.definition.id, _item02ProductId, StringComparison.Ordinal))
        {
            //후속 처리...
        }
        else if (string.Equals(e.purchasedProduct.definition.id, _item03ProductId, StringComparison.Ordinal))
        {
            //후속 처리...
        }

        return PurchaseProcessingResult.Complete;
    }

    // Use this for initialization
    void Start () {
        if (_storeController == null)
        {
            //인앱 시스템 참조 및 초기화(상품 등록, 이벤트 연결)
            InitializePurcharsing();
        }
	}

    //아이템1 구매 버튼 클릭
    public void OnPurchaseItem01ButtonClick()
    {
        PurchaseProduct(_item01ProductId);
    }

    //아이템2 구매 버튼 클릭
    public void OnPurchaseItem02ButtonClick()
    {
        PurchaseProduct(_item02ProductId);
    }

    //아이템3 구매 버튼 클릭
    public void OnPurchaseItem03ButtonClick()
    {
        PurchaseProduct(_item03ProductId);
    }
}
