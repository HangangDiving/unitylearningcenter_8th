﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//유니티 광고 사용
using UnityEngine.Advertisements;

public class cUnityAdsManager : MonoBehaviour {

    [SerializeField]
    string _androidGameId = ""; //안드로이드 게임 아이디

    [SerializeField]
    bool _testMode = true; //테스트 모드 사용 여부

    public Button _adsButton; //광고 보기 버튼
    public Text _messageText; //로그 출력 텍스트 

    // Use this for initialization
    void Start () {

        //서비스 준비가 되었고 초기화가 된 상태가 아니라면
        if (Advertisement.isSupported && !Advertisement.isInitialized)
        {
            //유니티 광고 초기화
            Advertisement.Initialize(_androidGameId, _testMode);
        }

	}
	
	// Update is called once per frame
	void Update () {

        //광고를 볼 수 있을때만 버튼을 활성화 함
        //interactable : true이어야 버튼기능이 활성화
        _adsButton.interactable = 
            (Advertisement.isInitialized && Advertisement.IsReady(null));

	}

    public void OnUnityAdsButtonClick()
    {
        //광고 보기 옵션
        ShowOptions option = new ShowOptions();
        option.resultCallback = UnityAdsShowCallback;

        //옵션 설정 후 광고를 띄움
        Advertisement.Show(null, option);
    }

    //광고 출력 결과 이벤트 메소드
    void UnityAdsShowCallback(ShowResult result)
    {
        switch (result)
        {
            //광고 시청을 완료함
            case ShowResult.Finished:
                _messageText.text = "광고 시청을 완료 했음";
                //보상 지급 처리 코드...
                break;
            //광고를 스킵함
            case ShowResult.Skipped:
                _messageText.text = "광고 시청을 그냥 넘김";
                break;
            //광고 보기를 실패함
            case ShowResult.Failed:
                _messageText.text = "광고 시청이 실패하였음";
                break;
        }

        Debug.Log(_messageText.text);
    }
}
