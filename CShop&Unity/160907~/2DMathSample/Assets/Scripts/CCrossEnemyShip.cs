﻿using UnityEngine;
using System.Collections;

public class CCrossEnemyShip : MonoBehaviour {

    public Transform _playerShip;

	// Use this for initialization
	void Start () {
        _playerShip = GameObject.Find("PlayerShip").transform;
	}
	
	// Update is called once per frame
	void Update () {

        //플레이어 쉽을 향한 방향 벡터를 구함
        Vector2 dir = _playerShip.position - transform.position;

        //적기가 방향으로 회전하기 위한 각도를 구함
        float angle = Vector2.Angle(-transform.up, dir.normalized);

        //Debug.Log("angle : " + angle.ToString());

        //회전 방향에 따른 수직벡터를 구함
        Vector3 cross = Vector3.Cross(-transform.up, dir.normalized);

        //z 수직 벡터가 음수일 경우에는 360도에서 감산함
        if (cross.z < 0)
        {
            angle = 360 - angle;
        }

        //회전
        transform.Rotate(0f, 0f, angle);
	}
}
