﻿using UnityEngine;
using System.Collections;

public class CDotEnemyShip : MonoBehaviour {

    Transform _playerShip;
    SpriteRenderer _spriteRenderer;

	// Use this for initialization
	void Start () {
        _playerShip = GameObject.Find("PlayerShip").transform;
	}
	
	// Update is called once per frame
	void Update () {

        //플레이어 쉽을 향한 방향 벡터를 구함
        Vector2 dir = _playerShip.position - transform.position;

        //적기의 시선과 플레이어 쉽간의 내적을 구함
        // - 활용예 : 시야 범위를 구할 때 사용

        //[값] (기준 방향벡터의 전방 기준)
        // - 양수 : 예각 (플레이어 쉽이 예각 범위안에 있다) -> 0도 ~ 90도 사이
        // - 음수 : 둔각 (플레이어 쉽이 둔각 범위안에 있다) -> 90도 ~ 180도 사이
        // - 0 : 직각 (플레이어 쉽이 직각 상태이다)
        float dotValue = Vector2.Dot(-transform.up, dir.normalized);

        //Debug.Log("내적값 : " + dotValue.ToString());

        //현재 적기가 플레이어쉽의 뒤로 넘어가면 모습을 감춤
        if (dotValue < 0)
        {
            _spriteRenderer.enabled = false;
        }
        else
        {
            _spriteRenderer.enabled = true;
        }
    }
}
