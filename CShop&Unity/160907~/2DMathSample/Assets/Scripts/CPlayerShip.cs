﻿using UnityEngine;
using System.Collections;

public class CPlayerShip : MonoBehaviour {

    SpriteRenderer _spriteRenderer;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        transform.Translate(Vector2.right * h * 7f * Time.deltaTime);
        transform.Translate(Vector2.up * v * 7f * Time.deltaTime);
    }
}
