#include<iostream>>
#include<stdlib.h>
#include<string.h>
#include<time.h>

using namespace std;

class Monster
{
public:
	int _hp;
	int _damage;
	int _level;
};

void main()
{
	int value1 = 100;
	int value2 = 200;

	Monster monster;
	monster._hp = 100;
	monster._damage = 20;
	monster._level = 1;

	cout << "value1 : " << value1 << endl;
	cout << "value2 : " << value2 << endl;

	//[주소 연산자]
	//&변수명 : 현재 변수명을 가진 변수의 주소를 지칭함
	cout << "value1 변수의 주소 : " << &value1 << endl;
	cout << "value2 변수의 주소 : " << &value2 << endl;

	//[포인터 연산자]
	//*(주소) : 주소의 해당하는 변수를 지칭함
	cout << "value1 주소를 통한 값 접근 : " << *(&value1) << endl;
	cout << "value2 주소를 통한 값 접근 : " << *(&value2) << endl;
	cout << "*(&(*(&value1))) : " << *(&(*(&value1))) << endl;

	//*기호 뒤에는 반드시 주소가 와야함
	//cout << "*value1 : " << *value1 << endl;

	cout << "monster 객체 변수의 주소 : " << &monster << endl;
	cout << "monster 객체의 hp 변수 주소 : " << &(monster._hp) << endl;
	cout << "monster 객체의 damage 변수 주소 : " << &(monster._damage) << endl;
	cout << "monster 객체의 level 변수 주소 : " << &(monster._level) << endl;
	
	cout << "monster 객체의 damage 변수 : " << *(&(monster._damage)) << endl;

	cout << "monster 객체의 크기 : " << sizeof(monster) << endl;
	cout << "monster 객체의 damage 변수 크기 : " << sizeof(monster._damage) << endl;

	int* pvalue1 = &value1;

	cout << "pvalue1 : " << pvalue1 << ", &value1 : " << &value1 << endl;
	cout << "*pvalue1 : " << *pvalue1 << ", *(&value1)" << *(&value1) << endl;

	int *pvalue2 = &value2;
	cout << "pvalue2 : " << pvalue2 << ", &value2 : " << &value2 << endl;
	cout << "*pavalue2 : " << *pvalue2 << ", *(&value2) : " << *(&value2) << endl;

	Monster* pMonster;
	pMonster = &monster;

	cout << "sizeof(value1) : " << sizeof(value1) << endl;
	cout << "sizeof(pvalue1) : " << sizeof(pvalue1) << endl;
	cout << "sizeof(monster) : " << sizeof(monster) << endl;
	cout << "sizeof(pMonster) : " << sizeof(pMonster) << endl;

	cout << "pMonster : " << pMonster << ", &monster : " << &monster << endl;
	
	cout << "monster.hp : " << monster._hp << endl;
	cout << "(&monster)->hp : " << (&monster)->_hp << endl;
	cout << "pMonster->hp : " << pMonster->_hp << endl;

	cout << "(*pMonster).hp : " << (*pMonster)._hp << endl;
}