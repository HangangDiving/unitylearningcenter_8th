//객체의 동적 배열 생성

#include<iostream>
#include<stdlib.h>
#include<string>
#include<time.h>

using namespace std;

class Monster
{
public:
	string name;
	int hp;
	int level;

	void CreateMonster(string name, int hp, int level)
	{
		cout << "this의 값 : " << this << endl;
		this->name = name;
		this->hp = hp;
		this->level = level;
	}

	void PrintMonsterStat(int index)
	{
		cout << index << "번째 몬스터 스탯 : " << this->name << ", "
			<< this->hp << ", " << this->level << endl;
	}
};

//몬스터 배열을 포인터 받는 법
void CreateMonsters(Monster* monsters, int count)
{
	for (int i = 0; i < count; i++)
	{
		string name;
		int hp;
		int level;

		cout << i + 1 << "번째 몬스터의 이름 입력 >>> "; cin >> name;
		cout << i + 1 << "번째 몬스터의 체력 입력 >>> "; cin >> hp;
		cout << i + 1 << "번째 몬스터의 레벨 입력 >>> "; cin >> level;

		cout << "&monster[" << i << "]의 주소 : " << &monsters[i] << endl;

		//(*(monsters + i)).CreateMonster(name, hp, level);
		monsters[i].CreateMonster(name, hp, level);
	}
}

//몬스터 배열을 래퍼런스로 받는 법
void PrintMonsters(Monster monsters[], int count)
{
	for (int i = 0; i < count; i++) {
		//(monsters + i)->PrintMonsterStat(i + 1);
		//(&(monsters[i]))->PrintMonsterStat(i + 1);

		//(*(monsters + i)).PrintMonsterStat(i + 1);
		monsters[i].PrintMonsterStat(i + 1);
	}
}

void main()
{
	int count = 0;
	cout << "몬스터의 마리수를 입력하세요 >>> "; cin >> count;

	//int* arr = new int[크기];
	//동적으로 지정한 마리수만큼 몬스터를 생성함
	Monster* monsters = new Monster[count];
	//Monster monsters[5];

	CreateMonsters(monsters, count);

	PrintMonsters(monsters, count);

	delete[] monsters;
}