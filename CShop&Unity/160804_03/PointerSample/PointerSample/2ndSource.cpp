#include<iostream>
#include<stdlib.h>
#include<string>
#include<time.h>

using namespace std;

void PrintArr1(int* arr)
{
	cout << "main().arr의 값 : " << arr << endl;
	for (int i = 0; i < 5; i++) {
		//cout << i + 1 << "번째 값 출력 <<< " << *(arr + i) << endl;
		cout << i + 1 << "번째 값 출력 <<< " << arr[i] << endl;
	}
}

void PrintArr2(int arr[5]) //<-생성이 아닌 포인터를 받는거다(배열 참조)
{
	cout << "main().arr의 값 : " << arr << endl;
	for (int i = 0; i < 5; i++) {
		cout << i + 1 << "번째 값 출력 <<< " << arr[i] << endl;
	}
}

void main()
{
	//arr => 배열명 => 상수 => 배열의 시작 주소값
	int arr[5] = { 0, 1, 2, 3, 4 };
	cout << "arr : " << arr << endl;
	cout << "&arr[0] : " << &arr[0] << endl;

	//arr 주소에 1을 더하면 1은 하나의 자료형크기(int)를 지칭함
	cout << "arr + 1 : " << arr + 1 << endl;
	cout << "&arr[1] : " << &arr[1] << endl;
	cout << "arr + 2 : " << arr + 2 << endl;

	cout << "*(arr + 3) : " << *(arr + 3) << endl;
	cout << "*(&arr[3]) : " << *(&arr[3]) << endl;
	cout << "arr[3] : " << arr[3] << endl;

	for (int i = 0; i < 5; i ++) {
		cout << i + 1 << "번째 값 입력 >>> ";
		cin >> arr[i];
	}

	PrintArr1(arr);
	//PrintArr2(&arr[0]);
}