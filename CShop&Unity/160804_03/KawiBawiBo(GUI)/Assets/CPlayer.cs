﻿using UnityEngine;
using System.Collections;

// 1. 가위바위보 버튼을 통해 가위바위보를 입력 받음
// 2. 게임 매니저에게 본인의 가위바위보를 
//      알려줘서 게임을 진행시킴

public class CPlayer : MonoBehaviour {

    public int _kbb;

    // 게임 매니저 참조
    public CGameManager _gameManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // 버튼을 눌렀을때 실행될 이벤트 메소드
    public void OnKawiBawiBoButtonClick(int kbb)
    {
        //this._kbb = kbb;
        _kbb = kbb;

        if (!this._gameManager._isPlaying)
            return;

        // 게임 매니저에게 가위바위보 게임 진행을 요청함
        _gameManager.Play(_kbb);
    }
}
