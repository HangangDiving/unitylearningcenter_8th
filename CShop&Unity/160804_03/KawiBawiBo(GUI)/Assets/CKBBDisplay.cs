﻿using UnityEngine;
using System.Collections;

public class CKBBDisplay : MonoBehaviour {

    // c++
    // Sprite _sprites[3];
    // Sprite* _sprites = new Sprite[3];
    // public Sprite[] _sprites = new Sprite[3]; // 3개짜리 배열 생성
    public Sprite[] _sprites; 

    // 스프라이트 랜더러 컴포넌트(객체) 참조
    public SpriteRenderer _spriteRenderer;

	// Use this for initialization
	void Start () {
        ShowStart();
    }

    // 게임 시작
	public void ShowStart()
    {
        StopCoroutine("KawibawiboLoopCoroutine");

        // 코루틴 실행
        // StartCoroutine("코루틴명");
        StartCoroutine("KawibawiboLoopCoroutine");
    }

    // 게임 종료
    public void ShowEnd(int comKbb)
    {
        // 코루틴 중지
        // StopCoroutine("코루틴명");
        StopCoroutine("KawibawiboLoopCoroutine");

        // 컴퓨터가 낸 가위바위보로 표시를 변경함
        _spriteRenderer.sprite = _sprites[comKbb];
    }
    

    // 코루틴 함수
    IEnumerator KawibawiboLoopCoroutine()
    {
        int index = 0;

        while (true)
        {
            // 코루틴의 시간 지연
            // yield return new WaitForSeconds(지연시간);
            yield return new WaitForSeconds(0.1f);

            _spriteRenderer.sprite = _sprites[index++];

            // if (index > 2) { index = 0; }
            // [삼항 연산자] : if else 문을 연산식으로 표현한 것
            // 변수 = (조건식) ? 참결과 : 거짓결과;
            index = (index > 2) ? 0 : index;
        }

    }
}
