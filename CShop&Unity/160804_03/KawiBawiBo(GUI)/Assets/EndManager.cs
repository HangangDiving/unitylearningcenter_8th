﻿using UnityEngine;
using System.Collections;

//UI 관리
using UnityEngine.UI;

//씬 관리
using UnityEngine.SceneManagement;

public class EndManager : MonoBehaviour {

    public Text _gameResultText; //게임 결과 텍스트
    public Image _winImage;
    public Image _loseImage;

    // Use this for initialization
    void Start () {
        //게임 결과값 출력
        this._gameResultText.text = CGameManager.gameResult;
        if (this._gameResultText.text.Equals("Winner"))
            this.SetResultImage(true); //승리 이미지 활성화
        else
            this.SetResultImage(false); //승리 이미지 비활성화
    }

    //승리/패배 이미지 활성/비활성화
    private void SetResultImage(bool isWin)
    {
        this._winImage.gameObject.SetActive(isWin);
        this._loseImage.gameObject.SetActive(!isWin);
    }

    public void OnRestartButtonClick()
    {
        SceneManager.LoadScene("Demo");
    }
}
