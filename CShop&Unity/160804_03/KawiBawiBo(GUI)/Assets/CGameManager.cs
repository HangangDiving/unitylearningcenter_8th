﻿using UnityEngine;
using System.Collections;

// UGUI 사용
using UnityEngine.UI;

//씬 관리
using UnityEngine.SceneManagement;

public class CGameManager : MonoBehaviour {
        
    private const int KAWI = 0;
    private const int BAWI = 1;
    private const int BO = 2;

    public static string gameResult = "Loser";

    //플레이어 승리 횟수
    //public int _playerWinCount = 0;
    //public int _comWinCount = 0;
    public Text _playerWinCountText; //플레이어 승리 카운트
    public Text _comWinCountText; //컴퓨터 승리 카운트

    // 컴퓨터(컴퓨터 게임오브젝트의 
    //      CComputer 스크립트 컴포넌트)를 참조함
    public CComputer _computer;

    // 디스플레이를 참조함
    public CKBBDisplay _display;

    // 게임 결과 텍스트 참조
    public Text _resultText;

    public bool _isPlaying = false; //게임의 진행 유무

    void Start()
    {
        this._isPlaying = true;
    }

    // 가위바위보 게임 플레이!!!
    public void Play(int playerKbb)
    {
        // 컴퓨터의 가위바위보를 요청함
        int comKbb = _computer.GetKbb();

        // 디스플레이에게 가위바위보 쇼를 멈추라고 함
        _display.ShowEnd(comKbb);

        // 가위바위보 게임 판정
        if (playerKbb == comKbb)
        {
            _resultText.text = "DRAW!!";

            Debug.Log(playerKbb + " vs " + comKbb + " => 무승부");
        }
        else if ((playerKbb == KAWI && comKbb == BAWI) ||
            (playerKbb == BAWI && comKbb == BO) ||
            (playerKbb == BO && comKbb == KAWI))
        {
            _resultText.text = "LOSE!!";
            this.ScoreUp(this._comWinCountText);
            Debug.Log(playerKbb + " vs " + comKbb + " => 패배");
        }
        else
        {
            _resultText.text = "WIN!!";
            this.ScoreUp(this._playerWinCountText);
            Debug.Log(playerKbb + " vs " + comKbb + " => 승리");
        }
        
        this._isPlaying = false;
    }

    private void ScoreUp(Text scoreText)
    {
        //숫자형문자열 => 정수형으로 변경
        //int 정수형변수 = int.Parse("숫자형문자열")
        int count = int.Parse(scoreText.text);
        count++;

        if (count >= 3)
        {
            this.GameEnd();
            return;
        }

        //정수형변수 => 문자형변수
        //string 문자형변수 = 정수형변수.ToString();
        scoreText.text = count.ToString();
    }

    void GameEnd()
    {
        if(int.Parse(this._playerWinCountText.text)
            > int.Parse(this._comWinCountText.text))
            CGameManager.gameResult = "Winner";
        else
            CGameManager.gameResult = "Loser";

        //게임종료씬으로 이동
        SceneManager.LoadScene("End");
    }

    // 게임 재시작
    public void OnRestartGameButtonClick()
    {
        Debug.Log("Game ReStart!!!");
        if (!this._isPlaying)
        {
            //가위바위보 루프 재가동
            this._display.ShowStart();
            this._isPlaying = true;
            this._resultText.text = "Play!!";
        }
    }
}
