﻿using UnityEngine;
using System.Collections;

public class CComputer : MonoBehaviour {

    // 컴퓨터의 가위바위보
    public int _kbb;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // 컴퓨터가 본인의 가위바위보를 뽑아서 리턴함
    public int GetKbb()
    {
        _kbb = Random.Range(0, 3);

        return _kbb;
    }
}
