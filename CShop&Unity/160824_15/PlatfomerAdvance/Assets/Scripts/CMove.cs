﻿using UnityEngine;
using System.Collections;

//이동 클래스
public class CMove : MonoBehaviour {

    //캐릭터 상태
    protected CCharacterState _characterState;

    protected virtual void Awake()
    {
        _characterState = GetComponent<CCharacterState>();
    }

	// Use this for initialization
	protected virtual void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Flip()
    {
        Vector3 theScale = transform.localScale;

        //스프라이트 오브젝트 돌리기
        theScale.x *= -1;
        transform.localScale = theScale;

        _characterState.Flip();
    }
}
