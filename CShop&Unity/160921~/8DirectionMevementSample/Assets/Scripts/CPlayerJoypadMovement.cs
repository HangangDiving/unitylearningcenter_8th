﻿using UnityEngine;
using System.Collections;

//CNControl 사용
using CnControls;

public class CPlayerJoypadMovement : MonoBehaviour {

    CharacterController _cc;

    Animator _animator;

    Vector3 _moveDirection;

    public float _moveSpeed;

    public float _gravity;

    string _attackAnimName;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _cc = GetComponent<CharacterController>();
    }

	// Use this for initialization
	void Start () {
        _attackAnimName = GetComponent<CPlayerJoystickAttack>()._attackAnimName;
	}
	
	// Update is called once per frame
	void Update () {
        //이미 공격 애니메이션을 실행중이면 패스
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName(_attackAnimName))
        {
            return;
        }

        //float h = Input.GetAxis("Horizontal");
        //float v = Input.GetAxis("Vertical");

        float h = CnInputManager.GetAxis("Horizontal");
        float v = CnInputManager.GetAxis("Vertical");

        //이동 방향 벡터
        _moveDirection = new Vector3(h, 0f, v);

        //이동 키를 눌렀다면(왼, 오, 위, 아)
        if (_moveDirection.x != 0 || _moveDirection.z != 0)
        {
            _animator.SetBool("Move", true);
        }
        else //이동 키를 누르지 않았다면
        {
            _animator.SetBool("Move", false);
        }

        //캐릭터의 앞 방향 설정
        if (_moveDirection != Vector3.zero)
        {
            transform.forward = _moveDirection.normalized;
        }

        //대각선 이동 속도 처리
        float speed = _moveSpeed;
        if (h != 0 && v != 0)
        {
            float degree = Mathf.Cos(45f * Mathf.Deg2Rad);
            speed = speed * degree;
        }

        //이동 방향 및 크기 설정
        _moveDirection *= speed;

        //이동
        _cc.Move(_moveDirection * Time.deltaTime);
    }
}
