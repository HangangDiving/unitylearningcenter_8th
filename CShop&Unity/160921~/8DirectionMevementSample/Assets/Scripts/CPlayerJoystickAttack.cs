﻿using UnityEngine;
using System.Collections;

using CnControls;

public class CPlayerJoystickAttack : MonoBehaviour {

    Animator _animator;

    public string _attackAnimName;

    public Transform _hitPoint; //타격 위치

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

        //이미 공격 애니메이션을 실행중이면 패스
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName(_attackAnimName)) return;

        //공격 버튼(키)을 누르면 공격 애니메이션을 수행함
        if (CnInputManager.GetButtonDown("Attack"))
        {
            _animator.SetTrigger(_attackAnimName);
        }
	}

    void Attack()
    {
        //Debug.Log("Attack!!!");

        Collider[] hitColliders = Physics.OverlapSphere(
            _hitPoint.position, 1f, 1 << LayerMask.NameToLayer("Monster"));

        if (hitColliders.Length <= 0) return;

        Debug.Log("빠쌰!!!");

        hitColliders[0].SendMessage("Hit");
    }
}
