﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Monster
{
    public int num;

    public Monster(int num)
    {
        this.num = num;
    }

    public void Attack()
    {
        Debug.Log(this.num + "번 몬스터가 공격을 시작합니다.");
    }
}

public class CListTest : MonoBehaviour {

    //List<아이템타입>
    List<Monster> monsterList;

	// Use this for initialization
	void Start () {
        monsterList = new List<Monster>();

        Monster monster1 = new Monster(1);
        monsterList.Add(monster1);
        Monster monster2 = new Monster(2);
        monsterList.Add(monster2);
        Monster monster3 = new Monster(3);
        monsterList.Add(monster3);
        Monster monster4 = new Monster(4);
        monsterList.Add(monster4);
        Monster monster5 = new Monster(5);
        monsterList.Add(monster5);

        Debug.Log("----------------------------------------------------------------------");

        Debug.Log("Monster List Count : " + monsterList.Count);

        //지정한 객체랑 같은 아이템의 인덱스를 추출함

        int index = monsterList.IndexOf(monster2);

        Debug.Log("Monster Num : " + index);

        //특정 인덱스 또는 아이템을 삭제함
        //monsterList.RemoveAt(3);
        monsterList.RemoveAt(4);
        monsterList.Remove(monster4);

        Debug.Log("Monster List Count : " + monsterList.Count);

        Debug.Log("----------------------------------------------------------------------");

        foreach (Monster mon in monsterList)
        {
            mon.Attack();
        }

        //특정 아이템을 5번째 인덱스 사이에 추가함
        monsterList.Insert(3, new Monster(6));

        Debug.Log("----------------------------------------------------------------------");

        foreach (Monster mon in monsterList)
        {
            mon.Attack();
        }

        //리스트를 클리어함
        monsterList.Clear();

        Debug.Log("Monster List Count : " + monsterList.Count);
    }

}
