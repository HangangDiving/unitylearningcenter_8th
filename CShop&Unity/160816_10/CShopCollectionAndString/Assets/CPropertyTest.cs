﻿using UnityEngine;
using System.Collections;

public class CMonster
{
    private string name;
    private int hp;

    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }

    public int Hp
    {
        get { return this.hp; }
        set {
            if (value <= 0) value = 0;
            this.hp = value;
        }
    }
}

public class CPropertyTest : MonoBehaviour {

    private int _gameLevel;

    public void SetGameLevel(int level)
    {
        _gameLevel = level;
    }

    public int GetGameLevel()
    {
        return _gameLevel;
    }
    //===========================================================//
    public int GameLevel
    {
        set { _gameLevel = value; }
        get { return this._gameLevel; }
    }

	// Use this for initialization
	void Start () {
        //SetGameLevel(1);
        GameLevel = 1;

        //Debug.Log("게임 레벨 : " + GetLevel());
        Debug.Log("게임 레벨 : " + GameLevel);

        /*
        CMonster monster = new CMonster();
        monster.Name = "오우거";
        monster.Hp = 100;

        Debug.Log("몬스터 이름 : " + monster.Name);
        Debug.Log("몬스터 체력 : " + monster.Hp);
        */
    }

}
