﻿// [Dictionary]

// 1. 아이템 추가 : Dictionary.Add(아이템)
// 2. 아이템 검색 : Dictionary["키이름"]
// 3. 아이템 삭제 : Dictionary.Remove("키이름")
// 4. 아이템 존재 유무 검사 : Dictionary.ContainsKey("키이름")

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item
{
    string itemName;
    int str;
    int dex;
    int hp;

    public Item(string itemName, int str, int dex, int hp)
    {
        this.itemName = itemName;
        this.str = str;
        this.dex = dex;
        this.hp = hp;
    }

    public void ShowItemStat()
    {
        Debug.Log(
            this.itemName + ", STR(" + this.str + "), DEX(" + this.dex + "), HP(" + this.hp + ")");
    }
}

public class CDictionaryTest : MonoBehaviour {

    Dictionary<string, Item> itemMap;

	// Use this for initialization
	void Start () {

        itemMap = new Dictionary<string, Item>();

        string itemName;

        //아이템 추가
        itemName = "영혼의검";
        itemMap.Add(itemName, new Item(itemName, 100, 80, 80));
        itemName = "은둔자의방패";
        itemMap.Add(itemName, new Item(itemName, 30, 30, 50));
        itemName = "혼돈의망토";
        itemMap.Add(itemName, new Item(itemName, 10, 80, 50));
        itemName = "도적의단검";
        itemMap.Add(itemName, new Item(itemName, 60, 90, 40));

        //아이템 검색
        if (itemMap.ContainsKey("혼돈의망토"))
        {
            Item item = itemMap["혼돈의망토"];
            item.ShowItemStat();
        }

        Debug.Log("Dictionary Count : " + itemMap.Count);

        //아이템 목록 출력(while)
        var enumerator = itemMap.GetEnumerator();
        while (enumerator.MoveNext())
        {
            var pair = enumerator.Current;
            Item item = pair.Value;
            item.ShowItemStat();
        }

        //키 이름의 아이템을 삭제함
        bool result = itemMap.Remove("혼돈의망토");
        if (result)
        {
            Debug.Log("혼돈의망토 아이템이 삭제됨");
        }

        //아이템 목록 출력(for)
        foreach (KeyValuePair<string, Item> pair in itemMap)
        {
            Item item = pair.Value;
            item.ShowItemStat();
        }

        //딕셔너리 모두 삭제
        itemMap.Clear();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
