﻿using UnityEngine;
using System.Collections;

public class CStringTest : MonoBehaviour {

	// Use this for initialization
	void Start () {

        bool bResult = false;
        int iResult = 0;

        //---------------------------------------------------
        // 1.문자열 비교 : Compare(비교문자열1, 비교문자열2),
        //    CompareTo(비교문자열2)
        // - 결과 : 작으면(-1), 크면(1), 같으면(0)
        //---------------------------------------------------

        string str1 = "abc";
        string str2 = "def";
        string temp = "abc";
        Debug.Log("str1 : " + str1 + ", str2 : " + str2);

        iResult = string.Compare(str1, temp);
        Debug.Log("string.Compare(str1, temp) : " + iResult);

        iResult = str1.CompareTo(str2);
        Debug.Log("str1.CompareTo(str2) : " + iResult);

        iResult = str2.CompareTo(str1);
        Debug.Log("str2.CompareTo(str1)" + iResult);

        //---------------------------------------------------
        // 2.문자열 포함 여부 확인 : Contains(포함체크할문자열)
        // - 결과 : 포함함(true), 포함하지 않음(false)
        //---------------------------------------------------

        bResult = str2.Contains("ef");
        Debug.Log("str2.Contains(\"ef\") : " + bResult);

        //---------------------------------------------------
        // 3.문자열 위치 앞에서 확인 : IndexOf(찾을려는문자열)
        // - 결과 : 있으면(위치), 없으면(-1)
        //---------------------------------------------------

        string str3 = "Hello Unity!";
        iResult = str3.IndexOf("Unity");
        Debug.Log("str3.IndexOf(\"Unity\") : " + iResult);

        //---------------------------------------------------
        // 4.특정 위치에 문자열 삽입 : Insert(삽입위치, 삽입문자열)
        // - 결과 : 삽입된 문자열
        //---------------------------------------------------

        iResult = str3.IndexOf("!");
        temp = str3.Insert(iResult, "5");
        Debug.Log("str3.Insert(str3.IndexOf(\"!\"), \"5\") : " + temp);

        //---------------------------------------------------
        // 5.문자열 배열을 하나의 문자열로 만들기 : Join(기호, 조인할 배열)
        // - 결과 : 조인된 문자열
        //---------------------------------------------------

        string[] arr = { "Hello", "Unity", "2D" };
        string str4 = string.Join(" ", arr);
        Debug.Log("string.Join(\" \", arr) : " + str4);

        //---------------------------------------------------
        // 6.문자열 길이 : Length
        // - 결과 : 문자열 길이
        //---------------------------------------------------

        iResult = str3.LastIndexOf("Unity");
        Debug.Log("str3.LastIndexOf(\"Unity\") : " + iResult);

        iResult = str3.Length;
        Debug.Log("str3.Length : " + iResult);

        //---------------------------------------------------
        // 7.문자열 제거 : Remove(시작인덱스, 갯수)
        // - 결과 : 제거된 문자열
        //---------------------------------------------------

        string sResult = str3.Remove(6, 5);
        Debug.Log("str3.Remove(6, 5) : " + sResult);

        //---------------------------------------------------
        // 8.문자열 교체 : Replace
        // - 결과 : 교체된 문자열
        //---------------------------------------------------

        string str5 = "_____ Unity!!";
        sResult = str5.Replace("_____", "Hello");
        Debug.Log("str5.Replace(\"_____\", \"Hello\") : " + sResult);

        //---------------------------------------------------
        // 9.문자열 교체 : Split
        // - 결과 : 문자열 분리
        //---------------------------------------------------

        string[] strs = str3.Split(" ".ToCharArray(), System.StringSplitOptions.None);
        foreach (string str in strs)
        {
            Debug.Log("str3.Split : " + str);
        }

        //---------------------------------------------------
        // 10.문자열 교체 : SubString(시작인덱스, 카운트)
        // - 결과 : 분리된 문자열 배열
        //---------------------------------------------------

        sResult = str3.Substring(6, 5);
        Debug.Log("str3.Substring(6, 5) : " + sResult);

        //---------------------------------------------------
        // 11.문자열 대소문자 변경 : ToLower(), ToUpper()
        // - 결과 : 대문자->소문자, 소문자->대문자
        //---------------------------------------------------

        string str6 = "ABCdef";
        temp = str6.ToLower();
        Debug.Log("str6.ToLower() : " + temp);
        temp = str6.ToUpper();
        Debug.Log("str6.ToUpper() : " + temp);

        //---------------------------------------------------
        // 12. 문자열 공백 제거
        // - 결과 : 공백 제거된 문자열
        //---------------------------------------------------

        string str7 = "   Hello Unity!!   ";
        temp = str7.Trim();
        Debug.Log("str7.Trim() : " + temp);
    }
}
