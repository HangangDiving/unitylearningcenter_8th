﻿using UnityEngine;
using System.Collections;

//방향 이동
public class CDirectionMovement : MonoBehaviour {

    public Vector2 _direction; //방향
    public float _speed= 5f; //속도

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.Move();
	}

    private void Move()
    {
        this.transform.Translate(this._direction * this._speed * Time.deltaTime);
    }
}
