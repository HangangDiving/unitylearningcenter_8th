﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CPlayerShipItemPickUp : MonoBehaviour {

    public Text _starCount; //별점 정보
    public CPlayerShipDamage _shipDamage;
    public CPlayerShipShot _shipShot;
    public Transform[] _miniShipGenPos; //아기 비행기 생성 위치
    public GameObject _miniShipPrefab; //아기 비행기 프리팹

    void OnTriggerEnter2D(Collider2D col)
    {
        //별 아이템을 먹었다면
        if (col.tag.Equals("StarItem"))
        {
            //별 카운트를 증가함
            int cnt = int.Parse(_starCount.text);
            _starCount.text = (++cnt).ToString();
            Destroy(col.gameObject);
            return;
        }
        //체력 보충 아이템을 먹었다면
        else if (col.tag.Equals("HpItem"))
        {
            //체력을 20만큼 올려줌
            this._shipDamage.HpUp(20);

            Destroy(col.gameObject);
        }
        //발포 카운트 증가 아이템을 먹었다면
        else if (col.tag.Equals("ShotCountUpItem"))
        {
            _shipShot.ShotCountUp(); //발포 카운트를 3으로 올려 줌

            Destroy(col.gameObject);
        }
        else if (col.tag.Equals("MiniShipItem"))
        {
            foreach (Transform genPos in this._miniShipGenPos)
            {
                //미니 비행기 자리에 이미 비행기가 있다면
                if (genPos.childCount > 0) continue;
                //미니 비행기를 생성함
                GameObject miniShip =
                    Instantiate(this._miniShipPrefab, genPos.position, genPos.rotation)
                    as GameObject;
                //미니 비행기를 현재 미니 비행기 생성위치의 자식으로 등록함
                miniShip.transform.SetParent(genPos);

                break;
            }

            Destroy(col.gameObject);
        }
    }
}
