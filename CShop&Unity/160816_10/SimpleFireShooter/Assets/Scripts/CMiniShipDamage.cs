﻿using UnityEngine;
using System.Collections;

public class CMiniShipDamage : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //미니 비행기가 적기나 적기의 총알에 충돌하면
        if (col.tag.Equals("Enemy") || col.tag.Equals("ESLaser"))
        {
            //파괴됨
            Destroy(this.gameObject);
        }
    }
}
