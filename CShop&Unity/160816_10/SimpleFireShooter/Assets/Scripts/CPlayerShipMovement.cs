﻿using UnityEngine;
using System.Collections;

public class CPlayerShipMovement : MonoBehaviour {

    private const float H_LIMIT = 2.25f;
    private const float V_LIMIT = 4.5f;
    private const float ROOT_2 = 1.414213562373095f;

    public float _speed = 5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        if ((h > 0f || h < 0) && (v > 0f || v < 0))
        {
            h = h / ROOT_2;
            v = v / ROOT_2;
        }
        Vector2 dir = new Vector2(h, v);
        this.transform.Translate(dir * this._speed * Time.deltaTime);

        Vector2 newPos = this.transform.position;

        if (this.transform.position.x < -H_LIMIT)
            newPos.x = -H_LIMIT;
        else if (this.transform.position.x > H_LIMIT)
            newPos.x = H_LIMIT;

        if (this.transform.position.y < -V_LIMIT)
            newPos.y = -V_LIMIT;
        else if (this.transform.position.y > V_LIMIT)
            newPos.y = V_LIMIT;

        this.transform.position = newPos;
    }
}
