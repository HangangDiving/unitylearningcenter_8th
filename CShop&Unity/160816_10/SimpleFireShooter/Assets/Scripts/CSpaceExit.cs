﻿using UnityEngine;
using System.Collections;

public class CSpaceExit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerExit2D(Collider2D col)
    {
        //레이저가 우주 영역을 벗어나면
        if (col.tag.Equals("Space"))
        {
            Destroy(this.gameObject); //파괴됨
        }
    }
}
