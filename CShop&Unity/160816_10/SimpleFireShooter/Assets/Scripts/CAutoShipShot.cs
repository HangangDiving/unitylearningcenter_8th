﻿using UnityEngine;
using System.Collections;

//적기 발포 클래스
public class CAutoShipShot : MonoBehaviour {

    public Transform[] _shotPositions; //발포위치
    public float _shotDelayTime = 3f; //전체 발포 지연 시간
    public GameObject _laserPrefabs; //레이저 프리팹
    public float _fireDelayTime = 0.125f; //단일 발포 지연 시간

    // Use this for initialization
    void Start () {
        StartCoroutine("ShotCoroutine");
	}
	
    private IEnumerator ShotCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(this._shotDelayTime);

            //for(int i = 0; i < 배열요소.Length; i++) { ...배열[i]... }
            //foreach(순차요소 in 배열요소) { ... }
            foreach (Transform shotPosition in this._shotPositions)
            {
                if(_shotPositions.Length > 1)
                    yield return new WaitForSeconds(this._fireDelayTime);

                Instantiate(this._laserPrefabs, shotPosition.position, shotPosition.rotation);
            }            
        }
    }
}
