﻿using UnityEngine;
using System.Collections;

public class CPlayerShipShot : MonoBehaviour {

    public GameObject _laserPrefab; //레이저 프리팹 참조
    public Transform[] _shotPositions; //발포 위치 참조
    public int _shotCount = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        this.Shot();

	}

    //레이저 발포
    private void Shot()
    {
        //발포 키를 눌렀는지를 체크
        //발포 키를 누른게 맞다면
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //레이저 생성(발포)
            //Instantiate(프리팹, 생성위치, 생성회전축)
            //Instantiate(this._laserPrefab, this.transform.position, Quaternion.identity);
            //Instantiate(this._laserPrefab, _shotPos.position, Quaternion.identity);

            for (int i = 0; i < this._shotCount; i++)
            {
                Instantiate(this._laserPrefab, this._shotPositions[i].position, Quaternion.identity);
            }
        }
    }

    public void ShotCountUp()
    {
        this._shotCount = 3;
    }
}
