﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

using GameData; //MiniJson

public class CNetManager : MonoBehaviour {

    public InputField _idInputField;
    public InputField _ctypeInputField;
    public InputField _speedInputField;
    public InputField _findInputField;

    public string _baseUrl;

    public Text _resultMessageText;

    // Use this for initialization
    void Start () {
	
	}

    void InputFieldClear()
    {
        _idInputField.text = "";
        _ctypeInputField.text = "";
        _speedInputField.text = "";
        _findInputField.text = string.Empty;
    }

    public void OnFindEditEnter()
    {
        InputFieldClear();
        StopCoroutine("FindRequestCoroutine");
        StartCoroutine("FindRequestCoroutine");
    }

    private IEnumerator FindRequestCoroutine()
    {
        //데이터 추가 url 설정
        string url = _baseUrl + "/select_test.php";

        //form-data 객체 생성 및 설정 (POST)
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("id", _findInputField.text);
        wwwForm.AddField("pw", "1234");

        //POST Request 요청 객체를 생성함
        WWW www = new WWW(url, wwwForm);

        //요청 및 지연
        yield return www;

        //통신 오류 체크 및 데이타 수신(Response)
        if (www.error == null) //송수신이 성공적으로 이루어 졌다면
        {
            //_resultMessageText.text = www.text;

            //json 문자열 => 유니티의 딕셔너리 객체로 디코딩함
            string response_json_data = www.text.Trim();
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(response_json_data) as Dictionary<string, object>;

            //유저 정보 검색에 대한 결과 코드를 출력함
            string code = responseData["code"].ToString().Trim();

            //검색 결과 코드 메시지 출력
            _resultMessageText.text = code;

            //코드의 결과가 성공이라면
            if (code.Equals("SELECT_SUCCESS"))
            {
                //유저의 데이터 객체를 추출함
                Dictionary<string, object> userData =
                    responseData["data"] as Dictionary<string, object>;

                //유저 데이터 객체의 요소를 추출함
                _idInputField.text = userData["id"].ToString();
                _ctypeInputField.text = userData["ctype"].ToString();
                _speedInputField.text = userData["speed"].ToString();
            }
        }
        else
        {
            _resultMessageText.text = "Http Connect Error";
        }
    }

    public void OnDeleteButtonClick()
    {
        InputFieldClear();
        StopCoroutine("DeleteRequestCoroutine");
        StartCoroutine("DeleteRequestCoroutine");
    }

    private IEnumerator DeleteRequestCoroutine()
    {
        string url = _baseUrl + "delete_test.php";

        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("id", _idInputField.text);

        WWW www = new WWW(url, wwwForm);

        yield return www;

        if (www.error == null)
        {
            string response_json_data = www.text.Trim();
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(response_json_data) as Dictionary<string, object>;

            _resultMessageText.text = responseData["code"].ToString();
        }
        else
        {
            _resultMessageText.text = "Http Connect Error";
        }
    }

    public void OnUpdateButtonClick()
    {
        StopCoroutine("UpdateRequestCoroutine");
        StartCoroutine("UpdateRequestCoroutine");
    }

    private IEnumerator UpdateRequestCoroutine()
    {
        //데이터 수정 url 설정
        string url = _baseUrl + "/update_test.php";

        //form-data 객체 생성 및 설정 (POST)
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("id", _idInputField.text);
        wwwForm.AddField("pw", "1234");
        wwwForm.AddField("ctype", _ctypeInputField.text);
        wwwForm.AddField("speed", _speedInputField.text);

        //POST Request 요청 객체를 생성함
        WWW www = new WWW(url, wwwForm);

        //요청 및 지연
        yield return www;

        //통신 오류 체크 및 데이타 수신(Response)
        if (www.error == null) //송수신이 성공적으로 이루어 졌다면
        {
            //_resultMessageText.text = www.text;

            //json 문자열 => 유니티의 딕셔너리 객체로 디코딩함
            string response_json_data = www.text.Trim();
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(response_json_data) as Dictionary<string, object>;

            //유저 정보 추가에 대한 결과 코드를 출력함
            _resultMessageText.text = responseData["code"].ToString();
        }
        else
        {
            _resultMessageText.text = "Http Connect Error";
        }

    }

    public void OnAddButtonClick()
    {
        StopCoroutine("AddRequestCoroutine");
        StartCoroutine("AddRequestCoroutine");
    }

    private IEnumerator AddRequestCoroutine()
    {
        //데이터 추가 url 설정
        string url = _baseUrl + "/insert_test.php";

        //form-data 객체 생성 및 설정 (POST)
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("id", _idInputField.text);
        wwwForm.AddField("pw", "1234");
        wwwForm.AddField("ctype", _ctypeInputField.text);
        wwwForm.AddField("speed", _speedInputField.text);

        //POST Request 요청 객체를 생성함
        WWW www = new WWW(url, wwwForm);

        //요청 및 지연
        yield return www;

        //통신 오류 체크 및 데이타 수신(Response)
        if (www.error == null) //송수신이 성공적으로 이루어 졌다면
        {
            //_resultMessageText.text = www.text;

            //json 문자열 => 유니티의 딕셔너리 객체로 디코딩함
            string response_json_data = www.text.Trim();
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(response_json_data) as Dictionary<string, object>;

            //유저 정보 추가에 대한 결과 코드를 출력함
            _resultMessageText.text = responseData["code"].ToString();

            InputFieldClear();
        }
        else
        {
            _resultMessageText.text = "Http Connect Error";
        }
    }
}
