﻿using UnityEngine;
using System.Collections;

public class CDummy : MonoBehaviour {

    //오브젝트가 생성될 때 호출되는 이벤트 함수
    void Awake()
    {
        Debug.Log(name + " => Awake Call");
    }

    //오브젝트가 첫번째 랜더링 될 때 호출되는 이벤트 함수
    void Start()
    {
        Debug.Log(name + " => Start Call");
    }

    // Update is called once per frame
    void Update () {
	
	}
}
