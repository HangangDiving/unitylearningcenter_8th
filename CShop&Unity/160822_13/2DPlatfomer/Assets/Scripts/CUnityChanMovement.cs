﻿using UnityEngine;
using System.Collections;

public class CUnityChanMovement : MonoBehaviour {

    [HideInInspector] //public 접근 지정자를 가진 속성을 Inspector쪽에 표시하지 않음
    public Rigidbody2D _rigidbody2d;
    private SpriteRenderer _spriteRenderer; //스프라이트 랜더러 참조
    private Animator _animator; //애니메이터 참조
    public float _speed; //이동속도
    public bool _isJump = false; //1단 점프 유무
    public float _jumpPower = 1300f;
    public bool _isGround = false; //현재 지면 위에 있는지 여부

    //오브젝트가 생성될 때 호출되는 이벤트 함수
    void Awake()
    {
        //Debug.Log(name + " => Awake Call");
        //오브젝트가 소유한 컴포넌트들을 참조함
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
        this._spriteRenderer = this.GetComponent<SpriteRenderer>();
        this._animator = this.GetComponent<Animator>();
    }

	//오브젝트가 첫번째 랜더링 될 때 호출되는 이벤트 함수
	void Start () {
        //Debug.Log(name + " => Start Call");
    }
	
	// Update is called once per frame
	void Update () {

        this.Move();
        this.Jump();

    }

    private void Jump()
    {
        //만약 점프키를 눌렀다면
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!this._isJump)
            {
                JumpUp(); //점프 수행
                this._isJump = true; //점프 했음 설정
            }
        }
    }

    private void JumpUp()
    {
        //점프 애니메이션을 실행함
        this._animator.SetTrigger("Jump");
        //수직 상승 속도 초기화
        this._rigidbody2d.velocity = new Vector2(this._rigidbody2d.velocity.x, 0f);
        //점프
        this._rigidbody2d.AddForce(Vector2.up * this._jumpPower);
    }

    private void Move()
    {
        //이동 키 방향 추출
        float moveDir = Input.GetAxis("Horizontal");

        if (Mathf.Abs(moveDir) > 0)
        {
            //값 = (조건식) ? 참결과 : 거짓결과
            _spriteRenderer.flipX = (Mathf.Sign(moveDir) == 1) ? false : true;
        }

        //이동
        this._rigidbody2d.velocity =
            new Vector2(moveDir * this._speed, this._rigidbody2d.velocity.y);

        //이동 방향 값을 가지고 애니메이션을 전환함
        this._animator.SetFloat("Horizontal", moveDir);
        //수직 이동값을 가지고 애니메이션을 전환함
        this._animator.SetFloat("Vertical", this._rigidbody2d.velocity.y);
    }

    //현재 지면위에 있는지 여부에 따라 애니메이션을 설정함
    private void GroundSetting(bool isGround)
    {
        this._isGround = isGround;
        this._animator.SetBool("IsGround", isGround);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        //지면에 닿았다.
        if (col.gameObject.tag.Equals("Ground"))
        {
            this._isJump = false; //점프가 끝나고
            this.GroundSetting(true); //지면에 닿았다.
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        //지면에 닿고 있다.
        if (col.gameObject.tag.Equals("Ground"))
        {
            this._isJump = false; //점프가 끝나고
            this.GroundSetting(true); //지면에 닿았다.
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        //지면에서 떨어졌다.
        if (col.gameObject.tag.Equals("Ground"))
        {
            this.GroundSetting(false); //지면에서 떨어짐
        }
    }
}
