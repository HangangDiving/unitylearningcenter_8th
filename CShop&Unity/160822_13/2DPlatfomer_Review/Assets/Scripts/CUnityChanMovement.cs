﻿using UnityEngine;
using System.Collections;

public class CUnityChanMovement : MonoBehaviour {

    private Rigidbody2D _rigidbody2d;
    private SpriteRenderer _spriteRenderer;
    private Animator _animator;
    public float _speed = 5f;
    public float _jumpPower = 1300f;
    private bool _isJump = false;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
        this._spriteRenderer = this.GetComponent<SpriteRenderer>();
        this._animator = this.GetComponent<Animator>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        this.Move();
        this.Jump();

	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Ground"))
        {
            this._isJump = false;

            this.SetIsGround(true);
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Ground"))
        {
            this.SetIsGround(false);
        }
    }

    private void Move()
    {
        float moveDir = Input.GetAxis("Horizontal");
        if (Mathf.Abs(moveDir) > 0)
        {
            this._spriteRenderer.flipX =
                (Mathf.Sign(moveDir) == 1) ? false : true;
        }
        this._animator.SetFloat("Horizontal", moveDir);

        Vector2 newVel = this._rigidbody2d.velocity;
        newVel.x = moveDir * this._speed;
        this._rigidbody2d.velocity = newVel;
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (!this._isJump)
            {
                this._isJump = true;

                this.JumpUp();
            }
        }
    }

    private void JumpUp()
    {
        this._animator.SetTrigger("Jump");
        Vector2 newVel = this._rigidbody2d.velocity;
        newVel.y = 0;
        this._rigidbody2d.velocity = newVel;
        this._rigidbody2d.AddForce(Vector2.up * this._jumpPower);
    }

    private void SetIsGround(bool isGround)
    {
        this._animator.SetBool("IsGround", isGround);
    }
}
