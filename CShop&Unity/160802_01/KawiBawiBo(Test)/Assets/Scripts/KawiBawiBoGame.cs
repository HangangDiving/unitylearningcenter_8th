﻿using UnityEngine;
using System.Collections;

public class KawiBawiBoGame : MonoBehaviour {

    const int KAWI = 0;
    const int BAWI = 1;
    const int BO = 2;

    //가위, 바위, 보 버튼 중에 하나를 누름
    public void OnKawiBawiBoButtonClick(int kbb)
    {
        //Debug.Log("유저가 낸 가위바위보 번호 => " + kbb.ToString());
        int comkbb = Random.Range(0, 3);
        //Debug.Log("컴퓨터가 낸 가위바위보 번호 => " + comkbb.ToString());
                
        string result = "";
        if (kbb == comkbb)
            //Debug.Log("유저가 무승부했습니다.");
            result = "무승부";
        else
        {
            //if, else if 이용
            //if (kbb == 0 && comkbb == 1)
            //    Debug.Log("유저가 패배했습니다.");
            //else if (kbb == 0 && comkbb == 2)
            //    Debug.Log("유저가 승리했습니다.");
            //else if (kbb == 1 && comkbb == 0)
            //    Debug.Log("유저가 승리했습니다.");
            //else if (kbb == 1 && comkbb == 2)
            //    Debug.Log("유저가 패배했습니다.");
            //else if (kbb == 2 && comkbb == 0)
            //    Debug.Log("유저가 패배했습니다.");
            //else if (kbb == 2 && comkbb == 1)
            //    Debug.Log("유저가 승리했습니다.");

            //swtich문 이용
            /*switch (kbb)
            {
                case KAWI:
                    if (comkbb == BAWI)
                        //Debug.Log("유저가 [가위]를 냈고 컴퓨터가 [바위]를 냈으므로 유저가 [패배]했습니다.");
                        result = "패배";
                    else if (comkbb == BO)
                        //Debug.Log("유저가 [가위]를 냈고 컴퓨터가 [보]를 냈으므로 유저가 [승리]했습니다.");
                        result = "승리";
                    break;
                case BAWI:
                    if (comkbb == KAWI)
                        //Debug.Log("유저가 [바위]를 냈고 컴퓨터가 [가위]를 냈으므로 유저가 [승리]했습니다.");
                        result = "승리";
                    else if (comkbb == BO)
                        //Debug.Log("유저가 [바위]를 냈고 컴퓨터가 [보]를 냈으므로 유저가 [패배]했습니다.");
                        result = "패배";
                    break;
                case BO:
                    if (comkbb == KAWI)
                        //Debug.Log("유저가 [보]를 냈고 컴퓨터가 [가위]를 냈으므로 유저가 [패배]했습니다.");
                        result = "패배";
                    else if (comkbb == BAWI)
                        //Debug.Log("유저가 [보]를 냈고 컴퓨터가 [바위]를 냈으므로 유저가 [승리]했습니다.");
                        result = "승리";
                    break;
            }*/

            if ((kbb == KAWI && comkbb == BO) || (kbb == BAWI && comkbb == KAWI)
                || (kbb == BO && comkbb == BAWI))
                result = "승리";
            else if ((kbb == KAWI && comkbb == BAWI) || (kbb == BAWI && comkbb == BO)
                || (kbb == BO && comkbb == KAWI))
                result = "패배";
        }//else

        this.PrintResultMessage(kbb, comkbb, result);
    }

    private void PrintResultMessage(int kbb, int comkbb, string result)
    {
        Debug.Log("유저가 " + SelectKbbName(kbb) + "를 냈고 컴퓨터가 "
            + SelectKbbName(comkbb) + "를 냈으므로 유저가 " + result + "했습니다.");
    }

    private string SelectKbbName(int kbb)
    {
        string kbbname = "";
        switch (kbb)
        {
            case KAWI:
                kbbname = "가위";
                break;
            case BAWI:
                kbbname = "바위";
                break;
            case BO:
                kbbname = "보";
                break;
        }
        return kbbname;
    }
}
