﻿using UnityEngine;
using System.Collections;

public class CShopStudy : MonoBehaviour {

    /*public bool isSpeedBuff = false; //스피드 버프 변수 선언
    public bool isHpBuff = false; //체력 버프 선언
    public bool isDefBuff = false; //방어력 버프 선언
    public int speed = 100; //속도 변수 선언
    public int hp = 100; //체력 변수 선언
    public int def = 30; //방어력 변수 선언*/

    public int swordLevel = 80; //검 레벨
    public int shieldLevel = 50; //방패 레벨
    public int armorLevel = 70; //갑옷 레벨

    //판단 버튼 클릭 이벤트 함수
    public void OnResultFunction()
    {
        /*//Debug.Log("버튼 클릭!!");
        //버프에 대한 처리를 수행하는 조건식을 만드시오.
        if (this.isSpeedBuff)
        {
            Debug.Log("버프가 걸리면 속도가 20만큼 증가합니다.");
            this.speed += 20;
        }
        if (this.isHpBuff)
        {
            Debug.Log("버프가 걸리면 체력이 200으로 증가합니다.");
            this.hp = 200;
        }
        if (this.isDefBuff)
        {
            Debug.Log("버프가 걸리면 방어력이 40만큼 증가합니다.");
            this.def += 40;
        }
        Debug.Log("속도 : " + this.speed.ToString()
            + ", 체력 : " + this.hp.ToString()
            + ", 방어력 : " + this.def.ToString() );*/

        // 검레벨이 60이상 "이고" 방패레벨이 60이상 "이고" 갑옷레벨이 60이상 "이면"
        if ((swordLevel > 60) && (shieldLevel > 60)
                    && (armorLevel > 60))
            Debug.Log("당신은 파티 리더급 입니다.");
        else
            Debug.Log("당신은 파티 리더급이 아닙니다.");
        //&& 연결된 조건식은 포함 if으로 변경할 수 있다.
        /*if (swordLevel > 60)
        {
            if (shieldLevel > 60)
            {
                if(armorLevel > 60)
                    Debug.Log("당신은 파티 리더급 입니다.");
                else
                    Debug.Log("당신은 파티 리더급이 아닙니다.");
            }
            else
                Debug.Log("당신은 파티 리더급이 아닙니다.");
        }
        else
            Debug.Log("당신은 파티 리더급이 아닙니다.");*/

        // 검레벨 "이나" 방패레벨 "이나" 갑옷레벨 "중에" 하나이상이 60이상이면
        if ((swordLevel > 60) || (shieldLevel > 60)
                    || (armorLevel > 60))
            Debug.Log("당신은 파티에 참여할 수 있습니다. ");
        else
            Debug.Log("당신은 파티에 참여할 수 없습니다. ");
    }
}
