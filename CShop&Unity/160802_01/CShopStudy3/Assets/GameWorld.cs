﻿using UnityEngine;
using System.Collections;

// 몬스터 클래스 선언
public class Monster
{
    // 특징
    public int _hp; // 체력을 가짐
    public int _damage; // 공격력을 가짐
    public string _name; // 이름을 가짐
    public float _speed; // 속도를 가짐
    private bool _isDie = false; // 사망 여부

    // [생성자]
    // 용도 : 객체가 생성될때 자동으로 호출되어 객체 초기화를 수행해 줌
    // 1. 리턴값이 없음
    // 2. 메소드드명이 방드시 클래스명이랑 같다
    // 3. 객체가 생성될때 자동으로 호출됨
    // 4. 생성자는 생성할때 딱 한번 호출됨 (인위적으로 호출할 수 없음)

    // [오버로드]
    // - 함수명은 같지만 매개변수가 다른 복수개의 메소드들은 호출할때
    // 정해준 매개변수의 특징에 맞는 메소드를 찾아 자동으로 호출되어짐

    // 매개변수가 없는 생성자 (기본 생성자)
    public Monster()
    {
        Create("이름없음", 100, 1, 10);
    }

    // 매개변수가 있는 생성자
    public Monster(string name, int hp, float speed, int damage)
    {
        Create(name, hp, speed, damage);
    }

    // 생성 메소드
    public void Create(string name, int hp, float speed, int damage)
    {
        _name = name;
        _hp = hp;
        _damage = damage;
        _speed = speed;
        Debug.Log(_name + " 몬스터를 생성함");
    }

    // 공격 메소드
    public void Attack()
    {
        if (_isDie) return;

        Debug.Log(_name + " 몬스터가 " + _damage + "만큼의 공격을 시도함");
    }

    // 이동 메소드
    public void Move()
    {
        if (_isDie) return;

        Debug.Log(_name + " 몬스터가 " + _speed + "만큼의 속도로 이동함");
    }

    // 몬스터 타격 메소드
    public void Hit(int damage)
    {
        _hp -= damage;

        if (_hp <= 0)
        {
            // 사망 메소드 호출
            Die();
        }
    }

    // 사망
    private void Die()
    {
        _isDie = true;
        Debug.Log(_name + " 몬스터가 사망함");
    }
}

public class GameWorld : MonoBehaviour {

	// Use this for initialization
	void Start () {

        // 객체 생성
        // 클래스명 객체의인스턴스명 = new 클래스명();

        // 1 객체 초기화 방법
        // 여러마리의 몬스터를 생성할 경우 초기화 코드가 많아짐
        Monster monster = new Monster();
        monster._name = "오우거";
        monster._hp = 100;
        monster._speed = 3;
        monster._damage = 20;

        // 2 객체 초기화 방법
        // 여러마리의 몬스터를 생성하면 함수 호출 코드만 있으면 됨
        Monster monster2 = new Monster();
        monster2.Create("고블린", 100, 4, 10);

        // 3 객체 초기화 방법
        // 여러 마리의 몬스터를 생성하더라도
        // 별도의 함수나 코드를 사용할 필요가 없음
        Monster monster3 = new Monster("멀록", 100, 10, 10);

    }
}
