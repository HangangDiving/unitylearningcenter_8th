﻿using UnityEngine;
using System.Collections;

//캐릭터 컨트롤러를 이용한 고급이동
public class CCCAdvanceMovement : CCCSimpleMovement {

    public float _jumpSpeed; //점프 속도
    public float _gravity; //중력 크기

    //이동
    public override void Move()
    {
        //바닥에 있는지를 체크함
        if (_cc.isGrounded)
        {
            //회전 입력을 받음
            float h = Input.GetAxis("Horizontal");
            //방향에 따른 애니메이션을 재생함
            _animator.SetFloat("Direction", h);
            //캐릭터를 회전함
            transform.Rotate(0f, h * _rotateSpeed, 0f);

            //회전에 따른 캐릭터의 월드축 이동 방향을 구함
            _moveDirection = transform.TransformDirection(Vector3.forward);

            //이동 입력을 받음
            float v = Input.GetAxis("Vertical");
            //이동 속도에 따른 애니메이션을 재생함
            _animator.SetFloat("Speed", v);

            //전 후방에 따른 속도 처리를 수행함
            float speed = 0;

            if (v > 0.1f)
            {
                speed = _forwardSpeed;
            }
            else if (v < 0.1f)
            {
                speed = _backwardSpeed;
            }

            //현재 이동 속도를 구함
            _currSpeed = speed * v;
            _moveDirection *= _currSpeed;

            //현재 점프 애니메이션을 수행하고 있지 않고 점프키를 눌렀다면
            if (Input.GetButton("Jump") && !_animator.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                //점프크기를 설정함
                _moveDirection.y = _jumpSpeed;
                //점프 애니메이션을 재생함
                _animator.SetBool("Jump", true);
            }
            //현재 착지를 했지만 점프 애니메이션을 수행 중이라면
            else if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                _currSpeed = 0; //이동 금지
                _moveDirection *= _currSpeed;
                _animator.SetBool("Jump", false); //점프 끝
            }
        }

        //중력 처리를 수행함(2DRigidbody처럼 개별적인 중력 적용을 표현)
        _moveDirection.y -= _gravity * Time.deltaTime;

        //캐릭터 이동을 수행함
        _cc.Move(_moveDirection * Time.deltaTime);
    }
}
