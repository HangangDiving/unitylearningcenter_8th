﻿using UnityEngine;
using System.Collections;

public class CBasicMovement : MonoBehaviour {

    public float _forwardSpeed; //전진 속도
    public float _backwardSpeed; //후진 속도

    public float _rotateSpeed; //회전 속도

    Animator _animator;

    public float _animSpeed; //애니메이션 재생 속도

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _animator.speed = _animSpeed;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        _animator.SetFloat("Speed", v);
        _animator.SetFloat("Direction", h);

        //이동 방향을 월드축을 기준으로 설정함
        Vector3 velocity = transform.TransformDirection(0f, 0f, v);

        if (v > 0.1f)
        {
            velocity *= _forwardSpeed;
        }
        else if (v < -0.1f)
        {
            velocity *= _backwardSpeed;
        }

        //이동
        transform.position += velocity * Time.deltaTime;

        //회전
        transform.Rotate(new Vector3(0f, h * _rotateSpeed, 0f) * Time.deltaTime);
    }
}
