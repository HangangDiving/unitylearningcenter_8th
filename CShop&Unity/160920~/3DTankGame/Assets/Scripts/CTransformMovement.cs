﻿using UnityEngine;
using System.Collections;

public class CTransformMovement : MonoBehaviour {

    public float _moveSpeed; //이동 속도

    public float _rotateSpeed; //회전 속도

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        //앞뒤 이동
        transform.Translate(Vector3.forward * v * _moveSpeed * Time.deltaTime);

        //좌우 회전
        transform.Rotate(Vector3.up * h * _rotateSpeed * Time.deltaTime);
        
    }
}
