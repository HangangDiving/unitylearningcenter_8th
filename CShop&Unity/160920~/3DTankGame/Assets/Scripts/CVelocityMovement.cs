﻿using UnityEngine;
using System.Collections;

public class CVelocityMovement : MonoBehaviour {

    private Rigidbody _rigidbody;

    public float _moveSpeed; //이동 속도

    public float _rotateSpeed; //회전 속도

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        //월드 축에 맞춰서 방향을 맞춰 줌
        Vector3 worldZDir = transform.TransformDirection(0f, 0f, v);

        //이동 속도 설정
        _rigidbody.velocity = worldZDir.normalized * _moveSpeed;

        //회전
        _rigidbody.angularVelocity = 
            new Vector3(0f, h * _rotateSpeed / 360f * (Mathf.PI * 2f), 0f);
    }
}
