﻿using UnityEngine;
using System.Collections;

public class CTankRotationAround : MonoBehaviour {

    public Transform _target; //주변을 회전할 기준 대상

    public float _rotSpeed; //회전 속도

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //타겟의 주변을 회전함
        transform.RotateAround(_target.position, Vector3.up, _rotSpeed * Time.deltaTime);

	}
}
