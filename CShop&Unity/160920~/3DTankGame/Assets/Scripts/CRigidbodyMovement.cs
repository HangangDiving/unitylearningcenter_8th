﻿using UnityEngine;
using System.Collections;

public class CRigidbodyMovement : MonoBehaviour {

    private Rigidbody _rigidbody;

    public float _moveSpeed; //이동 속도

    public float _rotateSpeed; //회전 속도

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        //월드 축에 맞춰서 방향을 맞춰 줌
        Vector3 worldZDir = transform.TransformDirection(0f, 0f, v);

        //이동 처리 (충돌과 물리적 작용에 대한 보간 처리)
        _rigidbody.MovePosition(_rigidbody.position
            + (worldZDir.normalized * _moveSpeed * Time.fixedDeltaTime));
        

        //회전에 대한 오일러 각도를 설정함
        Vector3 eulerRotValue = new Vector3(0f, h * _rotateSpeed, 0f);

        //오일러 각도를 쿼터니언 단위로 변환함 (오일러 -> 쿼터니언)
        Quaternion deltaRotValue = Quaternion.Euler(eulerRotValue * Time.fixedDeltaTime);
        //쿼터니언 -> 오일러
        //Vector3 eulerRotValue = deltaRotValue.eulerAngles;

        //회전 처리(충돌과 물리적 작용에 대한 보간 처리)
        _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotValue);
    }
}
