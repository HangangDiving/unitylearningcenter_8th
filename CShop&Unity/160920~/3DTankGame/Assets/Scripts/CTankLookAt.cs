﻿using UnityEngine;
using System.Collections;

public class CTankLookAt : MonoBehaviour {

    Transform _playerTank;

	// Use this for initialization
	void Start () {

        _playerTank = GameObject.Find("PlayerTank").transform;

	}
	
	// Update is called once per frame
	void Update () {

        //Transform.LookAt(타겟) : 지정한 타겟을 바라봄
        transform.LookAt(_playerTank);

	}
}
