﻿using UnityEngine;
using System.Collections;

public class CTankLookRotation : MonoBehaviour {

    Transform _playerTank;

    // Use this for initialization
    void Start()
    {

        _playerTank = GameObject.Find("PlayerTank").transform;

    }
        
	// Update is called once per frame
	void Update () {

        //플레이어 탱크를 향한 방향 벡터를 구함
        Vector3 targetDir = _playerTank.position - transform.position;

        //y축 방향은 0으로 설정함
        targetDir.y = 0f;

        //방향 벡터를 넣어서 회전 쿼터니언을 구함
        Quaternion rot = Quaternion.LookRotation(targetDir.normalized);

        //회전을 설정함
        //transform.rotation = rot;

        //부드럽게 보간 회전함
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, 2.5f * Time.deltaTime);

        //방향벡터와 방향벡터간의 쿼터니언을 구할 때 (오일러로 구할 경우 회전 벡터를 구할 수 있음)
        //Quaternion qt = Quaternion.FromToRotation(방향벡터, 방향벡터);

	}
}
