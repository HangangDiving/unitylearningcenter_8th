﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class CLoadScene2Manager : MonoBehaviour, IEventTrigger {

    public Rigidbody2D _eventHumanRigidbody2d;
    public CObjectMove _eventShipMove;
    public GameObject _GameStartUi;

    public void ReciveColliderName(string name, string senderName = "")
    {
        if (name.Equals("EventTrigger1") && senderName.Equals("EventHuman"))
        {
            this._eventHumanRigidbody2d.gameObject.SetActive(false);
            _eventShipMove._direction = Vector2.up;
            _eventShipMove.transform.Rotate(0f, 0f, 25f);
        }
        else if (name.Equals("EventTrigger2") && senderName.Equals("EventShip"))
        {
            SceneManager.LoadScene("SpaceShooterInGame");
        }
    }

    public void OnGameStartButtonClick()
    {
        StartCoroutine("GameStartCoroutine");
        this._GameStartUi.SetActive(false);
    }

    private IEnumerator GameStartCoroutine()
    {
        while (_eventHumanRigidbody2d.gameObject.activeSelf)
        {
            yield return null;
            _eventHumanRigidbody2d.velocity =
                new Vector2(1f, _eventHumanRigidbody2d.velocity.y);
        }
    }
}
