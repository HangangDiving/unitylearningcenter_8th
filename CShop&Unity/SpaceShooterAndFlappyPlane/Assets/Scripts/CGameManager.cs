﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CGameManager : CSingletone<CGameManager> {

    private static int starCount = 0;
    public static int StarCount {
        set { starCount = value; }
        get { return starCount; }
    }

    public Text _starCountText;
    public Image _hpProgressBarImage;
    public Image _spProgressBarImage;
    public GameObject _spImage;
    private float _hpPercentage;
    private float _spPercentage;
    private CPlayerShipControll _playerShipControll;
    private CPlayerPlaneControll _playerPlaneControll;
    private int _timer = 0;
    public int _waveUpTime = 5;
    public int _enemyLevelUpWaveCount = 5;
    public CEnemyGenerator[] _enemyGeneretes;
    public Animator _waveUpAnimator;
    public Text _waveUpCountText;
    private int _hp = 0; //체력
    private int _sp = 0; //기력
    public int _maxHp; //최대 체력
    public int _maxSp; //최대 기력
    public float _spUpDelayTime = 1f;
    private float _gameSpeed = 1f;
    public Animator _backgroundAnimator;

    public int Sp { get { return this._sp; } }
    public float GameSpeed {
        get { return this._gameSpeed; }
        set {
            this._gameSpeed = value;
            if (this._backgroundAnimator != null)
            {
                if (this._gameSpeed <= 1)
                    this._backgroundAnimator.speed = this._gameSpeed;
                else
                    this._backgroundAnimator.speed = this._gameSpeed * 0.5f;
            }
        }
    }

    void Awake()
    {
        this.gameObject.name = "_GameManager";
        this._hp = this._maxHp;
    }
    
	// Use this for initialization
	void Start () {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
            return;

        this._playerShipControll = player.GetComponent<CPlayerShipControll>();
        this._playerPlaneControll = player.GetComponent<CPlayerPlaneControll>();

        this._hpPercentage = 1f / this._maxHp;
        this._spPercentage = 1f / this._maxSp;
        this.SetHpProgressBar();
        this.SetSpProgressBar();
                
        this._starCountText.text = starCount.ToString();

        InvokeRepeating("Timer", _waveUpTime, 1f);
        this._waveUpAnimator.Play("WaveUpText");
        InvokeRepeating("SpUpInvoke", this._spUpDelayTime, this._spUpDelayTime);
    }

    //점수 추가
    public void AddStarScore(int score)
    {
        starCount += score; //점수 추가하기
        this._starCountText.text = starCount.ToString(); //UI도 적용
    }

    //체력 회복
    public void AddHp(int hp)
    {
        this._hp += hp; //체력회복

        //최대체력 설정
        if (this._hp > this._maxHp)
            this._hp = this._maxHp;

        //게임매니저 전달
        this.SetHpProgressBar();
    }

    //기력 회복
    public void AddSp(int sp)
    {
        this._sp += sp; //기력회복

        //최대기력 설정
        if (this._sp > this._maxSp)
            this._sp = this._maxSp;

        //게임매니저 전달
        this.SetSpProgressBar();
    }

    public void PopHp(int damage)
    {
        this._hp -= damage; //데미지만큼 체력 감소
        this.SetHpProgressBar();

        if (this._hp <= 0)
        {
            if (this._playerShipControll != null)
            {
                PlayerPrefs.SetFloat("Ship_XPos", this._playerShipControll.transform.position.x);
                PlayerPrefs.SetFloat("Ship_YPos", this._playerShipControll.transform.position.y);
                PlayerPrefs.Save();
                SceneManager.LoadScene("LoadScene3");
            }
            else if (this._playerPlaneControll != null)
            {
                this._playerPlaneControll.ReadyDie();
            }
        }
    }

    public void PopSp()
    {
        this._sp = 0;
        this.SetHpProgressBar();
    }

    //체력 프로그래스바 설정
    public void SetHpProgressBar()
    {
        //체력 프로그래스바 조절
        this._hpProgressBarImage.fillAmount =
            this._hp * this._hpPercentage;
    }

    //기력 프로그래스바 설정
    public void SetSpProgressBar()
    {
        //기력 프로그래스바 조절
        this._spProgressBarImage.fillAmount =
            this._sp * this._spPercentage;

        //꽉 차고 이미지가 비활성화 상태라면 이미지를 활성화한다.
        if (this._spProgressBarImage.fillAmount >= 1f && !this._spImage.activeSelf)
            this._spImage.SetActive(true);
        //꽉 차지 않은 상태이고 이미지가 활성화 상태라면 이미지를 비활성화한다.
        else if(this._spImage.activeSelf)
            this._spImage.SetActive(false);
    }

    private void Timer()
    {
        this._timer++; //초 세기
        //정해진 웨이브 레벨 업 시간이라면
        if ( (this._timer % this._waveUpTime) == 0)
        {
            int cnt = int.Parse(this._waveUpCountText.text);
            this._waveUpCountText.text = (++cnt).ToString();
            this._waveUpAnimator.Play("WaveUpText");

            bool levelUp = false;
            if (this._enemyLevelUpWaveCount > 0
                && cnt % this._enemyLevelUpWaveCount == 0)
                levelUp = true;

            for (int i = 0; i < this._enemyGeneretes.Length; i++)
            {
                this._enemyGeneretes[i].GenerateShorten();
                if (levelUp)
                {
                    this._enemyGeneretes[i].AddEnemyHp();
                    this._enemyGeneretes[i].AddEnemyDamage();
                }
            }
        }
    }

    public void SpUpInvoke()
    {
        this.AddSp(1);
    }
}
