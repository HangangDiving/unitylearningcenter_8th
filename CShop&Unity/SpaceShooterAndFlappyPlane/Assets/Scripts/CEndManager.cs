﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CEndManager : CSingletone<CEndManager> {

    public Text _bestCountText;
    public Text _starCountText;

    void Awake()
    {
        int bestScore = PlayerPrefs.GetInt("BestScore", 0);
        if (CGameManager.StarCount > bestScore)
        {
            PlayerPrefs.SetInt("BestScore", CGameManager.StarCount);
            bestScore = CGameManager.StarCount;
        }
        this._bestCountText.text = bestScore.ToString();

        this._starCountText.text = CGameManager.StarCount.ToString();
        CGameManager.StarCount = 0;
    }

	// Use this for initialization
	void Start () {
	
	}

    public void OnReStartButtonClick()
    {
        SceneManager.LoadScene("LoadScene2");
    }
}
