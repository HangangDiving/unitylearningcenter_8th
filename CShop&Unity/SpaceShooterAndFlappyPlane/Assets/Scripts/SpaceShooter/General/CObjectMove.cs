﻿using UnityEngine;
using System.Collections;
using System;

public class CObjectMove : MonoBehaviour, IStopComponent {

    public Vector2 _direction;
    public float _moveSpeed;
    private Rigidbody2D _rigidbody2d;
    public bool _isRigidbodyMove = false;
    public Space _isSpace = Space.Self;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
        StartCoroutine("MoveCoroutine");
    }

    private IEnumerator MoveCoroutine()
    {
        while (true)
        {
            yield return null;

            float addSpeed = CGameManager.Instance.GameSpeed;

            if (this._isRigidbodyMove == false)
            {
                this.transform.Translate(this._direction * (this._moveSpeed * addSpeed * Time.deltaTime), this._isSpace);
            }
            else
            {
                _rigidbody2d.velocity = -this.transform.up * (this._moveSpeed * addSpeed);
            }
        }
    }

    public void SetDirectionToTarget(Transform targetTM)
    {
        this._direction =
            targetTM.position - this.transform.position;
        this._direction.Normalize();
    }

    public void StopAllMethod()
    {
        StopCoroutine("MoveCoroutine");
    }
}
