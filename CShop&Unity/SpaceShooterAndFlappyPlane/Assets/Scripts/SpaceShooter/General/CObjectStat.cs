﻿using UnityEngine;
using System.Collections;

public class CObjectStat : MonoBehaviour {

    public int _damage;
    public int _hp = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool Damaged(int damage)
    {
        this._hp -= damage;

        if (this._hp <= 0)
            return true;

        return false;
    }
}
