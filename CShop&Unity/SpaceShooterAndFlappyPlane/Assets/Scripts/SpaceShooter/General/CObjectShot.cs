﻿using UnityEngine;
using System.Collections;
using System;

public class CObjectShot : MonoBehaviour, IStopComponent {

    public Transform[] _shotPositions;
    public GameObject _laserPrefab;
    public float _shotDelayTime;

	// Use this for initialization
	void Start () {
        InvokeRepeating("Shot", this._shotDelayTime * 0.5f, this._shotDelayTime);
	}

    public void Shot()
    {
        for (int i = 0; i < this._shotPositions.Length; i++)
        {
            Instantiate(this._laserPrefab,
            this._shotPositions[i].position,
            this._shotPositions[i].rotation);
        }
    }

    public void StopAllMethod()
    {
        CancelInvoke("Shot");
    }
}
