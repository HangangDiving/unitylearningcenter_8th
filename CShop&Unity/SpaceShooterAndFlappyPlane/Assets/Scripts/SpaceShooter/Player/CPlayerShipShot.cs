﻿using UnityEngine;
using System.Collections;

public class CPlayerShipShot : MonoBehaviour {

    public Transform[] _shotPositions;
    public GameObject _laserPrefab;
    private int[] _shotCount;
    public int _shotCountLimit = 5;
    public float _shotAngle = 10f;
    public float _shotDelayTime = 0.5f;
    private float _shotDeltaTime = 0f;
    private CPlayerShipControll _playerShipControll;

    void Awake()
    {
        this._playerShipControll = this.GetComponent<CPlayerShipControll>();
    }

    // Use this for initialization
    void Start () {
        this._shotCount = new int[_shotPositions.Length];
        for (int i = 0; i < this._shotCount.Length; i++)
            this._shotCount[i] = 1;
	}
	
	public void InputShot() {

        this._shotDeltaTime += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && this._shotDeltaTime > this._shotDelayTime)
        {
            for (int i = 0; i < this._shotPositions.Length; i++)
            {
                float newAngle = this._shotAngle;

                Instantiate(this._laserPrefab,
                    this._shotPositions[i].position,
                    this._shotPositions[i].rotation);

                for (int j = 1; j < this._shotCount[i]; j += 2)
                {
                    Quaternion rot = Quaternion.AngleAxis(newAngle, Vector3.forward);
                    Instantiate(this._laserPrefab,
                        this._shotPositions[i].position,
                        this._shotPositions[i].rotation * rot);

                    rot = Quaternion.AngleAxis(-newAngle, Vector3.forward);
                    Instantiate(this._laserPrefab,
                        this._shotPositions[i].position,
                        this._shotPositions[i].rotation * rot);

                    newAngle += this._shotAngle;
                }
            }

            this._shotDeltaTime = 0f;
        }
        //else if (Input.GetKeyDown(KeyCode.Return))
          //  this.ShotCountUp();

	}

    public void LaserCountUp()
    {
        if (this._shotCount[this._shotCount.Length - 1] >= this._shotCountLimit)
            return;

        for (int i = 1; i < this._shotCount.Length; i++)
        {
            if (this._shotCount[0] > this._shotCount[i])
            {
                this._shotCount[i] += 2;
                return;
            }  
        }

        this._shotCount[0] += 2;
    }

    public void Skill()
    {
        this._shotDeltaTime += Time.deltaTime;

        if (this._shotDeltaTime < this._shotDelayTime * 0.25f)
            return;

        //for (int i = 0; i < this._shotPositions.Length; i++)
        //{
        //    Instantiate(this._laserPrefab,
        //            this._shotPositions[i].position,
        //            this._shotPositions[i].rotation);
        //}

        for (int i = 0; i < this._shotPositions.Length; i++)
        {
            float newAngle = this._shotAngle;

            Instantiate(this._laserPrefab,
                this._shotPositions[i].position,
                this._shotPositions[i].rotation);

            for (int j = 1; j < this._shotCount[i]; j += 2)
            {
                Quaternion rot = Quaternion.AngleAxis(newAngle, Vector3.forward);
                Instantiate(this._laserPrefab,
                    this._shotPositions[i].position,
                    this._shotPositions[i].rotation * rot);

                rot = Quaternion.AngleAxis(-newAngle, Vector3.forward);
                Instantiate(this._laserPrefab,
                    this._shotPositions[i].position,
                    this._shotPositions[i].rotation * rot);

                newAngle += this._shotAngle;
            }
        }

        this._shotDeltaTime = 0f;
    }
}
