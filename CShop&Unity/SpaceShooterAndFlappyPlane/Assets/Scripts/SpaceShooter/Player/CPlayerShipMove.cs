﻿using UnityEngine;
using System.Collections;

public class CPlayerShipMove : MonoBehaviour {

    public float _moveSpeed = 5f; //이동속도
    public float _rotateSpeed = 45f; //회전속도
    public Vector2 _moveLimit;
    private CPlayerShipControll _playerShipControll;

    void Awake()
    {
        this._playerShipControll = this.GetComponent<CPlayerShipControll>();
    }

    public void InputMove()
    {
        //입력키 받기
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        //회전
        this.transform.Rotate(Vector3.forward, -h * this._rotateSpeed * Time.deltaTime);
        //이동
        this.transform.Translate(Vector2.up * v * this._moveSpeed * Time.deltaTime, Space.Self);

        Vector2 currPos = this.transform.position;
        if (currPos.x < -this._moveLimit.x)
            currPos.x = -this._moveLimit.x;
        else if (currPos.x > this._moveLimit.x)
            currPos.x = this._moveLimit.x;

        if (currPos.y < -this._moveLimit.y)
            currPos.y = -this._moveLimit.y;
        else if (currPos.y > this._moveLimit.y)
            currPos.y = this._moveLimit.y;

        this.transform.position = currPos;
    }

    public void Skill()
    {
        this.transform.Rotate(Vector3.forward, -4f * this._rotateSpeed * Time.deltaTime);
    }

    public void MoveToTarget(Transform targetTM)
    {
        Vector2 dir =
            targetTM.position - this.transform.position;

        this.transform.Translate(dir.normalized * this._moveSpeed * 1.5f * Time.deltaTime, Space.World);
    }
}
