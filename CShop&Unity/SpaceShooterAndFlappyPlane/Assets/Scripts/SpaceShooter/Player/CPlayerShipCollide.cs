﻿using UnityEngine;
using System.Collections;

public class CPlayerShipCollide : MonoBehaviour {

    private CPlayerShipControll _playerShipControll;

    void Awake()
    {
        this._playerShipControll = this.GetComponent<CPlayerShipControll>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Enemy":
            case "EnemyLaser":
            case "Meteor":
                CObjectStat colStat = col.GetComponent<CObjectStat>(); //스탯 컴포넌트 가져오기
                this._playerShipControll.Damaged(colStat._damage); //공격 받았다고 알림
                Destroy(col.gameObject); //파괴
                break;
            case "Item":
                CItem item = col.GetComponent<CItem>(); //아이템 컴포넌트 가져오기
                item.ItemApply(); //아이템 적용
                Destroy(col.gameObject);
                break;
            case "SkillZone":
                this._playerShipControll.SetState(CPlayerShipControll.State.SKILL); //스킬 실행
                break;
        }
    }

}
