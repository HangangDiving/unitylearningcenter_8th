﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof(CPlayerShipMove) )]
[RequireComponent(typeof(CPlayerShipShot))]
[RequireComponent(typeof(CPlayerShipCollide))]
public class CPlayerShipControll : MonoBehaviour {

    //상태
    public enum State
    {
        NORMAL, READY_SKILL, SKILL
    }

    private CPlayerShipMove _moveComponent; //이동 컴포넌트
    private CPlayerShipShot _shotComponent; //발사 컴포넌트
    private State _state = State.NORMAL; //상태 기억
    public Transform _skillZoneTM; //필살기 위치
    public float _skillTime = 5f; //필살기 시간
    private Animator _animator;
    public GameObject _shield;

    void Awake()
    {
        this._moveComponent = this.GetComponent<CPlayerShipMove>();
        this._shotComponent = this.GetComponent<CPlayerShipShot>();
        this._animator = this.GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Return) && CGameManager.Instance.Sp >= 100)
            this.SetState(State.READY_SKILL);

        switch (this._state)
        {
            case State.NORMAL:
                this._moveComponent.InputMove();
                this._shotComponent.InputShot();
                break;
            case State.READY_SKILL:
                this._moveComponent.Skill();
                this._moveComponent.MoveToTarget(this._skillZoneTM);
                break;
            case State.SKILL:
                this._moveComponent.Skill();
                this._shotComponent.Skill();
                break;
        }
    }

    public void SetState(State state)
    {
        switch (state)
        {
                //필살기 요청
            case State.READY_SKILL:
                this._skillZoneTM.gameObject.SetActive(true); //스킬존 활성화
                CGameManager.Instance.PopSp();
                this._shield.SetActive(true);
                if (IsInvoking("ShieldOff"))
                    CancelInvoke("ShieldOff");
                CGameManager.Instance.SetSpProgressBar();
                break;
                //필살기 실행
            case State.SKILL:
                this._skillZoneTM.gameObject.SetActive(false); //스킬존 비활성화
                Invoke("ReleaseSkill", _skillTime); //임의의 시간 이후에 필살기 중지
                break;
        }

        this._state = state; //상태 변경
    }

    //스킬 해제 인보크
    public void ReleaseSkill()
    {
        this._state = State.NORMAL; //기본 상태로 변경
        this._shield.SetActive(false);
    }

    //공격 받으면 호출
    public void Damaged(int damage)
    {
        //평소 상태가 아니라면 패스
        if (this._state != State.NORMAL)
            return;

        CGameManager.Instance.PopHp(damage); //게임 매니저에 전달

        this._animator.Play("PlayerShipDamaged"); //애니메이션 실행
    }

    public void ShieldOn(float time)
    {
        this._shield.SetActive(true);

        if (IsInvoking("ShieldOff"))
            CancelInvoke("ShieldOff");

        Invoke("ShieldOff", time);
    }

    private void ShieldOff()
    {
        this._shield.SetActive(false);
    }
}
