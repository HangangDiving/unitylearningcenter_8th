﻿using UnityEngine;
using System.Collections;

public class CShield : MonoBehaviour {
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Enemy"))
        {
            CEnemyCollide enemyCollide = col.GetComponent<CEnemyCollide>();
            enemyCollide.Die();
        }
        else if (col.tag.Equals("EnemyLaser"))
            Destroy(col.gameObject);
    }
}
