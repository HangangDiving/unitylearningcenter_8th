﻿using UnityEngine;
using System.Collections;

public class CEnemyGenerator : MonoBehaviour {

    public Transform[] _generatePostions;
    public GameObject[] _enemyPrefabs;
    public int[] _generateRandomPercent;
    public float _minTime = 4f;
    public float _maxTime = 5f;
    public float _shortenPercent = 0.8f;
    public int _addHp = 5;
    public int _addDamage = 5;
    private int _addHpCount = 0;
    private int _addDamageCount = 0;

    // Use this for initialization
    void Start () {
        StartCoroutine("GenerateCoroutine");
    }

    private IEnumerator GenerateCoroutine()
    {
        while (true)
        {
            float genTime = Random.Range(this._minTime, this._maxTime);

            yield return new WaitForSeconds(genTime);

            int rand = Random.Range(0, 10);
            int enemyIndex = 0;
                        
            for (int i = 0; i < this._enemyPrefabs.Length; i++)
            {
                if (rand <= this._generateRandomPercent[i])
                {
                    enemyIndex = i;
                    break;
                }
            }

            rand = Random.Range(0, this._generatePostions.Length);

            GameObject obj = Instantiate(this._enemyPrefabs[enemyIndex],
                this._generatePostions[rand].position,
                this._generatePostions[rand].rotation) as GameObject;
            CObjectStat stat = obj.GetComponent<CObjectStat>();
            if (stat != null)
            {
                stat._hp += (this._addHp * this._addHpCount);
                stat._damage += (this._addDamage * this._addDamageCount);
            }
        }
    }

    public void GenerateShorten()
    {
        this._minTime *= this._shortenPercent;
        if (this._minTime <= 0.00001f)
            this._minTime = 0.1f;

        this._maxTime *= this._shortenPercent;
        if (this._maxTime <= 1.00001f)
            this._maxTime = 1f;
    }

    public void AddEnemyHp()
    {
        this._addHpCount++;
    }

    public void AddEnemyDamage()
    {
        this._addDamageCount++;
    }
}
