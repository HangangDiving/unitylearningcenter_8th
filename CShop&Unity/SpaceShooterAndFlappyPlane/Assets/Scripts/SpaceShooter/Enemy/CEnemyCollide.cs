﻿using UnityEngine;
using System.Collections;

[RequireComponent ( typeof(CObjectStat) ) ]
public class CEnemyCollide : MonoBehaviour {

    public GameObject[] _dropItems;
    private CObjectStat _stat;
    public GameObject _destroyEffect;

    void Awake()
    {
        this._stat = this.GetComponent<CObjectStat>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "PlayerLaser":
                CObjectStat colStat = col.GetComponent<CObjectStat>();
                if (this._stat.Damaged(colStat._damage))
                    this.Die();
                Destroy(col.gameObject);
                break;
        }
    }

    public void Die()
    {
        //파괴 이펙트 실행
        GameObject effectObj = Instantiate(this._destroyEffect,
            this.transform.position,
            Quaternion.identity) as GameObject;
        Destroy(effectObj, 0.3f); //제거 예약

        //아이템 드랍
        int randIndex = Random.Range(0, this._dropItems.Length);

        Instantiate(this._dropItems[randIndex],
            this.transform.position,
            this.transform.rotation);

        Destroy(this.gameObject);
    }
}
