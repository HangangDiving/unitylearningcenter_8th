﻿using UnityEngine;
using System.Collections;

public class CMeteorMove : CObjectMove {
    
    // Use this for initialization
	void Start () {
        GameObject playerShip = GameObject.FindGameObjectWithTag("Player");
        this.SetDirectionToTarget(playerShip.transform);
	}
}
