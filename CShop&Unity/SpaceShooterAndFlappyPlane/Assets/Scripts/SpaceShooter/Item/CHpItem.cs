﻿using UnityEngine;
using System.Collections;

public class CHpItem : CItem {

    public int _hpValue = 30;

    public override void ItemApply()
    {
        CGameManager.Instance.AddHp(this._hpValue);
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        //CPlayerShipControll playerShipControll = player.GetComponent<CPlayerShipControll>();
        //playerShipControll.AddHp(this._hpValue);
    }
}
