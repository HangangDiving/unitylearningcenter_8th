﻿using UnityEngine;
using System.Collections;

public class CSpItem : CItem {

    public int _spValue = 30;
    
    public override void ItemApply()
    {
        CGameManager.Instance.AddSp(this._spValue);
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        //CPlayerShipControll playerShipControll = player.GetComponent<CPlayerShipControll>();
        //playerShipControll.AddSp(this._spValue);
    }
}
