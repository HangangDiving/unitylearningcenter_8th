﻿using UnityEngine;
using System.Collections;
using System;

public class CShieldItem : CItem
{
    public float _shieldTime = 4f;

    public override void ItemApply()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        CPlayerShipControll playerShipControll = player.GetComponent<CPlayerShipControll>();
        playerShipControll.ShieldOn(this._shieldTime);
    }
}
