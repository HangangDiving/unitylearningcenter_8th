﻿using UnityEngine;
using System.Collections;

public abstract class CItem : MonoBehaviour {

    public abstract void ItemApply();

}
