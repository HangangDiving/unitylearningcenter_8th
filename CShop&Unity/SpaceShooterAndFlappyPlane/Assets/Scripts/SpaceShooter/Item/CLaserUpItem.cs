﻿using UnityEngine;
using System.Collections;
using System;

public class CLaserUpItem : CItem {
    
    public override void ItemApply()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        CPlayerShipShot playerShipShot = player.GetComponent<CPlayerShipShot>();
        playerShipShot.LaserCountUp();
    }
}
