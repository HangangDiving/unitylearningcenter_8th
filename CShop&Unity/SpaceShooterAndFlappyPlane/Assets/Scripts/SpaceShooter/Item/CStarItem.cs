﻿using UnityEngine;
using System.Collections;
using System;

public class CStarItem : CItem {

    public int _score = 1;
    public int _sp;

    public override void ItemApply()
    {
        CGameManager.Instance.AddStarScore(this._score);
        CGameManager.Instance.AddSp(this._sp);
        //GameObject player = GameObject.FindGameObjectWithTag("Player");
        //CPlayerShipControll playerShipControll = player.GetComponent<CPlayerShipControll>();
        //playerShipControll.AddSp(_sp);
    }

}
