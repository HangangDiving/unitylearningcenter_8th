﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class CLoadScene4Manager : MonoBehaviour, IEventTrigger {

    public GameObject _eventHuman;
    public Animator _eventPlaneAnimator;
        
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ReciveColliderName(string name, string senderName)
    {
        if (name.Equals("EventTrigger1"))
        {
            this._eventHuman.SetActive(true);
        }
        else if (name.Equals("EventTrigger2") && senderName.Equals("EventHuman"))
        {
            this._eventHuman.SetActive(false);
            this._eventPlaneAnimator.Play("EventPlane");
        }
        else if (name.Equals("EventTrigger3"))
        {
            SceneManager.LoadScene("FlappyPlaneInGame");
        }
    }
}
