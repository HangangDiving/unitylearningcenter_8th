﻿using UnityEngine;
using System.Collections;

public class CSingletone<T> : MonoBehaviour where T : MonoBehaviour{

    private static T instance = null;
    public static T Instance
    {
        get {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType(typeof(T)) as T;

                if (instance == null)
                {
                    GameObject newGameObj = new GameObject("_TempManager");
                    instance = newGameObj.AddComponent<T>();
                }
            }

            return instance;
        }
    }
}
