﻿using UnityEngine;
using System.Collections;

public class CSendEventTrigger : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D col)
    {
        IEventTrigger eventTrigger = col.GetComponentInParent<IEventTrigger>();
        if (eventTrigger != null)
            eventTrigger.ReciveColliderName(col.name, this.name);
    }

}
