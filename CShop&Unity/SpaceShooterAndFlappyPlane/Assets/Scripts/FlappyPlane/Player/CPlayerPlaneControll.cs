﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent ( typeof(CPlayerPlaneMove) ) ]
public class CPlayerPlaneControll : MonoBehaviour {

    private CPlayerPlaneMove _playerPlaneMove;
    private CPlayerPlaneCollide _playerPlaneCollide;
    private Animator _animator;
    private bool _isSkill = false;
    public float _skillTime = 5f;
    private bool _isDead = false;
    public float _heightLimit = 5f;

    void Awake()
    {
        this._playerPlaneMove = this.GetComponent<CPlayerPlaneMove>();
        this._playerPlaneCollide = this.GetComponent<CPlayerPlaneCollide>();
        this._animator = this.GetComponent<Animator>();
        this._animator.Play("ReadyGame");
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

        if (this.transform.position.y < -this._heightLimit)
        {
            SceneManager.LoadScene("End");
        }
        else if (this.transform.position.y > this._heightLimit)
        {
            this.ReadyDie();
        }
        else if (_isDead)
            return;

        if (Input.GetKeyDown(KeyCode.Return) && CGameManager.Instance.Sp >= 100)
        {
            if (IsInvoking("ReleaseSkill"))
                CancelInvoke("ReleaseSkill");

            this._isSkill = true;
            this._animator.Play("PlayerPlaneSkill");
            Invoke("ReleaseSkill", this._skillTime);
            CGameManager.Instance.PopSp();
            CGameManager.Instance.GameSpeed = 10f;
            this._playerPlaneMove.SkillOn();
        }

        this._playerPlaneMove.InputMove();

	}

    public void Damaged(GameObject target)
    {
        if (this._isDead)
            return;

        if (this._isSkill)
        {
            CFly targetRigdbody2d = target.GetComponent<CFly>();
            Vector2 dir = target.transform.position - this.transform.position;
            targetRigdbody2d.FlyToDirection(dir.normalized);
            CGameManager.Instance.AddStarScore(5);
        }
        else
        {
            CObjectStat colStat = target.GetComponent<CObjectStat>(); //스탯 컴포넌트 가져오기

            CGameManager.Instance.PopHp(colStat._damage);  //게임 매니저에 전달

            this._animator.Play("PlayerPlaneDamaged"); //애니메이션 실행

            CEnemyCollide enemyCol = target.GetComponent<CEnemyCollide>();
            if (enemyCol != null)
                enemyCol.Die();
            else
                Destroy(target);
        }
    }

    public void ReadyDie()
    {
        this._isDead = true;
        CGameManager.Instance.GameSpeed = 0f;
    }

    private void ReleaseSkill()
    {
        this._playerPlaneMove.SkillOff();
        this._isSkill = false;
        this._animator.Play("Default");
        CGameManager.Instance.GameSpeed = 1f;
    }
}
