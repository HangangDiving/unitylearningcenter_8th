﻿using UnityEngine;
using System.Collections;

public class CPlayerPlaneMove : MonoBehaviour {

    private Rigidbody2D _rigidbody2d;
    public float _riseForce = 20f;
    private CPlayerPlaneControll _playerPlaneControll;
    private float _originalGravity;
    private float _skillSpeed = 1f;
    public float _addSkillSpeed = 3f;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
        this._playerPlaneControll = this.GetComponent<CPlayerPlaneControll>();
        this._originalGravity = this._rigidbody2d.gravityScale;
    }

	// Use this for initialization
	void Start () {
	
	}
	
    public void InputMove()
    {                
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this._rigidbody2d.velocity = Vector2.zero;
            this._rigidbody2d.velocity = new Vector2(0f, _riseForce * _skillSpeed);
        }
    }

    public void SkillOn()
    {
        this._rigidbody2d.gravityScale = _addSkillSpeed;
        this._skillSpeed = _addSkillSpeed;
    }

    public void SkillOff()
    {
        this._rigidbody2d.velocity = Vector2.zero;
        this._rigidbody2d.gravityScale = this._originalGravity;
        this._skillSpeed = 1f;
    }
}
