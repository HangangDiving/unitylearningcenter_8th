﻿using UnityEngine;
using System.Collections;

public class CPlayerPlaneCollide : MonoBehaviour {

    private CPlayerPlaneControll _playerPlaneControll;

    void Awake()
    {
        this._playerPlaneControll = this.GetComponent<CPlayerPlaneControll>();
    }

    // Use this for initialization
    void Start () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Enemy":
            case "EnemyLaser":
                this._playerPlaneControll.Damaged(col.gameObject); //공격 받았다고 알림
                break;
            case "Item":
                CItem item = col.GetComponent<CItem>(); //아이템 컴포넌트 가져오기
                item.ItemApply(); //아이템 적용
                Destroy(col.gameObject);
                break;
        }
    }
}
