﻿using UnityEngine;
using System.Collections;

public class CFly : MonoBehaviour {

    private Rigidbody2D _rigidbody2d;
    public float _flyForce = 1000f;
    public MonoBehaviour[] _components;
    public float _angle = 5f;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
    }

	// Use this for initialization
	void Start () {
	
	}

    public void FlyToDirection(Vector2 dir)
    {
        for (int i = 0; i < this._components.Length; i++)
        {
            IStopComponent stopCom = this._components[i] as IStopComponent;
            stopCom.StopAllMethod();
            this._components[i].enabled = false;
        }
        StartCoroutine("RotateCoroutine");
        this._rigidbody2d.AddForce(dir * this._flyForce);
    }

    private IEnumerator RotateCoroutine()
    {
        float angle = 0f;

        while (true)
        {            
            yield return null;

            angle += this._angle;

            if (angle >= 360f)
                angle = 0f;

            this._rigidbody2d.MoveRotation(angle);
        }
    }
}
