﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class CLoadScene3Manager : CSingletone<CLoadScene3Manager>, IEventTrigger {

    public Transform _eventTriggerTM;
    public CObjectMove _eventShip;
    public GameObject _destroyEffectPrefab;
    public string _nextSceneName = "LoadScene4";
    
	// Use this for initialization
	void Start () {
        Vector3 shipPos = Vector3.zero;
        shipPos.x = PlayerPrefs.GetFloat("Ship_XPos", -5);
        shipPos.y = PlayerPrefs.GetFloat("Ship_YPos", -2.5f);
        this._eventShip.transform.position = shipPos;

        Vector2 dir = this._eventTriggerTM.position - shipPos;
        dir.Normalize();
        this._eventShip.gameObject.SetActive(true);
        this._eventShip._direction = dir;
        this._eventShip._isSpace = Space.World;
        float angle = Vector2.Angle(this._eventShip.transform.up * -1f, dir);
        this._eventShip.transform.Rotate(0f, 0f,
            (this._eventShip.transform.position.x < 0) ? angle : -angle);

        GameObject gameObj = Instantiate(this._destroyEffectPrefab,
            this._eventShip.transform.position,
            Quaternion.identity) as GameObject;
        Destroy(gameObj, 0.5f);
    }

    public void ReciveColliderName(string name, string senderName = "")
    {
        if (name.Equals("EventTrigger1"))
        {
            this._eventShip.transform.rotation = Quaternion.identity;
            this._eventShip._direction = Vector2.down;
        }
        else if (name.Equals("EventTrigger2"))
        {
            SceneManager.LoadScene(this._nextSceneName);
        }
    }
}
