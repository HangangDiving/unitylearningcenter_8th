﻿using UnityEngine;
using System.Collections;

//입력 공격
public class CInputAttack : MonoBehaviour {

    protected Transform _attackPoint;   //공격 포인트
    protected Animator _animator;       //애니메이터
    protected int _attackIndex = 1;     //공격 모션 번호

    protected virtual void Awake()
    {
        _attackPoint = transform.FindChild("AttackPoint");
        _animator = GetComponent<Animator>();
    }

	// Use this for initialization
	protected virtual void Start () {
	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        //입력 공격 수행
        InputAttack();

	}

    //공격 중인지를 체크하다
    public virtual bool IsAttack()
    {
        return false;
    }

    //공격키를 입력하다
    public virtual void InputAttack()
    {

    }

    //공격 하다
    public virtual void Attack()
    {

    }
}
