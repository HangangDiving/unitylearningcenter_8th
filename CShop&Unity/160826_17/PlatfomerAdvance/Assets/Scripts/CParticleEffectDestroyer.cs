﻿using UnityEngine;
using System.Collections;

public class CParticleEffectDestroyer : CDestroyer {

    protected ParticleSystem _particleSystem;   //파티클 시스템

    protected override void Awake()
    {
        base.Awake();

        //파티클 시스템 컴포넌트 참조
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    // Use this for initialization
    protected override void Start () {

        //파티클 지연 시간
        _destroyTime = _particleSystem.duration;

        base.Start();

	}
}
