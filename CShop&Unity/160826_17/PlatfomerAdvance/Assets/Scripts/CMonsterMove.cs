﻿using UnityEngine;
using System.Collections;
using System;

//몬스터 이동 클래스
public class CMonsterMove : CMove, IMonsterMove {

    protected Animator _animator;               //애니메이터
    protected Rigidbody2D _rigidbody2d;         //물리 엔진
    protected CCharacterState _monsterState;    //몬스터 상태

    protected float _prevMoveSpeed;             //이전 몬스터 속도
    public float _moveSpeed;                    //몬스터 속도

    protected GameObject _player;               //플레이어

    protected override void Awake()
    {
        base.Awake();

        _animator = GetComponent<Animator>();
        _rigidbody2d = GetComponent<Rigidbody2D>();
        _monsterState = GetComponent<CCharacterState>();
    }

    // Use this for initialization
    protected override void Start () {
        base.Start();

        _prevMoveSpeed = _moveSpeed;

        //플레이어를 참조함
        _player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        //이동
        Move();
	}

    //이동
    public virtual void Move()
    {
        //이동 상태를 변경함
        _monsterState.state = CCharacterState.State.Move;

        if (_animator)
        {
            _animator.SetBool("Move", true);
        }
        
    }

    //대기 시간 정지
    public virtual void IdleTimeStop(float time)
    {
        if (_monsterState.state == CCharacterState.State.Idle) return;

        StartCoroutine(StopIdleDelayCoroutine(time));
    }

    //대기 정지
    public virtual void IdleStop()
    {
        //일시적인 대기 상태 전환
        _monsterState.state = CCharacterState.State.Idle;
        //정지 속도 설정
        _moveSpeed = 0;
    }

    //공격 정지
    public void AttackStop()
    {
        //정지 속도 설정
        _moveSpeed = 0;
    }

    //이동 재개
    public void MoveResume()
    {
        //속도 재 설정
        _moveSpeed = _prevMoveSpeed;
        //이동 상태로 다시 변경
        _monsterState.state = CCharacterState.State.Move;
    }

    //이동 정지 지연 코루틴
    public virtual IEnumerator StopIdleDelayCoroutine(float time)
    {
        IdleStop();

        //이동 지연
        yield return new WaitForSeconds(time);

        MoveResume();
    }
}
