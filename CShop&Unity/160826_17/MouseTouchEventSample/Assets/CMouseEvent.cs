﻿using UnityEngine;
using System.Collections;

public class CMouseEvent : MonoBehaviour {

    private bool bMouseButtonDown = false;

    void OnMouseDown()
    {
        bMouseButtonDown = true;
    }

    void OnMouseDrag()
    {
        if (bMouseButtonDown)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector2(wp.x, wp.y);
        }
    }

    void OnMouseUp()
    {
        bMouseButtonDown = false;
    }
}
