﻿using UnityEngine;
using System.Collections;

public class CDragAndDrop : MonoBehaviour {

    public Transform _eventObject;
    private bool bMouseButtonDown = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (bMouseButtonDown && Input.GetMouseButtonDown(0))
        {
            bMouseButtonDown = false;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 touchPos = new Vector2(wp.x, wp.y);

            Collider2D collider2d = Physics2D.OverlapPoint(touchPos);
            if (collider2d != null)
            {
                bMouseButtonDown = true;
            }
        }
        else if (bMouseButtonDown)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (_eventObject != null)
                _eventObject.transform.position = new Vector2(wp.x, wp.y);
        }

	}
}
