﻿using UnityEngine;
using System.Collections;

public class CShotGun : MonoBehaviour {

    private Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(wp.x, wp.y);

            Shot();
        }

	}

    void Shot()
    {
        //_animator.Play("Shot");

        //if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Shot")) return;

        _animator.SetTrigger("Shot");
    }

    void Fire()
    {
        Collider2D[] colliders =
            Physics2D.OverlapPointAll(transform.position, 1 << LayerMask.NameToLayer("Duck"));

        if (colliders.Length <= 0) return;

        Destroy(colliders[0].gameObject);
    }
}
