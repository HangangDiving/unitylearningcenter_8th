﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class CPushWooshManager : MonoBehaviour {

    Text _logText;

    // Use this for initialization
    void Start()
    {
        _logText = GameObject.Find("LogText").GetComponent<Text>();

        Pushwoosh.ApplicationCode = "CF3A8-50C76"; // 푸쉬우쉬 ID
        Pushwoosh.GcmProjectNumber = "512443648059"; // 구글 플레이 ID
        Pushwoosh.Instance.OnRegisteredForPushNotifications += OnRegisteredForPushNotifications;
        Pushwoosh.Instance.OnFailedToRegisteredForPushNotifications += OnFailedToRegisteredForPushNotifications;
        Pushwoosh.Instance.OnPushNotificationsReceived += OnPushNotificationsReceived;
        Pushwoosh.Instance.RegisterForPushNotifications();
    }

    //처음 등록했을 때 호출
    void OnRegisteredForPushNotifications(string token)
    {
        //do handling here
        _logText.text = "Received token: \n" + token;
    }

    //실패 했을 때 호출
    void OnFailedToRegisteredForPushNotifications(string error)
    {
        //do handling here
        _logText.text = "Error ocurred while registering to push notifications: \n" + error;
    }

    //화면이 켜져있을 때 호출 -> 즉, 정상적으로 푸쉬 왔다면 무조건 호출된다고 보면 된다.
    void OnPushNotificationsReceived(string payload)
    {
        //do handling here
        _logText.text = "Received push notificaiton: \n" + payload;
    }
}
