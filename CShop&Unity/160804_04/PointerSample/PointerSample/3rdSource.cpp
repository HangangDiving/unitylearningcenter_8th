#include<iostream>
#include<stdlib.h>
#include<string>
#include<time.h>

using namespace std;

//포인터를 사용한 배열의 참조
void PrintArr1(int* arr, int size)
{
	cout << "main().arr의 값 : " << arr << endl;
	for (int i = 0; i < size; i++) {
		cout << i + 1 << "번째 값 출력 <<< " << *(arr + 1) << endl;
	}
}

//래퍼런스를 이용한 배열의 참조1
void PrintArr2(int arr[], int size)
{
	cout << "main().arr의 값 : " << arr << endl;
	for (int i = 0; i < size; i++) {
		cout << i + 1 << "번째 값 출력 <<< " << arr[i] << endl;
	}
}

//래퍼런스를 이용한 배열의 참조2
void PrintArr3(int arr[5], int size)
{
	cout << "main().arr의 값 : " << arr << endl;
	for (int i = 0; i < size; i++) {
		cout << i + 1 << "번째 값 출력 : " << arr[i] << endl;
	}
}

void InputArr(int arr[], int size)
{
	for (int i = 0; i < size; i++) {
		cout << i + 1 << "번째 값 입력 >>> ";
		cin >> arr[i]; //*(arr + i)
	}
}

void main()
{
	int size = 0;
	cout << "할당 크기를 정하세요 >>> ";
	cin >> size;

	// 원하는 크기를 유저가 원할때 동적으로 할당하는 방법을 동적 할당이라고 한다.
	// 동적 할당은 new 키워드를 사용한다.
	// 동정 할당 문법 : 자료형* 포인터변수 = new 자료형[크기];
	// 동적할당시에는 자동변수 공간이 아닌 힙 메모리 공간을 사용한다.
	// 힙 메모리 공간은 반드시 사용자가 해제 해줘야 한다.
	// 힙 메모리 공간을 해제 하지 않으면 메모리 누수가 발생할 수 있다.
	// 물론 게임 종료될때는 모두 해제 된다.

	//int arr[size]; => 자동 변수 공간의 크기는 컴파일 이전에 결정되어야 한다.
	int* arr = new int[size];
	//int arr[5];

	InputArr(arr, size);
	PrintArr1(arr, size);

	delete[] arr;
}