#include<iostream>
#include<stdlib.h>
#include<string>
#include<time.h>
#include<windows.h>

using namespace std;

void main()
{
	int value = 10;
	int* pValue = &value;

	int** ppValue = &pValue;

	int*** pppValue = &ppValue;

	//ppValue -> *pppValue;
	//pValue -> **pppValue;
	//value -> ***pppValue;

	//2차원 포인터 변수에는 1차원 포인터 변수의 주소만 저장할 수 있음
	//int** ppValue2 = &value; //오류

	cout << "주소값들==============" << endl;
	cout << "&value : " << &value << endl;
	cout << "pValue : " << pValue << endl;
	cout << "ppValue : " << ppValue << endl;
	cout << "&pValue : " << &pValue << endl;

	cout << "데이터값들============" << endl;
	cout << "value : " << value << endl;
	cout << "*(&value) : " << *(&value) << endl;
	cout << "*pValue : " << *pValue << endl;
	cout << "*ppValue : " << *ppValue << endl;
	cout << "**ppValue : " << **ppValue << endl;
}