#include<iostream>
#include<stdlib.h>
#include<string>
#include<time.h>
#include<windows.h>

using namespace std;

void MonsterGen(int type)
{
	switch (type)
	{
	case 1:
	case 2:
		cout << "인간형 몬스터 타입이 생성됩니다." << endl;
		break;
	case 3:
	case 4:
		cout << "언데드형 몬스터 타입이 생성됩니다." << endl;
		break;
	default:
		cout << "동물형 타입이 생성됩니다." << endl;
		break;
	}

	switch (type)
	{
		case 1:
			cout << "멀록 몬스터가 생성되었습니다." << endl;
			break;
		case 2:
			cout << "오우거 몬스터가 생성되었습니다." << endl;
			break;
		case 3:
			cout << "언데드 기사 몬스터가 생성되었습니다." << endl;
			break;
		case 4:
			cout << "보스 몬스터가 생성되었습니다." << endl;
			break;
		case 5:
			cout << "늑대 몬스터가 생성되었습니다." << endl;
			break;
	}
}

// 2차원 배열 포인터를 이용해서 main()함수의 monsterGenType배열을 참조함
//void WaveStart(int (*pMonsterGenType)[4])
//void WaveStart(int refMonsterGenType[][4])
// 동적으로 생성된 2차원 배열에 대한 참조
void WaveStart(int** pMonsterGenType, int waveSize, int monsterCount)
{
	cout << "&pMonsterGenType[1][2] => " << &pMonsterGenType[1][2] << endl;

	//1차원 반복문은 행배열의 반복 담당
	for (int i = 0; i < waveSize; i++)
	{
		cout << i + 1 << "번째 웨이브가 시작되었습니다..." << endl;

		//2차원 반복문은 열배열의 반복을 담당
		for (int j = 0; j < monsterCount; j++)
		{
			Sleep(2000);
			MonsterGen(*(*(pMonsterGenType + i) + j));
			//MonsterGen(refMonsterGenType[i][j]);
			//MonsterGen(*(*(refMonsterGenType + i) + j));
		}

		for (int k = 0; k < 3; k++)
		{
			Sleep(500); cout << "경고!! ";
		}
		cout << endl;
	}
}

void main()
{
	int waveSize = 0;
	int monsterCount = 0;

	cout << "웨이브 갯수를 입력하시오 >>> "; cin >> waveSize;
	cout << "웨이브당 몬스터의 갯수를 입력하시오 >>> "; cin >> monsterCount;

	//동적으로 행배열을 생성함
	int** pMonsterGenType = new int*[waveSize];

	//동적으로 열배열을 생성함
	for (int i = 0; i < waveSize; i++)
	{
		pMonsterGenType[i] = new int[monsterCount];
	}

	for (int i = 0; i < waveSize; i++)
	{
		for (int j = 0; j < monsterCount; j++)
		{
			pMonsterGenType[i][j] = i + 1;
		}
	}

	WaveStart(pMonsterGenType, waveSize, monsterCount);

	//동적 배열을 제거함
	for (int i = 0; i < waveSize; i++)
	{
		delete[] pMonsterGenType[i];
	}

	delete[] pMonsterGenType;
}