﻿using UnityEngine;
using System.Collections;

public class CMeteorGenerator : MonoBehaviour {

    public GameObject _meteorPrefab;
    public float _genDelay;
    public Transform[] _genPositions;

	// Use this for initialization
	void Start () {
        StartCoroutine("Gen");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator Gen()
    {
        while (true)
        {
            //오브젝트 풀에서 오브젝트(운석)를 빼냄
            GameObject obj = CObjectPool.current.GetObject(_meteorPrefab);

            //운석 생성위치와 각도를 설정함
            int genPosNum = Random.Range(0, _genPositions.Length);
            obj.transform.position = _genPositions[genPosNum].position;
            obj.transform.rotation = _genPositions[genPosNum].rotation;

            //운석을 활성화함
            obj.SetActive(true);

            yield return new WaitForSeconds(_genDelay);
        }
    }
}
