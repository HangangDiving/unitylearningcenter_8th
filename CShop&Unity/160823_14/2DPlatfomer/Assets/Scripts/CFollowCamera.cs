﻿using UnityEngine;
using System.Collections;

public class CFollowCamera : MonoBehaviour {

    public Transform _followTarget; //추적 대상
    public float _smooth; //이동시 부드러움 정도
    public Vector3 _offset; //기본 간격

	// Use this for initialization
	void Start () {
        transform.position = Vector3.zero;
        transform.position = _followTarget.position;
        transform.position = _followTarget.position + _offset;
	}
	
	//랜더링 끝날 때 한번 호출되는 Update 메소드
    //모든 업데이트가 끝난 후
	void LateUpdate () {
        if (_followTarget == null) return;

        //타겟 위치에 간격을 두고
        Vector3 targetCameraPos = _followTarget.position + _offset;

        //대상을 선형 보간으로 추적함
        transform.position = Vector3.Lerp(transform.position,
            targetCameraPos, _smooth * Time.deltaTime);
	}
}
