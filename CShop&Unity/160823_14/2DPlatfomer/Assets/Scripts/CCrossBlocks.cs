﻿using UnityEngine;
using System.Collections;

public class CCrossBlocks : MonoBehaviour {

    private BoxCollider2D _blocksCollider;
    private CUnityChanMovement _unityChan;

    void Awake()
    {
        this._blocksCollider = this.GetComponent<BoxCollider2D>();
    }

	// Use this for initialization
	void Start () {
        this._unityChan =
            GameObject.Find("UnityChan").GetComponent<CUnityChanMovement>();
	}
    
    void FixedUpdate()
    {
        //유니티짱이 블록보다 위에 있고 현재 하강중인 상태라면
        if (this._unityChan.transform.position.y > this.transform.position.y
            && this._unityChan._rigidbody2d.velocity.y <= 0)
        {
            //콜라이더 관통 금지
            this._blocksCollider.isTrigger = false;
        }
        else //유니티짱이 블록보다 아래 있고 현재 상승중인 상태라면 (가로질러 점프 상태)
        {
            //콜라이더 관통 허용
            this._blocksCollider.isTrigger = true;
        }
    }
}
