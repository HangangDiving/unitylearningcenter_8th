﻿using UnityEngine;
using System.Collections;

public class CWorldSpaceUIBillboard : MonoBehaviour {
    	
	// Update is called once per frame
	void LateUpdate () {

        //게임 카메라와 동일한 시선을 보게 함(빌보드)
        Quaternion rot = Quaternion.LookRotation(Camera.main.transform.forward);
        transform.rotation = rot;

	}
}
