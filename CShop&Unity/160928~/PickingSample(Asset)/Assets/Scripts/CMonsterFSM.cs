﻿using UnityEngine;
using System.Collections;

public class CMonsterFSM : MonoBehaviour {

    //몬스터 상태
    CCharacterStat _stat;
    //몬스터 애니메이션
    CCharacterAnimation _anim;

    public float _traceDist; //추적 거리
    public float _attackDist; //공격 거리

    Transform _player; //추적 대상(플레이어)

    NavMeshAgent _navMeshAgent; //네비게이션 컴포넌트 참조

    CCharacterAttack _attack; //공격 컴포넌트 참조

    void Awake()
    {
        _stat = GetComponent<CCharacterStat>();
        _anim = GetComponent<CCharacterAnimation>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _attack = GetComponent<CCharacterAttack>();
    }

	// Use this for initialization
	void Start () {

        //플레이어 참조
        _player = GameObject.FindGameObjectWithTag("Player").transform;

        //이동 중지
        _navMeshAgent.Stop();

        //상태 머신 체크 코루틴 실행
        StartCoroutine("MonsterFSMCoroutine");

	}
	
    IEnumerator MonsterFSMCoroutine()
    {
        //몬스터가 생성하고 있는 중이면
        while (_stat._state == CCharacterStat.STATE.CREATE)
        {
            //상태 머신 대기
            yield return null;
        }

        //몬스터 사망한 상태가 아니면
        while (_stat._state != CCharacterStat.STATE.DIE)
        {
            //FSM 가동
            if (_player == null)
            {
                //대기 상태로 변경 후 종료
                yield break;
            }

            //플레이어와의 거리를 측정함
            float dist = Vector3.Distance(_player.position, transform.position);

            //플레이어가 공격 거리 안에 들어오면
            if (dist <= _attackDist)
            {
                _navMeshAgent.Stop();

                //방향 특정
                Vector3 direction = _player.position - transform.position;
                //회전 쿼터니언을 구함
                Quaternion rot = Quaternion.LookRotation(direction.normalized);
                //보간 회전
                transform.rotation = Quaternion.Slerp(transform.rotation, rot, 0.2f);

                //공격 애니메이션 재생
                _anim.PlayAnimation(CCharacterStat.STATE.ATTACK);

                _attack.AttackStart();
            }
            //공격 거리보다는 멀어지고 추적거리 안에 있으면
            else if (dist > _attackDist && dist <= _traceDist)
            {
                //공격 취소
                _attack.AttackCancel();

                //공격하기 위해 이동함
                _anim.PlayAnimation(CCharacterStat.STATE.ATTACK_MOVE);

                //이동 재설정
                _navMeshAgent.SetDestination(_player.position);
                _navMeshAgent.Resume(); //이동 시작
            }
            else //대기 상태
            {
                _attack.AttackCancel();

                _anim.PlayAnimation(CCharacterStat.STATE.IDLE);
                _navMeshAgent.Stop();
            }

            yield return null;
        }
    }
}
