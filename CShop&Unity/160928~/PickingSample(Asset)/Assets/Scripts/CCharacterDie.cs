﻿using UnityEngine;
using System.Collections;

//캐릭터 사망
public class CCharacterDie : MonoBehaviour {

    //캐릭터 파괴 지연 시간
    public float _destroyTime;

    NavMeshAgent _navMeshAgent; //네비게이션
    Collider _collider; //콜라이더

    protected virtual void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _collider = GetComponent<Collider>();
    }

    //사망이 완료 처리
    public virtual void DieComplete()
    {
        //네비게이션 및 콜라이더 비활성화
        _navMeshAgent.enabled = false;
        _collider.enabled = false;

        //지연 시간 후 오브젝트 파괴
        Destroy(gameObject, _destroyTime);
    }
}
