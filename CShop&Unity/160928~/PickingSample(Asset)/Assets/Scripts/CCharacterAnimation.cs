﻿using UnityEngine;
using System.Collections;

public class CCharacterAnimation : MonoBehaviour {

    //애니메이터 참조
    Animator _animator;
    //캐릭터 상태
    CCharacterStat _stat;
    //캐릭터 사망
    CCharacterDie _die;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _stat = GetComponent<CCharacterStat>();
        _die = GetComponent<CCharacterDie>();
    }

    //애니메이션 재생
    public void PlayAnimation(CCharacterStat.STATE state)
    {
        //상태 변경
        _stat._state = state;

        //상태에 맞는 애니메이션 수행
        switch (_stat._state)
        {
            case CCharacterStat.STATE.MOVE:
            case CCharacterStat.STATE.ATTACK_MOVE:
                _animator.SetBool("Attack", false);
                _animator.SetBool("Run", true);
                break;
            case CCharacterStat.STATE.IDLE:
                _animator.SetBool("Attack", false);
                _animator.SetBool("Run", false);
                break;
            case CCharacterStat.STATE.ATTACK:
                _animator.SetBool("Run", false);
                _animator.SetBool("Attack", true);
                break;
            case CCharacterStat.STATE.DIE:
                _animator.SetTrigger("Die");
                break;
        }
    }

    //캐릭터 생성 애니메이션 재생이 완료됨
    void CreateAnimationComplete()
    {
        //대기 애니메이션으로 변경함
        PlayAnimation(CCharacterStat.STATE.IDLE);
    }

    //사망 애니메이션 재생이 완료됨
    void DieAnimationComplete()
    {
        //사망 시 완료 처리
        _die.DieComplete();
    }
}
