﻿using UnityEngine;
using System.Collections;

public class CParticleDestroyer : MonoBehaviour {

    ParticleSystem _particle;

    void Awake()
    {
        _particle = GetComponentInChildren<ParticleSystem>();
    }

	// Use this for initialization
	void Start () {

        //파티클이 실행중이 아니면 파티클 재생
        if (!_particle.isPlaying) _particle.Play();

        //파티클 재생 후 파괴
        Destroy(gameObject, _particle.duration);

	}
}
