﻿using UnityEngine;
using System.Collections;

public class CMonsterHit : MonoBehaviour {

    //몬스터 스탯
    CCharacterStat _stat;
    //몬스터 애니메이션
    CCharacterAnimation _anim;
    //타격 이펙트 프리팹
    public GameObject _hitEffectPrefab;
    //타격 이펙트 표시 위치
    public Transform _hitEffectPoint;

    void Awake()
    {
        _stat = GetComponent<CCharacterStat>();
        _anim = GetComponent<CCharacterAnimation>();
    }

    //몬스터 피격
    public void Hit(int damage)
    {
        //체력 수치 감소
        int hp = _stat.HpDown(damage);

        //타격 이펙트 재생
        Instantiate(_hitEffectPrefab, _hitEffectPoint.position, Quaternion.identity);

        //사망 처리
        if (hp <= 0)
        {
            _anim.PlayAnimation(CCharacterStat.STATE.DIE);
        }
    }
}
