﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//캐릭터 공통 스탯
public class CCharacterStat : MonoBehaviour {

    //캐릭터 상태들
    public enum STATE { CREATE, IDLE, MOVE, ATTACK_MOVE, ATTACK, DIE };
    //캐릭터 기본 상태 설정
    public STATE _state = STATE.IDLE;

    //체력바
    public Image _hpProgress;

    protected virtual void Awake()
    {
    }

    //캐릭터 체력 감소
    public int HpDown(float damage)
    {
        //체력바 감소
        _hpProgress.fillAmount -= (damage * 0.01f);
        int hp = (int)(_hpProgress.fillAmount * 100f);

        //체력이 0이 되면
        if (hp <= 0)
        {
            //체력바 비활성화
            _hpProgress.transform.parent.gameObject.SetActive(false);
        }

        return hp;
    }

    //현재 넘겨준 상태값이 현재 상태와 일치하는지 검사함
    public bool IsState(CCharacterStat.STATE state)
    {
        if (_state == state) return true;

        return false;
    }
}
