﻿using UnityEngine;
using System.Collections;

public class CPlayerStat : CCharacterStat {

    //충돌 레이어 정보
    int _layerMask;
    //충돌 체크할 레이어 이름 배열
    public string[] _layerNames = { "Ground", "Monster", "Object" };

    protected override void Awake()
    {
        //레이어 이름 배열에 선언된 레이어 정보를 구함
        _layerMask = LayerMask.GetMask(_layerNames);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
