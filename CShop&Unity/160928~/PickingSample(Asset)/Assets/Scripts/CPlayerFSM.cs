﻿using UnityEngine;
using System.Collections;

public class CPlayerFSM : MonoBehaviour {

    //플레이어 상태
    CPlayerStat _stat;
    //이동 타겟 위치
    Vector3 _moveTargetPos = Vector3.zero;
    //플레이어 네비게이션
    NavMeshAgent _navMeshAgent;
    //플레이어 애니메이션
    CCharacterAnimation _anim;
    //플레이어 공격
    CPlayerAttack _attack;

    void Awake()
    {
        _stat = GetComponent<CPlayerStat>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _anim = GetComponent<CCharacterAnimation>();
        _attack = GetComponent<CPlayerAttack>();
    }

	// Use this for initialization
	void Start () {
        //이동 정지
        _navMeshAgent.Stop();
	}
	
	// Update is called once per frame
	void Update () {
        //터치 또는 마우스 클릭이 되었다면
        if (Input.GetMouseButtonDown(0))
        {
            //해당 위치로 카메라에서 발사하는 레이를 생성함
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //터치 충돌 구조체
            RaycastHit hit;

            //레이와의 충돌 여부와 정보를 추출함
            if (Physics.Raycast(ray, out hit, 100f))
            {
                //레이와 충돌한 오브젝트의 레이어 정보를 구함
                int layer = hit.transform.gameObject.layer;

                //오브젝트 피킹 처리
                if (layer == LayerMask.NameToLayer("Object")) return;

                //이미 공격 대상을 다시 선택했다면
                if (hit.transform.gameObject == _attack._attackTarget)
                {
                    return; //무시
                }

                //충돌 위치 파악
                _moveTargetPos = hit.point;

                //지면을 터치했다면
                if (layer == LayerMask.NameToLayer("Ground"))
                {
                    //공격 대상 선택을 취소함
                    _attack._attackTarget = null;

                    //이동 애니메이션 수행
                    _anim.PlayAnimation(CCharacterStat.STATE.MOVE);
                }
                else if (layer == LayerMask.NameToLayer("Monster"))
                {
                    //피킹한 오브젝트를 공격 대상으로 설정
                    _attack._attackTarget = hit.transform.gameObject;
                    //공격 이동 애니메이션 재생
                    _anim.PlayAnimation(CPlayerStat.STATE.ATTACK_MOVE);
                }

                Move();
            }//if(Raycast)
        }//if(GetMouseButtonDown)
        else
        {
            //타겟 대상과의 거리
            float dist = 0f;

            //공격 지정 대상이 있다면
            if (_attack._attackTarget != null)
            {
                //공격 대상과의 거리 측정
                dist = _attack.GetAttackTargetDistance();
            }
            else
            {
                //플레이어와 이동 위치간의 거리
                dist = Vector3.Distance(transform.position, _moveTargetPos);
            }

            //현재 이동 상태이고 도착과의 거리가 0.2이하면
            if (_stat.IsState(CCharacterStat.STATE.MOVE) && dist <= 0.2f)
            {
                //네비게이션 이동을 중지함
                _navMeshAgent.Stop();

                //도착했으니 대기 애니메이션으로 변경함
                _anim.PlayAnimation(CPlayerStat.STATE.IDLE);
            }
            //현재 공격을 하기위해 이동 중이고 공격 대상과의 거리가 2.3이하면
            else if (_stat.IsState(CCharacterStat.STATE.ATTACK_MOVE)
                && dist <= 2.3f)
            {
                _navMeshAgent.Stop(); //이동 중지

                //타겟 몬스터 공격 시작(애니메이ㅏ션 발동)
                _attack.AttackStart();
            }
            //현재 공격 하기 위해 가고 있지만 도착하지 않았을 때 또는
            //현재 공격하고 있다가 도망갔을 때
            else if ((_stat.IsState(CCharacterStat.STATE.ATTACK_MOVE)
                || _stat.IsState(CCharacterStat.STATE.ATTACK))
                && dist > 2.5f)
            {
                //네비게이션 재설정
                _navMeshAgent.SetDestination(_attack._attackTarget.transform.position);
                _navMeshAgent.Resume();

                //이동 애니메이션 재생
                _anim.PlayAnimation(CCharacterStat.STATE.ATTACK_MOVE);
            }
        }
	}

    void Move()
    {
        //현재 이동을 중지함
        _navMeshAgent.Stop();

        //이동 위치로 네비게이션 설정
        _navMeshAgent.SetDestination(_moveTargetPos);

        //이동 시작
        _navMeshAgent.Resume();
    }
}
