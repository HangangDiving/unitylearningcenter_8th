﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CTargetPicker : MonoBehaviour {

    //타겟 표시 오프셋
    public float _sufaceOffset = 0.1f;

    //피커를 표시할 타겟 오브젝트
    public Transform _target;

    //피커
    public Image _picker;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //터치 이벤트
        if (Input.GetMouseButtonDown(0))
        {
            //레이 생성
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f))
            {
                //선택한 오브젝트 레이어 추출
                int layer = hit.transform.gameObject.layer;

                _target = hit.transform;

                //오브젝트를 선택하면
                if (layer == LayerMask.NameToLayer("Object"))
                {
                    //피커를 숨겨라
                    CancelPicker();
                    return;
                }

                //몬스터를 선택했다면
                if (layer == LayerMask.NameToLayer("Monster"))
                {
                    //피커를 표시함
                    ShowPicker(hit.transform);
                    //피커의 위치를 변경함
                    transform.position = hit.transform.position +
                        hit.transform.up.normalized * _sufaceOffset;
                }

                //바닥을 선택했다면
                if (layer == LayerMask.NameToLayer("Ground"))
                {
                    ShowPicker(null);
                    transform.position = hit.point +
                        hit.transform.up.normalized * _sufaceOffset;
                }
            }//if(Raycast
            else
            {
                CancelPicker();
            }
        }//if(GetMouseButtonDown
        else
        {
            //선택했던 오브젝트가 파괴되면
            if (_target == null)
            {
                //선택 취소
                CancelPicker();
            }
        }
    }

    //피커를 보여 줌
    void ShowPicker(Transform parent)
    {
        //피커 이미지 활성화
        _picker.enabled = true;
        //현재 선택한 오브젝트의 자식으로 피커를 등록함
        //(몬스터를 피커가 따라가야 함)
        transform.SetParent(parent);
    }

    //피커를 숨김
    void CancelPicker()
    {
        //피커 이미지 비활성화
        _picker.enabled = false;
        //이전에 선택한 오브젝트의 자식 등록 상태를 해제함
        transform.SetParent(null);
    }
}
