﻿using UnityEngine;
using System.Collections;

public class CPlayerAttack : CCharacterAttack {

    //공격 대상
    public GameObject _attackTarget;
    //애니메이션
    CCharacterAnimation _anim;

    void Awake()
    {
        _anim = GetComponent<CCharacterAnimation>();
    }

    //공격 대상과의 거리를 측정함
    public float GetAttackTargetDistance()
    {
        float dist = Vector3.Distance(transform.position,
            _attackTarget.transform.position);

        return dist;
    }

    //공격 시작
    public override void AttackStart()
    {
        base.AttackStart();
        
        //타겟 몬스터를 향함
        Vector3 dir = _attackTarget.transform.position - transform.position;
        dir.y = 0f;

        Quaternion rot = Quaternion.LookRotation(dir.normalized);
        transform.rotation = rot;

        //공격 애니메이션 재생
        _anim.PlayAnimation(CCharacterStat.STATE.ATTACK);
    }

    //공격 애니메이션 이벤트
    void AttackAnimationEvent()
    {
        Attack();
    }

    //공격
    public override void Attack()
    {
        base.Attack();

        //무기에 피격 범위에 들어가 있는 몬스터를 체크함
        Collider[] hitColliders = Physics.OverlapSphere(_attackPoint.position,
            1.2f, 1 << LayerMask.NameToLayer("Monster"));

        //피격 대상 몬스터가 존재한다면
        if (hitColliders.Length > 0)
        {
            //몬스터의 상태를 추출해
            CCharacterStat monsterStat = hitColliders[0].GetComponent<CCharacterStat>();

            if (monsterStat == null) return;

            //몬스터가 사망 상태라면
            if (monsterStat._state == CCharacterStat.STATE.DIE)
            {
                //공격 타겟 설정 해제
                _attackTarget = null;
                //대기 애니메이션 전환
                _anim.PlayAnimation(CPlayerStat.STATE.IDLE);
                return;
            }

            //몬스터 피격 처리
            hitColliders[0].GetComponent<CMonsterHit>().Hit(_damage);
        }
    }
}
