﻿using UnityEngine;
using System.Collections;

public class CCharacterAttack : MonoBehaviour {

    public int _damage; //공격 데미지
    public Transform _attackPoint; //피격 포인트

    //공격 시작 이벤트
    public virtual void AttackStart() { }
    //공격 이벤트
    public virtual void Attack() { }
    //공격 완료 이벤트
    public virtual void AttackEnd() { }
    //공격 취소 이벤트
    public virtual void AttackCancel() { }

}
