﻿using UnityEngine;
using System.Collections;

public class CFollowCamera : MonoBehaviour {

    //추적 타겟
    public Transform _target;
    //부드러운 정도
    public float _smoothing = 5f;
    //타겟과의 거리 고정 간격
    public Vector3 _offset;
    
    //타겟 추적        	
	void LateUpdate () {

        //타겟이 정해지면 타겟을 추적함
        if (_target == null) return;
        
        //타겟과의 고정 간격을 둔 위치값을 설정함
        Vector3 tragetCamPos = _target.position + _offset;
        
        //추적
        transform.position = 
            Vector3.Lerp(transform.position, tragetCamPos, _smoothing * Time.deltaTime);

	}

    //추적 카메라를 초기화함
    public void Init(Transform target)
    {
        //타겟 설정
        _target = target;
        
        //위치 초기화
        transform.position = Vector3.zero;
        
        //타겟 위치 설정
        transform.position = _target.position;
        
        //고정 간격 설정
        transform.position = _target.position + _offset;
    }
}
