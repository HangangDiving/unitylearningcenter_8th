﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//정보 메시지 팝업
public class CInfoMessagePopup : MonoBehaviour {

    //안내 메시지 텍스트
    public Text _infoMessageText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //정보 메시지 팝업에 안내 메시지를 출력함
    public void ShowMessage(string msg)
    {
        gameObject.SetActive(true);

        _infoMessageText.text = msg;
    }

    //정보 메시지 팝업의 확인버튼 클릭함
    public void OnOkButtonClick()
    {
        gameObject.SetActive(false);
    }
}
