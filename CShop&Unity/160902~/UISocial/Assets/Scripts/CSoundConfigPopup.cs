﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class CSoundConfigPopup : MonoBehaviour {

    public CSoundManager _soundManager;
    public Slider _bgmSlider;
    public Slider _effectSlider;
    public Toggle _bgmToggle;

    void OnEnable()
    {
        _soundManager =
            GameObject.Find("SoundManager").GetComponent<CSoundManager>();

        //현재 사운드 설정 정보를 UI에 셋팅함
        _bgmSlider.value = _soundManager._bgmAudioSource.volume;
        _effectSlider.value = _soundManager._effectSoundVol;
        _bgmToggle.isOn = _soundManager._bgmAudioSource.mute;

        Debug.Log("OnEnable!!!");
    }

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
