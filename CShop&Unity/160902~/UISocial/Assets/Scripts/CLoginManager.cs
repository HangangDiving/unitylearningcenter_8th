﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

using GameData;

//로그인 매니저
public class CLoginManager : MonoBehaviour {

    //회원가입 매니저
    public CJoinManager _joinManager;

    //UI
    public Text _logMessageText;
    public InputField _userIdInputField;
    public InputField _userPwInputField;

    public CInfoMessagePopup _infoMessagePopup;

    // Use this for initialization
    void Start () {
        _logMessageText.text = "";
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    //회원 가입 팝업 띄우기 버튼 클릭
    public void OnOpenJoinPopupButtonClick()
    {
        _joinManager.gameObject.SetActive(true);
    }

    public void OnLoginButtonClick()
    {
        if (_userIdInputField.text.Length <= 0)
        {
            _infoMessagePopup.ShowMessage("아이디를 입력하세요");
            return;
        }
        else if (_userPwInputField.text.Length <= 0)
        {
            _infoMessagePopup.ShowMessage("비밀번호를 입력하세요");
            return;
        }

        StopCoroutine("LoginNetCoroutine");
        StartCoroutine("LoginNetCoroutine");
    }

    private IEnumerator LoginNetCoroutine()
    {
        //로그인을 요청함
        string url = "http://52.78.144.8/uisample/login.php";

        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("user_id", _userIdInputField.text);
        wwwForm.AddField("user_pw", _userPwInputField.text);

        WWW www = new WWW(url, wwwForm);

        //요청을 기다림
        yield return www;

        //로그인 요청에 대한 응답을 처리함
        if (www.error == null)
        {            
            //JSON 데이터를 -> 딕셔너리 객체로 생성
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text) as Dictionary<string,object>;

            //응답 데이터에서 결과 코드값을 추출함
            string code = responseData["code"].ToString();

            //로그인이 실패했다면
            if (code.Equals("LOGIN_FAIL"))
            {
                //로그인 실패 메시지 팝업을 띄움
                _infoMessagePopup.ShowMessage("로그인 정보가 틀립니다.");

                //코루틴 진행을 중단함
                yield break;
            }

            //로그인이 성공했다면
            
            //로그로 유저 아이디를 출력함
            Dictionary<string, object> userData =
                responseData["data"] as Dictionary<string, object>;

            //유저 데이터 딕셔너리 객체를 다시 json 문자열로 변경함
            string json_userData = MiniJSON.jsonEncode(userData);

            //유저의 데이터 정보를 Playerprefs에 저장함
            PlayerPrefs.SetString("USER_DATA", json_userData);
            PlayerPrefs.Save();

            //유저 데이터만 포함된 json 문자열을 출력함
            Debug.Log("유저 데이터 => " + json_userData);

            SceneManager.LoadScene("Game");
        }
        else
        {
            _infoMessagePopup.ShowMessage("서버와의 연결이 실패했습니다.");
        }
    }

    void ClearInputField()
    {
        _userIdInputField.text = _userPwInputField.text = string.Empty;
    }
}
