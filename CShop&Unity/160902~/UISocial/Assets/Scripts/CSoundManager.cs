﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class CSoundManager : MonoBehaviour {

    public GameObject _soundConfigPanel;
    public float _effectSoundVol = 1f; //효과음 볼륨

    public AudioSource _bgmAudioSource;

    void Awake()
    {
        _bgmAudioSource = GetComponent<AudioSource>();
    }

    //즉흥 사운드를 재생함
    public void PlayOnSound(AudioClip clip)
    {
        //즉흥적으로 사운드를 재생함(스스로 소멸함)
        AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, _effectSoundVol);
    }

	// Use this for initialization
	void Start () {
        SoundConfigLoad();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //사운드 설정창을 실행함
    public void OnOpenSoundPanel()
    {
        _soundConfigPanel.SetActive(true);
    }

    //사운드 설정창을 종료함
    public void OnCloseSoundConfigPanel()
    {
        SoundConfigSave();

        _soundConfigPanel.SetActive(false);
    }

    //배경음 볼륨 조절
    public void OnChangeBgmVolume(Slider volSlider)
    {
        //조절한 슬라이더의 값을 배경음의 볼륨에 적용함
        _bgmAudioSource.volume = volSlider.value;
    }

    //효과음 볼륨 조절
    public void OnChangeEffectVolume(Slider volSlider)
    {
        _effectSoundVol = volSlider.value;
    }

    //배경음 일시 정지 설정
    public void OnOffBgmSound(Toggle onoffToggle)
    {
        _bgmAudioSource.mute = onoffToggle.isOn;
    }

    public void SoundConfigSave()
    {
        //배경음 볼륨 저장
        PlayerPrefs.SetFloat("BGM_VOL", _bgmAudioSource.volume);
        //효과음 볼륨 저장
        PlayerPrefs.SetFloat("EFFECT_VOL", _effectSoundVol);
        //배경음 일시 정지 여부 저장
        PlayerPrefs.SetString("BGM_MUTE", _bgmAudioSource.mute.ToString());
        PlayerPrefs.Save();
    }

    public void SoundConfigLoad()
    {
        //저장된 사운드 설정 정보를 불러옴
        _bgmAudioSource.volume = PlayerPrefs.GetFloat("BGM_VOL", 1f);
        _effectSoundVol = PlayerPrefs.GetFloat("EFFECT_VOL", 1f);
        string strMute = PlayerPrefs.GetString("BGM_MUTE", "False");
        _bgmAudioSource.mute = bool.Parse(strMute);

        //추후 UI Slider와 Toggle도 셋팅해줘야 한다.
    }
}
