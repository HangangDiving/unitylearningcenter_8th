﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using GameData;

//회원 가입 매니저
public class CJoinManager : MonoBehaviour {

    public InputField _userIdInputField;
    public InputField _userPwInputField;
    public InputField _userRePwInputField;

    public Text _joinLogText;
    public CInfoMessagePopup _infoMessagePopup;

    // Use this for initialization
    void Start () {
        _joinLogText.text = string.Empty;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //회원 가입 처리 버튼 클릭
    public void OnOkJoinButtonClick()
    {
        //가입 정보 누락 체크
        if (_userIdInputField.text.Length <= 0 ||
            _userPwInputField.text.Length <= 0 ||
            _userRePwInputField.text.Length <= 0)
        {
            _joinLogText.text = "가입 정보 모두 입력하세요.";
            return;
        }
        //비밀번호 확인 체크
        else if (!_userPwInputField.text.Equals(_userRePwInputField.text))
        {
            _joinLogText.text = "비밀번호 확인 정보가 틀립니다.";
            return;
        }

        StopCoroutine("JoinNetCoroutine");
        StartCoroutine("JoinNetCoroutine");
    }

    private IEnumerator JoinNetCoroutine()
    {
        //회원가입을 요청함
        string url = "http://52.78.144.8/uisample/join.php";

        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("user_id", _userIdInputField.text);
        wwwForm.AddField("user_pw", _userPwInputField.text);

        WWW www = new WWW(url, wwwForm);

        //요청을 기다림
        yield return www;

        //회원 가입 요청에 대한 응답을 처리함
        if (www.error == null)
        {
            //JSON 데이터를 -> 딕셔너리 객체로 생성
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text) as Dictionary<string, object>;

            //응답 데이터에서 결과 코드값을 추출함
            string code = responseData["code"].ToString();

            //회원 가입이 실패했다면
            if (code.Equals("JOIN_FAIL"))
            {
                //회원 가입 실패 메시지 팝업을 띄움
                _joinLogText.text = "회원 가입 요청에 실패했습니다.";

                //코루틴 진행을 중단함
                yield break;
            }
            //이미 유저 아이디가 존재한다면
            else if (code.Equals("EXIST_USER_ID"))
            {
                //회원 가입 중복 메시지 팝업을 띄움
                _joinLogText.text = "이미 아이디가 존재합니다.";

                //코루틴 진행을 중단함
                yield break;
            }

            gameObject.SetActive(false);

            _infoMessagePopup.ShowMessage("회원 가입을 완료했습니다.");
        }
        else
        {
            _joinLogText.text = "서버와의 연결이 실패했습니다.";
        }
    }

    //회원 가입 취소 버튼 클릭
    public void OnCancelJoinPopupButtonClick()
    {
        gameObject.SetActive(false);
    }
}
