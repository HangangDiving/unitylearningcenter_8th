﻿using UnityEngine;
using System.Collections;

public class CSlime : MonoBehaviour {

    public GameObject _slimePrefab;
    public float _speed = 5f;
    CGameManager _gameManager;

    // Use this for initialization
    void Start () {

        _gameManager = GameObject.Find("GameManager").GetComponent<CGameManager>();

        name = "Slime";

        //콜라이더를 비활성화 함
        GetComponent<BoxCollider2D>().enabled = true;
        //스프라이트 비활성화 함
        GetComponent<SpriteRenderer>().enabled = true;

        GetComponent<Rigidbody2D>().velocity =
            Vector2.left * _speed;
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.Equals("Bullet"))
        {
            //점수를 올림
            _gameManager.ReLoadScore();

            StartCoroutine("DestroyAndCreateCoroutine");
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.name.Equals("Background"))
        {
            StartCoroutine("DestroyAndCreateCoroutine");
        }
    }

    private IEnumerator DestroyAndCreateCoroutine()
    {
        //콜라이더를 비활성화 함
        GetComponent<BoxCollider2D>().enabled = false;
        //스프라이트 비활성화 함
        GetComponent<SpriteRenderer>().enabled = false;
                
        yield return new WaitForSeconds(2f);

        //새로운 슬라임을 생성함
        float randHeight = Random.Range(-4f, 2f);
        Instantiate(_slimePrefab, new Vector2(7f, randHeight), Quaternion.identity);

        //슬라임을 파괴함
        Destroy(gameObject);
    }
}
