﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CRankManager : MonoBehaviour {

    public GameObject _rowInfoPanelPrefab;

    public Transform _scrollViewContentTr;

	// Use this for initialization
	void Start () {
        StartCoroutine("RankingNetCoroutine");
	}

    private IEnumerator RankingNetCoroutine()
    {
        string url = "http://52.78.144.8/uisample/rank.php";

        //회원 가입에 대한 Request를 요청함
        WWW www = new WWW(url);
        yield return www;

        if (www.error == null)
        {
            Dictionary<string, object> resData =
                GameData.MiniJSON.jsonDecode(www.text) as Dictionary<string, object>;

            string code = resData["code"].ToString();

            if (code.Trim().Equals("RANK_SUCCESS"))
            {
                List<object> ranking_list = resData["rank"] as List<object>;

                //Debug.Log("ranking list => " + www.text);
                Debug.Log("ranking list count => " + ranking_list.Count.ToString());

                //Content 뷰의 크기 설정
                _scrollViewContentTr.GetComponent<RectTransform>().sizeDelta =
                    new Vector2(_scrollViewContentTr.GetComponent<RectTransform>().sizeDelta.x, 60f * ranking_list.Count);

                float posY = 0f;
                foreach (object row in ranking_list)
                {
                    Dictionary<string, object> rowInfoDic = row as Dictionary<string, object>;

                    GameObject rowPanel =
                        Instantiate(_rowInfoPanelPrefab, Vector2.zero, Quaternion.identity) as GameObject;

                    //새로 생성한 로우 패널 오브젝트를 스크롤뷰의 Content의 자식으로 설정함
                    rowPanel.transform.SetParent(_scrollViewContentTr, false);

                    rowPanel.GetComponent<RectTransform>().localPosition =
                        new Vector2(0f, posY);
                    posY -= 60f;

                    //로우 패널에 정보를 출력함
                    rowPanel.SendMessage("SetRowInfo", rowInfoDic);
                }
            }
            else
            {
                Debug.Log("순위 조회 실패");
            }
        }
        else
        {
            Debug.Log("순위 조회 통신 실패");
        }
    }
}
