﻿using UnityEngine;
using System.Collections;

public class CPlayer : MonoBehaviour {

    public GameObject _bulletPrefab;
    public Transform _shotPos;
    public float _speed = 5f;

    //사운드 매니저 참조
    CSoundManager _soundManager;
    //발포 소리 사운드 파일
    public AudioClip _shotSound;

	// Use this for initialization
	void Start () {
        _soundManager =
            GameObject.Find("SoundManager").GetComponent<CSoundManager>();
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        transform.Translate(new Vector2(h, v) * _speed * Time.deltaTime);
        
        //아무 키를 누를 경우
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            //발포 소리 재생
            _soundManager.PlayOnSound(_shotSound);

            //총알을 발포함
            Instantiate(_bulletPrefab, _shotPos.position, Quaternion.identity);
        }
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        //슬라임과 충돌하면
        if (collider.name.Equals("Slime"))
        {
            //게임을 종료함
            GameObject.Find("GameManager").SendMessage("GameEnd");
        }
    }
}
