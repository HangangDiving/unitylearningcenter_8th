﻿using UnityEngine;
using System.Collections;

using GameData;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CGameManager : MonoBehaviour {

    public GameObject[] _playerPrefabs;
    public Text _scoreText;
    public Text _bestScoreText;
    public string _userId;

    void StaticResourceLoad(int ctype, float speed)
    {
        GameObject player = null;

        //캐릭터를 생성함
        player = Instantiate(_playerPrefabs[ctype],
            new Vector2(-6f, 0f),
            Quaternion.identity) as GameObject;

        //캐릭터의 속도를 변경함
        player.GetComponent<CPlayer>()._speed = speed;
    }

    // Use this for initialization
    void Start()
    {
        //Playerprefs에 저장된 유저의 데이터를 로드함
        string strUserData = PlayerPrefs.GetString("USER_DATA");

        Dictionary<string, object> userData =
            MiniJSON.jsonDecode(strUserData) as Dictionary<string, object>;

        _scoreText.text = "0";
        _bestScoreText.text = userData["best_score"].ToString();

        _userId = userData["user_id"].ToString();

        //캐릭터 타입을 추출함
        int ctype = int.Parse(userData["ctype"].ToString());

        //캐릭터 속도를 추출함
        float speed = float.Parse(userData["speed"].ToString());

        //정적 로드 (프리팹을 미리 로드하고 있어야 함)
        //StaticResourceLoad(ctype, speed);
        
        //동적 로드 (프리팹이 필요할 때 동적 로드함)
        DynamicResourceLoad(ctype, speed);
    }

    //동적 리소스 로드
    void DynamicResourceLoad(int ctype, float speed)
    {
        GameObject playerPrefab = null;
        GameObject player = null;

        switch (ctype)
        {
            //Resources.Load(Assets/Resources를 루트로 한 하위경로 및 파일명)
            case 0:
                playerPrefab = Resources.Load("RedPlayer") as GameObject;
                break;
            case 1:
                playerPrefab = Resources.Load("BluePlayer") as GameObject;
                break;
            case 2:
                playerPrefab = Resources.Load("GreenPlayer") as GameObject;
                break;
        }

        //캐릭터를 생성함
        player = Instantiate(playerPrefab,
            new Vector2(-6f, 0f),
            Quaternion.identity) as GameObject;

        //캐릭터의 속도를 변경함
        player.GetComponent<CPlayer>()._speed = speed;
    }

    //점수 UI 갱신
    public void ReLoadScore()
    {
        int score = int.Parse(_scoreText.text);
        int bestScore = int.Parse(_bestScoreText.text);

        score++;
        if (bestScore < score)
        {
            bestScore = score;
        }

        _scoreText.text = score.ToString();
        _bestScoreText.text = bestScore.ToString();
    }

    public void GameEnd()
    {
        Debug.Log("게임 종료 함");
        StartCoroutine("GameScoreUpdetaNetCoroutine");
    }

    IEnumerator GameScoreUpdetaNetCoroutine()
    {
        //로그인을 요청함
        string url = "http://52.78.144.8/uisample/update.php";

        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("user_id", _userId);
        wwwForm.AddField("score", _scoreText.text);
        wwwForm.AddField("best_score", _bestScoreText.text);

        WWW www = new WWW(url, wwwForm);

        //요청을 기다림
        yield return www;

        //로그인 요청에 대한 응답을 처리함
        if (www.error == null)
        {
            //JSON 데이터를 -> 딕셔너리 객체로 생성
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text) as Dictionary<string, object>;

            //응답 데이터에서 결과 코드값을 추출함
            string code = responseData["code"].ToString();

            //로그인이 실패했다면
            if (code.Equals("UPDATE_FAIL"))
            {
                Debug.Log("점수 업데이트 실패");

                //코루틴 진행을 중단함
                yield break;
            }
            else
            {
                Debug.Log("점수 업데이트 성공");
            }

            SceneManager.LoadScene("Rank");
        }
        else
        {
            Debug.Log("서버와의 연결이 실패했습니다.");
        }
    }
}
