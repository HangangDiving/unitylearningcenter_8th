﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

//행 패널 정보 클래스
public class CRowInfo : MonoBehaviour {

    public Text _userIdText;
    public Text _userBestScoreText;

    public void SetRowInfo(Dictionary<string, object> rowInfoDic)
    {
        _userIdText.text = rowInfoDic["user_id"].ToString();
        _userBestScoreText.text = rowInfoDic["best_score"].ToString();
    }
}
