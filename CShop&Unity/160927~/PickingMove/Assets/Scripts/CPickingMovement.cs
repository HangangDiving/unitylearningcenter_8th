﻿using UnityEngine;
using System.Collections;

public class CPickingMovement : MonoBehaviour {

    public enum STATE { IDLE, MOVE };
    public STATE _state = STATE.IDLE;

    int _layerMask; //충돌 정보

    //충돌 레이어 이름 배열
    public string[] _layerMaskNames = { "Ground", "Object" };

    Animator _animator;
    public float _animSpeed;

    NavMeshAgent _navMeshAgent; //네비게이션
    Vector3 _moveTargetPos; //이동할 목적지 위치

    void Awake()
    {
        _layerMask = LayerMask.GetMask(_layerMaskNames);

        _animator = GetComponent<Animator>();
        _animator.speed = _animSpeed;

        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

	// Use this for initialization
	void Start () {
        _navMeshAgent.Stop();
	}
	
	// Update is called once per frame
	void Update () {

        //터치 또는 클릭
        if (Input.GetMouseButtonDown(0))
        {
            //카메라에서 수직으로 된 충돌 레이를 생성함
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //레이와 충돌된 정보
            RaycastHit hitInfo;

            //Physics.Raycast(레이정보, 충돌된 정보(out), 레이체크 길이)
            if (Physics.Raycast(ray, out hitInfo, 100f))
            {                
                //레이와 충돌한 레이어 정보
                int layer = hitInfo.transform.gameObject.layer;

                //터치한 대상이 장애물과 같은 오브젝트면 이동을 하지 않음
                if (layer == LayerMask.NameToLayer("Object")) return;

                //레이 충돌 위치
                _moveTargetPos = hitInfo.point;

                //레이가 바닥에 충돌함
                if (layer == LayerMask.NameToLayer("Ground"))
                {
                    Move();
                }
            }
        }
        else
        {
            //이동 목적지간의 거리가
            float dist = Vector3.Distance(transform.position, _moveTargetPos);

            //0.3 이하면
            if (_state == STATE.MOVE && dist <= 0.3f)
            {
                //정지함
                Stop();
            }
        }

	}

    void Move()
    {
        //새로운 목적지로 이동하기 위해 현재 이동을 중단함
        _navMeshAgent.Stop();

        //이동 애니메이션으로 변경함
        PlayAnimation(STATE.MOVE);

        //새로운 목적지를 설정함
        _navMeshAgent.SetDestination(_moveTargetPos);

        //이동 시작
        _navMeshAgent.Resume();
    }

    void Stop()
    {
        //이동 애니메이션으로 변경함
        PlayAnimation(STATE.IDLE);

        //현재 이동을 중단함
        _navMeshAgent.Stop();
    }

    void PlayAnimation(STATE state)
    {
        _state = state;

        switch (_state)
        {
            case STATE.IDLE:
                _animator.SetBool("Move", false);
                break;
            case STATE.MOVE:
                _animator.SetBool("Move", true);
                break;
        }
    }
}
