﻿using UnityEngine;
using System.Collections;

//Ui 사용
using UnityEngine.UI;

public class CGameManager : MonoBehaviour {

    //게임 시간 (점수)
    public static int timeCount = 0;
    public Text _timeCountText;

	// Use this for initialization
	void Start () {
        //Invoke("Timer", 5f); //5초 뒤에 Timer함수를 1회 실행함
        InvokeRepeating("Timer", 1f, 1f);
	}

    //타이머 메소드
    void Timer()
    {
        timeCount++;
        this._timeCountText.text = timeCount.ToString();
    }
}
