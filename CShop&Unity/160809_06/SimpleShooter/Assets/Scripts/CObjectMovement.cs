﻿using UnityEngine;
using System.Collections;

public class CObjectMovement : MonoBehaviour {

    public float _speed = 5f; //이동속도
    private Vector2 _dir; //이동 방향
    private Transform _playerTransform; //플레이어 위치

	// Use this for initialization
	void Start () {

        //로드된 오브젝트를 참조함
        //GameObject obj = GameObject.Find("오브젝트이름");
        this._playerTransform = GameObject.Find("PlayerShip").GetComponent<Transform>();

        //방향 구하기
        //방향 = (목적지 위치 - 현재 위치).normalized;
        this._dir = this._playerTransform.position - this.transform.position;
        //Debug.Log("target direction vector : " + _dir.normalized.ToString());
	}
	
	// Update is called once per frame
	void Update () {

        //오브젝트를 이동함
        this.transform.Translate( this._dir.normalized * this._speed * Time.deltaTime);
	}
}