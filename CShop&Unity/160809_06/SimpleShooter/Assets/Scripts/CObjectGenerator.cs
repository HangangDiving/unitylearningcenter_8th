﻿using UnityEngine;
using System.Collections;

//운석 생성기
public class CObjectGenerator : MonoBehaviour {

    //생성 지연시간(최소, 최대)
    public float _delayMinTime = 1.5f;
    public float _delayMaxTime = 4.5f;

    //운석 프리팹
    //프리팹 : 게임오브젝트를 파일로 구워놓은 것
    public GameObject[] _objectPrefabs;

    // Use this for initialization
    void Start () {
        StartCoroutine("GenObjectCoroutine");
	}

    //게임오브젝트 생성 타이머(코루틴)
    private IEnumerator GenObjectCoroutine()
    {
        while (true)
        {
            //한번 생성 시 지연시간을 랜덤하게 설정함
            float delayTime = Random.Range(_delayMinTime, _delayMaxTime);

            //생성 지연
            yield return new WaitForSeconds(delayTime);

            int randNum = Random.Range(0, this._objectPrefabs.Length);

            //[유니티 동적 생성]
            //Instantiate(프리팹오브젝트, 위치, 기준 회전축);
            Instantiate(this._objectPrefabs[randNum], this.transform.position, Quaternion.identity);
        }
    }
}
