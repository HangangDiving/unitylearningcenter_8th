﻿using UnityEngine;
using System.Collections;

public class CObjectCollision : MonoBehaviour {

    //운석이 Trigger 충돌이 끝남
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.name.Equals("Main Camera"))
        {
            Destroy(this.gameObject);
        }
    }
}
