#include<iostream>
//난수 추출에 사용
#include<stdlib.h>
#include<time.h>
#include<windows.h>
#include<string>

using namespace std;

void main()
{
	//const : 상수형(읽기만 가능한) 메모리를 생성하는 키워드
	const int rottoBallCount = 6;
	const int rottoBallNumCount = 45;
	//rottoBallCount = 10; //읽기만 가능하므로 오류 발생
	const int gameDelayTime = 100;
	int rottoBalls[rottoBallCount];
	int rottoBallCounts[45] = { 0, }; //

	srand((unsigned int)time(NULL));

	int count = 0;
	cout << "추첨횟수 입력 : >>> "; cin >> count;

	//
	for (int l = 0; l < count; l++)
	{
		cout << l + 1 << "번째 볼 추첨 번호 : ";
		for (int i = 0; i < rottoBallCount; i++)
		{
			//
			rottoBalls[i] = (rand() % 45) + 1;

			//

			bool isSame = false;
			for (int j = 0; j < i; j++)
			{
				if (rottoBalls[i] == rottoBalls[j])
				{
					isSame = true;
					i--;
					break;
				}
			}

			if (isSame) continue;

			//로또 볼 빈도수 카운터 증가
			//int ballNum = rottoBalls[i] - 1;
			//rottoBallCounts[ballNum]++;
			rottoBallCounts[rottoBalls[i] - 1]++;
		}

		//[버블 정렬]
		//로또 볼 정렬
		int temp = 0;
		for (int i = 0; i < rottoBallCount - 1; i++)
		{
			for (int j = 0; j < rottoBallCount - 1 - i; j++)
			{
				if (rottoBalls[j] > rottoBalls[j + 1])
				{
					temp = rottoBalls[j];
					rottoBalls[j] = rottoBalls[j + 1];
					rottoBalls[j + 1] = temp;
				}
			}
		}

		for (int i = 0; i < rottoBallCount; i++)
		{
			cout << rottoBalls[i] << ((i < 5) ? ", " : "");
		}
		cout << endl;
	}

	cout << "[빈도수 정보 : 볼번호(빈도수)]" << endl;

	for (int i = 0; i < rottoBallNumCount; i++)
	{
		cout << i + 1 << "(" << rottoBallCounts[i];
		(i == (rottoBallNumCount - 1)) ? (cout << ")") : (cout << "), ");
		((i + 1) % 5 == 0) ? (cout << endl) : (cout << "");
	}

	//추첨 행운 번호
	int orderBalls[45];
	for (int i = 0; i < 45; i++)
	{
		orderBalls[i] = i + 1;
	}

	//볼 빈도수 배열을 정렬하면서 추첨 행운 번호 배열의 번호를 빈도수에 맞게 재배치함
	
	int temp = 0;
	int tempNum = 0;
	for (int i = 0; i < rottoBallNumCount; i++)
	{
		for (int j = 0; j < rottoBallNumCount - 1 - i; j++)
		{
			if (rottoBallCounts[j] < rottoBallCounts[j + 1])
			{
				temp = rottoBallCounts[j];
				rottoBallCounts[j] = rottoBallCounts[j + 1];
				rottoBallCounts[j + 1] = temp;

				tempNum = orderBalls[j];
				orderBalls[j] = orderBalls[j + 1];
				orderBalls[j + 1] = tempNum;
			}
		}
	}

	//
	cout << "행운의 추첨 번호 : ";
	for (int i = 0; i < rottoBallCount; i++)
	{
		cout << orderBalls[i] << ((i < 5) ? ", " : "");
	}
	cout << endl;
}