﻿using UnityEngine;
using System.Collections;

public class CPlayerShipMovement : MonoBehaviour {

    public float _speed = 5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        //입력에 따른 이동
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        this.transform.Translate( (new Vector2(h, v) * this._speed) * Time.deltaTime);

        //스크린 내부에 고정
        Vector2 fixVec = this.transform.position;
        if (this.transform.position.x > 6f)
            fixVec.x = 6f;
        else if(this.transform.position.x < -6f)
            fixVec.x = -6f;
        if (this.transform.position.y > 4.5f)
            fixVec.y = 4.5f;
        else if (this.transform.position.y < -4.5f)
            fixVec.y = -4.5f;
        this.transform.position = fixVec;
    }
}
