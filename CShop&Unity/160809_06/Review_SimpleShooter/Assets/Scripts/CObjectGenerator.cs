﻿using UnityEngine;
using System.Collections;

public class CObjectGenerator : MonoBehaviour {

    public GameObject[] _objectPrefabs;
    public float _delayMinTime = 5f;
    public float _delayMaxTime = 10f;

    private bool _isSpeedControl = false;
    private float _newSpeed;

    // Use this for initialization
    void Start () {
        StartCoroutine("GenerateCoroutine");
	}

    private IEnumerator GenerateCoroutine()
    {
        while (true)
        {
            float randDelayTime = Random.Range(this._delayMinTime, this._delayMaxTime);
            int randObjIdx = Random.Range(0, this._objectPrefabs.Length);

            yield return new WaitForSeconds(randDelayTime);
            
            GameObject newGameObj =
                Instantiate(this._objectPrefabs[randObjIdx], this.transform.position, Quaternion.identity) as GameObject;

            if (this._isSpeedControl)
            {
                CObjectMovement objectMovement = newGameObj.GetComponent<CObjectMovement>();
                objectMovement.SpeedDown(this._newSpeed);
            }

        }
    }

    public void SetSpeedOfNewObject(float newSpeed)
    {
        this._isSpeedControl = true;
        this._newSpeed = newSpeed;
    }

    public void DisableSpeedControl()
    {
        this._isSpeedControl = false;
    }
}
