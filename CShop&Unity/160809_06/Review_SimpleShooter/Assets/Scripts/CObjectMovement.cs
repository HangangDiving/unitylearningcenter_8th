﻿using UnityEngine;
using System.Collections;

public class CObjectMovement : MonoBehaviour {

    public float _speed = 5f;
    private float _backupSpeed;
    private Transform _playerTransform;
    private Vector2 _dir;

	// Use this for initialization
	void Start () {
        this._playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        this._dir = this._playerTransform.position - this.transform.position;
        this._dir.Normalize();
        this._backupSpeed = this._speed;
    }
	
	// Update is called once per frame
	void Update () {

        this.transform.Translate(this._dir * this._speed * Time.deltaTime);

	}

    public void SpeedDown(float newSpeed)
    {
        this._speed = newSpeed;
    }

    public void TurnBackSpeed()
    {
        this._speed = this._backupSpeed;
    }
}
