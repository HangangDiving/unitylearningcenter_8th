﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CEndManager : MonoBehaviour {

    public Text _timeText;

	// Use this for initialization
	void Start () {
        this._timeText.text = CGameManager.timeCount.ToString();   
	}		
}
