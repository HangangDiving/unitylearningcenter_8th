﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CGameManager : MonoBehaviour {

    public static int timeCount = 0;

    public Text _timeCountText;

	// Use this for initialization
	void Start () {
        InvokeRepeating("TimeCount", 1f, 1f);
	}

    private void TimeCount()
    {
        _timeCountText.text = (++timeCount).ToString();
    }	
}
