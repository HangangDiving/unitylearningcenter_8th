﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CPlayerShipCollision : MonoBehaviour {

    public CShield _shield;
    public float _speedDownNum = 3f;
    public float _speedDownDelayTime = 3f;

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Meteor":
                {
                    //if (!this._shield.gameObject.activeSelf)
                    {
                        Destroy(col.gameObject);
                        SceneManager.LoadScene("End");
                    }
                }
                break;
            case "DestroyAllMeteor":
                {
                    Destroy(col.gameObject);
                    GameObject[] findObjs = GameObject.FindGameObjectsWithTag("Meteor");
                    for (int i = 0; i < findObjs.Length; i++)
                        Destroy(findObjs[i]);
                }
                break;
            case "Shield":
                { 
                    Destroy(col.gameObject);
                    if (this._shield.gameObject.activeSelf)
                        this._shield.AddShieldTime();
                    else
                        this._shield.gameObject.SetActive(true);
                }
                break;
            case "SpeedDown":
                {
                    Destroy(col.gameObject);

                    GameObject[] findObjs = GameObject.FindGameObjectsWithTag("MeteorGenerator");
                    for (int i = 0; i < findObjs.Length; i++)
                    {
                        CObjectGenerator objectGenerator = findObjs[i].GetComponent<CObjectGenerator>();
                        objectGenerator.SetSpeedOfNewObject(this._speedDownNum);
                    }

                    findObjs = GameObject.FindGameObjectsWithTag("Meteor");
                    for (int i = 0; i < findObjs.Length; i++)
                    {
                        CObjectMovement objectMovement = findObjs[i].GetComponent<CObjectMovement>();
                        objectMovement.SpeedDown(this._speedDownNum);
                    }

                    //if (IsInvoking("ReleaseSpeedDown"))
                    //    CancelInvoke("ReleaseSpeedDown");
                    //Invoke("ReleaseSpeedDown", this._speedDownDelayTime);
                    StopCoroutine("ReleaseSpeedDownCoroutine");
                    StartCoroutine("ReleaseSpeedDownCoroutine");
                }
                break;
        }
    }

    private IEnumerator ReleaseSpeedDownCoroutine()
    {
        yield return new WaitForSeconds(this._speedDownDelayTime);

        GameObject[] findObjs = GameObject.FindGameObjectsWithTag("MeteorGenerator");
        for (int i = 0; i < findObjs.Length; i++)
        {
            CObjectGenerator objectGenerator = findObjs[i].GetComponent<CObjectGenerator>();
            objectGenerator.DisableSpeedControl();
        }

        yield return null;

        findObjs = GameObject.FindGameObjectsWithTag("Meteor");
        for (int i = 0; i < findObjs.Length; i++)
        {
            CObjectMovement objectMovement = findObjs[i].GetComponent<CObjectMovement>();
            objectMovement.TurnBackSpeed();
        }
    }

    private void ReleaseSpeedDown()
    {
        GameObject[] findObjs = GameObject.FindGameObjectsWithTag("MeteorGenerator");
        for (int i = 0; i < findObjs.Length; i++)
        {
            CObjectGenerator objectGenerator = findObjs[i].GetComponent<CObjectGenerator>();
            objectGenerator.DisableSpeedControl();
        }

        findObjs = GameObject.FindGameObjectsWithTag("Meteor");
        for (int i = 0; i < findObjs.Length; i++)
        {
            CObjectMovement objectMovement = findObjs[i].GetComponent<CObjectMovement>();
            objectMovement.TurnBackSpeed();
        }
    }

}
