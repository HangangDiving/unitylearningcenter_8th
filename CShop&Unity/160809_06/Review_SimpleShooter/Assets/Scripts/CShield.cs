﻿using UnityEngine;
using System.Collections;

public class CShield : MonoBehaviour {

    public float _delayTime = 3f;

    void OnEnable()
    {
        Invoke("DisableShield", _delayTime);
    }

    public void AddShieldTime()
    {
        if(IsInvoking("DisableShield"))
        {
            CancelInvoke("DisableShield");
            Invoke("DisableShield", _delayTime);
        }
    }

    private void DisableShield()
    {
        this.gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "Meteor":
                Destroy(col.gameObject);
                break;
        }
    }
}
