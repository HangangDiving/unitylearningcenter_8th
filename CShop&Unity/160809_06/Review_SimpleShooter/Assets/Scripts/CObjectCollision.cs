﻿using UnityEngine;
using System.Collections;

public class CObjectCollision : MonoBehaviour {

    void OnTriggerExit2D(Collider2D col)
    {
        switch (col.tag)
        {
            case "MainCamera":
                Destroy(this.gameObject);
                break;
        }
    }
	
}
