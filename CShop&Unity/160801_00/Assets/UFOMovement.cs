﻿using UnityEngine;
using System.Collections;

// C++ 문법
// class UFOMovement : public MonoBehaviour
public class UFOMovement : MonoBehaviour {

    // C++ 문법
    // public:
    //      float speed;
    public float speed;

    //public Transform tr; //변환 객체 참조 변수

    // C++ 문법
    // private :
    //      void Start() { ... }
	private void Start () {
        // printf("문자열, %포맷", 변수);
        // Debug.Log("문자열" + 변수.ToString() + "문자열" + ....);
        this.speed = 7f;
        //Debug.Log("UFO 속도 => " + speed.ToString());
        //Debug.Log("Transform 객체(컴포넌트)의 Position값 출력 = > " + tr.position.ToString());
	}
	
	// 
	private void Update () {
        //Debug.Log("UFO 업데이트 => " + speed.ToString());

        // Input.GetAxis("Horizontal")
        // 리턴값 : float (-1[왼쪽] ~ 0[정지] ~ 1[오른쪽])
        float h = Input.GetAxis("Horizontal");
        //Debug.Log("Input.GetAxis(\"Horizontal\") => " + h.ToString());
        // Input.GetAxis("Vertical")
        // 리턴값 : float (-1[아래] ~ 0[정지] ~ 1[위쪽])
        float v = Input.GetAxis("Vertical");
        //Debug.Log("Input.GetAxis(\"Vertical\") => " + v.ToString());

        // Translate(방향 * 속도 * Time.deltaTime);
        //tr.Translate(Vector2.right * h * speed * Time.deltaTime);
        //tr.Translate(Vector2.up * v * speed * Time.deltaTime);
        transform.Translate(Vector2.right * h * speed * Time.deltaTime);
        transform.Translate(Vector2.up * v * speed * Time.deltaTime);
    }
}
