﻿using UnityEngine;
using System.Collections;

public class CObjectGenerator : MonoBehaviour {

    public GameObject[] _objectPrefabs;
    public float _minDelayTime = 1f;
    public float _maxDelayTime = 5f;

    // Use this for initialization
    void Start () {
        StartCoroutine("GenerateCoroutine");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator GenerateCoroutine()
    {
        while (true)
        {
            float randDelayTime =
                Random.Range(this._minDelayTime, this._maxDelayTime);

            yield return new WaitForSeconds(randDelayTime);

            int randNum = Random.Range(1, 11);

            int index = 0;
            if (randNum <= 3)
                index = 0;
            else if (randNum <= 6)
                index = 1;
            else if (randNum <= 9)
                index = 2;

            Instantiate(this._objectPrefabs[index], this.transform.position, Quaternion.identity);
        }
    }
}
