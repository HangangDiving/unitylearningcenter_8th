﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CGameManager : MonoBehaviour {
        
    private static int starCount = 0;
    public static int StarCount
    {
        get { return starCount; }
        set { starCount = value; }
    }
    
    public Text _starCountText;
    public Image _hpProgressBarImage;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StarCountUp(int score)
    {
        starCount += score;
        this._starCountText.text = starCount.ToString();
    }

    public void SetHp(int currHp)
    {
        this._hpProgressBarImage.fillAmount =
            currHp * 0.01f;
    }
}
