﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CEndManager : MonoBehaviour {

    public Text _countText;

	// Use this for initialization
	void Start () {
        this._countText.text = CGameManager.StarCount.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnReStartButtonClick()
    {
        SceneManager.LoadScene("Demo");
    }
}
