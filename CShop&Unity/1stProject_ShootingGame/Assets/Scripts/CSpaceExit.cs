﻿using UnityEngine;
using System.Collections;

public class CSpaceExit : MonoBehaviour {

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
            return;

        Destroy(col.gameObject);
    }

}
