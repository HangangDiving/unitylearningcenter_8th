﻿using UnityEngine;
using System.Collections;

public class CPlayerShipShot : MonoBehaviour {

    public Transform _shotPosition;
    public GameObject _laserPrefab;
    public int _laserCount = 1;
    public float _laserAngle = 10f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            float currAngle = this._laserAngle * ((this._laserCount - 1) * 0.5f);

            for (int i = 0; i < this._laserCount; i++)
            {
                Quaternion newRotation =
                    Quaternion.AngleAxis(currAngle, Vector3.forward);

                Instantiate(this._laserPrefab,
                        this._shotPosition.position,
                        newRotation);

                currAngle -= this._laserAngle;
            }
        }
	}

    public void LaserCountUp()
    {
        this._laserCount += 2;
    }
}
