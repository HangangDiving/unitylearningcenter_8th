﻿using UnityEngine;
using System.Collections;

public class CMiniShipManager : MonoBehaviour {

    public CMiniShip[] _miniShips;

    public void CreateMiniShip()
    {
        for (int i = 0; i < _miniShips.Length; i++)
        {
            if (!_miniShips[i].gameObject.activeSelf)
            {
                _miniShips[i].gameObject.SetActive(true);
                CFire fire = _miniShips[i].GetComponent<CFire>();
                fire.StartFire();
                break;
            }
        }
    }
}
