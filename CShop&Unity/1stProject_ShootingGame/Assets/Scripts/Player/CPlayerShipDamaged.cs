﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CPlayerShipDamaged : MonoBehaviour {

    public int _hp = 100;
    public CGameManager _gameManager;
    public Animator _animator;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Enemy") || col.tag.Equals("EnemyLaser"))
        {
            //이펙트 주기


            CObjectStat colStat = col.GetComponent<CObjectStat>();
            this.Damaged(colStat._damage);

            if (col.tag.Equals("EnemyLaser"))
                Destroy(col.gameObject);
        }
        else if (col.tag.Equals("Item"))
        {
            CItem item = col.GetComponent<CItem>();
            item.ItemApply();
            Destroy(col.gameObject);
        }
    }

    public void Damaged(int damage)
    {
        this._animator.Play("PlayerShipDamaged");

        this._hp -= damage;

        //게임 매니저 통해서 HP바 조절
        _gameManager.SetHp(this._hp);

        if (this._hp <= 0)
        {
            SceneManager.LoadScene("End");
        }
    }
}
