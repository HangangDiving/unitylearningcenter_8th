﻿using UnityEngine;
using System.Collections;

public class CPlayerShipMove : MonoBehaviour {

    public float _moveSpeed = 5f; //이동속도
    public float _horizontalLimit = 8f; //좌우 이동 제한

	// Use this for initialization
	void Start () {

        if (this._horizontalLimit < 0)
            this._horizontalLimit *= -1;

	}
	
	// Update is called once per frame
	void Update () {

        //입력키 받기
        float h = Input.GetAxis("Horizontal");

        //이동
        this.transform.Translate(Vector2.right * h * this._moveSpeed * Time.deltaTime);

        //이동 제한
        Vector2 currPos = this.transform.position;
        if (currPos.x < -this._horizontalLimit)
            currPos.x = -this._horizontalLimit;
        else if (currPos.x > this._horizontalLimit)
            currPos.x = this._horizontalLimit;
        this.transform.position = currPos;

    }
}
