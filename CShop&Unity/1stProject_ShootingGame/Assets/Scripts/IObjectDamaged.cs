﻿using UnityEngine;
using System.Collections;

public interface IObjectDamaged {

    bool Damaged(int damage);

}
