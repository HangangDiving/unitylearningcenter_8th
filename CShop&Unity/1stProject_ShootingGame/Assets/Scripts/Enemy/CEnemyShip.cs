﻿using UnityEngine;
using System.Collections;
using System;

public class CEnemyShip : CShip {

    public int _starScore = 10; //점수
    private CGameManager _gameManager;

    void Awake()
    {
        this._gameManager = GameObject.FindObjectOfType<CGameManager>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            this.DestroySelf();
        }
        else if (col.tag.Equals("PlayerLaser"))
        {
            CObjectStat colStat = col.GetComponent<CObjectStat>();
            this.Damaged(colStat._damage);
            Destroy(col.gameObject);
        }
    }

    protected override void DestroySelf()
    {
        _gameManager.StarCountUp(this._starScore);

        GameObject effectObj = Instantiate(this._destroyEffectPrefab,
            this.transform.position,
            Quaternion.identity) as GameObject;

        Destroy(effectObj, 0.3f);

        Destroy(this.gameObject);
    }
}
