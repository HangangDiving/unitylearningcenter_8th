﻿using UnityEngine;
using System.Collections;

public class CItemEnemyShip : CEnemyShip {

    public GameObject[] _dropItemPrefabs;

    protected override void DestroySelf()
    {
        int randNum = Random.Range(0, this._dropItemPrefabs.Length);

        Instantiate(this._dropItemPrefabs[randNum],
            this.transform.position,
            Quaternion.identity);

        base.DestroySelf();
    }

}
