﻿using UnityEngine;
using System.Collections;

public class CFire : MonoBehaviour {

    public Transform[] _shotPositions;
    public GameObject _laserPrefab;
    public float _fireDelayTime = 3f;

	// Use this for initialization
	void Start () {
        StartFire();
    }

    protected virtual void FireInvoke()
    {
        for (int i = 0; i < this._shotPositions.Length; i++)
        {
            Instantiate(this._laserPrefab,
                this._shotPositions[i].position,
                Quaternion.identity);
        }
    }

    public void StartFire()
    {
        InvokeRepeating("FireInvoke", this._fireDelayTime * 0.5f, this._fireDelayTime);
    }

    public void StopFire()
    {
        if (IsInvoking("FireInvoke"))
            CancelInvoke("FireInvoke");
    }
}
