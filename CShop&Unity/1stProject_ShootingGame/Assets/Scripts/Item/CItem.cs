﻿using UnityEngine;
using System.Collections;

public abstract class CItem : MonoBehaviour {

    public GameObject _playerShip;

    void Start()
    {
        _playerShip = GameObject.FindGameObjectWithTag("Player");
    }

    public abstract void ItemApply();
}
