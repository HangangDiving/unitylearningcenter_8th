﻿using UnityEngine;
using System.Collections;
using System;

public class CLaserUpItem : CItem
{
    public override void ItemApply()
    {
        CPlayerShipShot playerShipShot =
            this._playerShip.GetComponent<CPlayerShipShot>();

        playerShipShot.LaserCountUp();
    }
}
