﻿using UnityEngine;
using System.Collections;
using System;

public class CAllDestroyItem : CItem {

    public override void ItemApply()
    {
        GameObject[] enemyObjs =
            GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < enemyObjs.Length; i++)
        {
            CEnemyShip enemyShip = enemyObjs[i].GetComponent<CEnemyShip>();
            enemyShip.Damaged(10000);
        }

        GameObject[] enemyLaserObjs =
            GameObject.FindGameObjectsWithTag("EnemyLaser");
        for (int i = 0; i < enemyLaserObjs.Length; i++)
        {
            Destroy(enemyLaserObjs[i]);
        }
    }
}
