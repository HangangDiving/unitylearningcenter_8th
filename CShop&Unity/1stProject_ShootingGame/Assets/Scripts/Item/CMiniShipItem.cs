﻿using UnityEngine;
using System.Collections;
using System;

public class CMiniShipItem : CItem
{
    public override void ItemApply()
    {
        CMiniShipManager miniShipmgr =
            this._playerShip.GetComponent<CMiniShipManager>();

        miniShipmgr.CreateMiniShip();
    }
}
