﻿using UnityEngine;
using System.Collections;

public class CMiniShipFire : CFire {

    void OnEnable()
    {
        this.StartFire();
    }
        
    protected override void FireInvoke()
    {
        for (int i = 0; i < this._shotPositions.Length; i++)
        {            
            GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
            Transform currTarget = null;
            if (enemys.Length > 0)
            {

                currTarget = enemys[0].transform;
                for (int j = 1; j < enemys.Length; j++)
                {
                    if (currTarget.position.y > enemys[i].transform.position.y)
                    {
                        if (Vector2.Distance(this.transform.position, currTarget.position)
                            > Vector2.Distance(this.transform.position, enemys[i].transform.position))
                            currTarget = enemys[i].transform;
                    }
                }
            }
            GameObject gameObj = Instantiate(this._laserPrefab,
                this._shotPositions[i].position,
                Quaternion.identity) as GameObject;

            CGuidedLaser guidedLaser = gameObj.GetComponent<CGuidedLaser>();
            guidedLaser._targetTM = currTarget;
        }        

    }

}
