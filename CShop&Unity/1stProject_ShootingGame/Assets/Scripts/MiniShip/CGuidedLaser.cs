﻿using UnityEngine;
using System.Collections;

public class CGuidedLaser : MonoBehaviour {

    public float _moveSpeed = 5f;
    public Transform _targetTM;
    private Vector2 _currDir;

	// Use this for initialization
	void Start () {
        _currDir = Vector2.up;
    }
	
	// Update is called once per frame
	void Update () {

        if (_targetTM != null)
        {
            Vector2 dir = this._targetTM.position - this.transform.position;
            dir.Normalize();
            this.transform.Translate(dir * this._moveSpeed * Time.deltaTime);
            _currDir = dir;
        }
        else
        {
            this.transform.Translate(_currDir * this._moveSpeed * Time.deltaTime);
        }

	}
}
