﻿using UnityEngine;
using System.Collections;

public class CMiniShip : CShip {

    private int _originalHp;
    public CFire _fire;

    void Start()
    {
        this._originalHp = this._stat._hp;
    }

    void OnEnable()
    {
        this._stat._hp = this._originalHp;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Enemy") || col.tag.Equals("EnemyLaser"))
        {
            CObjectStat colStat = col.GetComponent<CObjectStat>();
            this.Damaged(colStat._damage);
        }
    }

    protected override void DestroySelf()
    {
        GameObject effectObj = Instantiate(this._destroyEffectPrefab,
            this.transform.position,
            Quaternion.identity) as GameObject;

        Destroy(effectObj, 0.3f);

        _fire.StopFire();

        this.gameObject.SetActive(false);
    }
}
