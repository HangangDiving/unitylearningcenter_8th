﻿using UnityEngine;
using System.Collections;

public class CShip : MonoBehaviour {

    public CObjectStat _stat;
    public GameObject _destroyEffectPrefab;
    
    public void Damaged(int damage)
    {
        this._stat._hp -= damage;
        if (this._stat._hp <= 0)
        {
            this.DestroySelf();
        }
    }

    protected virtual void DestroySelf()
    {
        GameObject effectObj = Instantiate(this._destroyEffectPrefab,
            this.transform.position,
            Quaternion.identity) as GameObject;

        Destroy(effectObj, 0.3f);

        Destroy(this.gameObject);
    }

}
