﻿using UnityEngine;
using System.Collections;
using System;

public class CObjectMove : MonoBehaviour{

    public Vector2 _direction; //방향
    public float _moveSpeed; //이동속도

    // Use this for initialization
    void Start () {
        if (this._moveSpeed < 0)
            this._moveSpeed *= -1f;
    }
	
	// Update is called once per frame
	void Update () {

        //설정한 방향과 속도에 따른 이동
        this.transform.Translate(this._direction * this._moveSpeed * Time.deltaTime, Space.Self);

	}
}
