﻿using UnityEngine;
using System.Collections;

//캐릭터 상태
public class CCharacterState : MonoBehaviour {

    public LayerMask _targetMask;       //충돌 레이어
    public float _hp;                   //체력 상태
    public bool _isDie = false;         //사망 여부
    public bool _isRightDir = false;    //시선

    //상태
    public enum State
    {
        Idle,   //대기
        Move,   //이동
        Attack, //공격
        Damage, //데미지
        Die     //사망
    }
    public State _state;
	
    public State state
    {
        get { return this._state; }
        set { _state = value; }
    }

    //시선 전환
    public void Flip()
    {
        _isRightDir = !_isRightDir;
    }
}
