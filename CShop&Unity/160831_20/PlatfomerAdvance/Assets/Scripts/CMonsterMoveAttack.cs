﻿using UnityEngine;
using System.Collections;
using System;

//몬스터 스윙 공격
public class CMonsterMoveAttack : CMonsterAttack {

    protected CMonsterTargetChecker _targetChecker; //몬스터 타겟 체크 컴포넌트 참조
    protected IMonsterMove _monsterMove;            //몬스터 이동 컴포넌트 참조

    protected override void Awake()
    {
        base.Awake();

        _targetChecker = GetComponent<CMonsterTargetChecker>();
        _monsterMove = GetComponent<IMonsterMove>();
    }

    // Use this for initialization
    protected override void Start () {

        base.Start();

        //몬스터 공격 타겟 체크
        _targetChecker.FrontTargetCheck(this);
	}

    //공격 준비(시작)
    public override void AttacklReady()
    {
        //이동 정지
        _monsterMove.AttackStop();
        //공격 준비
        base.AttacklReady();
    }

    //공격
    public override void Attack()
    {
        
    }

    //공격 종료
    public override void AttackFinish()
    {
        base.AttackFinish();

        //몬스터 이동 재개
        _monsterMove.MoveResume();
        //몬스터 공격 타겟 체크
        _targetChecker.FrontTargetCheck(this);
    }

    public void AttackAnimationEvent()
    {
        Attack();
    }

    public void AttackFinishAnimationEvent()
    {
        AttackFinish();
    }
}
