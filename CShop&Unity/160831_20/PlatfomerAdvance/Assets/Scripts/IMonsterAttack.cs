﻿using UnityEngine;
using System.Collections;

//몬스터 공격 인터페이스
public interface IMonsterAttack {

    void AttacklReady();    //공격 준비(시작)
    void Attack();          //공격
    void AttackFinish();    //공격 종료

}
