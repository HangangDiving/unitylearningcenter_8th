﻿using UnityEngine;
using System.Collections;

//총알 클래스
public abstract class CBullet : MonoBehaviour, IBullet {

    public float _dirValue;             //총알 발사 방향
    public float _maxSpeed;             //총알 속도
    protected Rigidbody2D _rigidbody2d; //물리 엔진 컴포넌트 참조

    public Object _destroyEffectPrefab; //총알이 파괴될 때 생성할 이펙트

    //총알 초기화
    public virtual void Init(bool isRightDir)
    {
        _rigidbody2d = GetComponent<Rigidbody2D>();

        //방향 설정
        _dirValue = (isRightDir) ? 1 : -1;
    }

    //이동 메소드
    public abstract void Move();

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ground"))
        {
            if (_destroyEffectPrefab != null)
            {
                Instantiate(_destroyEffectPrefab, transform.position, Quaternion.identity);
            }

            Destroy(gameObject);
        }
    }
}
