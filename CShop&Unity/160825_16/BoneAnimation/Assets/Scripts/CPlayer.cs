﻿using UnityEngine;
using System.Collections;

public class CPlayer : MonoBehaviour {

    private Rigidbody2D _rigidbody2d;
    private Animator _animator;
    public float _speed = 5f;
    public Transform _miningPos;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
        this._animator = this.GetComponent<Animator>();
        this._miningPos = this.transform.FindChild("TopBody/LeftArm/Hoe/HitPos");
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (this.IsMining())
            return;

        InputMove();
        InputMining();

	}

    private void InputMove()
    {
        float inputH = Input.GetAxis("Horizontal");

        this.Flip(inputH);

        //this.transform.Translate(Vector2.right * InputH * this._speed * Time.deltaTime);
        this._rigidbody2d.velocity =
            new Vector2(inputH * this._speed, this._rigidbody2d.velocity.y);

        this._animator.SetBool("IsMove",
            (inputH != 0f) ? true : false);
    }

    private void Flip(float Dir)
    {
        Vector2 currScale = this.transform.localScale;

        if (Dir < 0)
            currScale.x = 1;
        else if (Dir > 0)
            currScale.x = -1;

        this.transform.localScale = currScale;
    }

    private void InputMining()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            this._rigidbody2d.velocity = Vector2.zero;
            this._animator.SetTrigger("Mining");
        }
    }

    private void Mining()
    {
        Collider2D[] cols =
            Physics2D.OverlapCircleAll(this._miningPos.position, 0.5f, 1 << LayerMask.NameToLayer("Stone"));

        for (int i = 0; i < cols.Length; i++)
        {
            //if (cols[i].name.Equals("StoneGold"))
            {
                Destroy(cols[i].gameObject);
            }
        }
    }

    public void DoMining()
    {
        this.Mining();
    }

    private bool IsMining()
    {
        return this._animator.GetCurrentAnimatorStateInfo(0).IsName("Mining");
    }
}
