﻿using UnityEngine;
using System.Collections;
using System;

//입력 이동
public class CInputMovement : CMove, IInputMove {

    [HideInInspector]
    public Rigidbody2D _rigidbody2d;    //물리 엔진
    protected Animator _animator;       //애니메이터

    public float _maxSpeed;             //이동 속도
    public float _jumpPower;            //점프 크기
    public bool _isJump = false;        //1단 점프 유무
    public bool _isDoubleJump = false;  //2단 점프 유무
    public bool _isGround;              //지면 위 여부

    protected override void Awake()
    {
        base.Awake();

        _animator = GetComponent<Animator>();
        _rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	void Update () {

        //입력 이동
        InputMove();

        //입력 점프
        InputJump();

	}

    //이동키를 입력하다
    public void InputMove()
    {
        float move = Input.GetAxis("Horizontal");

        if ((_characterState._isRightDir && move < 0)
            || (!_characterState._isRightDir && move > 0))
            Flip();
        
        //이동하다
        Move(move);
    }

    //이동하다
    public void Move(float move)
    {
        //좌우 이동
        _rigidbody2d.velocity = new Vector2(move * _maxSpeed, _rigidbody2d.velocity.y);

        //수평 이동에 대한 애니메이션 수행
        _animator.SetFloat("Horizontal", move);
        //수직 상태에 대한 애니메이션 수행
        _animator.SetFloat("Vertical", _rigidbody2d.velocity.y);
    }

    //이동 관련 충돌 중
    void OnCollisionStay2D(Collision2D collision)
    {
        //지면위에 있음
        if (collision.gameObject.tag.Equals("Ground"))
        {
            Grounding(true);
        }
    }

    //이동 관련 충돌 됨
    void OnCollisionEnter2D(Collision2D collision)
    {
        //지면 위에 착지함
        if (collision.gameObject.tag.Equals("Ground"))
        {
            _isJump = _isDoubleJump = false; //점프 초기화

            Grounding(true);
        }
    }

    //이동 관련 충돌이 끝남
    void OnCollisionExit2D(Collision2D collision)
    {
        //지면 위를 벗어났다면
        if (collision.gameObject.tag.Equals("Ground"))
        {
            Grounding(false);
        }
    }

    public void InputJump()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            //첫번째 점프라면
            if (!_isJump)
            {
                Jump(_jumpPower); //점프업

                _isJump = true; //첫번째 점프
            }
            else if (_isJump && !_isDoubleJump)
            {
                Jump(_jumpPower); //점프업

                _isDoubleJump = true;
            }
        }
    }

    //점프하다
    public void Jump(float jumpPower)
    {
        _animator.SetTrigger("Jump");

        //점프 업 처리(물리)
        _rigidbody2d.velocity = new Vector2(_rigidbody2d.velocity.x, 0f);
        _rigidbody2d.AddForce(Vector2.up * jumpPower);
    }

    //지면 위 여부를 설정한다
    public void Grounding(bool isGround)
    {
        this._isGround = isGround; //지면 위

        //지면 상태에 따른 애니메이션 설정
        _animator.SetBool("IsGround", isGround);
    }
}
