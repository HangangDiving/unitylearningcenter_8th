﻿using UnityEngine;
using System.Collections;

//몬스터 데미지
public class CMonsterDamage : MonoBehaviour, IDamage {

    public Transform _damageEffectPos;          //타격 이펙트 효과 위치
    public Object _damageEffectPrefab;          //타격 이펙트 프리팹
    public bool _isShake;                       //쉐이크 여부
    public float _damageStopDelayTime;          //데미지 충격 시 이동 지연 시간
    public float _damageColorEffectTime;        //데미지 충격 시 타격 색상 효과 지연 시간
    protected SpriteRenderer _spriteRenderer;   //스프라이트 렌더러 컴포넌트 참조
    protected IMonsterMove _monsterMove;        //몬스터 이동 컴포넌트 참조
    protected CCharacterState _monsterState;    //몬스터 상태
    
    void Awake()
    {
        _monsterMove = GetComponent<CMonsterMove>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _monsterState = GetComponent<CCharacterState>();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //지정한 크기에 데미지를 처리함
    public void Damage(float damage)
    {
        _monsterState._hp -= damage;

        if (_monsterState._hp <= 0)
        {

        }

        StartCoroutine(DamageEffectCoroutine());
    }

    //데미지 이펙트 효과 코루틴
    public IEnumerator DamageEffectCoroutine()
    {
        if (_isShake)
        {

        }

        if(_monsterMove != null)
            _monsterMove.IdleTimeStop(_damageStopDelayTime);

        //이펙트 생성
        GameObject effect = Instantiate(_damageEffectPrefab,
            _damageEffectPos.position, Quaternion.identity) as GameObject;
        Destroy(effect, 0.24f);

        //타격 칼라 이펙트 효과
        _spriteRenderer.material.color = new Color(0.7f, 0f, 0f, 1f);
        yield return new WaitForSeconds(_damageColorEffectTime);
        _spriteRenderer.material.color = Color.white;
    }
}
