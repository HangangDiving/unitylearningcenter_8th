﻿using UnityEngine;
using System.Collections;

//몬스터 이동 인터페이스
public interface IMonsterMove {

    void Move();                    //이동
    void Flip();                    //방향 전환
    void IdleTimeStop(float time);  //대기 시간 정지
    void IdleStop();                //대기 정지
    void AttackStop();              //공격 정지
    void MoveResume();              //이동 재개
}
