#include<iostream>
#include<stdlib.h>
#include<string>
#include<time.h>
#include<windows.h>

using namespace std;

void main()
{
	//필요한 데이터 준비
	srand((unsigned int)time(NULL));
	
	int lottoCount = 0;
	cout << "추첨 횟수 입력 : "; cin >> lottoCount;
	
	int** pplottoArr = new int*[lottoCount];
	int numberCount[45];

	for (int i = 0; i < 45; i++)
		numberCount[i] = 0;

	//횟수만큼 로또 생성(중복되지 않게)
	for (int i = 0; i < lottoCount; i++)
	{
		pplottoArr[i] = new int[6];
		int index = 0;
		while (index < 6)
		{
			pplottoArr[i][index] = (rand() % 45) + 1;
			//for (int i = index - 1; i >= 0; i--)
			int j = index;
			while (true)
			{
				if (--j < 0)
				{
					numberCount[pplottoArr[i][index] - 1]++;
					index++;
					break;
				}
				else if (pplottoArr[i][j] == pplottoArr[i][index])
					break;
			}
		}
	}

	//생성한 로또 출력
	for (int i = 0; i < lottoCount; i++)
	{
		cout << i + 1 << "번째 볼 추첨 번호 : ";
		for (int j = 0; j < 6; j++)
		{
			cout << pplottoArr[i][j];
			if (j == 5)
				cout << endl;
			else
				cout << ", ";
		}
	}

	//번호별 빈도수 출력
	cout << "[빈도수 정보 : 볼번호(빈도수)]" << endl;
	for (int i = 0; i < 45; i++)
	{
		if ((i + 1) % 5 != 0)
			cout << i + 1 << "(" << numberCount[i] << "), ";
		else
			cout << i + 1 << "(" << numberCount[i] << ")" << endl;
	}

	//높은 빈도 숫자 표기
	int highCntNumArr[6];
	int index = 0;
	highCntNumArr[0] = 0;
	for(int i = 1; i < 45; i++)
	{
		//큰 경우
		if(numberCount[highCntNumArr[index]] < numberCount[i])
		{
			if(index >= 5)
			{
				for(int j = 0; j < 5; j++)
					highCntNumArr[j] = highCntNumArr[j + 1];
				highCntNumArr[5] = i;
			}
			else
			{
				highCntNumArr[++index] = i;
			}
		}
		//작은 경우
		else
		{
			/*int temp = highCntIdxArr[index];
			index = index >= 5 ? 5 : index + 1;
			highCntIdxArr[index] = temp;*/
			for(int j = index; j >= 0; j--)
			{
				if(numberCount[highCntNumArr[j]] < numberCount[i])
				{
					if(index >= 5)
					{
						for(int k = 0; k < j; k++)
							highCntNumArr[k] = highCntNumArr[k + 1];
						highCntNumArr[j] = i;
					}
					else
					{
						for(int k = ++index; k > j + 1; k--)
							highCntNumArr[k] = highCntNumArr[k - 1];
						highCntNumArr[j + 1] = i;
					}
					break;
				}
			}			
		}
	}

	cout << "행운의 숫자 번호 : ";
	for(int i = 0; i < 6; i++)
	{
		cout << highCntNumArr[i] + 1;
		if(i == 5)
			cout << endl;
		else
			cout << ", ";
	}

	cout << "종료 방지 : "; cin >> lottoCount;
}
