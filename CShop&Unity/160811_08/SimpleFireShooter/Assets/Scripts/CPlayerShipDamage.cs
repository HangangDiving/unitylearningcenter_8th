﻿using UnityEngine;
using System.Collections;

public class CPlayerShipDamage : MonoBehaviour {

    public Animator _animator;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //우주와 충돌한다면 패스
        if (col.tag.Equals("Space")) return;

        //우주가 아닌 오브젝트와 충돌했다면 해당 오브젝트를 파괴함
        Destroy(col.gameObject);

        //플레이어쉽 데미지 효과 애니메이션을 재생함
        this._animator.Play("PlayerShipDamage");
    }
}
