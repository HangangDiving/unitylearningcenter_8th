﻿using UnityEngine;
using System.Collections;

//적기 생성기
public class CEnemyGenerator : MonoBehaviour {

    public Transform[] _genPositions; //생성 위치
    public float _minGenDelayTime = 4f; //최소 지연 시간
    public float _maxGenDelayTime = 10f; //최대 지연 시간
    public GameObject[] _enemyPrefabs; //적기 프리팹 배열

    // Use this for initialization
    void Start () {
        StartCoroutine("EnemyGenCoroutine");
	}

    private IEnumerator EnemyGenCoroutine()
    {
        while (true)
        {
            //생성 지연을 가짐
            float time = Random.Range(this._minGenDelayTime, this._maxGenDelayTime);
            //생성 시간을 지연함
            yield return new WaitForSeconds(time);
            //생성할 적기 번호를 랜덤으로 추출함
            int enemyType = Random.Range(0, this._enemyPrefabs.Length);
            //생성 위치 번호를 랜덤으로 추출함
            int genPosNum = Random.Range(0, this._genPositions.Length);
            //적기를 생성함
            Instantiate(this._enemyPrefabs[enemyType],
                this._genPositions[genPosNum].position,
                Quaternion.identity);
        }
    }
}
