﻿using UnityEngine;
using System.Collections;

public class CEnemyShipDamage : MonoBehaviour {

    public int _hp;
    public GameObject _damageEffectPrefab; //피격이펙트 프리팹

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //충돌한 레이저가 플레이어쉽의 레이저라면
        if (col.tag.Equals("PSLaser"))
        {
            //레이저 피격(스파크) 오브젝트를 생성했다가 0.3초뒤에 파괴함
            GameObject effect = Instantiate(this._damageEffectPrefab, col.transform.position,
                Quaternion.identity) as GameObject;
            Destroy(effect, 0.3f);

            //피격된 레이저를 파괴함
            Destroy(col.gameObject);
        }
    }
}
