﻿using UnityEngine;
using System.Collections;

public class DirectionMovement : MonoBehaviour {

    public Vector2 _direction;
    public float _speed = 8f;
    private Rigidbody2D _rigidbody2d;

	// Use this for initialization
	void Start () {
        this._speed = (this._speed <= 0f) ? 1f : this._speed;
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {

        if (this._rigidbody2d != null)
        {
            Vector2 currPos = this.transform.position;
            this._rigidbody2d.MovePosition(currPos
                + (this._direction * this._speed * Time.deltaTime) );
        }
        else
            this.transform.Translate(this._direction * this._speed * Time.deltaTime);

	}
}
