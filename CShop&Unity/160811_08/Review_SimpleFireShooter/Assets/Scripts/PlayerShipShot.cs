﻿using UnityEngine;
using System.Collections;

public class PlayerShipShot : MonoBehaviour {

    public GameObject _laserPrefab;
    public Transform _shotTM;
    		
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(this._laserPrefab,
                this._shotTM.position,
                Quaternion.identity);
        }

	}
}
