﻿using UnityEngine;
using System.Collections;

public class PlayerShipDamaged : MonoBehaviour {

    private Animator _animator;

	// Use this for initialization
	void Start () {
        this._animator = this.GetComponent<Animator>();
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Enemy"))
        {
            this._animator.Play("PlayerShipDamaged");
        }
    }
}
