﻿using UnityEngine;
using System.Collections;

public class SpaceExit : MonoBehaviour {

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag.Equals("Space"))
        {
            Destroy(this.gameObject);
        }
    }

}
