﻿using UnityEngine;
using System.Collections;

public class PlayerShipMovement : MonoBehaviour {

    private const float ROOT_2 = 1.414213562373095f;
    private const float WIDTH_LIMIT = 2.25f;
    private const float HEIGHT_LIMIT = 4.5f;

    public float _speed = 5f;
    private Rigidbody2D _rigidbody2d;

	// Use this for initialization
	void Start () {
        this._speed = (this._speed <= 0f) ? 1f : this._speed;
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        Vector2 dir, pos;
        dir.x = Input.GetAxis("Horizontal");
        dir.y = Input.GetAxis("Vertical");
        if ((dir.x > 0.9f || dir.x < -0.9f) && (dir.y > 0.9f || dir.y < -0.9f))
            dir /= ROOT_2;

        pos = this.transform.position;
        this._rigidbody2d.MovePosition(pos + (dir * this._speed * Time.deltaTime));

        pos = this.transform.position;

        if (this.transform.position.x < -WIDTH_LIMIT)
            pos.x = -WIDTH_LIMIT;
        else if (this.transform.position.x > WIDTH_LIMIT)
            pos.x = WIDTH_LIMIT;

        if (this.transform.position.y < -HEIGHT_LIMIT)
            pos.y = -HEIGHT_LIMIT;
        else if (this.transform.position.y > HEIGHT_LIMIT)
            pos.y = HEIGHT_LIMIT;

        this.transform.position = pos;
    }
}
