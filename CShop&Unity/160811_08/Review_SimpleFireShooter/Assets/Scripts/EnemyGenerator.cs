﻿using UnityEngine;
using System.Collections;

public class EnemyGenerator : MonoBehaviour {

    public GameObject[] _enemyPrefabs;
    public Transform[] _generateTMs;
    public float _minGenDelayTime = 1f;
    public float _maxGenDelayTime = 3f;

    // Use this for initialization
    void Start () {
        this._minGenDelayTime =
            (this._minGenDelayTime <= 0f) ? 1f : this._minGenDelayTime;
        StartCoroutine("GenerateCoroutine");
	}

    private IEnumerator GenerateCoroutine()
    {
        while (true)
        {
            float randDelayTime =
                Random.Range(this._minGenDelayTime, this._maxGenDelayTime);

            yield return new WaitForSeconds(randDelayTime);

            int randEnemyType = Random.Range(0, this._enemyPrefabs.Length);
            int randGenPos = Random.Range(0, this._generateTMs.Length);

            Instantiate(this._enemyPrefabs[randEnemyType],
                this._generateTMs[randGenPos].position,
                Quaternion.identity);
        }
    }

}
