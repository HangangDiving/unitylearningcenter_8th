﻿using UnityEngine;
using System.Collections;

public class EnemyShipDamaged : MonoBehaviour {

    public GameObject _laserSparkPrefab;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("PSLaser"))
        {
            Destroy(col.gameObject);

            GameObject newGameObj = Instantiate(this._laserSparkPrefab,
                col.transform.position,
                Quaternion.identity) as GameObject;
            Destroy(newGameObj, 0.3f);
        }
    }

}
