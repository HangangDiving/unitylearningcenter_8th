﻿using UnityEngine;
using System.Collections;

public class CPlayerShipCollision : MonoBehaviour {

    //[유니티 이벤트 메소드]
    //Trigger 옵션이 설정된 오브젝트와 충돌이 됨

    //충돌이 시작됨
    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("Enter - " + col.name + "과 부딪힘");

        //if(col.tag == "Meteor")
        if (col.tag.Equals("Meteor"))
        {
            Destroy(col.gameObject);
            Debug.Log("Meteor Trigger Enter");
        }
    }

    //충돌이 진행중
    void OnTriggerStay2D(Collider2D col)
    {
        //Debug.Log("Stay - " + col.name + "과 부딪힘");
    }

    //충돌이 끝남
    void OnTriggerExit2D(Collider2D col)
    {
        //Debug.Log("Exit - " + col.name + "과 부딪힘");
    }
}
