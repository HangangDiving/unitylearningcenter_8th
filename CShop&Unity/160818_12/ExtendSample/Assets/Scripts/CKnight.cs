﻿using UnityEngine;
using System.Collections;

public class CKnight : CMagician {

    public int _holy; // 성령

    protected override void Eat()
    {
        base.Eat(); // CHuman.Eat(), CMagician.Eat()

        _holy += 5;
        Debug.Log(_name + "이(가) 음식을 먹고 2만큼 성령을 보충했습니다.[현재 성령 : " + _holy + "]");
    }

    public override void Attack(string monsterName)
    {
        // CMagician의 Attack() 메소드 호출
        base.Attack(monsterName);

        Debug.Log(_name + "캐릭터가 " + monsterName + " 몬스터를 "
            + _int + " 성령으로 공격합니다.");
    }
}
