﻿using UnityEngine;
using System.Collections;
using System;

public class CFireTrap : MonoBehaviour, IAttackUnit {

    public float _damage;

    public float GetDamage()
    {
        return this._damage;
    }

    public void Attack(string name)
    {
        Debug.Log(name + "왈 '불은 너무 뜨거워요. 앗 뜨거~' ");
    }
}
