﻿using UnityEngine;
using System.Collections;

// [구조체]
// 참조 형식이 아닌 값 형식으로 객체를 생성하고 싶을때 사용하는 문법 형식

// [클래스와의 차이]
// - 선언만으로도 생성 가능
// - 매개변수 없는 생성자 사용 불능
// - 상속, 정적 형식으로 사용할 수 없음
// - 반드시 초기화 해줘야 함

public struct SAttackInfo
{
    public string _attackName;
    public int _damage;
    public int _time;

    public SAttackInfo(string attackName, int damage, int time)
    {
        this._attackName = attackName;
        this._damage = damage;
        this._time = time;
    }

    public void PrintAttackInfo()
    {
        Debug.Log("공격명 : " + _attackName);
        Debug.Log("공격력 : " + _damage);
        Debug.Log("공격시간 : " + _time);
    }
}

public class StructTest : MonoBehaviour {

    void Start()
    {
        SAttackInfo attackInfo1;
        attackInfo1._attackName = "기본공격";
        attackInfo1._damage = 100;
        attackInfo1._time = 5;
        attackInfo1.PrintAttackInfo();
        AddAttackInfoValue(attackInfo1);
        attackInfo1.PrintAttackInfo();

        SAttackInfo attackInfo2 = new SAttackInfo("특수공격", 200, 3);
        attackInfo2.PrintAttackInfo();
        AddAttackInfoValue(attackInfo2);
        attackInfo2.PrintAttackInfo();

        //SendMessage로 복수개의 데이타를 보낼때
        gameObject.SendMessage("AddAttackInfoValue", attackInfo1);
        
    }

    public void AddAttackInfoValue(SAttackInfo attackInfo)
    {
        attackInfo._damage += 10;
        attackInfo._time += 2;
    }

    public void ShowAttackInfoValue(SAttackInfo attackInfo)
    {
        Debug.Log("공격명 : " + attackInfo._attackName);
        Debug.Log("공격력 : " + attackInfo._damage);
        Debug.Log("공격시간 : " + attackInfo._time);
    }
}
