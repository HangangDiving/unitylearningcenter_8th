﻿using UnityEngine;
using System.Collections;

//나는 인간이고 MonoBehaviour의 상위의 모든 기능을 물려 받으며
//IAttackUnit의 메소드를 반드시 구현하고 있다.
public class CHuman : MonoBehaviour, IAttackUnit {

    public string _name;    // 이름
    public int _hp;         // 체력
    public float _speed;    // 속도
    public float _damage;   //데미지

	// Use this for initialization
	void Start () {
        Move();
	}
	
    // 이동하다
    void Move()
    {
        Debug.Log(_name + " 캐릭터가 " + _speed + "로 이동했습니다.");
        transform.Translate(Vector2.up * _speed);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Food")
        {
            Destroy(col.gameObject);
            Eat();
        }
    }

    // 오버라이드될 메소드 정의
    
    // [부모]    
    // proteced virtual 오버라이드될메소드명() { ... }

    // 음식을 먹다
    protected virtual void Eat()
    {
        _hp += 30;
        Debug.Log(_name + "이(가) 음식을 먹고 20만큼 체력을 보충했습니다.[" + _hp + "]");
    }

    public virtual void Attack(string monsterName)
    {

    }

    public float GetDamage()
    {
        return this._damage;
    }
}
