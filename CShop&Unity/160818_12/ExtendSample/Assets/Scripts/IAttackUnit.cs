﻿using UnityEngine;
using System.Collections;

//공격 유닛 인터페이스
//공격을 할 수 있는 유닛들이 가진 공통 명세
public interface IAttackUnit {

    //공격하다
    void Attack(string name);
    //데미지 측정
    float GetDamage();

}
