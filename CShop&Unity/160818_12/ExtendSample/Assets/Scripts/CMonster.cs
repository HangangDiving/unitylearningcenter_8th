﻿using UnityEngine;
using System.Collections;

public class CMonster : MonoBehaviour {

    public string _monsterName;


    void OnTriggerEnter2D(Collider2D col)
    {
        Stop();
        //----------------------------------------------//
        //1. 다형적 메소드를 활용하지 않을 경우의 문제점//
        //----------------------------------------------//
        /*
        if (col.name.Equals("Fighter"))
        {
            // 전사의 공격 메소들 호출
            CFighter ch = col.GetComponent<CFighter>();
            ch.Attack(_monsterName);
        }
        else if (col.name.Equals("Magician"))
        {
            // 전사의 공격 메소들 호출
            CMagician ch = col.GetComponent<CMagician>();
            ch.Attack(_monsterName);
        }
        else if (col.name.Equals("Knight"))
        {
            // 전사의 공격 메소들 호출
            CKnight ch = col.GetComponent<CKnight>();
            ch.Attack(_monsterName);
        }
        */

        //---------------------------------------//
        //2. 업캐스팅을 활용한 다형적 메소드 호출//
        //---------------------------------------//
        /*
        if (col.tag.Equals("Human"))
        {
            // 업캐스팅을 통한 다형성
            CHuman ch = col.GetComponent<CHuman>();
            // 휴먼 명세를 통해 Attack 메소드를 호출하면
            // 오바라이드된 실 자식객체의 Attack메소드를 호출할 수 있음
            ch.Attack(_monsterName);
            //휴먼 명세를 통해 GetDamage 메소드를 호출하면 공격 피해량을 알 수 있음
            Debug.Log("몬스터의 " + ch.GetDamage() + "의 피해를 입었습니다.");
        }
        else if (col.tag.Equals("Trap"))
        {
            // 업캐스팅을 통한 다형성
            CFireTrap trap = col.GetComponent<CFireTrap>();
            // 휴먼 명세를 통해 Attack 메소드를 호출하면
            // 오바라이드된 실 자식객체의 Attack메소드를 호출할 수 있음
            trap.Attack(_monsterName);
            //휴먼 명세를 통해 GetDamage 메소드를 호출하면 공격 피해량을 알 수 있음
            Debug.Log("몬스터의 " + trap.GetDamage() + "의 피해를 입었습니다.");
        }
        */

        //---------------------------------------//
        //3. 인터페이스를 활용한 다형적 메소드 호출
        //---------------------------------------//
        /*
        //인터페이스를 통한 다형성
        IAttackUnit attackUnit = col.GetComponent<IAttackUnit>();
        //인터페이스 명세를 통해 Attack 메소드를 호출하면
        //구현된 실 구현객체의 Attack메소드를 호출할 수 있음
        attackUnit.Attack(_monsterName);
        //IAttackUnit 명세를 통해 GetDamage 메소드를 호출하면 공격 피해량을 알 수 있음
        Debug.Log("몬스터의 " + attackUnit.GetDamage() + "의 피해를 입었습니다.");
        */

        //------------------------------------------//
        //4. SendMessage를 이용한 다형적 메소드 호출//
        //------------------------------------------//
        col.SendMessage("Attack", _monsterName, SendMessageOptions.DontRequireReceiver);
    }

    void Stop()
    {
        Rigidbody2D rigidbody2d = GetComponent<Rigidbody2D>();
        rigidbody2d.velocity = Vector2.zero; // 속도 0으로
        rigidbody2d.gravityScale = 0f; // 중력 없음
    }
}
