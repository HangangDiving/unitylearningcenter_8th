﻿using UnityEngine;
using System.Collections;

public class CPlane : MonoBehaviour {

    public Rigidbody2D _rigidbody2d; //리짓바디2D 컴포넌트 참조
    public Vector2 _velocity;
    public float _riseForce = 350f;
    public CGameManager _gameManager;

    // Use this for initialization
    void Start () {

        //transform.Translate(new Vector3(3f, 0));
        //_rigidbody2d.velocity = new Vector2(3f, 0f);

	}
	
	// Update is called once per frame
	void Update () {

        //transform.Translate(new Vector3(3f, 0) * Time.deltaTime);
        //_rigidbody2d.velocity = new Vector2(3f, 0f);
                
        //만약에 비행기가 상단 영역을 벗어났다면
        if (transform.position.y > 5.5f)
        {
            //게임을 중지를 설정함
            CGameManager.isGameStop = true;
        }
        //만약에 비행기가 하단 영역을 벗어났다면
        else if (transform.position.y < -5.5f)
        {
            //종료씬으로 이동함
            CGameManager.GameEnd();
        }

        _velocity = _rigidbody2d.velocity;

        //내가 아무키나 누르면
        if (Input.anyKeyDown && !CGameManager.isGameStop)
        {
            //순간적으로 속력을 0으로 변경함
            _rigidbody2d.velocity = Vector2.zero;
            //특정 방향(위)으로 힘을 부여함
            _rigidbody2d.AddForce(new Vector2(0f, _riseForce));
        }
    }

    //기둥과 비행체가 충돌함(IsTrigger 체크 상태가 아님)
    void OnCollisionEnter2D(Collision2D col)
    {
        //비행체와 컬럼이 충돌하면
        if (col.gameObject.tag.Equals("Column"))
        {
            //게임을 중지 상태로 만듬
            CGameManager.isGameStop = true;

            StartCoroutine("GameEndCoroutine");
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //비행체가 별을 먹었다면
        if (col.tag.Equals("StarItem"))
        {
            _gameManager.StarCountUp();

            Destroy(col.gameObject);
        }
    }

    //2초 뒤에 게임을 강제 종료함
    private IEnumerator GameEndCoroutine()
    {
        yield return new WaitForSeconds(2f);

        CGameManager.GameEnd();
    }
}
