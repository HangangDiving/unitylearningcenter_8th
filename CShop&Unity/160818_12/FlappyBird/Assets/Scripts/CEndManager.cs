﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CEndManager : MonoBehaviour {

    public Text _starCount;

	// Use this for initialization
	void Start () {

        //환경설정(레지스트리)에 저장된 별점을 불러옴
        int starCount = PlayerPrefs.GetInt("STAR_COUNT");
        //UI 텍스트에 출력함
        _starCount.text = starCount.ToString();

        CGameManager.isGameStop = false;
        CGameManager.starCount = 0;

    }

    public void OnReStartButtonClick()
    {        
        SceneManager.LoadScene("Demo");
    }
}
