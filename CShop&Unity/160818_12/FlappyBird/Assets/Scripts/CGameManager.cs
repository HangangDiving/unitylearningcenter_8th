﻿using UnityEngine;
using System.Collections;
//씬매니저 사용
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//게임 매니저
public class CGameManager : MonoBehaviour {

    public static bool isGameStop = false; //게임 정지 여부
    public static int starCount = 0;

    public Text _starCount; //별점

    //종료씬 이동
    public static void GameEnd()
    {
        int bestCount = int.Parse(PlayerPrefs.GetString("STAR_COUNT", "0"));
        if (starCount > bestCount)
        {
            PlayerPrefs.SetString("STAR_COUNT", starCount.ToString());
            PlayerPrefs.Save();
        }

        //환경설정(레이지스트리)에 현재 별카운트를 저장함
        //PlayerPrefs.SetInt("STAR_COUNT", starCount);
        //PlayerPrefs.Save();

        SceneManager.LoadScene("End");
    }

    //별점 증가
    public void StarCountUp()
    {
        int count = int.Parse(_starCount.text);
        count++;
        starCount = count;
        _starCount.text = count.ToString();
    }
}
