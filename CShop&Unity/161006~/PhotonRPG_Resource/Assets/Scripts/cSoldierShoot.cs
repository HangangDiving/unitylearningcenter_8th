﻿using UnityEngine;
using System.Collections;

public class cSoldierShoot : Photon.MonoBehaviour {

    cCharacterStat _stat; //캐릭터 상태

    public float _shootDelayTime; //발포 딜레이
    float _timer;

    cCharacterAnimation _anim; //캐릭터 애니메이션

    public GameObject _bulletPrefab; //총알 프리팹
    public Transform _shootPos; //발포 위치
    public float _shootPower; //발포 힘

    void Awake()
    {
        _stat = GetComponent<cCharacterStat>();
        _anim = GetComponent<cCharacterAnimation>();
    }

    void Update()
    {
        if (_stat._state == cCharacterStat.STATE.DEATH) return;

        //로컬 플레이일 경우
        if (photonView.isMine && _stat._state != cCharacterStat.STATE.DEATH)
        {
            _timer += Time.deltaTime;

            //총알 발포 키를 누르면
            if (Input.GetButtonDown("Fire1") && _timer >= _shootDelayTime && Time.timeScale != 0)
            {
                //RPC로 총알 발포 명령을 내림
                photonView.RPC("Shoot", PhotonTargets.All, _shootPos.position,
                    _shootPos.forward, transform.rotation);
            }
        }
    }

    //총알 발포 메소드
    [PunRPC] //Photon RPC 메소드
    public void Shoot(Vector3 pos, Vector3 forward, Quaternion qt)
    {
        if (_stat._state == cCharacterStat.STATE.DEATH) return;

        _anim.PlayAnimation(cCharacterStat.STATE.ATTACK);

        _timer = 0f;

        GameObject bullet = (GameObject)Instantiate(_bulletPrefab, pos, qt);
        bullet.GetComponentInChildren<Rigidbody>().velocity = forward * _shootPower;
        bullet.GetComponentInChildren<cBullet>()._photonViewId = photonView.viewID;
        Destroy(bullet, 0.5f);
    }
}
