﻿using UnityEngine;
using System.Collections;

//추적 카메라
public class cFollowCamera : MonoBehaviour {

    public Transform _target; //추적 대상

    public float _smoothing = 5f; //부드러움 정보

    public Vector3 _offset; //간격

    //타겟을 부드럽게 추적함
	void LateUpdate () {

        if (_target == null) return;

        Vector3 targetCamPos = _target.position + _offset;

        transform.position = Vector3.Lerp(transform.position,
            targetCamPos, _smoothing * Time.deltaTime);
	
	}

    //초기화
    public void Init(Transform target)
    {
        _target = target;

        transform.position = Vector3.zero;

        transform.position = _target.position;

        transform.position = _target.position + _offset;
    }
}
