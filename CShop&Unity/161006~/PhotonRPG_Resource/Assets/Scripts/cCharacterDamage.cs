﻿using UnityEngine;
using System.Collections;

public class cCharacterDamage : Photon.MonoBehaviour {

    public ParticleSystem _bloodEffect;

    cCharacterAnimation _anim;
    cCharacterStat _stat;

    public cHpBar _hpBar;

    void Awake()
    {
        _anim = GetComponent<cCharacterAnimation>();
        _stat = GetComponent<cCharacterStat>();
    }

    void Start()
    {
        LoadDamageInfo();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag.Equals("Bullet"))
        {
            if (_stat._state == cCharacterStat.STATE.DEATH) return;

            Destroy(collider.gameObject);

            //총알 피격 처리는 방장만 수행함
            //photonView.owner.isMasterClient : 현재 포톤뷰를 가진 오브젝트가 마스터 클라이언트인지 여부
            if (PhotonNetwork.isMasterClient)
            {
                //총알을 발사한 사람의 아이디를 추출함
                int bulletOwnerId = collider.GetComponent<cBullet>()._photonViewId;

                //방장을 제외한 모든 사용자에게 현재 오브젝트의 피격 처리 메소드를 호출 해줌
                photonView.RPC("TakeDamage", PhotonTargets.Others, bulletOwnerId);
                TakeDamage(bulletOwnerId);
            }
        }
    }

    //피격 처리용 RPC 메소드
    [PunRPC]
    public void TakeDamage(int bulletOwnerId)
    {
        if (_stat._state == cCharacterStat.STATE.DEATH) return;

        _anim.PlayAnimation(cCharacterStat.STATE.DAMAGE);
        _bloodEffect.Play();

        //체력을 10 감소함
        _stat._hp = _hpBar.HpDown(10);

        //현재 데미지를 입은 체력을 저장함
        SaveDamageInfo();

        //체력이 0이 되면
        if (_stat._hp <= 0)
        {
            //체력바 숨김
            _hpBar.Hidden();
            //사망 애니메이션 실행
            _anim.PlayAnimation(cCharacterStat.STATE.DEATH);

            if (PhotonNetwork.isMasterClient)
            {
                //총알을 발사한 아이를 가진 포톤뷰를 찾음
                PhotonView pv = PhotonView.Find(bulletOwnerId);
                //해당 포톤뷰 소유자의 점수를 올려줌
                pv.owner.AddScore(1);
                //캐릭터를 부활시킴
                StartCoroutine("ReviveCoroutine");
            }
        }
    }

    //부활 대기 시간
    public float _reviveDelayTime;
    
    IEnumerator ReviveCoroutine()
    {
        //대기시간동안 부활을 지연함
        yield return new WaitForSeconds(_reviveDelayTime);

        //부활을 수행함
        photonView.RPC("ReviveSoldier", PhotonTargets.Others);
        ReviveSoldier();
    }

    [PunRPC]
    public void ReviveSoldier()
    {
        //Debug.Log("플레이어가 부활함");

        _stat._hp = 100; //체력 초기화
        _hpBar.SetHp(_stat._hp); //체력바 설정
        _hpBar.Show();
        _anim.PlayAnimation(cCharacterStat.STATE.REVIVE); //부활 애니메이션

        //현재의 체력 상태 저장
        SaveDamageInfo();
    }

    //현재 피격 상태를 불러옴
    public void LoadDamageInfo()
    {
        if (photonView.owner.customProperties.ContainsKey("hp"))
        {
            //현재 포톤뷰를 소유한 유저의 설정속성들 중 hp 키값을 불러옴
            _stat._hp = (int)photonView.owner.customProperties["hp"];

            _hpBar.SetHp(_stat._hp);

            //불러온 체력값이 0이면
            if (_stat._hp <= 0)
            {
                //사망 처리함
                _anim.PlayAnimation(cCharacterStat.STATE.DEATH);
                _hpBar.Hidden();
            }
        }
    }

    //현재의 피격 상태를 저장함
    public void SaveDamageInfo()
    {
        //키/밸류 타입의 해쉬 테이블을 생성함
        ExitGames.Client.Photon.Hashtable stateInfo =
            new ExitGames.Client.Photon.Hashtable();

        stateInfo.Add("hp", _stat._hp);
        //현재 포톤뷰를 소유한 유저의 설정속성(Photon에서 제공하는 공간)에 해당 정보를 저장함
        photonView.owner.SetCustomProperties(stateInfo);
    }
}