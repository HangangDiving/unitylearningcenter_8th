﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class cCharacterStat : Photon.MonoBehaviour {

    public string _playerName; //플레이어 이름
    public int _score; //스코어

    public Text _nameText; //플레이어 이름 표시 텍스트
    public Text _scoreText; //스코어 표시 텍스트

    //상태
    public enum STATE { IDLE, MOVE, ATTACK, DAMAGE, DEATH, REVIVE };
    public STATE _state;

    public int _hp; //체력

	// Use this for initialization
	void Start () {
        //포톤뷰의 소유자의 이름을 표시함
        _playerName = photonView.owner.name;
        _nameText.text = photonView.owner.name;

        //현재 오브젝트가 클라이언트의 플레이어 오브젝트라면
        if (photonView.isMine)
        {
            //추적카메라에 현재 플레이어 추적을 설정함
            cFollowCamera camera = Camera.main.GetComponent<cFollowCamera>();
            camera.Init(transform);
        }
	}
	
	// Update is called once per frame
	void Update () {

        //포톤뷰의 소유자의 점수를 표시함
        _score = photonView.owner.GetScore();
        _scoreText.text = _score.ToString();

	}
}
