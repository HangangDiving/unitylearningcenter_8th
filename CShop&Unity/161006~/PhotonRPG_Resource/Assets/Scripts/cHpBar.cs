﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class cHpBar : MonoBehaviour {

    public Image _hpBarProgress; //체력 게이지

    //체력 감소
    public int HpDown(int damage)
    {
        _hpBarProgress.fillAmount -= (damage * 0.01f);

        return (int)(_hpBarProgress.fillAmount * 100f);
    }

    //체력바 숨김
    public void Hidden()
    {
        gameObject.SetActive(false);
    }

    //체력바 보이기
    public void Show()
    {
        gameObject.SetActive(true);
    }

    //체력값 설정
    public void SetHp(int hpValue)
    {
        _hpBarProgress.fillAmount = hpValue * 0.01f;
    }
}
