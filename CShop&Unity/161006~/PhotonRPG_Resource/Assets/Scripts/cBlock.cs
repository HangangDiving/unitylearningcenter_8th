﻿using UnityEngine;
using System.Collections;

public class cBlock : MonoBehaviour {

    public GameObject _hitEffectPrefab;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag.Equals("Bullet"))
        {
            //피격 이펙트(파이틀) 생성
            GameObject effect = Instantiate(_hitEffectPrefab,
                collider.transform.position,
                Quaternion.LookRotation(-collider.transform.forward)) as GameObject;

            Destroy(collider.gameObject);
        }
    }
}
