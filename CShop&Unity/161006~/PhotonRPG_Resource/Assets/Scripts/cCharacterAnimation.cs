﻿using UnityEngine;
using System.Collections;

public class cCharacterAnimation : Photon.MonoBehaviour {

    cCharacterStat _stat;

    Animator _animator;

    void Awake()
    {
        _stat = GetComponent<cCharacterStat>();
        _animator = GetComponent<Animator>();
    }

    public bool IsPlayAnimation(string animName, int Layer = 0)
    {
        if (_animator.GetCurrentAnimatorStateInfo(Layer).IsName(animName)) return true;

        return false;
    }

    //애니메이션 재생
    public void PlayAnimation(cCharacterStat.STATE state)
    {
        _stat._state = state;

        switch (_stat._state)
        {
            case cCharacterStat.STATE.IDLE:
                _animator.SetBool("Move", false);
                break;
            case cCharacterStat.STATE.MOVE:
                _animator.SetBool("Move", true);
                break;
            case cCharacterStat.STATE.ATTACK:
                if (IsPlayAnimation("Shoot", 0)) break;
                _animator.SetTrigger("Shoot");
                break;
            case cCharacterStat.STATE.DAMAGE:
                if (IsPlayAnimation("Damage", 2)) break;
                _animator.SetTrigger("Damage");
                break;
            case cCharacterStat.STATE.DEATH:
                if (IsPlayAnimation("Death", 0)) break;
                    _animator.SetTrigger("Death");
                break;
            case cCharacterStat.STATE.REVIVE:
                if (IsPlayAnimation("Revive")) break;
                _animator.Play("Idle");
                _stat._state = cCharacterStat.STATE.IDLE;
                break;
        }
    }
}
