﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//게임 접속 매니저
public class cConnectionManager : MonoBehaviour {

    public Text _logMessage; //로그 메시지

    public string _version;

	// Use this for initialization
	void Start () {

        //포톤 네트워크에 접속 상태가 아니라면
        if (!PhotonNetwork.connected)
        {
            _logMessage.text = "포톤 클라우드 접속을 시도함";

            //포톤 네크워크(클라우드)에 접속을 시도함
            bool isConnect = PhotonNetwork.ConnectUsingSettings(_version);

            Debug.Log("[정보] 포톤 클라우드 접속 시도 여부 => " + isConnect);
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //[Photon Event Method]
    //포톤 클라우드의 로비 접속에 성공함
    public void OnJoinedLobby()
    {
        Debug.Log("[정보] 포톤 클라우드 접속에 성공함");

        _logMessage.text = "포톤 클라우드 접속 및 로비 진입을 완료함";

        //게임 방이 있으면 접속을, 없으면 생성을 함
        bool isSuccess = PhotonNetwork.JoinOrCreateRoom(
            "TestRoom",             //방 이름
            new RoomOptions()       //방 정보 옵션
            {
                MaxPlayers = 10,        //방의 최대 인원
                IsOpen = true,          //공개 여부
                IsVisible = true        //활성 여부(목록 상태) -> 방 목록에 보이게 할건지 결정
            },
            TypedLobby.Default
        );

        Debug.Log("[정보] 게임 방 생성 또는 방 접속 완료 : " + isSuccess);
    }

    //[Photon Event Method]
    //포톤 로비에 생성된 방에 접속이 완료됨
    public void OnJoinedRoom()
    {
        _logMessage.text = "게임 방에 접속이 완료됨";
        Debug.Log("[정보] 게임 방에 접속이 완료됨");
    }

    //포톤 로비에 방 생성을 실패함
    public void OnPhotonCreateRoomFailed(object[] errorMsg)
    {
        Debug.Log("[오류] 방 생성을 실패함 : " + errorMsg[1].ToString());
    }

    //포톤 로비에 생성된 방에 접속이 실패함
    public void OnPhotonJoinRoomFailed(object[] errorMsg)
    {
        Debug.Log("[오류] 방 접속을 실패함 : " + errorMsg[1].ToString());
    }

    //포톤 클라우드 접속 오류
    public void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        Debug.Log("[오류] 포톤 클라우드 접속을 실패함 : " + cause.ToString());
    }
}
