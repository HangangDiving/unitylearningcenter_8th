﻿using UnityEngine;
using System.Collections;

public class cSoldierMovement : Photon.MonoBehaviour {

    cCharacterStat _stat; //캐릭터 상태
    CharacterController _cc; //캐릭터 컨트롤러
    cCharacterAnimation _anim; //캐릭터 애니메이션

    Vector3 _moveDir = Vector3.zero; //이동 방향 및 크기
    public float _speed; //속도
    public float _gravity; //중력

    void Awake()
    {
        _stat = GetComponent<cCharacterStat>();
        _cc = GetComponent<CharacterController>();
        _anim = GetComponent<cCharacterAnimation>();
    }
    	
	// Update is called once per frame
	void Update () {

        //현재 스크립트 컴포넌트가 포함된 게임 오브젝트의
        //PhotonView 컴포넌트가 내꺼면
        if (photonView.isMine)
        {
            //이동과 회전을 수행함
            Move();
            Turn();
        }
	}

    //이동
    void Move()
    {
        //이미 사망 상태면 이동 중지
        if (_stat._state == cCharacterStat.STATE.DEATH) return;

        //키 입력에 따라 캐릭터 컨트롤러를 이용해 캐릭터를 이동시킴

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        _moveDir = new Vector3(h, 0f, v);

        if (h != 0 || v != 0)
        {
            transform.SetParent(null);

            _anim.PlayAnimation(cCharacterStat.STATE.MOVE);
        }
        else
        {
            _anim.PlayAnimation(cCharacterStat.STATE.IDLE);
        }

        float speed = _speed;
        if (h != 0 && v != 0)
        {
            float degree = Mathf.Cos(45f * Mathf.Deg2Rad);
            speed = speed * degree;
        }

        _moveDir *= speed;

        _moveDir.y -= _gravity;

        _cc.Move(_moveDir * Time.deltaTime);
    }

    //회전
    void Turn()
    {
        //이미 사망 상태면 회전 중지
        if (_stat._state == cCharacterStat.STATE.DEATH) return;

        //마우스 포인트 위치로 레이를 생성함
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        //현재 마우스 위치가 바닥과 충돌하면
        if (Physics.Raycast(camRay, out floorHit, 100f, 1 << LayerMask.NameToLayer("Floor")))
        {
            //마우스 방향을 향하는 회전 쿼터니언을 구함
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0;
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            
            //플레이어를 회전 시킴
            transform.rotation = newRotation;
        }
    }
}
