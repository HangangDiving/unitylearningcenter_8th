﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class cGameManager : MonoBehaviour {

    //이름 입력 및 시작 화면 패널
    public GameObject _startPanel;

    public Text _logMessage;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //플레이어 이름 입력을 완료함
    public void OnEnterPlayerNameInputField(InputField ipName)
    {
        if (!PhotonNetwork.connected)
        {
            _logMessage.text = "접속이 완료된 후 시도하세요.";
            return;
        }

        //패널 비활성화
        _startPanel.SetActive(false);

        //이름 추출
        string playerName = ipName.text.ToUpper();

        if (playerName.Length <= 0)
        {
            playerName = "PLAYER";
        }

        //포톤 프리팹을 게임오브젝트로 생성함
        CreateNetPlaery(playerName);
    }

    //포톤 프리팹을 게임오브젝트로 생성함
    public void CreateNetPlaery(string playerName)
    {
        //현재 클라이언트 유저의 이름을 포톤에 설정함
        PhotonNetwork.playerName = playerName;
        
        //현재 방에 존재하는 모든 플레이어들에게 해당 프리팹을 생성 시킴
        PhotonNetwork.Instantiate(
            "Prefabs/Characters/Soldier",   //포톤 프리팹 동적 경로
            Vector3.zero,                   //생성 위치
            Quaternion.identity, 0);        //회전 및 그룹
    }
}
