﻿using UnityEngine;
using System.Collections;

//플레이어 이동
public class CPlayerMovement : MonoBehaviour {

    public float _speed; //이동 속도
    public float _gravity; //중력
    public Vector3 _moveDirection; //이동 방향
    CharacterController _cc; //캐릭터 컨트롤러

    CPlayerAnimation _playerAnim;

    CPlayerStat _stat;

    void Awake()
    {
        _cc = GetComponent<CharacterController>();
        _playerAnim = GetComponent<CPlayerAnimation>();
        _stat = GetComponent<CPlayerStat>();
    }
    	
	// Update is called once per frame
	void Update () {
	
        Move();

	}

    public void Move()
    {
        if (_stat._state == CPlayerStat.STATE.DIE) return;

        if (_playerAnim.IsAttackAnimation()) return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        _moveDirection = new Vector3(h, 0f, v);

        //정지 상태
        if (_moveDirection == Vector3.zero)
        {
            //대기 애니메이션 실행
            _playerAnim.PlayAnimation(CPlayerStat.STATE.IDLE);
        }
        else //걷기 상태
        {
            //걷기 애니메이션 실행
            _playerAnim.PlayAnimation(CPlayerStat.STATE.WALK);

            //키 방향으로 턴
            transform.forward = _moveDirection.normalized;

            float speed = _speed;

            //대각선 속도 계산
            if (h != 0 && v != 0)
            {
                float degree = Mathf.Cos(45f * Mathf.Deg2Rad);
                speed *= degree;
            }

            _moveDirection *= speed;

            //중력 처리
            _moveDirection.y -= _gravity;

            //이동 처리 (캐릭터 컨트롤러 이용)
            _cc.Move(_moveDirection * Time.deltaTime);
        }
    }
}
