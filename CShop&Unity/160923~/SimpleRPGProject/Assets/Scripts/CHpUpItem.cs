﻿using UnityEngine;
using System.Collections;

//체력 증가 아이템
public class CHpUpItem : CItem {

    //체력 아이템
    public override void PickUp(CPlayerStat _stat)
    {
        //체력을 증가 시켜주고
        _stat.HpUp(50);

        base.PickUp(_stat);
    }
}
