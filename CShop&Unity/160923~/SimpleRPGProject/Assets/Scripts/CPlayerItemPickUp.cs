﻿using UnityEngine;
using System.Collections;

//플레이어 아이템 획득 처리
public class CPlayerItemPickUp : MonoBehaviour {

    //아이템을 먹었다면
    void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals("Item"))
        {
            col.SendMessage("PickUp", GetComponent<CPlayerStat>());
        }
    }

}
