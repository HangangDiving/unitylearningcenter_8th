﻿using UnityEngine;
using System.Collections;

public class CSpiderDamage : MonoBehaviour {

    public Object _hitEffectPrefab; //피격 이펙트 프리팹

    public Transform _hitPoint; //타격 이펙트 표시 위치

    CSpiderAnimation _anim; //애니메이션 컴포넌트

    CSpiderStat _stat; //거미 상태 정보 컴포넌트    

    public Object _dropItemPrefab; //사망 시 떨구는 아이템

    void Awake()
    {
        _anim = GetComponent<CSpiderAnimation>();
        _stat = GetComponent<CSpiderStat>();
    }

    // Use this for initialization
    void Start () {
	
	}

    public void Hit(int damage)
    {
        //이미 사망 상태면 아무것도 하지 않음
        if (_stat.IsDie()) return;

        //타격 이펙트 생성
        Instantiate(_hitEffectPrefab, _hitPoint.position, Quaternion.identity);

        //체력을 감소시킴
        int hp = _stat.HpDown(damage);

        if (hp <= 0)
        {
            //사망 애니메이션
            _anim.PlayAnimation(CSpiderStat.STATE.DIE);

            //3초 뒤에 사망 메소드 호출
            Invoke("Die", 3f);
        }
    }

    //거미 사망
    public void Die()
    {
        //아이템 떨굼
        Instantiate(_dropItemPrefab, transform.position, Quaternion.identity);
        
        //거미 파괴
        Destroy(gameObject);
    }
}
