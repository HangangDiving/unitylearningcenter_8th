﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class CHpManager : MonoBehaviour {

    public Image _hpBarProgress; //체력바

    public GameObject _uiCanvas; //UI 캔버스

    public bool _isHpCanvasVisible; //사망 시 캔버스 비활성 화 여부

    //체력 감소
    public int HpDown(int damage)
    {
        _hpBarProgress.fillAmount -= (damage * 0.01f);
        int hp = (int)(_hpBarProgress.fillAmount * 100f);

        if (hp <= 0) //체력이 0이 되면
        {
            //_uiCanvas.SetActive(false); //UI를 비활성화 함
            _uiCanvas.SetActive(!_isHpCanvasVisible); //UI를 비활성화 함
        }

        return hp;
    }

    //체력 증가
    public void HpUp(int value)
    {
        _hpBarProgress.fillAmount += (value * 0.01f);
    }
}
