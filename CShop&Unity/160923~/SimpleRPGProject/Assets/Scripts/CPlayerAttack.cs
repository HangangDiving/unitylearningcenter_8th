﻿using UnityEngine;
using System.Collections;

public class CPlayerAttack : MonoBehaviour {

    CPlayerAnimation _playerAnim;

    public int _normalDamage; //기본 데미지
    public float _normalAttackRange; //기본 타격 범위
    public int _skillDamage; //스킬 데미지
    public float _skillAttackRange; //스킬 타격 범위
    public Transform _attackPoint; //공격 위치
    public Object _skillEffectPrefab; //스킬 이펙트

    private int _addDamage; //버프 등으로 추가된 데미지
    public ParticleSystem _damageUpBuffPS;

    void Awake()
    {
        _playerAnim = GetComponent<CPlayerAnimation>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Attack();
	}

    public void Attack()
    {
        if (_playerAnim.IsAttackAnimation()) return;

        //기본 공격
        if (Input.GetKeyDown(KeyCode.Z))
        {
            _playerAnim.PlayAnimation(CPlayerStat.STATE.ATTACK);
        }
        //스킬 공격
        else if (Input.GetKeyDown(KeyCode.X))
        {
            _playerAnim.PlayAnimation(CPlayerStat.STATE.SKILL);
        }
    }

    //일반 공격
    public void NormalAttack()
    {
        //공격포인트(AttackPoint)를 기준으로 0.7 반경안에서 Spider 충돌레이어를 가진 오브젝트들을 검출함
        Collider[] hitColliders = Physics.OverlapSphere(_attackPoint.position,
            _normalAttackRange, 1 << LayerMask.NameToLayer("Spider"));

        //헛빵
        if (hitColliders.Length <= 0) return;

        hitColliders[0].SendMessage("Hit", _normalDamage);
    }

    public void SkillAttack()
    {
        Vector3 pos = new Vector3(_attackPoint.position.x,
            transform.position.y, _attackPoint.position.z);

        //스킬 공격 이펙트
        Instantiate(_skillEffectPrefab, pos, Quaternion.identity);

        //공격포인트(AttackPoint)를 기준으로 0.7 반경안에서 Spider 충돌레이어를 가진 오브젝트들을 검출함
        Collider[] hitColliders = Physics.OverlapSphere(pos,
            _skillAttackRange, 1 << LayerMask.NameToLayer("Spider"));

        //피격 오브젝트들에게 피격 처리를 요청함
        foreach (Collider hitCollider in hitColliders)
        {
            hitCollider.SendMessage("Hit", _skillDamage);
        }
    }

    public void DamageUp(int value, float time)
    {
        _addDamage = value;
        _normalDamage += _addDamage;
        _skillDamage += _addDamage;

        _damageUpBuffPS.Play();

        Invoke("RecoverDamageInvoke", time);
    }

    void RecoverDamageInvoke()
    {
        _damageUpBuffPS.Stop();

        _normalDamage -= _addDamage;
        _skillDamage -= _addDamage;
    }
}
