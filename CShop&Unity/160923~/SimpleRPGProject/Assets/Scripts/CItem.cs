﻿using UnityEngine;
using System.Collections;

public class CItem : MonoBehaviour {

    public virtual void PickUp(CPlayerStat _stat)
    {
        //아이템을 소멸함
        Destroy(gameObject);
    }
}
