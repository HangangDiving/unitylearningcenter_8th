﻿using UnityEngine;
using System.Collections;

//거미 이동
public class CSpiderMovement : MonoBehaviour {

    NavMeshAgent _navMeshAgent; //네비게이션 컴포넌트

    CSpiderAttack _attack; //공격 컴포넌트

    void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _attack = GetComponent<CSpiderAttack>();
    }

    void Start()
    {
        //일단 정지
        _navMeshAgent.Stop();
    }

    //이동 중지
    public void Stop()
    {
        _navMeshAgent.Stop();
    }

    //이동
    public void Trace()
    {
        //공격 대상을 이동 대상으로 설정함
        _navMeshAgent.SetDestination(_attack._attackTarget.transform.position);
        //대상을 향해 이동함
        _navMeshAgent.Resume();
    }
}
