﻿using UnityEngine;
using System.Collections;

//거미 상태 정보
public class CSpiderStat : MonoBehaviour {

    //현재 상태
    public enum STATE { IDLE, TRACE, ATTACK, DIE }
    public STATE _state = STATE.IDLE;

    CHpManager _hpManager; //hp 관리자

    void Awake()
    {
        _hpManager = GetComponent<CHpManager>();
    }

    //캐릭터의 HP를 감소함
    public int HpDown(int damage)
    {
        return _hpManager.HpDown(damage);
    }

    //사망 여부 체크
    public bool IsDie()
    {
        if (_state == CSpiderStat.STATE.DIE)
            return true;

        return false;
    }
}
