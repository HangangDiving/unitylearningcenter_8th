﻿using UnityEngine;
using System.Collections;

public class CSpiderGenerator : MonoBehaviour {

    public float _genDelayTime; //생성 지연 시간

    public Transform[] _genPos; //생성 위치

    public Object[] _spiderPrefabs; //거미 프리팹

    public Object _genEffectPrefab; //생성 이펙트 프리팹

    CPlayerStat _playerStat;

    // Use this for initialization
    void Start () {

        _playerStat =
            GameObject.FindGameObjectWithTag("Player").GetComponent<CPlayerStat>();

        StartCoroutine("SpiderGenCoroutine");
	}

    IEnumerator SpiderGenCoroutine()
    {
        //플레이어가 사망 상태가 아니라면 거미를 생성함
        while (!_playerStat.IsDie())
        {
            //거미를 생성할 번호를 추출함
            int posNum = Random.Range(0, _genPos.Length);

            //이미 현재 자리에 거미가 존재한다면
            if (_genPos[posNum].childCount > 0)
            {
                //다른 자리를 찾음
                yield return null;
                continue;
            }

            //생성 이펙트
            Instantiate(_genEffectPrefab, _genPos[posNum].position, Quaternion.identity);

            int spiderIdx = Random.Range(0, _spiderPrefabs.Length);

            //거미를 생성함
            GameObject spider =
                Instantiate(_spiderPrefabs[spiderIdx], _genPos[posNum].position, Quaternion.identity) as GameObject;

            //생성한 거미의 위치 오브젝트의 자식으로 등록함
            spider.transform.SetParent(_genPos[posNum]);

            //대기 지연
            yield return new WaitForSeconds(_genDelayTime);
        }
    }
}
