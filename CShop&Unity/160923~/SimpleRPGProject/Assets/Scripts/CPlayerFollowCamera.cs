﻿using UnityEngine;
using System.Collections;

public class CPlayerFollowCamera : MonoBehaviour {

    public Transform _target; //추적 타겟
    public float _smoothValue; //이동 보간(부드러움)
    public Vector3 _offset; //추적 간격

	// Use this for initialization
	void Start () {
        transform.position = Vector3.zero; //초기화

        //플레이어 타겟 설정
        _target = GameObject.FindGameObjectWithTag("Player").transform;
        transform.position = _target.position;

        //타겟 간격 위치 설정
        transform.position = _target.position + _offset;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        if (_target == null) return;

        //업데이트 간격 위치 갱신
        Vector3 targetCamPos = _target.position + _offset;

        //부드럽게 위치 이동 설정
        transform.position = Vector3.Lerp(transform.position,
            targetCamPos, _smoothValue * Time.deltaTime);
	}
}
