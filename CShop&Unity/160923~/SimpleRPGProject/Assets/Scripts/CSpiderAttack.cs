﻿using UnityEngine;
using System.Collections;

public class CSpiderAttack : MonoBehaviour {

    [HideInInspector]
    public GameObject _attackTarget; //공격 대상

    public Transform _attackPoint; //공격 위치

    public float _attackRange; //공격 범위

    public int _damage; //공격 데미지 수치

	// Use this for initialization
	void Start () {
        
        //_attackTarget = GameObject.FindGameObjectWithTag("Player");

    }

    public void InitTarget()
    {
        _attackTarget = GameObject.FindGameObjectWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public float GetAttackTargetDistance()
    {
        float dist = Vector3.Distance(
            _attackTarget.transform.position, transform.position);

        return dist;
    }

    public void Attack()
    {
        //Debug.Log(name + "가(이) 플레이어를 공격함!!!");

        //공격 충돌 체크 수행
        Collider[] hitColliders =
            Physics.OverlapSphere(_attackPoint.position, _attackRange, 1 << LayerMask.NameToLayer("Player"));

        //피격 대상이 없다면 패스
        if (hitColliders.Length <= 0) return;

        //플레이어에게 데미지를 부여함
        hitColliders[0].SendMessage("Hit", _damage);
    }
}
