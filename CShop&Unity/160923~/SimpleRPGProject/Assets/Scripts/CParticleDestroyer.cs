﻿using UnityEngine;
using System.Collections;

public class CParticleDestroyer : MonoBehaviour {

    ParticleSystem _particleSystem;

    void Awake()
    {
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }

	// Use this for initialization
	void Start () {
	    if(!_particleSystem.isPlaying)
        {
            _particleSystem.Play();
        }

        //ParticleSystem.duration : 파티클 재생 시간
        Destroy(gameObject, _particleSystem.duration);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
