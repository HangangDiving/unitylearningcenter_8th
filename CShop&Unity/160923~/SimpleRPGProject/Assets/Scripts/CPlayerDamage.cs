﻿using UnityEngine;
using System.Collections;

public class CPlayerDamage : MonoBehaviour {

    CPlayerAnimation _anim;
    CPlayerStat _stat;

    void Awake()
    {
        _anim = GetComponent<CPlayerAnimation>();
        _stat = GetComponent<CPlayerStat>();
    }

    public void Hit(int damage)
    {
        //플레이어가 사망할 경우 피격을 회피함
        if (_stat._state == CPlayerStat.STATE.DIE) return;

        //체력을 감소시킴
        int hp = _stat.HpDown(damage);

        //피격 애니메이션을 재생함
        _anim.PlayMultiLayerAnimation("Damage", 1);

        if (hp <= 0)
        {
            //사망 애니메이션
            _anim.PlayAnimation(CPlayerStat.STATE.DIE);

            SendMessage("RecoverDamageInvoke");
        }
    }
}
