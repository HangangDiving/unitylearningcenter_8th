﻿using UnityEngine;
using System.Collections;

public class CSpiderFSM : MonoBehaviour {

    CSpiderStat _stat; //거미 상태

    CSpiderAnimation _anim; //거미 애니메이션

    CSpiderMovement _movement; //거미 이동

    CSpiderAttack _attack; //거미 공격
    
    //상태 관련 속성들
    public float _attackDist; //공격 가능 거리
    public float _traceDist; //추적 가능 거리

    void Awake()
    {
        _stat = GetComponent<CSpiderStat>();
        _anim = GetComponent<CSpiderAnimation>();
        _movement = GetComponent<CSpiderMovement>();
        _attack = GetComponent<CSpiderAttack>();
    }

    void Start()
    {
        //공격 타겟을 초기화 함
        _attack.InitTarget();

        StartCoroutine("SpiderCheckFSMCoroutine");
        StartCoroutine("SpiderDoActionCoroutine");
    }

    //현재 거미의 상태를 판단
    IEnumerator SpiderCheckFSMCoroutine()
    {
        //현재 거미가 사망한게 아니라면 유한 상태를 유지함
        while(!_stat.IsDie())
        {
            //공격 대상(플레이어)이 존재하지 않으면
            if (_attack._attackTarget == null)
            {
                //대기 상태로 변경함
                _stat._state = CSpiderStat.STATE.IDLE;
            }

            //거미의 공격 대상과의 거리를 측정함
            float dist = _attack.GetAttackTargetDistance();

            if (dist <= _attackDist)
            {
                _stat._state = CSpiderStat.STATE.ATTACK;
            }
            else if (dist <= _traceDist)
            {
                _stat._state = CSpiderStat.STATE.TRACE;
            }
            else
            {
                _stat._state = CSpiderStat.STATE.IDLE;
            }

            yield return null;
        }
    }

    //현재 거미의 상태에 대한 행동을 수행함
    IEnumerator SpiderDoActionCoroutine()
    {
        while (!_stat.IsDie())
        {
            // 공격 대상(플레이어)가 사망 상태면 대기 상태로 변경함
            if (_attack._attackTarget.GetComponent<CPlayerStat>().IsDie())
            {
                _stat._state = CSpiderStat.STATE.IDLE;
            }

            //상태에 따른 행동을 처리함
            switch (_stat._state)
            {
                case CSpiderStat.STATE.IDLE: //현재 상태가 대기 상태면
                    _movement.Stop(); //이동 정지
                    _anim.PlayAnimation(CSpiderStat.STATE.IDLE); //대기 애니메이션 처리
                    break;
                case CSpiderStat.STATE.ATTACK: //현재 상태가 공격 상태면
                    _movement.Stop(); //이동을 멈추고
                    transform.LookAt(_attack._attackTarget.transform); //공격 대상을 바라보고
                    _anim.PlayAnimation(CSpiderStat.STATE.ATTACK); //공격 애니메이션을 수행함
                    break;
                case CSpiderStat.STATE.TRACE: //현재 상태가 추적 상태면
                    _movement.Trace(); //추적 이동 시작
                    _anim.PlayAnimation(CSpiderStat.STATE.TRACE); //이동 애니메이션을 수행
                    break;
            }

            yield return null;
        }
    }
}
