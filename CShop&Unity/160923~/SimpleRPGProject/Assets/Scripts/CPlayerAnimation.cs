﻿using UnityEngine;
using System.Collections;

public class CPlayerAnimation : MonoBehaviour {

    Animator _animator;

    public float _animSpeed; //애니메이션 재생 속도

    CPlayerStat _playerStat;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _playerStat = GetComponent<CPlayerStat>();
    }

	// Use this for initialization
	void Start () {
        //애니메이션 속도 설정
        _animator.speed = _animSpeed;
	}

    //애니메이션 재생
    public void PlayAnimation(CPlayerStat.STATE state)
    {
        if (_playerStat._state == CPlayerStat.STATE.DIE) return;

        _playerStat._state = state;

        switch (_playerStat._state)
        {
            case CPlayerStat.STATE.IDLE:
                _animator.SetBool("Walk", false);
                break;
            case CPlayerStat.STATE.WALK:
                _animator.SetBool("Walk", true);
                break;
            case CPlayerStat.STATE.ATTACK:
                _animator.SetTrigger("Attack");
                break;
            case CPlayerStat.STATE.SKILL:
                _animator.SetTrigger("Skill");
                break;
            case CPlayerStat.STATE.DIE:
                _animator.SetTrigger("Die");
                break;
        }
    }

    //멀티 레이어 애니메이션 수행
    public void PlayMultiLayerAnimation(string aniName, int layerIndex)
    {
        //이미 현재 애니메이션을 수행하고 있다면
        if (_animator.GetCurrentAnimatorStateInfo(layerIndex).IsName(aniName))
        {
            return; //패스
        }

        //지정한 이름을 가진 멀티 레이어 애니메이션을 수행함
        _animator.Play(aniName, layerIndex);
    }

    //현재 공격(일반, 스킬) 애니메이션을 수행 중인지 체크함
    public bool IsAttackAnimation()
    {
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack")
            || _animator.GetCurrentAnimatorStateInfo(0).IsName("Skill"))
        {
            return true;
        }

        return false;
    }
}
