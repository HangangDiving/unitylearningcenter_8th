﻿using UnityEngine;
using System.Collections;

//플레이어의 정보
public class CPlayerStat : MonoBehaviour {

    //현재 플레이어의 상태
    public enum STATE { IDLE, WALK, ATTACK, SKILL, DIE }
    public STATE _state = STATE.IDLE;

    CHpManager _hpManager; //hp 관리자

    void Awake()
    {
        _hpManager = GetComponent<CHpManager>();
    }

    //캐릭터의 HP를 감소함
    public int HpDown(int damage)
    {
        return _hpManager.HpDown(damage);
    }

    //캐릭터의 HP를 증가함
    public void HpUp(int value)
    {
        _hpManager.HpUp(value);
    }

    //사망 여부 체크
    public bool IsDie()
    {
        if (_state == CPlayerStat.STATE.DIE)
            return true;

        return false;
    }
}
