﻿using UnityEngine;
using System.Collections;

public class CDamageUpItem : CItem {

    public int _damage;
    public float _time;

	// Use this for initialization
	void Start () {
	
	}

    public override void PickUp(CPlayerStat _stat)
    {
        _stat.GetComponent<CPlayerAttack>().DamageUp(_damage, _time);

        base.PickUp(_stat);
    }
}
