﻿using UnityEngine;
using System.Collections;

public class CWorldUIBillboard : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {

        //현재 World Space UI가 메인 카메라와 같은 방향을 보게 함
        transform.rotation = Quaternion.LookRotation(
            Camera.main.transform.forward);

	}
}
