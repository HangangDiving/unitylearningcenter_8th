﻿using UnityEngine;
using System.Collections;

public class CSpiderAnimation : MonoBehaviour {

    Animator _animator;

    CSpiderStat _stat;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _stat = GetComponent<CSpiderStat>();
    }

    public void PlayAnimation(CSpiderStat.STATE state)
    {
        _stat._state = state;

        switch (_stat._state)
        {
            case CSpiderStat.STATE.IDLE: //대기 상태 애니메이션 처리
                _animator.SetBool("Walk", false);
                _animator.SetBool("Attack", false);
                break;
            case CSpiderStat.STATE.TRACE: //추적 상태 애니메이션 처리
                _animator.SetBool("Walk", true);
                _animator.SetBool("Attack", false);
                break;
            case CSpiderStat.STATE.ATTACK: //공격 상태 애니메이션 처리
                _animator.SetBool("Walk", false);
                _animator.SetBool("Attack", true);
                break;
            case CSpiderStat.STATE.DIE: //사망 상태 애니메이션 처리
                _animator.SetTrigger("Die");
                break;
        }
    }
}
