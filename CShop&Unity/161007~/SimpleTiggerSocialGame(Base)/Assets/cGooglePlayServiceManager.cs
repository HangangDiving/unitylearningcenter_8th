﻿using UnityEngine;
using System.Collections;

// 구글 플레이 사용
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

using UnityEngine.UI;

public class cGooglePlayServiceManager : MonoBehaviour
{

    public int _clickCount;

    public Text _messageText;

    public GameObject _loginButton;
    public GameObject _logoutButton;

    // Use this for initialization
    void Start()
    {
        _logoutButton.SetActive(false);
        _loginButton.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnGooglePlayLoginButtonClick()
    {
        _messageText.text = "";
        GooglePlayActivate();
    }

    // 구글 플레이 게임즈 인증 요청
    public void GooglePlayActivate()
    {
        // 구글 플레이 게임즈 활성화
        PlayGamesPlatform.Activate();

        // 구글 플레이 게임즈 인증
        Social.localUser.Authenticate(GooglePlayGamesLoginCallBack);
    }

    // 구글 플레이 게임즈 인증 콜백 함수
    void GooglePlayGamesLoginCallBack(bool result)
    {
        // 인증에 성공 했다면
        if (result)
        {
            _messageText.text = "구글 플레이 로그인 성공함";
            _loginButton.SetActive(false);
            _logoutButton.SetActive(true);
        }
        else
        {
            // 구글 플레이 계정 로그인 실패
            _messageText.text = "구글 플레이 로그인 실패함";
        }
    }

    public void OnClickButtonClick()
    {
        _clickCount++;
        _messageText.text = "클릭 카운트 : " + _clickCount.ToString();

        // 업적을 갱신함
        if (_clickCount >= 50)
        {
            Social.ReportProgress(GooglePlayServiceDefine.achievement_1,
                100f, AchievementSetCallback);
        }
        else if (_clickCount >= 40)
        {
            Social.ReportProgress(GooglePlayServiceDefine.achievement_2,
                100f, AchievementSetCallback);
        }
        else if (_clickCount >= 30)
        {
            Social.ReportProgress(GooglePlayServiceDefine.achievement_3,
                100f, AchievementSetCallback);
        }
        else if (_clickCount >= 20)
        {
            Social.ReportProgress(GooglePlayServiceDefine.achievement_4,
                100f, AchievementSetCallback);
        }
        else if (_clickCount >= 10)
        {
            Social.ReportProgress(GooglePlayServiceDefine.achievement_5,
                100f, AchievementSetCallback);
        }

        // 리더 보드를 갱신함
        Social.ReportScore(_clickCount, GooglePlayServiceDefine.leaderboard_clickcount,
            LeaderBoardScoreSetCallback);
    }

    void AchievementSetCallback(bool result)
    {
        _messageText.text = "클릭 카운트 업적이 달정됨";
    }

    void LeaderBoardScoreSetCallback(bool result)
    {

    }

    public void OnGooglePlayGamesAchievementUI()
    {
        // 구글 업적 UI를 실행함
        Social.ShowAchievementsUI();
    }

    public void OnGooglePlayGamesLeaderBoardUI()
    {
        // 구글 순위 UI를 실행함
        Social.ShowLeaderboardUI();
    }

    public void OnGooglePlayLogoutButtonClick()
    {
        _messageText.text = "";
        GooglePlayDeActive();
    }

    // 구글 플레이 게임즈 인증 해제 요청
    public void GooglePlayDeActive()
    {
        PlayGamesPlatform play = (PlayGamesPlatform)Social.Active;

        if (play != null)
        {
            // 구글 게임즈 플레이로 인증 했을 경우 인증을 해제함
            play.SignOut();
            _messageText.text = "구글 플레이 로그아웃 성공함";
        }
        else
        {
            _messageText.text = "구글 플레이에 로그인 하지 않았음";
        }

        _logoutButton.SetActive(false);
        _loginButton.SetActive(true);
    }
}
