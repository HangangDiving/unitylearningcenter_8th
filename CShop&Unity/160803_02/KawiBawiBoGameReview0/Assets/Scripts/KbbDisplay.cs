﻿using UnityEngine;
using System.Collections;

public class KbbDisplay : MonoBehaviour {

    public SpriteRenderer _spriteRenderer;
    public Sprite[] _sprites;

	// Use this for initialization
	void Start () {
        this.StartShow();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartShow()
    {
        StartCoroutine("KbbDisplayCoroutine");
    }

    public void EndShow(int kbb)
    {
        StopCoroutine("KbbDisplayCoroutine");
        this._spriteRenderer.sprite = this._sprites[kbb];
    }

    private IEnumerator KbbDisplayCoroutine()
    {
        int index = 0;

        while (true)
        {
            this._spriteRenderer.sprite = this._sprites[index++];
            index = index > 2 ? 0 : index;
            yield return new WaitForSeconds(0.1f);
        }
    }
}
