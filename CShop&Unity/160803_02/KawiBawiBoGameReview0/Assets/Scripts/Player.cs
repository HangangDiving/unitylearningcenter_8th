﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private int _kbb;
    public GameManager _gameManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnSelectKbbButtonClick(int kbb)
    {
        this._kbb = kbb;
        this._gameManager.Play(this._kbb);
    }
}
