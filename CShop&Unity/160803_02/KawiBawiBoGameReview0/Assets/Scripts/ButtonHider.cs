﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonHider : MonoBehaviour {

    public Image _image;
    public Text _text;

    public void ApplyHide()
    {
        Color newColor = this._image.color;
        newColor.a = 0f;
        this._image.color = newColor;
        newColor = this._text.color;
        newColor.a = 0f;
        this._text.color = newColor;
    }

    public void ReleaseHide()
    {
        Color newColor = this._image.color;
        newColor.a = 1f;
        this._image.color = newColor;
        newColor = this._text.color;
        newColor.a = 1f;
        this._text.color = newColor;
    }
}
