﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private const int KAWI = 0;
    private const int BAWI = 1;
    private const int BO = 2;

    public Computer _computer;
    public KbbDisplay _kbbDisplay;
    public Text _resultText;
    public Text _playerScoreText;
    public Text _computerScoreText;
    //public Image _restartButtonImage;
    //public Text _restartButtonText;
    private int _playerScoreCount = 0;
    private int _computerScoreCount = 0;
    public bool _isPlay = true;
    public bool _isGameEnd = false;
    public ButtonHider _nextButtonHider;
    public ButtonHider _restartButtonHider;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Play(int playerKbb)
    {
        if (!this._isPlay || this._isGameEnd)
            return;

        this._isPlay = false;

        int comKbb = this._computer.GetKbb();

        this._kbbDisplay.EndShow(comKbb);

        if (playerKbb == comKbb)
        {
            this._resultText.text = "Draw~";
        }
        else if ((playerKbb == KAWI && comKbb == BAWI)
            || (playerKbb == BAWI && comKbb == BO)
            || (playerKbb == BO && comKbb == KAWI))
        {
            this._resultText.text = "Lose..";
            this._computerScoreText.text = (++this._computerScoreCount).ToString();
        }
        else
        {
            this._resultText.text = "Win!!";
            this._playerScoreText.text = (++this._playerScoreCount).ToString();
        }

        if (this._playerScoreCount >= 3)
        {
            this._resultText.text = "Winner!!!";
        }
        else if (this._computerScoreCount >= 3)
        {
            this._resultText.text = "Loser...";
        }
        else
        {
            _nextButtonHider.ReleaseHide();
            return;
        }

        //this._restartButtonImage.color = new Color(1f, 1f, 1f, 1f);
        //this._restartButtonText.color = new Color(0f, 0f, 0f, 1f);
        this._restartButtonHider.ReleaseHide();
        this._isGameEnd = true;
    }

    public void OnNextGameButtonClick()
    {
        if (this._isPlay || this._isGameEnd)
            return;

        this._isPlay = true;
        this._kbbDisplay.StartShow();
        this._nextButtonHider.ApplyHide();
        this._resultText.text = "Play~!";
    }

    public void OnRestartButtonClick()
    {
        if (!this._isGameEnd)
            return;

        //this._restartButtonImage.color = new Color(1f, 1f, 1f, 0f);
        //this._restartButtonText.color = new Color(0f, 0f, 0f, 0f);
        this._restartButtonHider.ApplyHide();
        _nextButtonHider.ReleaseHide();
        this._isGameEnd = false;
        this._isPlay = true;
        this._playerScoreCount = this._computerScoreCount = 0;
        this._playerScoreText.text = this._computerScoreText.text = "0";
        this._resultText.text = "Play~!";
        this._kbbDisplay.StartShow();
    }
}
