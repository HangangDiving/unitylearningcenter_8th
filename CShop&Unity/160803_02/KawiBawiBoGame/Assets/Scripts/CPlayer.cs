﻿using UnityEngine;
using System.Collections;

public class CPlayer : MonoBehaviour {

    public int _kbb;
    public CGameManager _gameManager;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //버튼을 눌렀을 때 실행될 이벤트 메소드
    public void OnKawiBawiBoButtonClick(int kbb)
    {
        this._kbb = kbb;

        //게임매니저에게 가위바위보 게임 진행을 요청함
        this._gameManager.Play(this._kbb);
    }
}
