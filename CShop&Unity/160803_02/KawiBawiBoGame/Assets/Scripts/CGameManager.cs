﻿using UnityEngine;
using System.Collections;

//UGUI 사용
using UnityEngine.UI;

public class CGameManager : MonoBehaviour {

    //컴퓨터(컴퓨터 게임오브젝트의 CComputer 스크립트 컴포넌트)를 참조함
    public CComputer _computer;
    public CKBBDisplay _display; //디스플레이를 참조함
    public Text _resultText; //게임 결과 텍스트 참조
    private const int KAWI = 0;
    private const int BAWI = 1;
    private const int BO = 2;
    private string result = "";

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //가위바위보 게임 플레이!!
    public void Play(int playerKbb)
    {
        //컴퓨터의 가위바위보를 요청함
        int comKbb = this._computer.GetKbb();

        //디스플레이에게 가위바위보 쇼를 멈추라고 함
        this._display.ShowEnd(comKbb);

        //가위바위보 게임 판정
        if (playerKbb == comKbb)
        {
            Debug.Log(playerKbb.ToString() + " vs " + comKbb.ToString() + " => 무승부");
            this._resultText.text = "Draw!!";
        }
        else if ((playerKbb == KAWI && comKbb == BAWI) ||
            (playerKbb == BAWI && comKbb == BO) ||
            (playerKbb == BO && comKbb == KAWI))
        {
            Debug.Log(playerKbb.ToString() + " vs " + comKbb.ToString() + " => 패배");
            this._resultText.text = "Lose!!";
        }
        else
        {
            Debug.Log(playerKbb.ToString() + " vs " + comKbb.ToString() + " => 승리");
            this._resultText.text = "Win!!";
        }
    }

    //게임 재시작
    public void OnRestartGameButtonClick()
    {
        Debug.Log("Game Restart");
    }
}
