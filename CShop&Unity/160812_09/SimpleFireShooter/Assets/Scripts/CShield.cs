﻿using UnityEngine;
using System.Collections;

public class CShield : MonoBehaviour {

    private SpriteRenderer _spriteRenderer;
    public float _translucentValue = 0.25f;
    public float _translucentTime = 0.5f;

	// Use this for initialization
	void Start () {
        this._spriteRenderer = this.GetComponent<SpriteRenderer>();
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("ESLaser") || col.tag.Equals("Enemy"))
        {
            if(col.tag.Equals("ESLaser"))
                Destroy(col.gameObject);

            Color newColor = this._spriteRenderer.color;
            newColor.a = this._translucentValue;
            this._spriteRenderer.color = newColor;
            Invoke("ReleaseAlpha", this._translucentTime);
        }
    }

    private void ReleaseAlpha()
    {
        Color newColor = this._spriteRenderer.color;
        newColor.a = 1f;
        this._spriteRenderer.color = newColor;
    }
}
