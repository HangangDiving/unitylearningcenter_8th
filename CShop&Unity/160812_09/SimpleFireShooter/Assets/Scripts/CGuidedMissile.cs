﻿using UnityEngine;
using System.Collections;

public class CGuidedMissile : MonoBehaviour {

    public float _speed = 5f;
    public Transform _targetTM;
    //public float _limitHeight;
    private Vector2 _dir;

    void Update()
    {
        if (this._targetTM != null)
        {
            this._dir = this._targetTM.position - this.transform.position;
            this._dir.Normalize();
        }

        this.transform.Translate(this._dir * this._speed * Time.deltaTime);
    }

    //public void SetTarget(Transform targetTM, float limitHeight)
    public void SetTarget(Transform targetTM)
    {
        this._targetTM = targetTM;
        //this._limitHeight = limitHeight;
        this._dir =
            (this._targetTM.position - this.transform.position).normalized;
    }
}
