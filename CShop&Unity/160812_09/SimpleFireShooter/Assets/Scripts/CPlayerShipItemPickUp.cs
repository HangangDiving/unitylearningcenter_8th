﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CPlayerShipItemPickUp : MonoBehaviour {

    public Text _starCount; //별점 정보
    public CPlayerShipDamage _shipDamage;
    public CPlayerShipShot _playerShipShot;
    public Transform[] _sidekickPositions;
    private int _sidekickCount = 0;
    public GameObject _sidekickPrefab;
    public GameObject _shield;
    public float _shieldTime = 3f;

    void OnTriggerEnter2D(Collider2D col)
    {
        //별 아이템을 먹었다면
        if (col.tag.Equals("StarItem"))
        {
            //별 카운트를 증가함
            int cnt = int.Parse(_starCount.text);
            _starCount.text = (++cnt).ToString();
            Destroy(col.gameObject);
            return;
        }
        //체력 보충 아이템을 먹었다면
        else if (col.tag.Equals("HpItem"))
        {
            //체력을 20만큼 올려줌
            this._shipDamage.HpUp(20);

            Destroy(col.gameObject);
        }
        else if (col.tag.Equals("LaserItem"))
        {
            _playerShipShot.LaserUpgrade();

            Destroy(col.gameObject);
        }
        else if (col.tag.Equals("SidekickItem"))
        {
            Destroy(col.gameObject);

            if (this._sidekickCount >= this._sidekickPositions.Length)
                return;

            GameObject newGameObj = Instantiate(this._sidekickPrefab,
                this._sidekickPositions[this._sidekickCount].position,
                Quaternion.identity) as GameObject;

            newGameObj.transform.parent =
                this._sidekickPositions[this._sidekickCount].transform;

            CSidekickShipDamage SSDam = newGameObj.GetComponent<CSidekickShipDamage>();
            SSDam._playerShipItemPickUp = this;

            this._sidekickCount++;
        }
        else if (col.tag.Equals("ShieldItem"))
        {
            Destroy(col.gameObject);

            if (IsInvoking("ReleaseShield"))
                CancelInvoke("ReleaseShield");

            this._shield.SetActive(true);
            Invoke("ReleaseShield", this._shieldTime);
        }
    }

    public void PopSidekickShip()
    {
        this._sidekickCount--;

        this._sidekickCount =
            this._sidekickCount < 0 ? 0 : this._sidekickCount;
    }

    private void ReleaseShield()
    {
        this._shield.SetActive(false);
    }
}
