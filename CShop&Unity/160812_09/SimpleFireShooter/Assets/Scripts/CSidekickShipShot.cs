﻿using UnityEngine;
using System.Collections;

public class CSidekickShipShot : MonoBehaviour {

    public Transform _shotPosition; //발포위치
    public float _shotDelayTime = 3f; //발포 지연 시간
    public float _shotInterval = 0.125f;
    public GameObject _laserPrefab; //레이저 프리팹
    private Transform currTarget;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("ShotCoroutine");
    }

    private IEnumerator ShotCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(this._shotDelayTime);

            GameObject[] findEnemys = GameObject.FindGameObjectsWithTag("Enemy");
            if (findEnemys.Length != 0)
            {
                float myHeight = this.transform.position.y;
                Transform targetTM = findEnemys[0].transform;
                int findIdx = 0;
                //일단 현재 내 위치보다 위에 있는 적기를 첫 비교 대상으로 정한다.
                for (int i = 0; i < findEnemys.Length; i++)
                {
                    if (myHeight < findEnemys[i].transform.position.y)
                    {
                        targetTM = findEnemys[i].transform;
                        findIdx = i;
                    }
                }

                if (myHeight < targetTM.position.y)
                {
                    //다른 적기와 비교해가며 가장 가까운 적기를 찾는다
                    for (int i = 0; i < findEnemys.Length; i++)
                    {
                        //내 위치보다 위에 있고 비교하는 적기가 기준 적기보다 아래 위치라면
                        if (myHeight < findEnemys[i].transform.position.y
                            && targetTM.position.y > findEnemys[i].transform.position.y)
                        {
                            targetTM = findEnemys[i].transform;
                        }
                    }

                    GameObject newGameObj =
                        Instantiate(this._laserPrefab, _shotPosition.position, _shotPosition.rotation)
                        as GameObject;

                    CGuidedMissile guidedMissile = newGameObj.GetComponent<CGuidedMissile>();
                    guidedMissile.SetTarget(targetTM);
                }
            }//if(length != 0
        }//while
    }
}
