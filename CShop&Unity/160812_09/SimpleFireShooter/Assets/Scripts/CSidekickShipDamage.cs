﻿using UnityEngine;
using System.Collections;

public class CSidekickShipDamage : MonoBehaviour {

    public GameObject _damageEffectPrefab; //피격이펙트 프리팹
    public CPlayerShipItemPickUp _playerShipItemPickUp;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Enemy") || col.tag.Equals("ESLaser"))
        {
            if(col.tag.Equals("ESLaser"))
                Destroy(col.gameObject);

            GameObject effectGameObj =
                Instantiate(this._damageEffectPrefab, col.transform.position, Quaternion.identity)
                as GameObject;
            Destroy(effectGameObj, 0.3f);

            _playerShipItemPickUp.PopSidekickShip();

            Destroy(this.gameObject);
        }
    }
}
