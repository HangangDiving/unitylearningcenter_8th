﻿using UnityEngine;
using System.Collections;

public class CEnemyShipDamage : MonoBehaviour {

    public int _hp;
    public GameObject _damageEffectPrefab; //피격이펙트 프리팹
    public GameObject[] _dropItemPrefabs; //적기 드랍 아이템 프리팹

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //충돌한 레이저가 플레이어쉽의 레이저라면
        //if (col.tag.Equals("PSLaser") || col.tag.Equals("Sidekick"))
        if (col.tag.Equals("PSLaser"))
        {
            //레이저 피격(스파크) 오브젝트를 생성했다가 0.3초뒤에 파괴함
            GameObject effect = Instantiate(this._damageEffectPrefab, col.transform.position,
                Quaternion.identity) as GameObject;
            Destroy(effect, 0.3f);

            //레이저의 데미지 정보를 추출함
            int damage = col.GetComponent<CAttackInfo>()._damage;
            //체력 감소
            this._hp -= damage;
            //피격된 레이저를 파괴함
            Destroy(col.gameObject);

            //체력이 0보다 작아지면
            if (this._hp <= 0)
            {
                int itemIdx = 0;
                //적기가 아이템을 드랍함
                if (this._dropItemPrefabs.Length > 1)
                    itemIdx = Random.Range(0, this._dropItemPrefabs.Length);

                Instantiate(this._dropItemPrefabs[itemIdx], this.transform.position, Quaternion.identity);
                Destroy(this.gameObject); //적기 파괴됨
            }
        }
        else if (col.tag.Equals("Sidekick") || col.tag.Equals("Shield"))
        {
            int itemIdx = 0;
            //적기가 아이템을 드랍함
            if (this._dropItemPrefabs.Length > 1)
                itemIdx = Random.Range(0, this._dropItemPrefabs.Length);

            Instantiate(this._dropItemPrefabs[itemIdx], this.transform.position, Quaternion.identity);
            Destroy(this.gameObject); //적기 파괴됨
        }
    }
}
