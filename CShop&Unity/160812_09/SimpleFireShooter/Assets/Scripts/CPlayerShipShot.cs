﻿using UnityEngine;
using System.Collections;

public class CPlayerShipShot : MonoBehaviour {

    public GameObject _laserPrefab; //레이저 프리팹 참조
    public Transform _shotPos; //발포 위치 참조

    public int _shotNum = 1;
    public float _angle = 0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        this.Shot();

	}

    //레이저 발포
    private void Shot()
    {
        //발포 키를 눌렀는지를 체크
        //발포 키를 누른게 맞다면
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //레이저 생성(발포)
            //Instantiate(프리팹, 생성위치, 생성회전축)
            //Instantiate(this._laserPrefab, this.transform.position, Quaternion.identity);
            //Instantiate(this._laserPrefab, _shotPos.position, Quaternion.identity);
            float newAngle = this._angle;
            for (int i = 0; i < this._shotNum; i++)
            {
                Quaternion rot = Quaternion.AngleAxis(newAngle, Vector3.forward);
                Instantiate(this._laserPrefab, _shotPos.position, rot);
                newAngle += 5f;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Return))
            this.LaserUpgrade();
    }

    public void LaserUpgrade()
    {
        if (this._angle <= -90f + 0.00001f)
            return;

        this._shotNum += 2;
        this._angle -= 5f;
    }
}
