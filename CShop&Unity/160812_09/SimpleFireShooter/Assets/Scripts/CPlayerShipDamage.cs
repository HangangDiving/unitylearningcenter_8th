﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CPlayerShipDamage : MonoBehaviour {

    public Animator _animator;
    public Image _hpProgress; //체력 게이지

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        //우주와 충돌한다면 패스
        if (col.tag.Equals("Space")) return;
        else if (col.tag.Equals("Enemy") || col.tag.Equals("ESLaser"))
        {
            //적기의 데미지를 추출함
            int damage = col.GetComponent<CAttackInfo>()._damage;
            //플레이어쉽의 체력을 감소함
            int hp = (int)(this._hpProgress.fillAmount * 100);
            hp -= damage;

            //우주가 아닌 오브젝트와 충돌했다면 해당 오브젝트를 파괴함
            Destroy(col.gameObject);

            //플레이어쉽 데미지 효과 애니메이션을 재생함
            this._animator.Play("PlayerShipDamage");

            if (hp <= 0)
            {
                Debug.Log("플레이어쉽 파괴됨");
            }
            else
            {
                //감소된 체력을 게이지에 적용함
                this._hpProgress.fillAmount = hp * 0.01f;
            }
        }
    }

    //체력 업
    public void HpUp(int value)
    {
        this._hpProgress.fillAmount += (value * 0.01f);
    }

    //체력 다운
    public void HpDown(int value)
    {
        this._hpProgress.fillAmount -= (value * 0.01f);
    }
}
