﻿using UnityEngine;
using System.Collections;

//적기 발포 클래스
public class CEnemyShipShot : MonoBehaviour {

    public Transform[] _shotPositions; //발포위치
    public float _shotDelayTime = 3f; //발포 지연 시간
    public float _shotInterval = 0.125f;
    public GameObject _laserPrefabs; //레이저 프리팹

	// Use this for initialization
	void Start () {
        StartCoroutine("ShotCoroutine");
	}
	
    private IEnumerator ShotCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(this._shotDelayTime);

            //for(int i = 0; i < 배열요소.Length; i++) { ...배열[i]... }
            //foreach(순차요소 in 배열요소) { ... }
            foreach (Transform shotPosition in this._shotPositions)
            {
                if(_shotPositions.Length > 1)
                    yield return new WaitForSeconds(this._shotInterval);

                Instantiate(this._laserPrefabs, shotPosition.position, shotPosition.rotation);
            }            
        }
    }
}
