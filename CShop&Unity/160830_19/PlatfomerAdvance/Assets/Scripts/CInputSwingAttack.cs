﻿using UnityEngine;
using System.Collections;

public class CInputSwingAttack : CInputAttack {

    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    //공격 중인지를 체크하다
    public override bool IsAttack()
    {
        //전사의 공격 상태는 Attack1 ~ Attack3 애니메이션 상태를 의미함
        if (_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack1")
            || _animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2")
            || _animator.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
            return true;

        return false;
    }

    //공격키를 입력하다
    public override void InputAttack()
    {
        if (IsAttack()) return;

        if (Input.GetKeyDown(KeyCode.X))
        {
            _animator.SetTrigger("Attack" + _attackIndex++);

            if (_attackIndex > 3) _attackIndex = 1;
        }
    }

    //공격하다
    public override void Attack()
    {
        Collider2D[] colliders =
            Physics2D.OverlapCircleAll(_attackPoint.position, 1f, 1 << LayerMask.NameToLayer("Monster"));

        //Debug.Log("Hit Count : " + colliders.Length);

        foreach (Collider2D collider in colliders)
        {
            if (collider.tag.Equals("Monster"))
            {
                //collider.SendMessage("Damage", 1f);
                IDamage d = collider.GetComponent<IDamage>();
                d.Damage(1f);
                return;
            }
        }
    }

    public void DoAttack()
    {
        Attack();
    }
}
