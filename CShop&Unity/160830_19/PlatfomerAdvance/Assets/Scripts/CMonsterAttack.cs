﻿using UnityEngine;
using System.Collections;
using System;

//몬스터 공격(최상위)
public abstract class CMonsterAttack : MonoBehaviour, IMonsterAttack {

    protected Transform _attackPoint;           //공격 위치
    protected Animator _animator;               //애니메이터
    protected CCharacterState _monsterState;    //몬스터 상태 컴포넌트 참조

    protected virtual void Awake()
    {
        //애니메이터 컴포넌트 참조
        _animator = GetComponent<Animator>();
        //몬스터 상태 컴포넌트 참조
        _monsterState = GetComponent<CCharacterState>();
        //공격 위치 참조
        _attackPoint = transform.FindChild("AttackPoint");
    }

    // Use this for initialization
    protected virtual void Start () {
	
	}

    //공격 준비
    public virtual void AttacklReady()
    {
        //공격 상태 변경
        _monsterState.state = CCharacterState.State.Attack;
        //공격 애니메이션
        _animator.SetBool("Attack", true);
    }

    //공격
    public abstract void Attack();

    //공격 끝
    public virtual void AttackFinish()
    {
        //대기 상태 변경
        _monsterState.state = CCharacterState.State.Idle;
        //대기 애니메이션
        _animator.SetBool("Attack", false);
    }
}
