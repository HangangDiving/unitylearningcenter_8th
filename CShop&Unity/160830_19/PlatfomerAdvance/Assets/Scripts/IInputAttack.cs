﻿using UnityEngine;
using System.Collections;

//입력 공격 인터페이스
public interface IInputAttack {

    bool IsAttack();    //공격 중 체크
    void InputAttack(); //입력 공격
    void Attack();      //공격

}
