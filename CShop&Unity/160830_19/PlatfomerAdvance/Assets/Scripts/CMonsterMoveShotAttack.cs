﻿using UnityEngine;
using System.Collections;

public class CMonsterMoveShotAttack : CMonsterMoveAttack {

    //총알 프리팹
    public Object _bulletPrefab;

    public override void Attack()
    {
        //총알 발사
        GameObject bullet = Instantiate(_bulletPrefab,
            _attackPoint.position, Quaternion.identity) as GameObject;

        bullet.SendMessage("Init", _monsterState._isRightDir);
    }
}
