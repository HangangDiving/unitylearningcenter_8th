﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class CNetManager : MonoBehaviour {

    public string _serverBaseUrl;
    public string _requestData;

    public Text _responseDataText;

    public void OnGetEchoRequestButtonClick()
    {
        StopCoroutine("GetRequestCoroutine");
        StartCoroutine("GetRequestCoroutine");
    }

    private IEnumerator GetRequestCoroutine()
    {
        string url = _serverBaseUrl + "gettest.php?value=" + _requestData;

        //Get 요청 클라이언트 객체 생성
        WWW www = new WWW(url);

        //Get 요청에 대한 응답을 대기함
        yield return www;

        if (www.error == null)
        {
            _responseDataText.text = www.text;
            Debug.Log("Get 에코 데이터 => " + www.text);
        }
        else
        {
            Debug.Log("서버 통신 에러 발생");
        }
    }

    public void OnPostEchoRequestButtonClick()
    {
        StopCoroutine("PostRequestCoroutine");
        StartCoroutine("PostRequestCoroutine");
    }

    private IEnumerator PostRequestCoroutine()
    {
        string url = _serverBaseUrl + "posttest.php";

        //Post 방식의 요청을 위해 form-data 객체를 생성함
        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("value", _requestData);

        //Post 요청 클라이언트 객체 생성
        WWW www = new WWW(url, wwwForm);

        //Post 요청에 대한 응답을 대기함
        yield return www;

        if (www.error == null)
        {
            _responseDataText.text = www.text;
            Debug.Log("Post 에코 데이터 => " + www.text);
        }
        else
        {
            Debug.Log("서버 통신 에러 발생");
        }
    }
}
