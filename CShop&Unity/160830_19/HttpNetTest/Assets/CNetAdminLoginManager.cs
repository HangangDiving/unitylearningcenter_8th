﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class CNetAdminLoginManager : MonoBehaviour {

    public string _serverBaseUrl;
    public string _serverPhpFileName;
    public string _adminId;
    public string _adminPassword;

    public Text _responseText;

    public void OnPostEchoRequestButtonClick()
    {
        StopCoroutine("PostRequestCoroutine");
        StartCoroutine("PostRequestCoroutine");
    }

    private IEnumerator PostRequestCoroutine()
    {
        string url =
            string.Format("{0}/{1}/", this._serverBaseUrl, this._serverPhpFileName);

        WWWForm wwwForm = new WWWForm();
        wwwForm.AddField("id", this._adminId);
        wwwForm.AddField("pw", this._adminPassword);

        WWW www = new WWW(url, wwwForm);

        yield return www;

        if (www.error == null)
        {
            _responseText.text = www.text;
            print("서버 통신 성공" + www.text);
        }
        else
        {
            print("서버 통신 에러");
        }
    }
}
