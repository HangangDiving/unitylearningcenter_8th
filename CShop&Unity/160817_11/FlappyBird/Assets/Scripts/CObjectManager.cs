﻿using UnityEngine;
using System.Collections;

public class CObjectManager : MonoBehaviour {

    public GameObject _objectPrefab; //오브젝트 프리팹
    public Transform _createPos; //생성 위치
    //오브젝트 생성 y위치
    public float _topCreatePosY;
    public float _bottomCreatePosY;
    public float _createStartDelayTime; //최초 생성 지연 시간
    public float _createDelayTime; //생성 지연 시간

    // Use this for initialization
    void Start () {
        //InvokeRepeating("타이머메소드명", 시작지연시간, 타이머지연간격);
        InvokeRepeating("ObjectCreateInvoke", _createStartDelayTime, _createDelayTime);
	}

    void Update()
    {
        //만약에 게임이 중지된 상태라면
        if (CGameManager.isGameStop)
        {
            //오브젝트 생성을 중지함
            CancelInvoke("ObjectCreateInvoke");
        }
    }
	
    public void ObjectCreateInvoke()
    {
        //생성할 오브젝트의 y 위치를 설정함
        float randYPos = Random.Range(_topCreatePosY, _bottomCreatePosY);

        //생성할 오브젝트의 위치를 설정함
        Vector2 pos =
            new Vector2(_createPos.position.x, _createPos.position.y + randYPos);

        //오브젝트을 생성함
        Instantiate(_objectPrefab, pos, Quaternion.identity);
    }
}
