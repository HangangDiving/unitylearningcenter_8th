﻿using UnityEngine;
using System.Collections;

public class CObjectMove : MonoBehaviour {

    public Rigidbody2D _rigidbody2d;
    public float _speed = 3f;
    public Vector2 _direction;

	// Use this for initialization
	void Start () {

        //속도 부여 (방향 * 속도)
        _rigidbody2d.velocity = _direction * _speed;

	}
	
	// Update is called once per frame
	void Update () {

        //만약에 게임이 중지된 상태라면
        if (CGameManager.isGameStop)
        {
            //컬럼의 이동을 중지함
            _rigidbody2d.velocity = Vector2.zero;
        }

        if (transform.position.x < -12f)
            Destroy(this.gameObject);

	}
}
