﻿using UnityEngine;
using System.Collections;

public class CSky : MonoBehaviour {

    public Animator _animator;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        //만약 게임이 중지된 상태라면
        if (CGameManager.isGameStop)
        {
            //스크롤 애니메이션을 중지함
            //_animator.Stop();
            _animator.speed = 0f;
        }

	}
}
