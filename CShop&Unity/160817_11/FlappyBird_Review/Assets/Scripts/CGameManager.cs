﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CGameManager : MonoBehaviour {

    public static bool isGameStop = false;

    public Text _starCountText;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GameEnd()
    {
        PlayerPrefs.SetString("STAR_COUNT", this._starCountText.text);
        PlayerPrefs.Save();

        SceneManager.LoadScene("End");
    }

    public void StarCountUp()
    {
        int cnt = int.Parse(this._starCountText.text);
        this._starCountText.text = (++cnt).ToString();
    }
}
