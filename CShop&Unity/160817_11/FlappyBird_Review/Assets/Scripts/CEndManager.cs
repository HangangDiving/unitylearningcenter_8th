﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CEndManager : MonoBehaviour {

    public Text _starCountText;

	// Use this for initialization
	void Start () {
        CGameManager.isGameStop = false;
        this._starCountText.text = PlayerPrefs.GetString("STAR_COUNT");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnReStartButtonClick()
    {
        SceneManager.LoadScene("Demo");
    }
}
