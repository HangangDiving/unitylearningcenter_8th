﻿using UnityEngine;
using System.Collections;

public class CObjectGenerator : MonoBehaviour {

    public Transform _createPos;
    public GameObject _objectPrefab;
    public float _topCreateYPos;
    public float _bottomCreateYPos;
    public float _createStartDelayTime = 3f;
    public float _createDelayTime = 3f;

    // Use this for initialization
    void Start () {
        InvokeRepeating("CreateObjectInvoke", this._createStartDelayTime, this._createDelayTime);
	}
	
	// Update is called once per frame
	void Update () {

        if (CGameManager.isGameStop)
            CancelInvoke("CreateObjectInvoke");

	}

    private void CreateObjectInvoke()
    {
        Vector2 newPos = _createPos.position;
        newPos.y += Random.Range(this._bottomCreateYPos, this._topCreateYPos);
        Instantiate(this._objectPrefab, newPos, Quaternion.identity);
    }
}
