﻿using UnityEngine;
using System.Collections;

public class CPlane : MonoBehaviour {

    private Rigidbody2D _rigidbody2d;
    public float _riseForce = 400f;
    public CGameManager _gameManager;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (this.transform.position.y > 5.5f)
            CGameManager.isGameStop = true;
        else if (this.transform.position.y < -5.5f)
            this._gameManager.GameEnd();

        if (Input.anyKeyDown && !CGameManager.isGameStop)
        {
            this._rigidbody2d.velocity = Vector2.zero;

            this._rigidbody2d.AddForce(Vector2.up * this._riseForce);
        }

	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Column"))
        {
            CGameManager.isGameStop = true;
            Invoke("GameEndDelayInvoke", 2f);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("StarItem"))
        {
            this._gameManager.StarCountUp();

            Destroy(col.gameObject);
        }
    }

    private void GameEndDelayInvoke()
    {
        this._gameManager.GameEnd();
    }
}
