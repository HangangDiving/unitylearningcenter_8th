﻿using UnityEngine;
using System.Collections;

public class CObjectMove : MonoBehaviour {

    private Rigidbody2D _rigidbody2d;
    public Vector2 _direction = new Vector2(-1f, 0f);
    public float _speed = 1.8f;

    void Awake()
    {
        this._rigidbody2d = this.GetComponent<Rigidbody2D>();
    }

	// Use this for initialization
	void Start () {
        this._rigidbody2d.velocity = this._direction * this._speed;
    }
	
	// Update is called once per frame
	void Update () {
        if (CGameManager.isGameStop)
            this._rigidbody2d.velocity = Vector2.zero;
	}
}
