﻿using UnityEngine;
using System.Collections;

//몬스터 파괴자
public class CMonsterDestoyer : CDestroyer {

    public Object _destroyEffectPrefabs;    //파괴 이펙트 프리팹

    protected override void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    protected override void Start () {

        base.Start();

	}

    //몬스터 파괴
    public override void Destroy()
    {
        //파괴시 사라지는 효과 이펙트 프리팹을 생성함
        Instantiate(_destroyEffectPrefabs, transform.position, Quaternion.identity);

        base.Destroy();
    }
}
