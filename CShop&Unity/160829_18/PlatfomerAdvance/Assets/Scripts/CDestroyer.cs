﻿using UnityEngine;
using System.Collections;

//오브젝트 파괴자
public class CDestroyer : MonoBehaviour {

    public float _destroyTime;  //파괴 시간(기본 : 0)
    public bool _isAutoDestroy; //자동 파괴 여부

    protected virtual void Awake()
    { }

	// Use this for initialization
	protected virtual void Start () {

        //자동 파괴 여부에 따라 자동 파괴를 수행함
        if (_isAutoDestroy)
            Destroy();
	
	}

    //파괴 실행
    public virtual void Destroy()
    {
        //Destroy(게임오브젝트, 파괴지연시간)
        Destroy(gameObject, _destroyTime);
    }
}
