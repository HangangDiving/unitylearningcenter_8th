﻿using UnityEngine;
using System.Collections;

//총알 인터페이스
public interface IBullet {

    void Init(bool isRightDir); //초기화
    void Move();                //총알 이동

}
