﻿using UnityEngine;
using System.Collections;

//입력 이동 인터페이스
public interface IInputMove {

    void InputMove();               //입력 이동
    void Move(float move);          //이동
    void InputJump();               //입력 점프
    void Jump(float jumpPower);     //점프
    void Grounding(bool isGround);  //지명 여부 체크

}
