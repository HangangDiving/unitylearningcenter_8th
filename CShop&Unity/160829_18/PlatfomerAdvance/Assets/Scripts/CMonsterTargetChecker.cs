﻿using UnityEngine;
using System.Collections;

//몬스터 공격 타겟 체커
public class CMonsterTargetChecker : MonoBehaviour {

    protected CCharacterState _monsterState;    //몬스터 상태 컴포넌트 참조
    public CCharacterState.State _checkState;   //체크할 상태 설정
    public float _checkRange;                   //체크 범위
    protected Transform _attackPoint;           //체크 시작 위치(공격 위치)

    protected Transform _playerPos;             //플레이어 위치

    void Awake()
    {
        _monsterState = GetComponent<CCharacterState>();
        _attackPoint = transform.FindChild("AttackPoint");
    }

	// Use this for initialization
	void Start () {
	
	}
    
    public void CircleAreaTargetCheck(IMonsterAttack monsterAttack)
    {
        _playerPos = GameObject.FindGameObjectWithTag("Player").transform;

        Debug.Log("player pos => " + _playerPos.position.ToString());

        StartCoroutine("CircleAreaTargetCheckCoroutine", monsterAttack);
    }

    public IEnumerator CircleAreaTargetCheckCoroutine(IMonsterAttack monsterAttack)
    {
        //몬스터가 대기 상태라면 감시를 진행함
        while (_monsterState.state == _checkState)
        {
            float distance = Vector2.Distance(_attackPoint.position, _playerPos.position);

            if (distance < _checkRange)
            {
                //공격 시작
                monsterAttack.AttacklReady();
            }

            yield return null;
        }
    }

    //공격 정면 타겟 체크
    public void FrontTargetCheck(IMonsterAttack monsterAttack)
    {
        StartCoroutine("FrontTargetCheckCoroutine", monsterAttack);
    }

    //앞 방향 타겟 체크 코루틴
    public IEnumerator FrontTargetCheckCoroutine(IMonsterAttack monsterAttack)
    {
        //몬스터가 대기 상태라면 감시를 진행함
        while (_monsterState.state == _checkState)
        {
            //수평 방향으로 발포 감지 레이저를 발사함
            Vector2 endPos = new Vector2(
                _attackPoint.position.x - ((_monsterState._isRightDir) ? -_checkRange : _checkRange),
                _attackPoint.position.y);

            //충돌 체크 디버그 레이저 발사
            Debug.DrawLine(_attackPoint.position, endPos, Color.green);

            //라인 체크
            RaycastHit2D hitInfo =
                Physics2D.Linecast(_attackPoint.position, endPos, _monsterState._targetMask);

            //충돌체가 존재한다면
            if (hitInfo.collider != null)
            {
                //공격 시작
                monsterAttack.AttacklReady();
            }

            yield return null;
        }
    }
}
