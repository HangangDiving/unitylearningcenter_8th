﻿using UnityEngine;
using System.Collections;
using System;

//정면 몬스터 발포형 공격
public class CMonsterStandShot : CMonsterAttack {

    public GameObject _bulletPrefab;    //총알 프리팹

    //타겟 체크 컴포넌트 참조
    protected CMonsterTargetChecker _targetChecker;

    public enum CheckType { FRONT, CIRCLE };
    public CheckType _checkType = CheckType.FRONT;

    protected override void Awake()
    {
        base.Awake();

        //타겟 체크 컴포넌트 참조
        _targetChecker = GetComponent<CMonsterTargetChecker>();
    }

    // Use this for initialization
    protected override void Start () {

        base.Start();


        StartTargetChecker();
	}

    void StartTargetChecker()
    {
        switch (_checkType)
        {
            case CheckType.FRONT:
                _targetChecker.FrontTargetCheck(this);
                break;
            case CheckType.CIRCLE:
                _targetChecker.CircleAreaTargetCheck(this);
                break;
        }
    }

    //공격을 준비함
    public override void AttacklReady()
    {
        base.AttacklReady();
    }

    //공격함
    public override void Attack()
    {
        //화살 발사
        GameObject bullet =
            Instantiate(_bulletPrefab, _attackPoint.position, Quaternion.identity) as GameObject;
        bullet.SendMessage("Init", _monsterState._isRightDir);
    }

    //공격을 마침
    public override void AttackFinish()
    {
        base.AttackFinish();

        //감시용 코루틴 실행
        StartTargetChecker();
    }

    //공격 애니메이션 이벤트
    public virtual void AttackAnimationEvent()
    {
        Attack();
    }

    //공격 종료 애니메이션 이벤트
    public virtual void AttackFinishAnimationEvent()
    {
        AttackFinish();
    }
}
