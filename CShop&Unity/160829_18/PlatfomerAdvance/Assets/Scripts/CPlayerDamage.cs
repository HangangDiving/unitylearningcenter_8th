﻿using UnityEngine;
using System.Collections;

//플레이어 데미지
public class CPlayerDamage : MonoBehaviour {

    protected CCharacterState _playerState;     //플레이어 상태
    protected SpriteRenderer _spriteRenderer;   //스프라이트 렌더러

    public Object _damageEffectPrefab;          //데미지 효과 프리팹
    protected Transform _damageEffectPoint;     //데미지 이펙트 포인트

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _playerState = GetComponent<CCharacterState>();
        _damageEffectPoint = transform.FindChild("DamagePoint");
    }

	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag.Equals("Monster"))
        {
            Damage();
        }
        else if (collider.tag.Equals("Bullet"))
        {
            Damage();
            Destroy(collider.gameObject);
        }
    }

    //몬스터와 충돌 중
    void OnTriggerStay2D(Collider2D collider)
    {
        //지면위에 있음
        if (collider.tag.Equals("Monster"))
        {
            Damage();
        }
        else if (collider.tag.Equals("Bullet"))
        {
            Damage();
            Destroy(collider.gameObject);
        }
    }

    //데미지 처리
    public void Damage()
    {
        if (_playerState.state == CCharacterState.State.Damage) return;

        //데미지 상태 변경
        _playerState.state = CCharacterState.State.Damage;

        StopCoroutine("DamageEffectCoroutine");
        StartCoroutine("DamageEffectCoroutine");
    }

    //데미지 이펙트 처리 코루틴
    private IEnumerator DamageEffectCoroutine()
    {
        int blinkCount = 0;

        //이펙트 생성
        GameObject effect = Instantiate(_damageEffectPrefab,
            _damageEffectPoint.position, Quaternion.identity) as GameObject;
        Destroy(effect, 0.24f);

        while (true)
        {
            //Color(R, G, B, A)
            _spriteRenderer.material.color = Color.red; //붉은 색

            yield return new WaitForSeconds(0.1f);

            _spriteRenderer.material.color = Color.clear; //투명

            yield return new WaitForSeconds(0.1f);

            blinkCount++;

            if (blinkCount > 5)
            {
                _spriteRenderer.material.color = Color.white; //복원
                break;
            }
        }

        _playerState.state = CCharacterState.State.Idle;
    }
}
