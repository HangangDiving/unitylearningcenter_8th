﻿using UnityEngine;
using System.Collections;

//몬스터 이동 감지 발포 공격
public class CMonsterMoveSwingAttack : CMonsterMoveAttack {

    //공격
    public override void Attack()
    {
        //공격!!
        Collider2D[] colliders =
            Physics2D.OverlapCircleAll(_attackPoint.position, 1f, _monsterState._targetMask);

        foreach (Collider2D collider in colliders)
        {
            if (collider.tag.Equals("Player"))
            {
                //collider.SendMessage("Damage");
                return;
            }
        }
    }
}
