﻿using UnityEngine;
using System.Collections;

//몬스터 데미지 인터페이스
public interface IDamage {

    void Damage(float damage); //데미지 처리


}
